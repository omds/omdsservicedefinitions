# Build mit Maven
Build mit Maven 3.9.6 auf Java 17

# Maven Goals
Mit Maven Goal "clean package" kann ein Release als ZIP-File mit XSDs und WSDLs sowie der Dokumentation gebaut werden.
Mit Maven Goal "clean deploy" wird der Release in das Kap Dion- Maven - OMDS Repository hochgeladen.

# Codegenerierung
Mit Maven Goal "clean package" und Profil "genJavaFromWsdl_VU" bzw. "genJavaFromWsdl_Broker" wird
der zugehörige Java-Code generiert.


# MTOM in Java nach Codegenerierung
Nach der Code-Generierung müssen einige Schritte von Hand durchgeführt werden, damit MTOM
enthalten ist:

1) Server (Class: OmdsServicePortImpl)

   Add a class annotation (above the class name):
   @MTOM(enabled = true, threshold = 1024)
   
   Add import:
      import jakarta.xml.ws.soap.MTOM;

2) Client (Class: OmdsServicePortType_OmdsServicePort_Client)

   Rewrite the OMDSService creation like following:
   from
      OmdsService ss = new OmdsService(wsdlURL, SERVICE_NAME);
      OmdsServicePortType port = ss.getOmdsServicePort();
   to  
      OmdsService ss = new OmdsService(wsdlURL, SERVICE_NAME);
      OmdsServicePortType port = ss.getOmdsServicePort(new MTOMFeature(1024));

   Add import:
      import jakarta.xml.ws.soap.MTOMFeature;

# Fehlersuche
* Wenn Bindings-Files fehlen, z.B. nicht im Pom deklariert sind, werden die Klassen zwar generiert,
  aber der Pfad erscheint falsch, z.B. als _0._1._1
* Wenn die X-Path-Ausdrücke in den Binding-Files versagen wird im Log nur eine Warnung ausgegeben,
  die Generierung scheitert aber.

