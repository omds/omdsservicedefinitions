# Schritte beim Release einer neuen Version

## Bearbeitung der Norm

### OMDS 2
Prüfen ob die richtige OMDS 2 XSD enthalten ist.

### OMDS 3
Abgrenzung des Veröffentlichungsumfangs - nicht alles aus dem Development-Branch wird veröffentlicht.

Insbesondere ist hier um etwaige Fehler auszuschließen nochmals gegen die tatsächlich veröffentlichten XSDs der 
letzten Veröffentlichung zu prüfen.

Anpassung der Versions-Tags im in den XSDs und den Kommentar im WSDL.


## Überarbeitung der Dokumentation
Dokumente haben einen Kopf bestehend aus:
* Titel
* Status: Empfehlung, vorgeschlagener oder freigegebener Standard
* Release: Versionsnummer des Releases
* Ansprechpartner
* Dokumentenhistorie
* Voraussetzungen
* Rechtliche Hinweise
* Inhaltsverzeichnis

Dokumente sollen im Kopf die Versionsnummer tragen, damit der Leser
gleich erkennen kann, zu welcher Version das Dokument gehört.

Der Dokumentenstatus soll im Kopf des Dokuments enthalten sein.

In der Fußzeile findet sich ein Copyright Hinweis, der ggf. aktualisiert werden muss.

Wenn neue Methoden veröffentlicht werden oder der Status von Dokumenten
hochgestuft wird, dann ist die Excel-Liste mit den Methoden "VerzeichnisOperationen.xlsx" anzupassen.

## Versionsnummer im Pom
Die Versionsnummer im Pom ist zu setzen, ein etwaiges "SNAPSHOT" zu streichen.
Das Verzeichnis ist auf die richtige Versionsnummer zu setzen.


## Code-Generierung
Der Code für die neue Version ist einmalig mit Java zu generieren. 
Es gibt dazu ein Maven-Target (bei package inkludiert).

## Generierung der XSD Dokumentation mit XmlSpy
Mit Hilfe von XML-Spy die Dokumentation für jedes XSD in das Verzeichnis
OMDSServiceDefinition/docGen generieren (wird nicht ins Git geladen).


## JavaDoc Generierung
Es sind die Javadocs per Maven Target zu generieren (bei package inkludiert).



# Build
In der Datei assembly.xml ist vorgegeben, welche Verzeichnisse für die Veröffentlichung 
zusammengepackt werden.
Hier ist die Versionsnummmer des Release anzupassen!
Mit Maven "install" kann der Release gebaut werden.
