
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktGenerischType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VerkaufsproduktGenerischType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VersichertePersonType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Verkaufsprodukt in der Sparte Krankenversicherung
 * 
 * <p>Java-Klasse für VerkaufsproduktKranken_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktKranken_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VerkaufsproduktGenerisch_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Krankenprodukte" type="{urn:at.vvo.omds.types.omds3types.v1-6-0.on2antrag.kranken}ProduktKranken_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Zusatzprodukte" type="{urn:omds3CommonServiceTypes-1-1-0}ProduktGenerisch_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VersichertePersonen" type="{urn:omds3CommonServiceTypes-1-1-0}VersichertePerson_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Gruppe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktKranken_Type", propOrder = {
    "krankenprodukte",
    "zusatzprodukte",
    "versichertePersonen",
    "gruppe"
})
public class VerkaufsproduktKrankenType
    extends VerkaufsproduktGenerischType
{

    @XmlElement(name = "Krankenprodukte", required = true)
    protected List<ProduktKrankenType> krankenprodukte;
    @XmlElement(name = "Zusatzprodukte")
    protected List<ProduktGenerischType> zusatzprodukte;
    @XmlElement(name = "VersichertePersonen", required = true)
    protected List<VersichertePersonType> versichertePersonen;
    @XmlElement(name = "Gruppe")
    protected String gruppe;

    /**
     * Gets the value of the krankenprodukte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the krankenprodukte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKrankenprodukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktKrankenType }
     * 
     * 
     */
    public List<ProduktKrankenType> getKrankenprodukte() {
        if (krankenprodukte == null) {
            krankenprodukte = new ArrayList<ProduktKrankenType>();
        }
        return this.krankenprodukte;
    }

    /**
     * Gets the value of the zusatzprodukte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusatzprodukte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusatzprodukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktGenerischType }
     * 
     * 
     */
    public List<ProduktGenerischType> getZusatzprodukte() {
        if (zusatzprodukte == null) {
            zusatzprodukte = new ArrayList<ProduktGenerischType>();
        }
        return this.zusatzprodukte;
    }

    /**
     * Gets the value of the versichertePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the versichertePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersichertePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VersichertePersonType }
     * 
     * 
     */
    public List<VersichertePersonType> getVersichertePersonen() {
        if (versichertePersonen == null) {
            versichertePersonen = new ArrayList<VersichertePersonType>();
        }
        return this.versichertePersonen;
    }

    /**
     * Ruft den Wert der gruppe-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGruppe() {
        return gruppe;
    }

    /**
     * Legt den Wert der gruppe-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGruppe(String value) {
        this.gruppe = value;
    }

}
