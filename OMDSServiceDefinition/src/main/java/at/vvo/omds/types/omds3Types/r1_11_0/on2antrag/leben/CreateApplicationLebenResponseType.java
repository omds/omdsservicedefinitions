
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateApplicationResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type des Responseobjekts für die Erstellung eines Leben-Antrags
 * 
 * <p>Java-Klasse für CreateApplicationLebenResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationLebenResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateApplicationResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}SpezAntragLeben_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationLebenResponse_Type", propOrder = {
    "antragsantwort"
})
public class CreateApplicationLebenResponseType
    extends CreateApplicationResponseGenType
{

    @XmlElement(name = "Antragsantwort", required = true)
    protected SpezAntragLebenType antragsantwort;

    /**
     * Ruft den Wert der antragsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragLebenType }
     *     
     */
    public SpezAntragLebenType getAntragsantwort() {
        return antragsantwort;
    }

    /**
     * Legt den Wert der antragsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragLebenType }
     *     
     */
    public void setAntragsantwort(SpezAntragLebenType value) {
        this.antragsantwort = value;
    }

}
