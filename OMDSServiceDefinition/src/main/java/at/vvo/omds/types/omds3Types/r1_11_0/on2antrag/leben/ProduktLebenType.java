
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktMitVpType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Produkt in der Sparte Leben.
 * 
 * <p>Java-Klasse für ProduktLeben_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktLeben_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProduktMitVp_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Tarife" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}TarifLeben_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktLeben_Type", propOrder = {
    "tarife"
})
public class ProduktLebenType
    extends ProduktMitVpType
{

    @XmlElement(name = "Tarife")
    protected List<TarifLebenType> tarife;

    /**
     * Gets the value of the tarife property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the tarife property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTarife().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TarifLebenType }
     * 
     * 
     */
    public List<TarifLebenType> getTarife() {
        if (tarife == null) {
            tarife = new ArrayList<TarifLebenType>();
        }
        return this.tarife;
    }

}
