
package at.vvo.omds.types.omds3Types.r1_11_0.servicetypes;

import at.vvo.omds.types.omds3Types.r1_11_0.common.AuthorizationFilter;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeepLinkPartnerRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeepLinkPartnerRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *         &lt;element name="partnerNumber" type="{urn:omds20}Personennr"/&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}withoutFrame" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeepLinkPartnerRequest", propOrder = {
    "vuNr",
    "authFilter",
    "partnerNumber",
    "withoutFrame"
})
public class DeepLinkPartnerRequest {

    @XmlElement(name = "VUNr")
    protected String vuNr;
    @XmlElement(name = "AuthFilter")
    protected AuthorizationFilter authFilter;
    @XmlElement(required = true)
    protected String partnerNumber;
    protected Boolean withoutFrame;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der partnerNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerNumber() {
        return partnerNumber;
    }

    /**
     * Legt den Wert der partnerNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerNumber(String value) {
        this.partnerNumber = value;
    }

    /**
     * Ruft den Wert der withoutFrame-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithoutFrame() {
        return withoutFrame;
    }

    /**
     * Legt den Wert der withoutFrame-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithoutFrame(Boolean value) {
        this.withoutFrame = value;
    }

}
