
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import at.vvo.omds.types.omds3Types.r1_11_0.common.ZusatzproduktGenerischType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für eine Zusatzversicherung in der Sparte Leben (unterhalb von Elementarprodukt)
 * 
 * <p>Java-Klasse für ZusatzversicherungLeben_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusatzversicherungLeben_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ZusatzproduktGenerisch_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusatzversicherungLeben_Type")
@XmlSeeAlso({
    ZusatzversicherungBerufsunfaehigkeitType.class,
    ZusatzversicherungErwerbsunfaehigkeitType.class,
    ZusatzversicherungPraemienuebernahmeAblebenType.class,
    ZusatzversicherungUnfalltodType.class,
    ZusatzversicherungUnfallinvaliditaetType.class
})
public class ZusatzversicherungLebenType
    extends ZusatzproduktGenerischType
{


}
