
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PersonNamentlichesBezugsrecht_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PersonNamentlichesBezugsrecht_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Prozentsatz" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="Zugunsten" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonNamentlichesBezugsrecht_Type", propOrder = {
    "prozentsatz",
    "zugunsten"
})
public class PersonNamentlichesBezugsrechtType {

    @XmlElement(name = "Prozentsatz")
    protected Double prozentsatz;
    @XmlElement(name = "Zugunsten")
    @XmlSchemaType(name = "unsignedShort")
    protected int zugunsten;

    /**
     * Ruft den Wert der prozentsatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getProzentsatz() {
        return prozentsatz;
    }

    /**
     * Legt den Wert der prozentsatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setProzentsatz(Double value) {
        this.prozentsatz = value;
    }

    /**
     * Ruft den Wert der zugunsten-Eigenschaft ab.
     * 
     */
    public int getZugunsten() {
        return zugunsten;
    }

    /**
     * Legt den Wert der zugunsten-Eigenschaft fest.
     * 
     */
    public void setZugunsten(int value) {
        this.zugunsten = value;
    }

}
