
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Fragen gemäß FATCA
 * 
 * <p>Java-Klasse für FATCA_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FATCA_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="US_TIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FATCA_NatPerson" type="{urn:omds3CommonServiceTypes-1-1-0}FATCA_NatPersonType"/&gt;
 *         &lt;element name="FATCA_SonstPerson" type="{urn:omds3CommonServiceTypes-1-1-0}FATCA_SonstPersonType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FATCA_Type", propOrder = {
    "ustin",
    "fatcaNatPerson",
    "fatcaSonstPerson"
})
public class FATCAType {

    @XmlElement(name = "US_TIN")
    protected String ustin;
    @XmlElement(name = "FATCA_NatPerson")
    protected FATCANatPersonType fatcaNatPerson;
    @XmlElement(name = "FATCA_SonstPerson")
    protected FATCASonstPersonType fatcaSonstPerson;

    /**
     * Ruft den Wert der ustin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSTIN() {
        return ustin;
    }

    /**
     * Legt den Wert der ustin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSTIN(String value) {
        this.ustin = value;
    }

    /**
     * Ruft den Wert der fatcaNatPerson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FATCANatPersonType }
     *     
     */
    public FATCANatPersonType getFATCANatPerson() {
        return fatcaNatPerson;
    }

    /**
     * Legt den Wert der fatcaNatPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FATCANatPersonType }
     *     
     */
    public void setFATCANatPerson(FATCANatPersonType value) {
        this.fatcaNatPerson = value;
    }

    /**
     * Ruft den Wert der fatcaSonstPerson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FATCASonstPersonType }
     *     
     */
    public FATCASonstPersonType getFATCASonstPerson() {
        return fatcaSonstPerson;
    }

    /**
     * Legt den Wert der fatcaSonstPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FATCASonstPersonType }
     *     
     */
    public void setFATCASonstPerson(FATCASonstPersonType value) {
        this.fatcaSonstPerson = value;
    }

}
