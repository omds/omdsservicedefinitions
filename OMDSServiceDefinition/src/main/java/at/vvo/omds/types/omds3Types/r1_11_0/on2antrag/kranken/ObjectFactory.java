
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken;

import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CalculateKrankenRequest }
     * 
     */
    public CalculateKrankenRequest createCalculateKrankenRequest() {
        return new CalculateKrankenRequest();
    }

    /**
     * Create an instance of {@link SpezBerechnungKrankenType }
     * 
     */
    public SpezBerechnungKrankenType createSpezBerechnungKrankenType() {
        return new SpezBerechnungKrankenType();
    }

    /**
     * Create an instance of {@link CalculateKrankenResponse }
     * 
     */
    public CalculateKrankenResponse createCalculateKrankenResponse() {
        return new CalculateKrankenResponse();
    }

    /**
     * Create an instance of {@link CreateOfferKrankenRequest }
     * 
     */
    public CreateOfferKrankenRequest createCreateOfferKrankenRequest() {
        return new CreateOfferKrankenRequest();
    }

    /**
     * Create an instance of {@link SpezOffertKrankenType }
     * 
     */
    public SpezOffertKrankenType createSpezOffertKrankenType() {
        return new SpezOffertKrankenType();
    }

    /**
     * Create an instance of {@link CreateOfferKrankenResponse }
     * 
     */
    public CreateOfferKrankenResponse createCreateOfferKrankenResponse() {
        return new CreateOfferKrankenResponse();
    }

    /**
     * Create an instance of {@link CreateApplicationKrankenRequest }
     * 
     */
    public CreateApplicationKrankenRequest createCreateApplicationKrankenRequest() {
        return new CreateApplicationKrankenRequest();
    }

    /**
     * Create an instance of {@link SpezAntragKrankenType }
     * 
     */
    public SpezAntragKrankenType createSpezAntragKrankenType() {
        return new SpezAntragKrankenType();
    }

    /**
     * Create an instance of {@link CreateApplicationKrankenResponse }
     * 
     */
    public CreateApplicationKrankenResponse createCreateApplicationKrankenResponse() {
        return new CreateApplicationKrankenResponse();
    }

    /**
     * Create an instance of {@link SubmitApplicationKrankenRequest }
     * 
     */
    public SubmitApplicationKrankenRequest createSubmitApplicationKrankenRequest() {
        return new SubmitApplicationKrankenRequest();
    }

    /**
     * Create an instance of {@link SubmitApplicationKrankenResponse }
     * 
     */
    public SubmitApplicationKrankenResponse createSubmitApplicationKrankenResponse() {
        return new SubmitApplicationKrankenResponse();
    }

    /**
     * Create an instance of {@link VerkaufsproduktKrankenType }
     * 
     */
    public VerkaufsproduktKrankenType createVerkaufsproduktKrankenType() {
        return new VerkaufsproduktKrankenType();
    }

    /**
     * Create an instance of {@link ProduktKrankenType }
     * 
     */
    public ProduktKrankenType createProduktKrankenType() {
        return new ProduktKrankenType();
    }

    /**
     * Create an instance of {@link ElementarproduktKrankenType }
     * 
     */
    public ElementarproduktKrankenType createElementarproduktKrankenType() {
        return new ElementarproduktKrankenType();
    }

}
