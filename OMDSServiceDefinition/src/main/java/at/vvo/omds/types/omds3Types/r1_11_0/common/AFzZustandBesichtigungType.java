
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.FzZustandBesichtigungType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Ergebnisse einer Besichtigung
 * 
 * <p>Java-Klasse für AFzZustandBesichtigung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AFzZustandBesichtigung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AFzZustandBesichtigung_Type")
@XmlSeeAlso({
    FzZustandBesichtigungType.class
})
public abstract class AFzZustandBesichtigungType {


}
