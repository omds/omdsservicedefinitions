
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Fragen FATCA bei sonstigen Personen
 * 
 * <p>Java-Klasse für FATCA_SonstPersonType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FATCA_SonstPersonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LandFirmensitz" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type"/&gt;
 *         &lt;element name="GIIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="KonzessionFinanzen" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="NichtFinanzielleDienstleistungen" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FATCA_SonstPersonType", propOrder = {
    "landFirmensitz",
    "giin",
    "konzessionFinanzen",
    "nichtFinanzielleDienstleistungen"
})
public class FATCASonstPersonType {

    @XmlElement(name = "LandFirmensitz", required = true)
    protected AttributEnumType landFirmensitz;
    @XmlElement(name = "GIIN")
    protected String giin;
    @XmlElement(name = "KonzessionFinanzen")
    protected boolean konzessionFinanzen;
    @XmlElement(name = "NichtFinanzielleDienstleistungen")
    protected boolean nichtFinanzielleDienstleistungen;

    /**
     * Ruft den Wert der landFirmensitz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getLandFirmensitz() {
        return landFirmensitz;
    }

    /**
     * Legt den Wert der landFirmensitz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setLandFirmensitz(AttributEnumType value) {
        this.landFirmensitz = value;
    }

    /**
     * Ruft den Wert der giin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGIIN() {
        return giin;
    }

    /**
     * Legt den Wert der giin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGIIN(String value) {
        this.giin = value;
    }

    /**
     * Ruft den Wert der konzessionFinanzen-Eigenschaft ab.
     * 
     */
    public boolean isKonzessionFinanzen() {
        return konzessionFinanzen;
    }

    /**
     * Legt den Wert der konzessionFinanzen-Eigenschaft fest.
     * 
     */
    public void setKonzessionFinanzen(boolean value) {
        this.konzessionFinanzen = value;
    }

    /**
     * Ruft den Wert der nichtFinanzielleDienstleistungen-Eigenschaft ab.
     * 
     */
    public boolean isNichtFinanzielleDienstleistungen() {
        return nichtFinanzielleDienstleistungen;
    }

    /**
     * Legt den Wert der nichtFinanzielleDienstleistungen-Eigenschaft fest.
     * 
     */
    public void setNichtFinanzielleDienstleistungen(boolean value) {
        this.nichtFinanzielleDienstleistungen = value;
    }

}
