
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Gemeinsame Details zu einer Vorversicherung. Dieser Typ wird erweitert von Vorversicherung_Type und ist der Typ von VorversicherungRechtschutz und VorversicherungKfz in Kfz
 * 
 * <p>Java-Klasse für VorversicherungenDetail_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VorversicherungenDetail_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WurdenVorversicherungenAufgeloest" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Aufloesungsgrund" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="aus Schaden"/&gt;
 *               &lt;enumeration value="durch einvernehmliche Kündigung"/&gt;
 *               &lt;enumeration value="durch Ablauf"/&gt;
 *               &lt;enumeration value="durch Besitzwechsel"/&gt;
 *               &lt;enumeration value="aus sonstigen Gründen"/&gt;
 *               &lt;enumeration value="zum Ablauf gekündigte Vorversicherung"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AuslaendischeVersicherungsgesellschaft" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Versicherungsgesellschaft" type="{urn:omds3CommonServiceTypes-1-1-0}Versicherungsgesellschaft_Type"/&gt;
 *         &lt;element name="VersInteresseRefLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheVorversicherungsdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheVorversicherungsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VorversicherungenDetail_Type", propOrder = {
    "wurdenVorversicherungenAufgeloest",
    "aufloesungsgrund",
    "auslaendischeVersicherungsgesellschaft",
    "versicherungsgesellschaft",
    "versInteresseRefLfnr",
    "zusaetzlicheVorversicherungsdaten"
})
@XmlSeeAlso({
    VorversicherungType.class,
    at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.VorversicherungenKfzType.VorversicherungKfz.class
})
public class VorversicherungenDetailType {

    @XmlElement(name = "WurdenVorversicherungenAufgeloest")
    protected boolean wurdenVorversicherungenAufgeloest;
    @XmlElement(name = "Aufloesungsgrund")
    protected String aufloesungsgrund;
    @XmlElement(name = "AuslaendischeVersicherungsgesellschaft")
    protected Boolean auslaendischeVersicherungsgesellschaft;
    @XmlElement(name = "Versicherungsgesellschaft", required = true)
    protected String versicherungsgesellschaft;
    @XmlElement(name = "VersInteresseRefLfnr", type = Integer.class)
    @XmlSchemaType(name = "unsignedShort")
    protected List<Integer> versInteresseRefLfnr;
    @XmlElement(name = "ZusaetzlicheVorversicherungsdaten")
    protected List<ZusaetzlicheVorversicherungsdatenType> zusaetzlicheVorversicherungsdaten;

    /**
     * Ruft den Wert der wurdenVorversicherungenAufgeloest-Eigenschaft ab.
     * 
     */
    public boolean isWurdenVorversicherungenAufgeloest() {
        return wurdenVorversicherungenAufgeloest;
    }

    /**
     * Legt den Wert der wurdenVorversicherungenAufgeloest-Eigenschaft fest.
     * 
     */
    public void setWurdenVorversicherungenAufgeloest(boolean value) {
        this.wurdenVorversicherungenAufgeloest = value;
    }

    /**
     * Ruft den Wert der aufloesungsgrund-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAufloesungsgrund() {
        return aufloesungsgrund;
    }

    /**
     * Legt den Wert der aufloesungsgrund-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAufloesungsgrund(String value) {
        this.aufloesungsgrund = value;
    }

    /**
     * Ruft den Wert der auslaendischeVersicherungsgesellschaft-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAuslaendischeVersicherungsgesellschaft() {
        return auslaendischeVersicherungsgesellschaft;
    }

    /**
     * Legt den Wert der auslaendischeVersicherungsgesellschaft-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAuslaendischeVersicherungsgesellschaft(Boolean value) {
        this.auslaendischeVersicherungsgesellschaft = value;
    }

    /**
     * Ruft den Wert der versicherungsgesellschaft-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersicherungsgesellschaft() {
        return versicherungsgesellschaft;
    }

    /**
     * Legt den Wert der versicherungsgesellschaft-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersicherungsgesellschaft(String value) {
        this.versicherungsgesellschaft = value;
    }

    /**
     * Gets the value of the versInteresseRefLfnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the versInteresseRefLfnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersInteresseRefLfnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getVersInteresseRefLfnr() {
        if (versInteresseRefLfnr == null) {
            versInteresseRefLfnr = new ArrayList<Integer>();
        }
        return this.versInteresseRefLfnr;
    }

    /**
     * Gets the value of the zusaetzlicheVorversicherungsdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheVorversicherungsdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheVorversicherungsdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheVorversicherungsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheVorversicherungsdatenType> getZusaetzlicheVorversicherungsdaten() {
        if (zusaetzlicheVorversicherungsdaten == null) {
            zusaetzlicheVorversicherungsdaten = new ArrayList<ZusaetzlicheVorversicherungsdatenType>();
        }
        return this.zusaetzlicheVorversicherungsdaten;
    }

}
