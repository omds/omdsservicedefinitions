
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CalculateRequestGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Requestobjekts für eine Berechnung Unfall
 * 
 * <p>Java-Klasse für CalculateUnfallRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateUnfallRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateRequestGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}SpezBerechnungUnfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateUnfallRequest_Type", propOrder = {
    "berechnungsanfrage"
})
public class CalculateUnfallRequestType
    extends CalculateRequestGenType
{

    @XmlElement(name = "Berechnungsanfrage", required = true)
    protected SpezBerechnungUnfallType berechnungsanfrage;

    /**
     * Ruft den Wert der berechnungsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungUnfallType }
     *     
     */
    public SpezBerechnungUnfallType getBerechnungsanfrage() {
        return berechnungsanfrage;
    }

    /**
     * Legt den Wert der berechnungsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungUnfallType }
     *     
     */
    public void setBerechnungsanfrage(SpezBerechnungUnfallType value) {
        this.berechnungsanfrage = value;
    }

}
