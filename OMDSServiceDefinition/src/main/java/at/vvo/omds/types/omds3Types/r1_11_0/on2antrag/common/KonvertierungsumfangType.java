
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.KonvertierungsumfangVertragType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Spezifiziert die Konvertierung mehrerer Verträge in einen neuen Vertrag
 * 
 * <p>Java-Klasse für Konvertierungsumfang_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Konvertierungsumfang_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vertrag" type="{urn:omds3CommonServiceTypes-1-1-0}KonvertierungsumfangVertrag_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Konvertierungsumfang_Type", propOrder = {
    "vertraege"
})
public class KonvertierungsumfangType {

    @XmlElement(name = "Vertrag", required = true)
    protected List<KonvertierungsumfangVertragType> vertraege;

    /**
     * <p>Die Liste der Verträge, die in der Konvertierungsanforderung behandelt werden.</p>Gets the value of the vertraege property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vertraege property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertraege().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KonvertierungsumfangVertragType }
     * 
     * 
     */
    public List<KonvertierungsumfangVertragType> getVertraege() {
        if (vertraege == null) {
            vertraege = new ArrayList<KonvertierungsumfangVertragType>();
        }
        return this.vertraege;
    }

}
