
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Zuordnung zu Mahnung
 * 
 * <p>Java-Klasse für MahnverfahrenZuordnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MahnverfahrenZuordnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}DokumentenZuordnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Mahnverfahrennummer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Vermnr" type="{urn:omds20}Vermnr"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MahnverfahrenZuordnung_Type", propOrder = {
    "mahnverfahrennummer",
    "vermnr"
})
public class MahnverfahrenZuordnungType
    extends DokumentenZuordnungType
{

    @XmlElement(name = "Mahnverfahrennummer", required = true)
    protected String mahnverfahrennummer;
    @XmlElement(name = "Vermnr", required = true)
    protected String vermnr;

    /**
     * Ruft den Wert der mahnverfahrennummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnverfahrennummer() {
        return mahnverfahrennummer;
    }

    /**
     * Legt den Wert der mahnverfahrennummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnverfahrennummer(String value) {
        this.mahnverfahrennummer = value;
    }

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

}
