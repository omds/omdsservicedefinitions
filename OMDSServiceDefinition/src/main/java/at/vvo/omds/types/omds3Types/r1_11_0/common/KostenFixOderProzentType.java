
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.math.BigDecimal;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Dient zur Abbildung von Kosten als absoluter oder prozentualer Wert
 * 
 * <p>Java-Klasse für KostenFixOderProzent_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KostenFixOderProzent_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="AbsoluterBetrag" type="{urn:omds20}decimal"/&gt;
 *           &lt;element name="ProzentVs" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KostenFixOderProzent_Type", propOrder = {
    "absoluterBetrag",
    "prozentVs"
})
public class KostenFixOderProzentType {

    @XmlElement(name = "AbsoluterBetrag")
    protected BigDecimal absoluterBetrag;
    @XmlElement(name = "ProzentVs")
    @XmlSchemaType(name = "unsignedByte")
    protected Short prozentVs;

    /**
     * Ruft den Wert der absoluterBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAbsoluterBetrag() {
        return absoluterBetrag;
    }

    /**
     * Legt den Wert der absoluterBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAbsoluterBetrag(BigDecimal value) {
        this.absoluterBetrag = value;
    }

    /**
     * Ruft den Wert der prozentVs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getProzentVs() {
        return prozentVs;
    }

    /**
     * Legt den Wert der prozentVs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setProzentVs(Short value) {
        this.prozentVs = value;
    }

}
