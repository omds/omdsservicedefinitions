
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DateianhangType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokumentType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateApplicationKfzRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Request für den Antrag
 * 
 * <p>Java-Klasse für CreateApplicationRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}BOAProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DokAnfordVermittler" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokRequest_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokument_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationRequest_Type", propOrder = {
    "dokumentenAnforderungenVermittler",
    "dateianhaenge",
    "dokumente"
})
@XmlSeeAlso({
    CreateApplicationKfzRequestType.class,
    CreateApplicationRequestGenType.class
})
public abstract class CreateApplicationRequestType
    extends BOAProcessRequestType
{

    @XmlElement(name = "DokAnfordVermittler")
    protected List<ProzessDokRequestType> dokumentenAnforderungenVermittler;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;
    @XmlElement(name = "Dokumente")
    protected List<ProzessDokumentType> dokumente;

    /**
     * <p>Die Dokumente, welche der Vermittler für den Response anfordert.</p>Gets the value of the dokumentenAnforderungenVermittler property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumentenAnforderungenVermittler property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumentenAnforderungenVermittler().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokRequestType }
     * 
     * 
     */
    public List<ProzessDokRequestType> getDokumentenAnforderungenVermittler() {
        if (dokumentenAnforderungenVermittler == null) {
            dokumentenAnforderungenVermittler = new ArrayList<ProzessDokRequestType>();
        }
        return this.dokumentenAnforderungenVermittler;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokumentType }
     * 
     * 
     */
    public List<ProzessDokumentType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<ProzessDokumentType>();
        }
        return this.dokumente;
    }

}
