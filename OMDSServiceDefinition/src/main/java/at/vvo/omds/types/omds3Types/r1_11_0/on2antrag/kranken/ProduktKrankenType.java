
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktMitVpType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.SelbstbehaltType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Produkt in der Sparte Kranken. Von diesem Typ können einzelne VUs ihre eigenen Produkte ableiten, wenn sie möchten.
 * 
 * <p>Java-Klasse für ProduktKranken_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktKranken_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProduktMitVp_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Elementarprodukte" type="{urn:at.vvo.omds.types.omds3types.v1-6-0.on2antrag.kranken}ElementarproduktKranken_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktKranken_Type", propOrder = {
    "elementarprodukte",
    "selbstbehalt"
})
public class ProduktKrankenType
    extends ProduktMitVpType
{

    @XmlElement(name = "Elementarprodukte")
    protected List<ElementarproduktKrankenType> elementarprodukte;
    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;

    /**
     * Gets the value of the elementarprodukte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the elementarprodukte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElementarprodukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElementarproduktKrankenType }
     * 
     * 
     */
    public List<ElementarproduktKrankenType> getElementarprodukte() {
        if (elementarprodukte == null) {
            elementarprodukte = new ArrayList<ElementarproduktKrankenType>();
        }
        return this.elementarprodukte;
    }

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

}
