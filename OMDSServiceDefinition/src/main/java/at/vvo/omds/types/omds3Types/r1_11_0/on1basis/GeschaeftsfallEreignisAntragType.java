
package at.vvo.omds.types.omds3Types.r1_11_0.on1basis;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Ereignis in einem Geschäftsprozess vom Typ Antrag
 * 
 * <p>Java-Klasse für GeschaeftsfallEreignisAntrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GeschaeftsfallEreignisAntrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}GeschaeftsfallEreignis_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AntragsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BisherigerAntragsstatus" type="{urn:omds3CommonServiceTypes-1-1-0}SubmitApplicationStatus_Type" minOccurs="0"/&gt;
 *         &lt;element name="Antragsstatus" type="{urn:omds3CommonServiceTypes-1-1-0}SubmitApplicationStatus_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeschaeftsfallEreignisAntrag_Type", propOrder = {
    "antragsId",
    "bisherigerAntragsstatus",
    "antragsstatus"
})
public class GeschaeftsfallEreignisAntragType
    extends GeschaeftsfallEreignisType
{

    @XmlElement(name = "AntragsId", required = true)
    protected String antragsId;
    @XmlElement(name = "BisherigerAntragsstatus")
    protected Integer bisherigerAntragsstatus;
    @XmlElement(name = "Antragsstatus")
    protected int antragsstatus;

    /**
     * Ruft den Wert der antragsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAntragsId() {
        return antragsId;
    }

    /**
     * Legt den Wert der antragsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAntragsId(String value) {
        this.antragsId = value;
    }

    /**
     * Ruft den Wert der bisherigerAntragsstatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBisherigerAntragsstatus() {
        return bisherigerAntragsstatus;
    }

    /**
     * Legt den Wert der bisherigerAntragsstatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBisherigerAntragsstatus(Integer value) {
        this.bisherigerAntragsstatus = value;
    }

    /**
     * Ruft den Wert der antragsstatus-Eigenschaft ab.
     * 
     */
    public int getAntragsstatus() {
        return antragsstatus;
    }

    /**
     * Legt den Wert der antragsstatus-Eigenschaft fest.
     * 
     */
    public void setAntragsstatus(int value) {
        this.antragsstatus = value;
    }

}
