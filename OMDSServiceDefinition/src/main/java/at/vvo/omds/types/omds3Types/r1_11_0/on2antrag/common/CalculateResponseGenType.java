
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.CalculateKrankenResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.CalculateLebenResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CalculateSachPrivatResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.CalculateUnfallResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Response Berechnung 
 * 
 * <p>Java-Klasse für CalculateResponseGen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateResponseGen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateResponse_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateResponseGen_Type")
@XmlSeeAlso({
    CalculateSachPrivatResponseType.class,
    CalculateUnfallResponseType.class,
    CalculateLebenResponseType.class,
    CalculateKrankenResponse.class
})
public abstract class CalculateResponseGenType
    extends CalculateResponseType
{


}
