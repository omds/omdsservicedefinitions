
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Type Bezugsrecht, von diesem erben die unterschiedlichen Typen: Gesetzliche Erben, Überbringer, Namentlich, Individuell
 * 
 * <p>Java-Klasse für Bezugsberechtigung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Bezugsberechtigung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Art"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *               &lt;enumeration value="0"/&gt;
 *               &lt;enumeration value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bezugsberechtigung_Type", propOrder = {
    "art"
})
@XmlSeeAlso({
    BezugsberechtigungGesetzlicheErbenType.class,
    BezugsberechtigungTestamentarischeErbenType.class,
    BezugsberechtigungUeberbringerType.class,
    BezugsberechtigungNamentlich.class,
    BezugsberechtigungVersicherungsnehmerType.class,
    BezugsberechtigungVersichertePersonType.class,
    BezugsberechtigungIndividuell.class
})
public abstract class BezugsberechtigungType {

    @XmlElement(name = "Art")
    protected short art;

    /**
     * Ruft den Wert der art-Eigenschaft ab.
     * 
     */
    public short getArt() {
        return art;
    }

    /**
     * Legt den Wert der art-Eigenschaft fest.
     * 
     */
    public void setArt(short value) {
        this.art = value;
    }

}
