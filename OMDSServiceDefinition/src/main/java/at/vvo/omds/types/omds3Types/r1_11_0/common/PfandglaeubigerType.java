
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Eigenschaften Pfandgläubiger
 * 
 * <p>Java-Klasse für Pfandglaeubiger_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Pfandglaeubiger_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Anteil" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="Glaeubiger" type="{urn:omds3CommonServiceTypes-1-1-0}GlaeubigerSicherstellung_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pfandglaeubiger_Type", propOrder = {
    "anteil",
    "glaeubiger"
})
public class PfandglaeubigerType {

    @XmlElement(name = "Anteil")
    protected double anteil;
    @XmlElement(name = "Glaeubiger", required = true)
    protected GlaeubigerSicherstellungType glaeubiger;

    /**
     * Ruft den Wert der anteil-Eigenschaft ab.
     * 
     */
    public double getAnteil() {
        return anteil;
    }

    /**
     * Legt den Wert der anteil-Eigenschaft fest.
     * 
     */
    public void setAnteil(double value) {
        this.anteil = value;
    }

    /**
     * Ruft den Wert der glaeubiger-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GlaeubigerSicherstellungType }
     *     
     */
    public GlaeubigerSicherstellungType getGlaeubiger() {
        return glaeubiger;
    }

    /**
     * Legt den Wert der glaeubiger-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GlaeubigerSicherstellungType }
     *     
     */
    public void setGlaeubiger(GlaeubigerSicherstellungType value) {
        this.glaeubiger = value;
    }

}
