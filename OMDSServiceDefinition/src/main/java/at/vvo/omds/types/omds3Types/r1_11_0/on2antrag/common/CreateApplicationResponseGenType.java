
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.CreateApplicationKrankenResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.CreateApplicationLebenResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateApplicationSachPrivatResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.CreateApplicationUnfallResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Response der Antragserzeugung
 * 
 * <p>Java-Klasse für CreateApplicationResponseGen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationResponseGen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateApplicationResponse_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationResponseGen_Type")
@XmlSeeAlso({
    CreateApplicationSachPrivatResponseType.class,
    CreateApplicationUnfallResponseType.class,
    CreateApplicationLebenResponseType.class,
    CreateApplicationKrankenResponse.class
})
public abstract class CreateApplicationResponseGenType
    extends CreateApplicationResponseType
{


}
