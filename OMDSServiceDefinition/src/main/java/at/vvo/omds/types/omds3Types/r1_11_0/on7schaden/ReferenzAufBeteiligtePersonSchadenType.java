
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Das Zuordnungsobjekt, welches die fachliche Zuordnung eines Schadens ermöglicht
 * 
 * <p>Java-Klasse für ReferenzAufBeteiligtePersonSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReferenzAufBeteiligtePersonSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="BetTxt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="100"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenzAufBeteiligtePersonSchaden_Type")
@XmlSeeAlso({
    at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SchadenType.BeteiligtePersonen.class
})
public class ReferenzAufBeteiligtePersonSchadenType {

    @XmlAttribute(name = "BetLfnr", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int betLfnr;
    @XmlAttribute(name = "BetTxt", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden")
    protected String betTxt;

    /**
     * Ruft den Wert der betLfnr-Eigenschaft ab.
     * 
     */
    public int getBetLfnr() {
        return betLfnr;
    }

    /**
     * Legt den Wert der betLfnr-Eigenschaft fest.
     * 
     */
    public void setBetLfnr(int value) {
        this.betLfnr = value;
    }

    /**
     * Ruft den Wert der betTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetTxt() {
        return betTxt;
    }

    /**
     * Legt den Wert der betTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetTxt(String value) {
        this.betTxt = value;
    }

}
