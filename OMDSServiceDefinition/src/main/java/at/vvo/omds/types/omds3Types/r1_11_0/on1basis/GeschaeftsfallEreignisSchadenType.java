
package at.vvo.omds.types.omds3Types.r1_11_0.on1basis;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Ereignis in einem Geschäftsprozess vom Typ Schaden
 * 
 * <p>Java-Klasse für GeschaeftsfallEreignisSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GeschaeftsfallEreignisSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}GeschaeftsfallEreignis_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BisherigerBearbStandCd" type="{urn:omds20}BearbStandCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="BearbStandCd" type="{urn:omds20}BearbStandCd_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeschaeftsfallEreignisSchaden_Type", propOrder = {
    "bisherigerBearbStandCd",
    "bearbStandCd"
})
public class GeschaeftsfallEreignisSchadenType
    extends GeschaeftsfallEreignisType
{

    @XmlElement(name = "BisherigerBearbStandCd")
    protected String bisherigerBearbStandCd;
    @XmlElement(name = "BearbStandCd", required = true)
    protected String bearbStandCd;

    /**
     * Ruft den Wert der bisherigerBearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBisherigerBearbStandCd() {
        return bisherigerBearbStandCd;
    }

    /**
     * Legt den Wert der bisherigerBearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBisherigerBearbStandCd(String value) {
        this.bisherigerBearbStandCd = value;
    }

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

}
