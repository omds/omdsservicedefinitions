
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.SubmitApplicationResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type des Response, um den Antrag einzureichen
 * 
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SubmitApplicationResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-6-0.on2antrag.kranken}SpezAntragKranken_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "antragsantwort"
})
@XmlRootElement(name = "SubmitApplicationKrankenResponse")
public class SubmitApplicationKrankenResponse
    extends SubmitApplicationResponseGenType
{

    @XmlElement(name = "Antragsantwort", required = true)
    protected SpezAntragKrankenType antragsantwort;

    /**
     * Ruft den Wert der antragsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragKrankenType }
     *     
     */
    public SpezAntragKrankenType getAntragsantwort() {
        return antragsantwort;
    }

    /**
     * Legt den Wert der antragsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragKrankenType }
     *     
     */
    public void setAntragsantwort(SpezAntragKrankenType value) {
        this.antragsantwort = value;
    }

}
