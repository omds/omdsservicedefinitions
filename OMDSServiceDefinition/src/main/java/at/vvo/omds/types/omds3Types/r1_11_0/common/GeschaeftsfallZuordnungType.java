
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Zuordnung für Geschaeftsfallreferenzen
 * 
 * <p>Java-Klasse für GeschaeftsfallZuordnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GeschaeftsfallZuordnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}DokumentenZuordnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Geschaeftsfallnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *         &lt;element name="GeschaeftsfallArt" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallArt_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeschaeftsfallZuordnung_Type", propOrder = {
    "geschaeftsfallnummer",
    "geschaeftsfallArt"
})
public class GeschaeftsfallZuordnungType
    extends DokumentenZuordnungType
{

    @XmlElement(name = "Geschaeftsfallnummer", required = true)
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "GeschaeftsfallArt")
    protected String geschaeftsfallArt;

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallArt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeschaeftsfallArt() {
        return geschaeftsfallArt;
    }

    /**
     * Legt den Wert der geschaeftsfallArt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeschaeftsfallArt(String value) {
        this.geschaeftsfallArt = value;
    }

}
