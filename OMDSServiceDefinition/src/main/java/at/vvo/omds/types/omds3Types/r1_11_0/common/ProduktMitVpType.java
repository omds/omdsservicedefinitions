
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.ProduktKrankenType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.ProduktLebenType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.ProduktUnfallType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Basistyp für ein Produkt vom Typ "Versicherte Person" für Personensparten
 * 
 * <p>Java-Klasse für ProduktMitVp_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktMitVp_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProduktGenerisch_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Bezugsberechtigungen" type="{urn:omds3CommonServiceTypes-1-1-0}Bezugsberechtigung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VersPersonenRefLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktMitVp_Type", propOrder = {
    "bezugsberechtigungen",
    "versPersonenRefLfnr"
})
@XmlSeeAlso({
    ProduktUnfallType.class,
    ProduktKrankenType.class,
    ProduktLebenType.class
})
public abstract class ProduktMitVpType
    extends ProduktGenerischType
{

    @XmlElement(name = "Bezugsberechtigungen")
    protected List<BezugsberechtigungType> bezugsberechtigungen;
    @XmlElement(name = "VersPersonenRefLfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int versPersonenRefLfnr;

    /**
     * Gets the value of the bezugsberechtigungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bezugsberechtigungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBezugsberechtigungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BezugsberechtigungType }
     * 
     * 
     */
    public List<BezugsberechtigungType> getBezugsberechtigungen() {
        if (bezugsberechtigungen == null) {
            bezugsberechtigungen = new ArrayList<BezugsberechtigungType>();
        }
        return this.bezugsberechtigungen;
    }

    /**
     * Ruft den Wert der versPersonenRefLfnr-Eigenschaft ab.
     * 
     */
    public int getVersPersonenRefLfnr() {
        return versPersonenRefLfnr;
    }

    /**
     * Legt den Wert der versPersonenRefLfnr-Eigenschaft fest.
     * 
     */
    public void setVersPersonenRefLfnr(int value) {
        this.versPersonenRefLfnr = value;
    }

}
