
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CalculateRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Requestobjekts für eine Kfz-Berechnung
 * 
 * <p>Java-Klasse für CalculateKfzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateKfzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezBerechnungKfz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateKfzRequest_Type", propOrder = {
    "berechnungsanfrage"
})
public class CalculateKfzRequestType
    extends CalculateRequestType
{

    @XmlElement(name = "Berechnungsanfrage", required = true)
    protected SpezBerechnungKfzType berechnungsanfrage;

    /**
     * Ruft den Wert der berechnungsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungKfzType }
     *     
     */
    public SpezBerechnungKfzType getBerechnungsanfrage() {
        return berechnungsanfrage;
    }

    /**
     * Legt den Wert der berechnungsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungKfzType }
     *     
     */
    public void setBerechnungsanfrage(SpezBerechnungKfzType value) {
        this.berechnungsanfrage = value;
    }

}
