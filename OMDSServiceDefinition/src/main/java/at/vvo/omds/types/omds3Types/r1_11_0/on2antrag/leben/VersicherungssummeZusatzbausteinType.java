
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import at.vvo.omds.types.omds3Types.r1_11_0.common.AttributDezimalType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.AttributDoubleType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Definition einer Versicherungssumme in einem Zusatzbaustein
 * 
 * <p>Java-Klasse für VersicherungssummeZusatzbaustein_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersicherungssummeZusatzbaustein_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="ProzentVersicherungssumme" type="{urn:omds3CommonServiceTypes-1-1-0}AttributDouble_Type" minOccurs="0"/&gt;
 *         &lt;element name="Betrag" type="{urn:omds3CommonServiceTypes-1-1-0}AttributDezimal_Type" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersicherungssummeZusatzbaustein_Type", propOrder = {
    "prozentVersicherungssumme",
    "betrag"
})
public class VersicherungssummeZusatzbausteinType {

    @XmlElement(name = "ProzentVersicherungssumme")
    protected AttributDoubleType prozentVersicherungssumme;
    @XmlElement(name = "Betrag")
    protected AttributDezimalType betrag;

    /**
     * Ruft den Wert der prozentVersicherungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributDoubleType }
     *     
     */
    public AttributDoubleType getProzentVersicherungssumme() {
        return prozentVersicherungssumme;
    }

    /**
     * Legt den Wert der prozentVersicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributDoubleType }
     *     
     */
    public void setProzentVersicherungssumme(AttributDoubleType value) {
        this.prozentVersicherungssumme = value;
    }

    /**
     * Ruft den Wert der betrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributDezimalType }
     *     
     */
    public AttributDezimalType getBetrag() {
        return betrag;
    }

    /**
     * Legt den Wert der betrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributDezimalType }
     *     
     */
    public void setBetrag(AttributDezimalType value) {
        this.betrag = value;
    }

}
