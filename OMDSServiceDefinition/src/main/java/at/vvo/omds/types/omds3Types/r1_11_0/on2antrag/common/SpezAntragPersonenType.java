
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.common.FATCAType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.GMSGType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.PEPType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.TreuhaenderfrageType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.SpezAntragLebenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Basistyp für Antrag in der Personenversicherung, der bei Request und bei Response gleich ist
 * 
 * <p>Java-Klasse für SpezAntragPersonen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezAntragPersonen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezAntrag_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FATCA" type="{urn:omds3CommonServiceTypes-1-1-0}FATCA_Type" minOccurs="0"/&gt;
 *         &lt;element name="GSGM" type="{urn:omds3CommonServiceTypes-1-1-0}GMSG_Type" minOccurs="0"/&gt;
 *         &lt;element name="PEP" type="{urn:omds3CommonServiceTypes-1-1-0}PEP_Type"/&gt;
 *         &lt;element name="Treuhaenderfrage" type="{urn:omds3CommonServiceTypes-1-1-0}Treuhaenderfrage_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezAntragPersonen_Type", propOrder = {
    "fatca",
    "gsgm",
    "pep",
    "treuhaenderfrage"
})
@XmlSeeAlso({
    SpezAntragLebenType.class
})
public abstract class SpezAntragPersonenType
    extends SpezAntragType
{

    @XmlElement(name = "FATCA")
    protected FATCAType fatca;
    @XmlElement(name = "GSGM")
    protected GMSGType gsgm;
    @XmlElement(name = "PEP", required = true)
    protected PEPType pep;
    @XmlElement(name = "Treuhaenderfrage", required = true)
    protected TreuhaenderfrageType treuhaenderfrage;

    /**
     * Ruft den Wert der fatca-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FATCAType }
     *     
     */
    public FATCAType getFATCA() {
        return fatca;
    }

    /**
     * Legt den Wert der fatca-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FATCAType }
     *     
     */
    public void setFATCA(FATCAType value) {
        this.fatca = value;
    }

    /**
     * Ruft den Wert der gsgm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GMSGType }
     *     
     */
    public GMSGType getGSGM() {
        return gsgm;
    }

    /**
     * Legt den Wert der gsgm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GMSGType }
     *     
     */
    public void setGSGM(GMSGType value) {
        this.gsgm = value;
    }

    /**
     * Ruft den Wert der pep-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PEPType }
     *     
     */
    public PEPType getPEP() {
        return pep;
    }

    /**
     * Legt den Wert der pep-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PEPType }
     *     
     */
    public void setPEP(PEPType value) {
        this.pep = value;
    }

    /**
     * Ruft den Wert der treuhaenderfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TreuhaenderfrageType }
     *     
     */
    public TreuhaenderfrageType getTreuhaenderfrage() {
        return treuhaenderfrage;
    }

    /**
     * Legt den Wert der treuhaenderfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TreuhaenderfrageType }
     *     
     */
    public void setTreuhaenderfrage(TreuhaenderfrageType value) {
        this.treuhaenderfrage = value;
    }

}
