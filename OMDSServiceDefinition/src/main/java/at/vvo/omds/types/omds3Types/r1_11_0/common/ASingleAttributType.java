
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Basistyp für Attribute mit einem Wert
 * 
 * <p>Java-Klasse für ASingleAttribut_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ASingleAttribut_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Attribut_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Pflichtfeld" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ASingleAttribut_Type", propOrder = {
    "pflichtfeld"
})
@XmlSeeAlso({
    AttributStringType.class,
    AttributIntType.class,
    AttributDezimalType.class,
    AttributDoubleType.class,
    AttributDatumType.class
})
public abstract class ASingleAttributType
    extends AttributType
{

    @XmlElement(name = "Pflichtfeld")
    protected Boolean pflichtfeld;

    /**
     * Ruft den Wert der pflichtfeld-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPflichtfeld() {
        return pflichtfeld;
    }

    /**
     * Legt den Wert der pflichtfeld-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPflichtfeld(Boolean value) {
        this.pflichtfeld = value;
    }

}
