
package at.vvo.omds.types.omds3Types.r1_11_0.on1basis;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GeschaeftsfallStatus_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="GeschaeftsfallStatus_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Erzeugt"/&gt;
 *     &lt;enumeration value="Laufend"/&gt;
 *     &lt;enumeration value="Unterbrochen"/&gt;
 *     &lt;enumeration value="Wartet"/&gt;
 *     &lt;enumeration value="Abgebrochen"/&gt;
 *     &lt;enumeration value="Abgeschlossen"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GeschaeftsfallStatus_Type")
@XmlEnum
public enum GeschaeftsfallStatusType {


    /**
     * Der Prozess wurde angelegt, aber noch nicht gestartet.
     * 
     */
    @XmlEnumValue("Erzeugt")
    ERZEUGT("Erzeugt"),

    /**
     * Der Prozess wird ausgeführt.
     * 
     */
    @XmlEnumValue("Laufend")
    LAUFEND("Laufend"),

    /**
     * An dem Prozess wird nicht mehr gearbeitet. Die Arbeit kann später wieder aufgenommen werden.
     * 
     */
    @XmlEnumValue("Unterbrochen")
    UNTERBROCHEN("Unterbrochen"),

    /**
     * An dem Prozess wird nicht gearbeitet, da auf Rückmeldung vom Vermittler gewartet wird.
     * 
     */
    @XmlEnumValue("Wartet")
    WARTET("Wartet"),

    /**
     * Der Prozess wurde dauerhaft abgebrochen ohne ein Ergebnis zu erreichen.
     * 
     */
    @XmlEnumValue("Abgebrochen")
    ABGEBROCHEN("Abgebrochen"),

    /**
     * Der Prozess wurde mit Ergebnis abgeschlossen.
     * 
     */
    @XmlEnumValue("Abgeschlossen")
    ABGESCHLOSSEN("Abgeschlossen");
    private final String value;

    GeschaeftsfallStatusType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GeschaeftsfallStatusType fromValue(String v) {
        for (GeschaeftsfallStatusType c: GeschaeftsfallStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
