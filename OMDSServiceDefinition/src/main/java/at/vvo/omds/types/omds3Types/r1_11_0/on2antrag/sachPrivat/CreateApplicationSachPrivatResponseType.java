
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateApplicationResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Responseobjekts für einen Antrag Besitz
 * 
 * <p>Java-Klasse für CreateApplicationSachPrivatResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationSachPrivatResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateApplicationResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}AntragSachPrivat_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationSachPrivatResponse_Type", propOrder = {
    "antragsantwort"
})
public class CreateApplicationSachPrivatResponseType
    extends CreateApplicationResponseGenType
{

    @XmlElement(name = "Antragsantwort", required = true)
    protected AntragSachPrivatType antragsantwort;

    /**
     * Ruft den Wert der antragsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AntragSachPrivatType }
     *     
     */
    public AntragSachPrivatType getAntragsantwort() {
        return antragsantwort;
    }

    /**
     * Legt den Wert der antragsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AntragSachPrivatType }
     *     
     */
    public void setAntragsantwort(AntragSachPrivatType value) {
        this.antragsantwort = value;
    }

}
