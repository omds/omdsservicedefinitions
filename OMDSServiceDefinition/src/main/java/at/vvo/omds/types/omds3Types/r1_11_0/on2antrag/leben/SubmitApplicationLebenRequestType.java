
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.SubmitApplicationRequestGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type des Requests, um den Antrag einzureichen
 * 
 * <p>Java-Klasse für SubmitApplicationLebenRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationLebenRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SubmitApplicationRequestGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}SpezAntragLeben_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationLebenRequest_Type", propOrder = {
    "antragsanfrage"
})
public class SubmitApplicationLebenRequestType
    extends SubmitApplicationRequestGenType
{

    @XmlElement(name = "Antragsanfrage")
    protected SpezAntragLebenType antragsanfrage;

    /**
     * Ruft den Wert der antragsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragLebenType }
     *     
     */
    public SpezAntragLebenType getAntragsanfrage() {
        return antragsanfrage;
    }

    /**
     * Legt den Wert der antragsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragLebenType }
     *     
     */
    public void setAntragsanfrage(SpezAntragLebenType value) {
        this.antragsanfrage = value;
    }

}
