
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Typ für Einwilligung personenbezogene Daten
 * 
 * <p>Java-Klasse für EinwilligungPersDaten_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EinwilligungPersDaten_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Einwilligung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PersonRefLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EinwilligungPersDaten_Type", propOrder = {
    "personRefLfnr"
})
@XmlSeeAlso({
    EinwVerarbGesDatenType.class,
    EinwGesDatenVNType.class,
    EinwSprachaufzType.class
})
public abstract class EinwilligungPersDatenType
    extends EinwilligungType
{

    @XmlElement(name = "PersonRefLfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int personRefLfnr;

    /**
     * Ruft den Wert der personRefLfnr-Eigenschaft ab.
     * 
     */
    public int getPersonRefLfnr() {
        return personRefLfnr;
    }

    /**
     * Legt den Wert der personRefLfnr-Eigenschaft fest.
     * 
     */
    public void setPersonRefLfnr(int value) {
        this.personRefLfnr = value;
    }

}
