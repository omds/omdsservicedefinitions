
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateOfferResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type des Responseobjekts für eine Erstellung eines Kranken-Offerts
 * 
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertantwort" type="{urn:at.vvo.omds.types.omds3types.v1-6-0.on2antrag.kranken}SpezOffertKranken_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "offertantwort"
})
@XmlRootElement(name = "CreateOfferKrankenResponse")
public class CreateOfferKrankenResponse
    extends CreateOfferResponseGenType
{

    @XmlElement(name = "Offertantwort", required = true)
    protected SpezOffertKrankenType offertantwort;

    /**
     * Ruft den Wert der offertantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezOffertKrankenType }
     *     
     */
    public SpezOffertKrankenType getOffertantwort() {
        return offertantwort;
    }

    /**
     * Legt den Wert der offertantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezOffertKrankenType }
     *     
     */
    public void setOffertantwort(SpezOffertKrankenType value) {
        this.offertantwort = value;
    }

}
