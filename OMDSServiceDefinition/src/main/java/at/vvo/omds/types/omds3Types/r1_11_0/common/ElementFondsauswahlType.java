
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Ein einzelnes Element einer Fondsauswahl
 * 
 * <p>Java-Klasse für ElementFondsauswahl_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementFondsauswahl_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="ISIN" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type"/&gt;
 *           &lt;element name="WKN" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Prozentanteil" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float"&gt;
 *               &lt;minInclusive value="0"/&gt;
 *               &lt;maxInclusive value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ZusaetzlicheFondsdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheFondsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Bezeichnung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementFondsauswahl_Type", propOrder = {
    "isin",
    "wkn",
    "prozentanteil",
    "zusaetzlicheFondsdaten",
    "bezeichnung"
})
public class ElementFondsauswahlType {

    @XmlElement(name = "ISIN")
    protected AttributEnumType isin;
    @XmlElement(name = "WKN")
    protected AttributEnumType wkn;
    @XmlElement(name = "Prozentanteil")
    protected Float prozentanteil;
    @XmlElement(name = "ZusaetzlicheFondsdaten")
    protected List<ZusaetzlicheFondsdatenType> zusaetzlicheFondsdaten;
    @XmlElement(name = "Bezeichnung")
    protected String bezeichnung;

    /**
     * Ruft den Wert der isin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getISIN() {
        return isin;
    }

    /**
     * Legt den Wert der isin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setISIN(AttributEnumType value) {
        this.isin = value;
    }

    /**
     * Ruft den Wert der wkn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getWKN() {
        return wkn;
    }

    /**
     * Legt den Wert der wkn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setWKN(AttributEnumType value) {
        this.wkn = value;
    }

    /**
     * Ruft den Wert der prozentanteil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getProzentanteil() {
        return prozentanteil;
    }

    /**
     * Legt den Wert der prozentanteil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setProzentanteil(Float value) {
        this.prozentanteil = value;
    }

    /**
     * Gets the value of the zusaetzlicheFondsdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheFondsdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheFondsdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheFondsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheFondsdatenType> getZusaetzlicheFondsdaten() {
        if (zusaetzlicheFondsdaten == null) {
            zusaetzlicheFondsdaten = new ArrayList<ZusaetzlicheFondsdatenType>();
        }
        return this.zusaetzlicheFondsdaten;
    }

    /**
     * Ruft den Wert der bezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Legt den Wert der bezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

}
