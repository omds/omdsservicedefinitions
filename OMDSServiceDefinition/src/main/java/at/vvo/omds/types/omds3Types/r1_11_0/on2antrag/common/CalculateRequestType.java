
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CalculateKfzRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Request für die Berechnung
 * 
 * <p>Java-Klasse für CalculateRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}BOAProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequestUpselling" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateRequest_Type", propOrder = {
    "requestUpselling"
})
@XmlSeeAlso({
    CalculateKfzRequestType.class,
    CalculateRequestGenType.class
})
public abstract class CalculateRequestType
    extends BOAProcessRequestType
{

    @XmlElement(name = "RequestUpselling", defaultValue = "false")
    protected Boolean requestUpselling;

    /**
     * Ruft den Wert der requestUpselling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequestUpselling() {
        return requestUpselling;
    }

    /**
     * Legt den Wert der requestUpselling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequestUpselling(Boolean value) {
        this.requestUpselling = value;
    }

}
