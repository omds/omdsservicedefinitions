
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ zur leichtgewichtigen Abbildung von Schadenereignis-Objekten
 * 
 * <p>Java-Klasse für SchadenereignisLight_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenereignisLight_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BasisSchadenereignis_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Schaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SchadenLight_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenereignisLight_Type", propOrder = {
    "schaeden"
})
public class SchadenereignisLightType
    extends BasisSchadenereignisType
{

    @XmlElement(name = "Schaeden", required = true)
    protected List<SchadenLightType> schaeden;

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenLightType }
     * 
     * 
     */
    public List<SchadenLightType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenLightType>();
        }
        return this.schaeden;
    }

}
