
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.CreateOfferKrankenResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.CreateOfferLebenResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateOfferSachPrivatResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.CreateOfferUnfallResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Response der Offerterstellung
 * 
 * <p>Java-Klasse für CreateOfferResponseGen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferResponseGen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferResponse_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferResponseGen_Type")
@XmlSeeAlso({
    CreateOfferSachPrivatResponseType.class,
    CreateOfferUnfallResponseType.class,
    CreateOfferLebenResponseType.class,
    CreateOfferKrankenResponse.class
})
public abstract class CreateOfferResponseGenType
    extends CreateOfferResponseType
{


}
