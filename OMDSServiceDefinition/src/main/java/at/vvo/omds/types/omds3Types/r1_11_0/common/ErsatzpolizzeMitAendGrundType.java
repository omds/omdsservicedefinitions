
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Einfache Implementierung einer zu ersetzenden Polizze mit Angabe eines Änderungsgrunds
 * 
 * <p>Java-Klasse für ErsatzpolizzeMitAendGrund_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErsatzpolizzeMitAendGrund_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Ersatzpolizze_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AendGrundCd" type="{urn:omds20}AendGrundCd_Type"/&gt;
 *         &lt;element name="AendGrundbez" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErsatzpolizzeMitAendGrund_Type", propOrder = {
    "aendGrundCd",
    "aendGrundbez"
})
public class ErsatzpolizzeMitAendGrundType
    extends ErsatzpolizzeType
{

    @XmlElement(name = "AendGrundCd", required = true)
    protected String aendGrundCd;
    @XmlElement(name = "AendGrundbez")
    protected String aendGrundbez;

    /**
     * Ruft den Wert der aendGrundCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAendGrundCd() {
        return aendGrundCd;
    }

    /**
     * Legt den Wert der aendGrundCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAendGrundCd(String value) {
        this.aendGrundCd = value;
    }

    /**
     * Ruft den Wert der aendGrundbez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAendGrundbez() {
        return aendGrundbez;
    }

    /**
     * Legt den Wert der aendGrundbez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAendGrundbez(String value) {
        this.aendGrundbez = value;
    }

}
