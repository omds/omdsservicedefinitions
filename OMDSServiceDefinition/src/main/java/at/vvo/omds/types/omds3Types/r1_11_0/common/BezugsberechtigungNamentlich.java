
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Bezugsberechtigung namentlich
 * 
 * <p>Java-Klasse für BezugsberechtigungNamentlich complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BezugsberechtigungNamentlich"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Bezugsberechtigung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Unwiderruflich" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Personen" type="{urn:omds3CommonServiceTypes-1-1-0}PersonNamentlichesBezugsrecht_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BezugsberechtigungNamentlich", propOrder = {
    "unwiderruflich",
    "personen"
})
public class BezugsberechtigungNamentlich
    extends BezugsberechtigungType
{

    @XmlElement(name = "Unwiderruflich")
    protected boolean unwiderruflich;
    @XmlElement(name = "Personen", required = true)
    protected List<PersonNamentlichesBezugsrechtType> personen;

    /**
     * Ruft den Wert der unwiderruflich-Eigenschaft ab.
     * 
     */
    public boolean isUnwiderruflich() {
        return unwiderruflich;
    }

    /**
     * Legt den Wert der unwiderruflich-Eigenschaft fest.
     * 
     */
    public void setUnwiderruflich(boolean value) {
        this.unwiderruflich = value;
    }

    /**
     * Gets the value of the personen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the personen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersonNamentlichesBezugsrechtType }
     * 
     * 
     */
    public List<PersonNamentlichesBezugsrechtType> getPersonen() {
        if (personen == null) {
            personen = new ArrayList<PersonNamentlichesBezugsrechtType>();
        }
        return this.personen;
    }

}
