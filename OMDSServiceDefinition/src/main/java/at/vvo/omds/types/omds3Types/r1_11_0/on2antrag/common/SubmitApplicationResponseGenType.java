
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.SubmitApplicationKrankenResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.SubmitApplicationLebenResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.SubmitApplicationSachPrivatResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.SubmitApplicationUnfallResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Response der Antragsüberleitung
 * 
 * <p>Java-Klasse für SubmitApplicationResponseGen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationResponseGen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SubmitApplicationResponse_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationResponseGen_Type")
@XmlSeeAlso({
    SubmitApplicationSachPrivatResponseType.class,
    SubmitApplicationUnfallResponseType.class,
    SubmitApplicationLebenResponseType.class,
    SubmitApplicationKrankenResponse.class
})
public abstract class SubmitApplicationResponseGenType
    extends SubmitApplicationResponseType
{


}
