
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.SpezBerechnungType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ der das Produkt beschreibt und in Berechnungsanfrage und Berechnungsantwort verwendet wird
 * 
 * <p>Java-Klasse für BerechnungSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BerechnungSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezBerechnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}VerkaufsproduktSachPrivat_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BerechnungSachPrivat_Type", propOrder = {
    "verkaufsprodukt"
})
public class BerechnungSachPrivatType
    extends SpezBerechnungType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktSachPrivatType verkaufsprodukt;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktSachPrivatType }
     *     
     */
    public VerkaufsproduktSachPrivatType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktSachPrivatType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktSachPrivatType value) {
        this.verkaufsprodukt = value;
    }

}
