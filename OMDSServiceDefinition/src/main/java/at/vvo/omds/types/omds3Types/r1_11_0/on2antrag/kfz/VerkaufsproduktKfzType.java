
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_16.PersArtCdType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.FahrzeugType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VerkaufsproduktType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Kfz-Produktbündel, welches einem Vertrag entspricht
 * 
 * <p>Java-Klasse für VerkaufsproduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Verkaufsprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Gebdat" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="PLZ" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="7"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PersArtCd" type="{urn:omds20}PersArtCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="KfzVersicherung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ProduktKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="KfzZusatzVersicherung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ZusatzproduktKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VersicherteFahrzeuge" type="{urn:omds3CommonServiceTypes-1-1-0}Fahrzeug_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktKfz_Type", propOrder = {
    "gebdat",
    "plz",
    "persArtCd",
    "kfzVersicherung",
    "kfzZusatzVersicherung",
    "versicherteFahrzeuge"
})
public class VerkaufsproduktKfzType
    extends VerkaufsproduktType
{

    @XmlElement(name = "Gebdat")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gebdat;
    @XmlElement(name = "PLZ")
    protected String plz;
    @XmlElement(name = "PersArtCd")
    @XmlSchemaType(name = "string")
    protected PersArtCdType persArtCd;
    @XmlElement(name = "KfzVersicherung")
    protected List<ProduktKfzType> kfzVersicherung;
    @XmlElement(name = "KfzZusatzVersicherung")
    protected List<ZusatzproduktKfzType> kfzZusatzVersicherung;
    @XmlElement(name = "VersicherteFahrzeuge")
    protected List<FahrzeugType> versicherteFahrzeuge;

    /**
     * Ruft den Wert der gebdat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGebdat() {
        return gebdat;
    }

    /**
     * Legt den Wert der gebdat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGebdat(XMLGregorianCalendar value) {
        this.gebdat = value;
    }

    /**
     * Ruft den Wert der plz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLZ() {
        return plz;
    }

    /**
     * Legt den Wert der plz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLZ(String value) {
        this.plz = value;
    }

    /**
     * Ruft den Wert der persArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersArtCdType }
     *     
     */
    public PersArtCdType getPersArtCd() {
        return persArtCd;
    }

    /**
     * Legt den Wert der persArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersArtCdType }
     *     
     */
    public void setPersArtCd(PersArtCdType value) {
        this.persArtCd = value;
    }

    /**
     * Gets the value of the kfzVersicherung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the kfzVersicherung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKfzVersicherung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktKfzType }
     * 
     * 
     */
    public List<ProduktKfzType> getKfzVersicherung() {
        if (kfzVersicherung == null) {
            kfzVersicherung = new ArrayList<ProduktKfzType>();
        }
        return this.kfzVersicherung;
    }

    /**
     * Gets the value of the kfzZusatzVersicherung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the kfzZusatzVersicherung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKfzZusatzVersicherung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusatzproduktKfzType }
     * 
     * 
     */
    public List<ZusatzproduktKfzType> getKfzZusatzVersicherung() {
        if (kfzZusatzVersicherung == null) {
            kfzZusatzVersicherung = new ArrayList<ZusatzproduktKfzType>();
        }
        return this.kfzZusatzVersicherung;
    }

    /**
     * Gets the value of the versicherteFahrzeuge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the versicherteFahrzeuge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersicherteFahrzeuge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FahrzeugType }
     * 
     * 
     */
    public List<FahrzeugType> getVersicherteFahrzeuge() {
        if (versicherteFahrzeuge == null) {
            versicherteFahrzeuge = new ArrayList<FahrzeugType>();
        }
        return this.versicherteFahrzeuge;
    }

}
