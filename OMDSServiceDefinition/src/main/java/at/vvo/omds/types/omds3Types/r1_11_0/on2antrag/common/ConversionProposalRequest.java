
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Konvertierungsplan" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}Konvertierungsumfang_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "konvertierungsplan"
})
@XmlRootElement(name = "ConversionProposalRequest")
public class ConversionProposalRequest
    extends CommonRequestType
{

    @XmlElement(name = "Konvertierungsplan", required = true)
    protected KonvertierungsumfangType konvertierungsplan;

    /**
     * Ruft den Wert der konvertierungsplan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KonvertierungsumfangType }
     *     
     */
    public KonvertierungsumfangType getKonvertierungsplan() {
        return konvertierungsplan;
    }

    /**
     * Legt den Wert der konvertierungsplan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KonvertierungsumfangType }
     *     
     */
    public void setKonvertierungsplan(KonvertierungsumfangType value) {
        this.konvertierungsplan = value;
    }

}
