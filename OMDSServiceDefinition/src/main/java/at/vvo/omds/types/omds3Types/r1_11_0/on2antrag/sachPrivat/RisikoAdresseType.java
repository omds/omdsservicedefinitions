
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import at.vvo.omds.types.omds2Types.v2_16.ADRESSEType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type zur Beschreibung einer Risikoadresse
 * 
 * <p>Java-Klasse für RisikoAdresse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RisikoAdresse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds20}ADRESSE_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Horazone" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/&gt;
 *         &lt;element name="ImVerbautenOrt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AusserhalbDesOrtsgebiets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ErreichbarkeitFuerLoeschfahrzeuge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Tarifzone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RisikoAdresse_Type", propOrder = {
    "horazone",
    "imVerbautenOrt",
    "ausserhalbDesOrtsgebiets",
    "erreichbarkeitFuerLoeschfahrzeuge",
    "tarifzone"
})
public class RisikoAdresseType
    extends ADRESSEType
{

    @XmlElement(name = "Horazone")
    @XmlSchemaType(name = "unsignedByte")
    protected Short horazone;
    @XmlElement(name = "ImVerbautenOrt")
    protected Boolean imVerbautenOrt;
    @XmlElement(name = "AusserhalbDesOrtsgebiets")
    protected Boolean ausserhalbDesOrtsgebiets;
    @XmlElement(name = "ErreichbarkeitFuerLoeschfahrzeuge")
    protected Boolean erreichbarkeitFuerLoeschfahrzeuge;
    @XmlElement(name = "Tarifzone")
    protected String tarifzone;

    /**
     * Ruft den Wert der horazone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getHorazone() {
        return horazone;
    }

    /**
     * Legt den Wert der horazone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setHorazone(Short value) {
        this.horazone = value;
    }

    /**
     * Ruft den Wert der imVerbautenOrt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isImVerbautenOrt() {
        return imVerbautenOrt;
    }

    /**
     * Legt den Wert der imVerbautenOrt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setImVerbautenOrt(Boolean value) {
        this.imVerbautenOrt = value;
    }

    /**
     * Ruft den Wert der ausserhalbDesOrtsgebiets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAusserhalbDesOrtsgebiets() {
        return ausserhalbDesOrtsgebiets;
    }

    /**
     * Legt den Wert der ausserhalbDesOrtsgebiets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAusserhalbDesOrtsgebiets(Boolean value) {
        this.ausserhalbDesOrtsgebiets = value;
    }

    /**
     * Ruft den Wert der erreichbarkeitFuerLoeschfahrzeuge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isErreichbarkeitFuerLoeschfahrzeuge() {
        return erreichbarkeitFuerLoeschfahrzeuge;
    }

    /**
     * Legt den Wert der erreichbarkeitFuerLoeschfahrzeuge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setErreichbarkeitFuerLoeschfahrzeuge(Boolean value) {
        this.erreichbarkeitFuerLoeschfahrzeuge = value;
    }

    /**
     * Ruft den Wert der tarifzone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarifzone() {
        return tarifzone;
    }

    /**
     * Legt den Wert der tarifzone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarifzone(String value) {
        this.tarifzone = value;
    }

}
