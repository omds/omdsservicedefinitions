
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;


/**
 * Dokumentenrefernz im Postservice
 * 
 * <p>Java-Klasse für DokumentenReferenzPostservice_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DokumentenReferenzPostservice_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}DokumentenReferenz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Kontrollwert"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;base64Binary"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Zuordnung" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentenZuordnung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DokumentenReferenzPostservice_Type", propOrder = {
    "kontrollwert",
    "zuordnung"
})
public class DokumentenReferenzPostserviceType
    extends DokumentenReferenzType
{

    @XmlElement(name = "Kontrollwert", required = true)
    protected DokumentenReferenzPostserviceType.Kontrollwert kontrollwert;
    @XmlElement(name = "Zuordnung")
    protected List<DokumentenZuordnungType> zuordnung;

    /**
     * Ruft den Wert der kontrollwert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DokumentenReferenzPostserviceType.Kontrollwert }
     *     
     */
    public DokumentenReferenzPostserviceType.Kontrollwert getKontrollwert() {
        return kontrollwert;
    }

    /**
     * Legt den Wert der kontrollwert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DokumentenReferenzPostserviceType.Kontrollwert }
     *     
     */
    public void setKontrollwert(DokumentenReferenzPostserviceType.Kontrollwert value) {
        this.kontrollwert = value;
    }

    /**
     * Gets the value of the zuordnung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zuordnung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZuordnung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentenZuordnungType }
     * 
     * 
     */
    public List<DokumentenZuordnungType> getZuordnung() {
        if (zuordnung == null) {
            zuordnung = new ArrayList<DokumentenZuordnungType>();
        }
        return this.zuordnung;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;base64Binary"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Kontrollwert {

        @XmlValue
        protected byte[] value;

        /**
         * Ruft den Wert der value-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getValue() {
            return value;
        }

        /**
         * Legt den Wert der value-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setValue(byte[] value) {
            this.value = value;
        }

    }

}
