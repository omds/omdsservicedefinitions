
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CalculateKfzResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Response, der das Ergebnis der Berechnung enthält bzw. Fehlermeldungen
 * 
 * <p>Java-Klasse für CalculateResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}BOAProcessResponse_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateResponse_Type")
@XmlSeeAlso({
    CalculateKfzResponseType.class,
    CalculateResponseGenType.class
})
public abstract class CalculateResponseType
    extends BOAProcessResponseType
{


}
