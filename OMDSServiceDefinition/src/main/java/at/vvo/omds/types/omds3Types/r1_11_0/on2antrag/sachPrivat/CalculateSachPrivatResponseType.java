
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CalculateResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Responseobjekts für eine Berechnung Sach-Privat
 * 
 * <p>Java-Klasse für CalculateSachPrivatResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateSachPrivatResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}BerechnungSachPrivat_Type"/&gt;
 *         &lt;element name="ResponseUpselling" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}UpsellingSachPrivatResponse_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateSachPrivatResponse_Type", propOrder = {
    "berechnungsantwort",
    "responseUpselling"
})
public class CalculateSachPrivatResponseType
    extends CalculateResponseGenType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected BerechnungSachPrivatType berechnungsantwort;
    @XmlElement(name = "ResponseUpselling")
    protected UpsellingSachPrivatResponseType responseUpselling;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BerechnungSachPrivatType }
     *     
     */
    public BerechnungSachPrivatType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BerechnungSachPrivatType }
     *     
     */
    public void setBerechnungsantwort(BerechnungSachPrivatType value) {
        this.berechnungsantwort = value;
    }

    /**
     * Ruft den Wert der responseUpselling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UpsellingSachPrivatResponseType }
     *     
     */
    public UpsellingSachPrivatResponseType getResponseUpselling() {
        return responseUpselling;
    }

    /**
     * Legt den Wert der responseUpselling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsellingSachPrivatResponseType }
     *     
     */
    public void setResponseUpselling(UpsellingSachPrivatResponseType value) {
        this.responseUpselling = value;
    }

}
