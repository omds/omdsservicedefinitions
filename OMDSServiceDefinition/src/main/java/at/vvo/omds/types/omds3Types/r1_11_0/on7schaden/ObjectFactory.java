
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import javax.xml.namespace.QName;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_11_0.on7schaden package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "CheckClaimRequest");
    private final static QName _CheckClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "CheckClaimResponse");
    private final static QName _SubmitClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SubmitClaimRequest");
    private final static QName _SubmitClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SubmitClaimResponse");
    private final static QName _GetClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GetClaimRequest");
    private final static QName _GetClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GetClaimResponse");
    private final static QName _SearchClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SearchClaimRequest");
    private final static QName _SearchClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SearchClaimResponse");
    private final static QName _GeschaeftsfallSchadenereignis_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GeschaeftsfallSchadenereignis");
    private final static QName _GeschaeftsfallSchadenanlage_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GeschaeftsfallSchadenanlage");
    private final static QName _Schadenzuordnung_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "Schadenzuordnung");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_11_0.on7schaden
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckCoverageResponse }
     * 
     */
    public CheckCoverageResponse createCheckCoverageResponse() {
        return new CheckCoverageResponse();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenKrankenType }
     * 
     */
    public SpartendetailSchadenKrankenType createSpartendetailSchadenKrankenType() {
        return new SpartendetailSchadenKrankenType();
    }

    /**
     * Create an instance of {@link SchadenType }
     * 
     */
    public SchadenType createSchadenType() {
        return new SchadenType();
    }

    /**
     * Create an instance of {@link CheckClaimRequestType }
     * 
     */
    public CheckClaimRequestType createCheckClaimRequestType() {
        return new CheckClaimRequestType();
    }

    /**
     * Create an instance of {@link CheckClaimResponseType }
     * 
     */
    public CheckClaimResponseType createCheckClaimResponseType() {
        return new CheckClaimResponseType();
    }

    /**
     * Create an instance of {@link SubmitClaimRequestType }
     * 
     */
    public SubmitClaimRequestType createSubmitClaimRequestType() {
        return new SubmitClaimRequestType();
    }

    /**
     * Create an instance of {@link SubmitClaimResponseType }
     * 
     */
    public SubmitClaimResponseType createSubmitClaimResponseType() {
        return new SubmitClaimResponseType();
    }

    /**
     * Create an instance of {@link GetClaimRequestType }
     * 
     */
    public GetClaimRequestType createGetClaimRequestType() {
        return new GetClaimRequestType();
    }

    /**
     * Create an instance of {@link GetClaimResponseType }
     * 
     */
    public GetClaimResponseType createGetClaimResponseType() {
        return new GetClaimResponseType();
    }

    /**
     * Create an instance of {@link SearchClaimRequestType }
     * 
     */
    public SearchClaimRequestType createSearchClaimRequestType() {
        return new SearchClaimRequestType();
    }

    /**
     * Create an instance of {@link SearchClaimResponseType }
     * 
     */
    public SearchClaimResponseType createSearchClaimResponseType() {
        return new SearchClaimResponseType();
    }

    /**
     * Create an instance of {@link SchadenzuordnungType }
     * 
     */
    public SchadenzuordnungType createSchadenzuordnungType() {
        return new SchadenzuordnungType();
    }

    /**
     * Create an instance of {@link CheckCoverageRequest }
     * 
     */
    public CheckCoverageRequest createCheckCoverageRequest() {
        return new CheckCoverageRequest();
    }

    /**
     * Create an instance of {@link CheckCoverageResponse.Auskuenfte }
     * 
     */
    public CheckCoverageResponse.Auskuenfte createCheckCoverageResponseAuskuenfte() {
        return new CheckCoverageResponse.Auskuenfte();
    }

    /**
     * Create an instance of {@link SubmitReceiptRequest }
     * 
     */
    public SubmitReceiptRequest createSubmitReceiptRequest() {
        return new SubmitReceiptRequest();
    }

    /**
     * Create an instance of {@link SubmitReceiptResponse }
     * 
     */
    public SubmitReceiptResponse createSubmitReceiptResponse() {
        return new SubmitReceiptResponse();
    }

    /**
     * Create an instance of {@link AddInformationToClaimRequest }
     * 
     */
    public AddInformationToClaimRequest createAddInformationToClaimRequest() {
        return new AddInformationToClaimRequest();
    }

    /**
     * Create an instance of {@link ErgaenzungSchadenereignisType }
     * 
     */
    public ErgaenzungSchadenereignisType createErgaenzungSchadenereignisType() {
        return new ErgaenzungSchadenereignisType();
    }

    /**
     * Create an instance of {@link AddInformationToClaimResponse }
     * 
     */
    public AddInformationToClaimResponse createAddInformationToClaimResponse() {
        return new AddInformationToClaimResponse();
    }

    /**
     * Create an instance of {@link SearchClaimResponseResultType }
     * 
     */
    public SearchClaimResponseResultType createSearchClaimResponseResultType() {
        return new SearchClaimResponseResultType();
    }

    /**
     * Create an instance of {@link SchadenereignisType }
     * 
     */
    public SchadenereignisType createSchadenereignisType() {
        return new SchadenereignisType();
    }

    /**
     * Create an instance of {@link SchadenereignisLightType }
     * 
     */
    public SchadenereignisLightType createSchadenereignisLightType() {
        return new SchadenereignisLightType();
    }

    /**
     * Create an instance of {@link SchadenLightType }
     * 
     */
    public SchadenLightType createSchadenLightType() {
        return new SchadenLightType();
    }

    /**
     * Create an instance of {@link BasisSchadenType }
     * 
     */
    public BasisSchadenType createBasisSchadenType() {
        return new BasisSchadenType();
    }

    /**
     * Create an instance of {@link BeteiligtePersonType }
     * 
     */
    public BeteiligtePersonType createBeteiligtePersonType() {
        return new BeteiligtePersonType();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenKfzType }
     * 
     */
    public SpartendetailSchadenKfzType createSpartendetailSchadenKfzType() {
        return new SpartendetailSchadenKfzType();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenUnfallType }
     * 
     */
    public SpartendetailSchadenUnfallType createSpartendetailSchadenUnfallType() {
        return new SpartendetailSchadenUnfallType();
    }

    /**
     * Create an instance of {@link OrtType }
     * 
     */
    public OrtType createOrtType() {
        return new OrtType();
    }

    /**
     * Create an instance of {@link GeokoordinatenType }
     * 
     */
    public GeokoordinatenType createGeokoordinatenType() {
        return new GeokoordinatenType();
    }

    /**
     * Create an instance of {@link GeschaedigtesInteresseType }
     * 
     */
    public GeschaedigtesInteresseType createGeschaedigtesInteresseType() {
        return new GeschaedigtesInteresseType();
    }

    /**
     * Create an instance of {@link GeschaedigtesObjektKfzType }
     * 
     */
    public GeschaedigtesObjektKfzType createGeschaedigtesObjektKfzType() {
        return new GeschaedigtesObjektKfzType();
    }

    /**
     * Create an instance of {@link GeschaedigtesObjektImmobilieType }
     * 
     */
    public GeschaedigtesObjektImmobilieType createGeschaedigtesObjektImmobilieType() {
        return new GeschaedigtesObjektImmobilieType();
    }

    /**
     * Create an instance of {@link SchadenmelderVermittlerType }
     * 
     */
    public SchadenmelderVermittlerType createSchadenmelderVermittlerType() {
        return new SchadenmelderVermittlerType();
    }

    /**
     * Create an instance of {@link ReferenzAufBeteiligtePersonSchadenType }
     * 
     */
    public ReferenzAufBeteiligtePersonSchadenType createReferenzAufBeteiligtePersonSchadenType() {
        return new ReferenzAufBeteiligtePersonSchadenType();
    }

    /**
     * Create an instance of {@link SachbearbVUType }
     * 
     */
    public SachbearbVUType createSachbearbVUType() {
        return new SachbearbVUType();
    }

    /**
     * Create an instance of {@link NatPersonType }
     * 
     */
    public NatPersonType createNatPersonType() {
        return new NatPersonType();
    }

    /**
     * Create an instance of {@link ErgaenzungSchadenType }
     * 
     */
    public ErgaenzungSchadenType createErgaenzungSchadenType() {
        return new ErgaenzungSchadenType();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenKrankenType.Behandlungen }
     * 
     */
    public SpartendetailSchadenKrankenType.Behandlungen createSpartendetailSchadenKrankenTypeBehandlungen() {
        return new SpartendetailSchadenKrankenType.Behandlungen();
    }

    /**
     * Create an instance of {@link SchadenType.BeteiligtePersonen }
     * 
     */
    public SchadenType.BeteiligtePersonen createSchadenTypeBeteiligtePersonen() {
        return new SchadenType.BeteiligtePersonen();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckClaimRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CheckClaimRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "CheckClaimRequest")
    public JAXBElement<CheckClaimRequestType> createCheckClaimRequest(CheckClaimRequestType value) {
        return new JAXBElement<CheckClaimRequestType>(_CheckClaimRequest_QNAME, CheckClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckClaimResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CheckClaimResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "CheckClaimResponse")
    public JAXBElement<CheckClaimResponseType> createCheckClaimResponse(CheckClaimResponseType value) {
        return new JAXBElement<CheckClaimResponseType>(_CheckClaimResponse_QNAME, CheckClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitClaimRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitClaimRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SubmitClaimRequest")
    public JAXBElement<SubmitClaimRequestType> createSubmitClaimRequest(SubmitClaimRequestType value) {
        return new JAXBElement<SubmitClaimRequestType>(_SubmitClaimRequest_QNAME, SubmitClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitClaimResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitClaimResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SubmitClaimResponse")
    public JAXBElement<SubmitClaimResponseType> createSubmitClaimResponse(SubmitClaimResponseType value) {
        return new JAXBElement<SubmitClaimResponseType>(_SubmitClaimResponse_QNAME, SubmitClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetClaimRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GetClaimRequest")
    public JAXBElement<GetClaimRequestType> createGetClaimRequest(GetClaimRequestType value) {
        return new JAXBElement<GetClaimRequestType>(_GetClaimRequest_QNAME, GetClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetClaimResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GetClaimResponse")
    public JAXBElement<GetClaimResponseType> createGetClaimResponse(GetClaimResponseType value) {
        return new JAXBElement<GetClaimResponseType>(_GetClaimResponse_QNAME, GetClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchClaimRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchClaimRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SearchClaimRequest")
    public JAXBElement<SearchClaimRequestType> createSearchClaimRequest(SearchClaimRequestType value) {
        return new JAXBElement<SearchClaimRequestType>(_SearchClaimRequest_QNAME, SearchClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchClaimResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchClaimResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SearchClaimResponse")
    public JAXBElement<SearchClaimResponseType> createSearchClaimResponse(SearchClaimResponseType value) {
        return new JAXBElement<SearchClaimResponseType>(_SearchClaimResponse_QNAME, SearchClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GeschaeftsfallSchadenereignis")
    public JAXBElement<ObjektIdType> createGeschaeftsfallSchadenereignis(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_GeschaeftsfallSchadenereignis_QNAME, ObjektIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GeschaeftsfallSchadenanlage")
    public JAXBElement<ObjektIdType> createGeschaeftsfallSchadenanlage(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_GeschaeftsfallSchadenanlage_QNAME, ObjektIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SchadenzuordnungType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SchadenzuordnungType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "Schadenzuordnung")
    public JAXBElement<SchadenzuordnungType> createSchadenzuordnung(SchadenzuordnungType value) {
        return new JAXBElement<SchadenzuordnungType>(_Schadenzuordnung_QNAME, SchadenzuordnungType.class, null, value);
    }

}
