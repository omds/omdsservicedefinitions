
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Bereitstellung eines Dokuments durch die VU zum Unterschreiben bzw. Ausfüllen durch den Kunden
 * 
 * <p>Java-Klasse für ProzessDokToReturn_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProzessDokToReturn_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokumentBasis_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DokAnforderungsId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *         &lt;element name="Autorisierungen" type="{urn:omds3CommonServiceTypes-1-1-0}AutorisierungsAnforderung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DocUploadRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="ZulaessigeMimetypes" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DokData" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentData_Type"/&gt;
 *         &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProzessDokToReturn_Type", propOrder = {
    "dokAnforderungsId",
    "autorisierungen",
    "docUploadRequired",
    "zulaessigeMimetypes",
    "dokData",
    "meldungen"
})
public class ProzessDokToReturnType
    extends ProzessDokumentBasisType
{

    @XmlElement(name = "DokAnforderungsId", required = true)
    protected ObjektIdType dokAnforderungsId;
    @XmlElement(name = "Autorisierungen")
    protected List<AutorisierungsAnforderungType> autorisierungen;
    @XmlElement(name = "DocUploadRequired")
    protected boolean docUploadRequired;
    @XmlElement(name = "ZulaessigeMimetypes")
    protected List<String> zulaessigeMimetypes;
    @XmlElement(name = "DokData", required = true)
    protected DokumentDataType dokData;
    @XmlElement(name = "Meldungen")
    protected List<ServiceFault> meldungen;

    /**
     * Ruft den Wert der dokAnforderungsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getDokAnforderungsId() {
        return dokAnforderungsId;
    }

    /**
     * Legt den Wert der dokAnforderungsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setDokAnforderungsId(ObjektIdType value) {
        this.dokAnforderungsId = value;
    }

    /**
     * Gets the value of the autorisierungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the autorisierungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutorisierungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutorisierungsAnforderungType }
     * 
     * 
     */
    public List<AutorisierungsAnforderungType> getAutorisierungen() {
        if (autorisierungen == null) {
            autorisierungen = new ArrayList<AutorisierungsAnforderungType>();
        }
        return this.autorisierungen;
    }

    /**
     * Ruft den Wert der docUploadRequired-Eigenschaft ab.
     * 
     */
    public boolean isDocUploadRequired() {
        return docUploadRequired;
    }

    /**
     * Legt den Wert der docUploadRequired-Eigenschaft fest.
     * 
     */
    public void setDocUploadRequired(boolean value) {
        this.docUploadRequired = value;
    }

    /**
     * Gets the value of the zulaessigeMimetypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zulaessigeMimetypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZulaessigeMimetypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getZulaessigeMimetypes() {
        if (zulaessigeMimetypes == null) {
            zulaessigeMimetypes = new ArrayList<String>();
        }
        return this.zulaessigeMimetypes;
    }

    /**
     * Ruft den Wert der dokData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DokumentDataType }
     *     
     */
    public DokumentDataType getDokData() {
        return dokData;
    }

    /**
     * Legt den Wert der dokData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DokumentDataType }
     *     
     */
    public void setDokData(DokumentDataType value) {
        this.dokData = value;
    }

    /**
     * Gets the value of the meldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the meldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getMeldungen() {
        if (meldungen == null) {
            meldungen = new ArrayList<ServiceFault>();
        }
        return this.meldungen;
    }

}
