
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateOfferRequestGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Requestobjekts für eine Erstellung eines Unfall-Offerts
 * 
 * <p>Java-Klasse für CreateOfferUnfallRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferUnfallRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferRequestGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}SpezOffertUnfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferUnfallRequest_Type", propOrder = {
    "offertanfrage"
})
public class CreateOfferUnfallRequestType
    extends CreateOfferRequestGenType
{

    @XmlElement(name = "Offertanfrage", required = true)
    protected SpezOffertUnfallType offertanfrage;

    /**
     * Ruft den Wert der offertanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezOffertUnfallType }
     *     
     */
    public SpezOffertUnfallType getOffertanfrage() {
        return offertanfrage;
    }

    /**
     * Legt den Wert der offertanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezOffertUnfallType }
     *     
     */
    public void setOffertanfrage(SpezOffertUnfallType value) {
        this.offertanfrage = value;
    }

}
