
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Sicherstellung vom Typ Verpfändung. Der VN und der Pfandgläubiger können Änderungen
 * 				im Vertrag nur gemeinsam durchführen, VN bleibt aber Träger der Rechte un Pflichten aus dem Versicherungsvertrag.
 * 
 * <p>Java-Klasse für Verpfaendung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Verpfaendung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Sicherstellung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Pfandglaeubiger" type="{urn:omds3CommonServiceTypes-1-1-0}Pfandglaeubiger_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Verpfaendung_Type", propOrder = {
    "pfandglaeubiger"
})
public class VerpfaendungType
    extends SicherstellungType
{

    @XmlElement(name = "Pfandglaeubiger", required = true)
    protected List<PfandglaeubigerType> pfandglaeubiger;

    /**
     * Gets the value of the pfandglaeubiger property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the pfandglaeubiger property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPfandglaeubiger().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PfandglaeubigerType }
     * 
     * 
     */
    public List<PfandglaeubigerType> getPfandglaeubiger() {
        if (pfandglaeubiger == null) {
            pfandglaeubiger = new ArrayList<PfandglaeubigerType>();
        }
        return this.pfandglaeubiger;
    }

}
