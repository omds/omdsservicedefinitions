
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.ElementarproduktKfzType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.VerkehrsrechtsschutzKfzType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Basistyp für ein Elementarprodukt
 * 
 * <p>Java-Klasse für Elementarprodukt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Elementarprodukt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VtgBeg" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="RefSicherstellungLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheElementarproduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheElementarproduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Elementarprodukt_Type", propOrder = {
    "vtgBeg",
    "refSicherstellungLfnr",
    "zusaetzlicheElementarproduktdaten"
})
@XmlSeeAlso({
    ElementarproduktKfzType.class,
    VerkehrsrechtsschutzKfzType.class,
    ElementarproduktGenerischType.class
})
public abstract class ElementarproduktType
    extends ProduktbausteinType
{

    @XmlElement(name = "VtgBeg")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar vtgBeg;
    @XmlElement(name = "RefSicherstellungLfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer refSicherstellungLfnr;
    @XmlElement(name = "ZusaetzlicheElementarproduktdaten")
    protected List<ZusaetzlicheElementarproduktdatenType> zusaetzlicheElementarproduktdaten;

    /**
     * Ruft den Wert der vtgBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgBeg() {
        return vtgBeg;
    }

    /**
     * Legt den Wert der vtgBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgBeg(XMLGregorianCalendar value) {
        this.vtgBeg = value;
    }

    /**
     * Ruft den Wert der refSicherstellungLfnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefSicherstellungLfnr() {
        return refSicherstellungLfnr;
    }

    /**
     * Legt den Wert der refSicherstellungLfnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefSicherstellungLfnr(Integer value) {
        this.refSicherstellungLfnr = value;
    }

    /**
     * Gets the value of the zusaetzlicheElementarproduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheElementarproduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheElementarproduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheElementarproduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheElementarproduktdatenType> getZusaetzlicheElementarproduktdaten() {
        if (zusaetzlicheElementarproduktdaten == null) {
            zusaetzlicheElementarproduktdaten = new ArrayList<ZusaetzlicheElementarproduktdatenType>();
        }
        return this.zusaetzlicheElementarproduktdaten;
    }

}
