
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokHandoutType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VBNr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="VBDokument" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokHandout_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vbNr",
    "vbDokument"
})
@XmlRootElement(name = "CreateVBResponse")
public class CreateVBResponse
    extends CommonResponseType
{

    @XmlElement(name = "VBNr", required = true)
    protected String vbNr;
    @XmlElement(name = "VBDokument", required = true)
    protected ProzessDokHandoutType vbDokument;

    /**
     * Ruft den Wert der vbNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVBNr() {
        return vbNr;
    }

    /**
     * Legt den Wert der vbNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVBNr(String value) {
        this.vbNr = value;
    }

    /**
     * Ruft den Wert der vbDokument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProzessDokHandoutType }
     *     
     */
    public ProzessDokHandoutType getVBDokument() {
        return vbDokument;
    }

    /**
     * Legt den Wert der vbDokument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProzessDokHandoutType }
     *     
     */
    public void setVBDokument(ProzessDokHandoutType value) {
        this.vbDokument = value;
    }

}
