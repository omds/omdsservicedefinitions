
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ErgaenzungSchadenereignis" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ErgaenzungSchadenereignis_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ergaenzungSchadenereignis"
})
@XmlRootElement(name = "AddInformationToClaimResponse")
public class AddInformationToClaimResponse
    extends CommonResponseType
{

    @XmlElement(name = "ErgaenzungSchadenereignis", required = true)
    protected ErgaenzungSchadenereignisType ergaenzungSchadenereignis;

    /**
     * Ruft den Wert der ergaenzungSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ErgaenzungSchadenereignisType }
     *     
     */
    public ErgaenzungSchadenereignisType getErgaenzungSchadenereignis() {
        return ergaenzungSchadenereignis;
    }

    /**
     * Legt den Wert der ergaenzungSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ErgaenzungSchadenereignisType }
     *     
     */
    public void setErgaenzungSchadenereignis(ErgaenzungSchadenereignisType value) {
        this.ergaenzungSchadenereignis = value;
    }

}
