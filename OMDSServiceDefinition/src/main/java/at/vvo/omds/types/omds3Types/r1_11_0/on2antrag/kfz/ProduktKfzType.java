
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Kfz-Produkt, welches einer Vertragssparte entspricht
 * 
 * <p>Java-Klasse für ProduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Haftpflicht" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}HaftpflichtKfz_Type" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Teilkasko" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}TeilkaskoKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="Vollkasko" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}VollkaskoKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Kasko" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}KaskoKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Insassenunfall" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}InsassenUnfallKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Assistance" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}AssistanceKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktKfz_Type", propOrder = {
    "haftpflicht",
    "teilkasko",
    "vollkasko",
    "kasko",
    "insassenunfall",
    "assistance"
})
public class ProduktKfzType
    extends ProduktType
{

    @XmlElement(name = "Haftpflicht")
    protected HaftpflichtKfzType haftpflicht;
    @XmlElement(name = "Teilkasko")
    protected List<TeilkaskoKfzType> teilkasko;
    @XmlElement(name = "Vollkasko")
    protected List<VollkaskoKfzType> vollkasko;
    @XmlElement(name = "Kasko")
    protected List<KaskoKfzType> kasko;
    @XmlElement(name = "Insassenunfall")
    protected List<InsassenUnfallKfzType> insassenunfall;
    @XmlElement(name = "Assistance")
    protected List<AssistanceKfzType> assistance;

    /**
     * Ruft den Wert der haftpflicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HaftpflichtKfzType }
     *     
     */
    public HaftpflichtKfzType getHaftpflicht() {
        return haftpflicht;
    }

    /**
     * Legt den Wert der haftpflicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HaftpflichtKfzType }
     *     
     */
    public void setHaftpflicht(HaftpflichtKfzType value) {
        this.haftpflicht = value;
    }

    /**
     * Gets the value of the teilkasko property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the teilkasko property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTeilkasko().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TeilkaskoKfzType }
     * 
     * 
     */
    public List<TeilkaskoKfzType> getTeilkasko() {
        if (teilkasko == null) {
            teilkasko = new ArrayList<TeilkaskoKfzType>();
        }
        return this.teilkasko;
    }

    /**
     * Gets the value of the vollkasko property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vollkasko property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVollkasko().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VollkaskoKfzType }
     * 
     * 
     */
    public List<VollkaskoKfzType> getVollkasko() {
        if (vollkasko == null) {
            vollkasko = new ArrayList<VollkaskoKfzType>();
        }
        return this.vollkasko;
    }

    /**
     * Gets the value of the kasko property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the kasko property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKasko().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KaskoKfzType }
     * 
     * 
     */
    public List<KaskoKfzType> getKasko() {
        if (kasko == null) {
            kasko = new ArrayList<KaskoKfzType>();
        }
        return this.kasko;
    }

    /**
     * Gets the value of the insassenunfall property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the insassenunfall property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsassenunfall().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InsassenUnfallKfzType }
     * 
     * 
     */
    public List<InsassenUnfallKfzType> getInsassenunfall() {
        if (insassenunfall == null) {
            insassenunfall = new ArrayList<InsassenUnfallKfzType>();
        }
        return this.insassenunfall;
    }

    /**
     * Gets the value of the assistance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the assistance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssistance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssistanceKfzType }
     * 
     * 
     */
    public List<AssistanceKfzType> getAssistance() {
        if (assistance == null) {
            assistance = new ArrayList<AssistanceKfzType>();
        }
        return this.assistance;
    }

}
