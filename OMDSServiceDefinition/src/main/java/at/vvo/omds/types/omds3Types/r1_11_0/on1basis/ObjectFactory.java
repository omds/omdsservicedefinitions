
package at.vvo.omds.types.omds3Types.r1_11_0.on1basis;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_11_0.on1basis package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddDocToBusinessCaseRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "AddDocToBusinessCaseRequest");
    private final static QName _AddDocToBusinessCaseResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "AddDocToBusinessCaseResponse");
    private final static QName _GetNumberOfDocumentsRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetNumberOfDocumentsRequest");
    private final static QName _GetNumberOfDocumentsResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetNumberOfDocumentsResponse");
    private final static QName _GetDocumentsOfObjectRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetDocumentsOfObjectRequest");
    private final static QName _GetDocumentsOfObjectResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetDocumentsOfObjectResponse");
    private final static QName _GetDocumentsOfPeriodRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetDocumentsOfPeriodRequest");
    private final static QName _GetDocumentsOfPeriodResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetDocumentsOfPeriodResponse");
    private final static QName _GetStateChangesRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetStateChangesRequest");
    private final static QName _GetStateChangesResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", "GetStateChangesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_11_0.on1basis
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GeschaeftsfallEreignisType }
     * 
     */
    public GeschaeftsfallEreignisType createGeschaeftsfallEreignisType() {
        return new GeschaeftsfallEreignisType();
    }

    /**
     * Create an instance of {@link GeschaeftsfallEreignisType.Objekt }
     * 
     */
    public GeschaeftsfallEreignisType.Objekt createGeschaeftsfallEreignisTypeObjekt() {
        return new GeschaeftsfallEreignisType.Objekt();
    }

    /**
     * Create an instance of {@link AddDocToBusinessCaseRequestType }
     * 
     */
    public AddDocToBusinessCaseRequestType createAddDocToBusinessCaseRequestType() {
        return new AddDocToBusinessCaseRequestType();
    }

    /**
     * Create an instance of {@link AddDocToBusinessCaseResponseType }
     * 
     */
    public AddDocToBusinessCaseResponseType createAddDocToBusinessCaseResponseType() {
        return new AddDocToBusinessCaseResponseType();
    }

    /**
     * Create an instance of {@link GetNumberOfDocumentsRequestType }
     * 
     */
    public GetNumberOfDocumentsRequestType createGetNumberOfDocumentsRequestType() {
        return new GetNumberOfDocumentsRequestType();
    }

    /**
     * Create an instance of {@link GetNumberOfDocumentsResponseType }
     * 
     */
    public GetNumberOfDocumentsResponseType createGetNumberOfDocumentsResponseType() {
        return new GetNumberOfDocumentsResponseType();
    }

    /**
     * Create an instance of {@link GetDocumentsOfObjectRequestType }
     * 
     */
    public GetDocumentsOfObjectRequestType createGetDocumentsOfObjectRequestType() {
        return new GetDocumentsOfObjectRequestType();
    }

    /**
     * Create an instance of {@link GetDocumentsOfObjectResponseType }
     * 
     */
    public GetDocumentsOfObjectResponseType createGetDocumentsOfObjectResponseType() {
        return new GetDocumentsOfObjectResponseType();
    }

    /**
     * Create an instance of {@link GetDocumentsOfPeriodRequestType }
     * 
     */
    public GetDocumentsOfPeriodRequestType createGetDocumentsOfPeriodRequestType() {
        return new GetDocumentsOfPeriodRequestType();
    }

    /**
     * Create an instance of {@link GetDocumentsOfPeriodResponseType }
     * 
     */
    public GetDocumentsOfPeriodResponseType createGetDocumentsOfPeriodResponseType() {
        return new GetDocumentsOfPeriodResponseType();
    }

    /**
     * Create an instance of {@link AcknowledgeDocumentsRequest }
     * 
     */
    public AcknowledgeDocumentsRequest createAcknowledgeDocumentsRequest() {
        return new AcknowledgeDocumentsRequest();
    }

    /**
     * Create an instance of {@link AcknowledgeDocumentsResponse }
     * 
     */
    public AcknowledgeDocumentsResponse createAcknowledgeDocumentsResponse() {
        return new AcknowledgeDocumentsResponse();
    }

    /**
     * Create an instance of {@link GetStateChangesRequestType }
     * 
     */
    public GetStateChangesRequestType createGetStateChangesRequestType() {
        return new GetStateChangesRequestType();
    }

    /**
     * Create an instance of {@link GetStateChangesResponseType }
     * 
     */
    public GetStateChangesResponseType createGetStateChangesResponseType() {
        return new GetStateChangesResponseType();
    }

    /**
     * Create an instance of {@link DocumentInfosResponseResultType }
     * 
     */
    public DocumentInfosResponseResultType createDocumentInfosResponseResultType() {
        return new DocumentInfosResponseResultType();
    }

    /**
     * Create an instance of {@link GeschaeftsfallEreignisAntragType }
     * 
     */
    public GeschaeftsfallEreignisAntragType createGeschaeftsfallEreignisAntragType() {
        return new GeschaeftsfallEreignisAntragType();
    }

    /**
     * Create an instance of {@link GeschaeftsfallEreignisSchadenType }
     * 
     */
    public GeschaeftsfallEreignisSchadenType createGeschaeftsfallEreignisSchadenType() {
        return new GeschaeftsfallEreignisSchadenType();
    }

    /**
     * Create an instance of {@link StateChangeEventType }
     * 
     */
    public StateChangeEventType createStateChangeEventType() {
        return new StateChangeEventType();
    }

    /**
     * Create an instance of {@link StatusAntragsGeschaeftsfall }
     * 
     */
    public StatusAntragsGeschaeftsfall createStatusAntragsGeschaeftsfall() {
        return new StatusAntragsGeschaeftsfall();
    }

    /**
     * Create an instance of {@link GeschaeftsfallEreignisType.Dokument }
     * 
     */
    public GeschaeftsfallEreignisType.Dokument createGeschaeftsfallEreignisTypeDokument() {
        return new GeschaeftsfallEreignisType.Dokument();
    }

    /**
     * Create an instance of {@link GeschaeftsfallEreignisType.Objekt.Dokument }
     * 
     */
    public GeschaeftsfallEreignisType.Objekt.Dokument createGeschaeftsfallEreignisTypeObjektDokument() {
        return new GeschaeftsfallEreignisType.Objekt.Dokument();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddDocToBusinessCaseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddDocToBusinessCaseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "AddDocToBusinessCaseRequest")
    public JAXBElement<AddDocToBusinessCaseRequestType> createAddDocToBusinessCaseRequest(AddDocToBusinessCaseRequestType value) {
        return new JAXBElement<AddDocToBusinessCaseRequestType>(_AddDocToBusinessCaseRequest_QNAME, AddDocToBusinessCaseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddDocToBusinessCaseResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddDocToBusinessCaseResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "AddDocToBusinessCaseResponse")
    public JAXBElement<AddDocToBusinessCaseResponseType> createAddDocToBusinessCaseResponse(AddDocToBusinessCaseResponseType value) {
        return new JAXBElement<AddDocToBusinessCaseResponseType>(_AddDocToBusinessCaseResponse_QNAME, AddDocToBusinessCaseResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNumberOfDocumentsRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetNumberOfDocumentsRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetNumberOfDocumentsRequest")
    public JAXBElement<GetNumberOfDocumentsRequestType> createGetNumberOfDocumentsRequest(GetNumberOfDocumentsRequestType value) {
        return new JAXBElement<GetNumberOfDocumentsRequestType>(_GetNumberOfDocumentsRequest_QNAME, GetNumberOfDocumentsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNumberOfDocumentsResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetNumberOfDocumentsResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetNumberOfDocumentsResponse")
    public JAXBElement<GetNumberOfDocumentsResponseType> createGetNumberOfDocumentsResponse(GetNumberOfDocumentsResponseType value) {
        return new JAXBElement<GetNumberOfDocumentsResponseType>(_GetNumberOfDocumentsResponse_QNAME, GetNumberOfDocumentsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfObjectRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfObjectRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetDocumentsOfObjectRequest")
    public JAXBElement<GetDocumentsOfObjectRequestType> createGetDocumentsOfObjectRequest(GetDocumentsOfObjectRequestType value) {
        return new JAXBElement<GetDocumentsOfObjectRequestType>(_GetDocumentsOfObjectRequest_QNAME, GetDocumentsOfObjectRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfObjectResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfObjectResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetDocumentsOfObjectResponse")
    public JAXBElement<GetDocumentsOfObjectResponseType> createGetDocumentsOfObjectResponse(GetDocumentsOfObjectResponseType value) {
        return new JAXBElement<GetDocumentsOfObjectResponseType>(_GetDocumentsOfObjectResponse_QNAME, GetDocumentsOfObjectResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfPeriodRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfPeriodRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetDocumentsOfPeriodRequest")
    public JAXBElement<GetDocumentsOfPeriodRequestType> createGetDocumentsOfPeriodRequest(GetDocumentsOfPeriodRequestType value) {
        return new JAXBElement<GetDocumentsOfPeriodRequestType>(_GetDocumentsOfPeriodRequest_QNAME, GetDocumentsOfPeriodRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfPeriodResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetDocumentsOfPeriodResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetDocumentsOfPeriodResponse")
    public JAXBElement<GetDocumentsOfPeriodResponseType> createGetDocumentsOfPeriodResponse(GetDocumentsOfPeriodResponseType value) {
        return new JAXBElement<GetDocumentsOfPeriodResponseType>(_GetDocumentsOfPeriodResponse_QNAME, GetDocumentsOfPeriodResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStateChangesRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetStateChangesRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetStateChangesRequest")
    public JAXBElement<GetStateChangesRequestType> createGetStateChangesRequest(GetStateChangesRequestType value) {
        return new JAXBElement<GetStateChangesRequestType>(_GetStateChangesRequest_QNAME, GetStateChangesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStateChangesResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetStateChangesResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen", name = "GetStateChangesResponse")
    public JAXBElement<GetStateChangesResponseType> createGetStateChangesResponse(GetStateChangesResponseType value) {
        return new JAXBElement<GetStateChangesResponseType>(_GetStateChangesResponse_QNAME, GetStateChangesResponseType.class, null, value);
    }

}
