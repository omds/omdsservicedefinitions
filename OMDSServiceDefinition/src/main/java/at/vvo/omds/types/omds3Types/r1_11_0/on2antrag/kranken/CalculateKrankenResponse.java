
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CalculateResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Responseobjekts für eine Kranken-Berechnung
 * 
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-6-0.on2antrag.kranken}SpezBerechnungKranken_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "berechnungsantwort"
})
@XmlRootElement(name = "CalculateKrankenResponse")
public class CalculateKrankenResponse
    extends CalculateResponseGenType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected SpezBerechnungKrankenType berechnungsantwort;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungKrankenType }
     *     
     */
    public SpezBerechnungKrankenType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungKrankenType }
     *     
     */
    public void setBerechnungsantwort(SpezBerechnungKrankenType value) {
        this.berechnungsantwort = value;
    }

}
