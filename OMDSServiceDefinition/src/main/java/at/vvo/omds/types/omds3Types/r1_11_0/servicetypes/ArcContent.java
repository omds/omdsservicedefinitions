
package at.vvo.omds.types.omds3Types.r1_11_0.servicetypes;

import jakarta.activation.DataHandler;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlMimeType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Informationen zu einem Dokument und das Dokument base64encoded
 * 
 * <p>Java-Klasse für ArcContent complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ArcContent"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arcImageInfo" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" minOccurs="0"/&gt;
 *         &lt;element name="arcImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArcContent", propOrder = {
    "arcImageInfo",
    "arcImage"
})
public class ArcContent {

    protected ArcImageInfo arcImageInfo;
    @XmlElement(required = true)
    @XmlMimeType("application/octet-stream")
    protected DataHandler arcImage;

    /**
     * Ruft den Wert der arcImageInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ArcImageInfo }
     *     
     */
    public ArcImageInfo getArcImageInfo() {
        return arcImageInfo;
    }

    /**
     * Legt den Wert der arcImageInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ArcImageInfo }
     *     
     */
    public void setArcImageInfo(ArcImageInfo value) {
        this.arcImageInfo = value;
    }

    /**
     * Ruft den Wert der arcImage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getArcImage() {
        return arcImage;
    }

    /**
     * Legt den Wert der arcImage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setArcImage(DataHandler value) {
        this.arcImage = value;
    }

}
