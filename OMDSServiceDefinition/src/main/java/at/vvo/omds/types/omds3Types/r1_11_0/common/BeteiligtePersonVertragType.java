
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für die Meldung von Personen, die an einem Vertrag beteiligt sind
 * 
 * <p>Java-Klasse für BeteiligtePersonVertrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BeteiligtePersonVertrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Lfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Person"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BeteiligtePersonVertrag_Type", propOrder = {
    "lfnr",
    "person"
})
public class BeteiligtePersonVertragType {

    @XmlElement(name = "Lfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;
    @XmlElement(name = "Person", required = true)
    protected PersonType person;

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

    /**
     * Objekt zur Übermittlung von Personendaten ähnlich zu omds:PERSON, aber Personennr ist nicht Pflichtfeld. Kann Adressdaten enthalten.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setPerson(PersonType value) {
        this.person = value;
    }

}
