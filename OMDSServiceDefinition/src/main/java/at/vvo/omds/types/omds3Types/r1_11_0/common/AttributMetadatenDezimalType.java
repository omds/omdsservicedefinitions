
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ mit Metadaten für omds:decimal-Attribute
 * 
 * <p>Java-Klasse für AttributMetadatenDezimal_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttributMetadatenDezimal_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}AttributMetadaten_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Default" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Values" type="{urn:omds20}decimal" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Min" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Max" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributMetadatenDezimal_Type", propOrder = {
    "_default",
    "values",
    "min",
    "max"
})
public class AttributMetadatenDezimalType
    extends AttributMetadatenType
{

    @XmlElement(name = "Default")
    protected BigDecimal _default;
    @XmlElement(name = "Values")
    protected List<BigDecimal> values;
    @XmlElement(name = "Min")
    protected BigDecimal min;
    @XmlElement(name = "Max")
    protected BigDecimal max;

    /**
     * Ruft den Wert der default-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDefault() {
        return _default;
    }

    /**
     * Legt den Wert der default-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDefault(BigDecimal value) {
        this._default = value;
    }

    /**
     * Gets the value of the values property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the values property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BigDecimal }
     * 
     * 
     */
    public List<BigDecimal> getValues() {
        if (values == null) {
            values = new ArrayList<BigDecimal>();
        }
        return this.values;
    }

    /**
     * Ruft den Wert der min-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMin() {
        return min;
    }

    /**
     * Legt den Wert der min-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMin(BigDecimal value) {
        this.min = value;
    }

    /**
     * Ruft den Wert der max-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMax() {
        return max;
    }

    /**
     * Legt den Wert der max-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMax(BigDecimal value) {
        this.max = value;
    }

}
