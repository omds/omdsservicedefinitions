
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VersichertesInteresseMitAttributMetadatenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type für ein versichertes Objekt in der Sach-Privat-Versicherung
 * 
 * <p>Java-Klasse für VersichertesObjektSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersichertesObjektSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresseMitAttributMetadaten_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ObjektId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="RisikoAdresse" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}RisikoAdresse_Type"/&gt;
 *         &lt;element name="RisikoGebaeude" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}RisikoGebaeude_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RisikoHaushalt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}RisikoHaushalt_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersichertesObjektSachPrivat_Type", propOrder = {
    "objektId",
    "risikoAdresse",
    "risikoGebaeude",
    "risikoHaushalt"
})
public class VersichertesObjektSachPrivatType
    extends VersichertesInteresseMitAttributMetadatenType
{

    @XmlElement(name = "ObjektId")
    protected ObjektIdType objektId;
    @XmlElement(name = "RisikoAdresse", required = true)
    protected RisikoAdresseType risikoAdresse;
    @XmlElement(name = "RisikoGebaeude")
    protected List<RisikoGebaeudeType> risikoGebaeude;
    @XmlElement(name = "RisikoHaushalt")
    protected RisikoHaushaltType risikoHaushalt;

    /**
     * Ruft den Wert der objektId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der risikoAdresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RisikoAdresseType }
     *     
     */
    public RisikoAdresseType getRisikoAdresse() {
        return risikoAdresse;
    }

    /**
     * Legt den Wert der risikoAdresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RisikoAdresseType }
     *     
     */
    public void setRisikoAdresse(RisikoAdresseType value) {
        this.risikoAdresse = value;
    }

    /**
     * Gets the value of the risikoGebaeude property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the risikoGebaeude property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRisikoGebaeude().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RisikoGebaeudeType }
     * 
     * 
     */
    public List<RisikoGebaeudeType> getRisikoGebaeude() {
        if (risikoGebaeude == null) {
            risikoGebaeude = new ArrayList<RisikoGebaeudeType>();
        }
        return this.risikoGebaeude;
    }

    /**
     * Ruft den Wert der risikoHaushalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RisikoHaushaltType }
     *     
     */
    public RisikoHaushaltType getRisikoHaushalt() {
        return risikoHaushalt;
    }

    /**
     * Legt den Wert der risikoHaushalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RisikoHaushaltType }
     *     
     */
    public void setRisikoHaushalt(RisikoHaushaltType value) {
        this.risikoHaushalt = value;
    }

}
