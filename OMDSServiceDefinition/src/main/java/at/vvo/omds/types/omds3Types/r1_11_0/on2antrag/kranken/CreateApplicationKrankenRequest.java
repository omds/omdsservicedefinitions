
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateApplicationRequestGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type des Requestobjekts für die Erstellung eines Krankenantrags
 * 
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateApplicationRequestGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-6-0.on2antrag.kranken}SpezAntragKranken_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "antragsanfrage"
})
@XmlRootElement(name = "CreateApplicationKrankenRequest")
public class CreateApplicationKrankenRequest
    extends CreateApplicationRequestGenType
{

    @XmlElement(name = "Antragsanfrage", required = true)
    protected SpezAntragKrankenType antragsanfrage;

    /**
     * Ruft den Wert der antragsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragKrankenType }
     *     
     */
    public SpezAntragKrankenType getAntragsanfrage() {
        return antragsanfrage;
    }

    /**
     * Legt den Wert der antragsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragKrankenType }
     *     
     */
    public void setAntragsanfrage(SpezAntragKrankenType value) {
        this.antragsanfrage = value;
    }

}
