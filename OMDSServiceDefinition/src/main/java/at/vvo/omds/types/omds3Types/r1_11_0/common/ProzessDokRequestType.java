
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Anforderung eines Dokuments durch den Client
 * 
 * <p>Java-Klasse für ProzessDokRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProzessDokRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="DokumentType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProzessDokRequest_Type")
public class ProzessDokRequestType {

    @XmlAttribute(name = "DokumentType", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected String dokumentType;

    /**
     * Ruft den Wert der dokumentType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDokumentType() {
        return dokumentType;
    }

    /**
     * Legt den Wert der dokumentType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDokumentType(String value) {
        this.dokumentType = value;
    }

}
