
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für NormierteGeschaeftsfallArten_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="NormierteGeschaeftsfallArten_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Antrag"/&gt;
 *     &lt;enumeration value="Schaden"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "NormierteGeschaeftsfallArten_Type")
@XmlEnum
public enum NormierteGeschaeftsfallArtenType {


    /**
     * Antragsprozess
     * 
     */
    @XmlEnumValue("Antrag")
    ANTRAG("Antrag"),

    /**
     * Schadenbearbeitung
     * 
     */
    @XmlEnumValue("Schaden")
    SCHADEN("Schaden");
    private final String value;

    NormierteGeschaeftsfallArtenType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NormierteGeschaeftsfallArtenType fromValue(String v) {
        for (NormierteGeschaeftsfallArtenType c: NormierteGeschaeftsfallArtenType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
