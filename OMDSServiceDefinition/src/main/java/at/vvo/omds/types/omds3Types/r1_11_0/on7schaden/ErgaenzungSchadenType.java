
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DokumentDataType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Objekt Nachmeldung Schaden
 * 
 * <p>Java-Klasse für ErgaenzungSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErgaenzungSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="Betreff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SchadenTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Belege" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentData_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheSchadensdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheSchadensdaten_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErgaenzungSchaden_Type", propOrder = {
    "schadennr",
    "id",
    "polizzennr",
    "vertragsID",
    "betreff",
    "schadenTxt",
    "belege",
    "zusaetzlicheSchadensdaten"
})
public class ErgaenzungSchadenType {

    @XmlElement(name = "Schadennr", required = true)
    protected String schadennr;
    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "Polizzennr")
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "Betreff")
    protected String betreff;
    @XmlElement(name = "SchadenTxt")
    protected String schadenTxt;
    @XmlElement(name = "Belege")
    protected List<DokumentDataType> belege;
    @XmlElement(name = "ZusaetzlicheSchadensdaten")
    protected ZusaetzlicheSchadensdatenType zusaetzlicheSchadensdaten;

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der betreff-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetreff() {
        return betreff;
    }

    /**
     * Legt den Wert der betreff-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetreff(String value) {
        this.betreff = value;
    }

    /**
     * Ruft den Wert der schadenTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenTxt() {
        return schadenTxt;
    }

    /**
     * Legt den Wert der schadenTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenTxt(String value) {
        this.schadenTxt = value;
    }

    /**
     * Gets the value of the belege property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the belege property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBelege().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentDataType }
     * 
     * 
     */
    public List<DokumentDataType> getBelege() {
        if (belege == null) {
            belege = new ArrayList<DokumentDataType>();
        }
        return this.belege;
    }

    /**
     * Ruft den Wert der zusaetzlicheSchadensdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheSchadensdatenType }
     *     
     */
    public ZusaetzlicheSchadensdatenType getZusaetzlicheSchadensdaten() {
        return zusaetzlicheSchadensdaten;
    }

    /**
     * Legt den Wert der zusaetzlicheSchadensdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheSchadensdatenType }
     *     
     */
    public void setZusaetzlicheSchadensdaten(ZusaetzlicheSchadensdatenType value) {
        this.zusaetzlicheSchadensdaten = value;
    }

}
