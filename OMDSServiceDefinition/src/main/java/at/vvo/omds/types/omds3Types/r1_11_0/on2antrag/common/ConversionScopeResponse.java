
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Konvertierungsmoeglichkeit" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}Konvertierungsumfang_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "konvertierungsmoeglichkeit"
})
@XmlRootElement(name = "ConversionScopeResponse")
public class ConversionScopeResponse
    extends CommonResponseType
{

    @XmlElement(name = "Konvertierungsmoeglichkeit", required = true)
    protected KonvertierungsumfangType konvertierungsmoeglichkeit;

    /**
     * Ruft den Wert der konvertierungsmoeglichkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KonvertierungsumfangType }
     *     
     */
    public KonvertierungsumfangType getKonvertierungsmoeglichkeit() {
        return konvertierungsmoeglichkeit;
    }

    /**
     * Legt den Wert der konvertierungsmoeglichkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KonvertierungsumfangType }
     *     
     */
    public void setKonvertierungsmoeglichkeit(KonvertierungsumfangType value) {
        this.konvertierungsmoeglichkeit = value;
    }

}
