
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.BeteiligtePersonVertragType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.SpezBerechnungKfzType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.SpezBerechnungKrankenType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.SpezBerechnungLebenType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.BerechnungSachPrivatType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.SpezBerechnungUnfallType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Basistyp Berechnung, der bei Request und bei Response gleich ist
 * 
 * <p>Java-Klasse für SpezBerechnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezBerechnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezBOASchritt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Personen" type="{urn:omds3CommonServiceTypes-1-1-0}BeteiligtePersonVertrag_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezBerechnung_Type", propOrder = {
    "personen"
})
@XmlSeeAlso({
    SpezBerechnungKfzType.class,
    BerechnungSachPrivatType.class,
    SpezBerechnungUnfallType.class,
    SpezBerechnungKrankenType.class,
    SpezBerechnungLebenType.class
})
public abstract class SpezBerechnungType
    extends SpezBOASchrittType
{

    @XmlElement(name = "Personen")
    protected List<BeteiligtePersonVertragType> personen;

    /**
     * Gets the value of the personen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the personen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonVertragType }
     * 
     * 
     */
    public List<BeteiligtePersonVertragType> getPersonen() {
        if (personen == null) {
            personen = new ArrayList<BeteiligtePersonVertragType>();
        }
        return this.personen;
    }

}
