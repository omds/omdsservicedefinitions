
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für eine Liste zulässiger Kombinationen von Produktbaustein-Ids als Restriktion.
 * 
 * <p>Java-Klasse für Kombinationen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Kombinationen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Kombination" type="{urn:omds3CommonServiceTypes-1-1-0}Kombination_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="alle" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="zulaessige" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Kombinationen_Type", propOrder = {
    "kombinationen"
})
public class KombinationenType {

    @XmlElement(name = "Kombination")
    protected List<KombinationType> kombinationen;
    @XmlAttribute(name = "alle", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected boolean alle;
    @XmlAttribute(name = "zulaessige", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected Boolean zulaessige;

    /**
     * <p>Die Liste der zulässigen Kombinationen.</p>Gets the value of the kombinationen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the kombinationen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKombinationen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KombinationType }
     * 
     * 
     */
    public List<KombinationType> getKombinationen() {
        if (kombinationen == null) {
            kombinationen = new ArrayList<KombinationType>();
        }
        return this.kombinationen;
    }

    /**
     * Ruft den Wert der alle-Eigenschaft ab.
     * 
     */
    public boolean isAlle() {
        return alle;
    }

    /**
     * Legt den Wert der alle-Eigenschaft fest.
     * 
     */
    public void setAlle(boolean value) {
        this.alle = value;
    }

    /**
     * Ruft den Wert der zulaessige-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isZulaessige() {
        if (zulaessige == null) {
            return true;
        } else {
            return zulaessige;
        }
    }

    /**
     * Legt den Wert der zulaessige-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setZulaessige(Boolean value) {
        this.zulaessige = value;
    }

}
