
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.RisikoGebaeudeType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.RisikoHaushaltType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.VersichertesObjektSachPrivatType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Obertyp für versicherte Interessen, unterstützt Attribut-Metadaten
 * 
 * <p>Java-Klasse für VersichertesInteresseMitAttributMetadaten_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersichertesInteresseMitAttributMetadaten_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AttributMetadaten" type="{urn:omds3CommonServiceTypes-1-1-0}AttributMetadaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersichertesInteresseMitAttributMetadaten_Type", propOrder = {
    "attributMetadaten"
})
@XmlSeeAlso({
    VersichertePersonType.class,
    VersicherteLiegenschaftType.class,
    VersichertesObjektSachPrivatType.class,
    RisikoHaushaltType.class,
    RisikoGebaeudeType.class
})
public abstract class VersichertesInteresseMitAttributMetadatenType
    extends VersichertesInteresseType
{

    @XmlElement(name = "AttributMetadaten")
    protected List<AttributMetadatenType> attributMetadaten;

    /**
     * Gets the value of the attributMetadaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the attributMetadaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributMetadaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributMetadatenType }
     * 
     * 
     */
    public List<AttributMetadatenType> getAttributMetadaten() {
        if (attributMetadaten == null) {
            attributMetadaten = new ArrayList<AttributMetadatenType>();
        }
        return this.attributMetadaten;
    }

}
