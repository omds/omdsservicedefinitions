
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import at.vvo.omds.types.omds3Types.r1_11_0.common.PersonType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für die Meldung von Personen, die an einem Schaden beteiligt sind
 * 
 * <p>Java-Klasse für BeteiligtePerson_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BeteiligtePerson_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Person"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Lfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BeteiligtePerson_Type", propOrder = {
    "person"
})
public class BeteiligtePersonType {

    @XmlElement(name = "Person", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected PersonType person;
    @XmlAttribute(name = "Lfnr", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;

    /**
     * Objekt ähnlich zu omds:PERSON, aber Personennr ist nicht Pflichtfeld
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setPerson(PersonType value) {
        this.person = value;
    }

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

}
