
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.SubmitApplicationResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Responseobjekts für eine Antragseinreichung Kfz
 * 
 * <p>Java-Klasse für SubmitApplicationKfzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationKfzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SubmitApplicationResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragantwort" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAntragKfz_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationKfzResponse_Type", propOrder = {
    "antragantwort"
})
public class SubmitApplicationKfzResponseType
    extends SubmitApplicationResponseType
{

    @XmlElement(name = "Antragantwort")
    protected SpezAntragKfzType antragantwort;

    /**
     * Ruft den Wert der antragantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragKfzType }
     *     
     */
    public SpezAntragKfzType getAntragantwort() {
        return antragantwort;
    }

    /**
     * Legt den Wert der antragantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragKfzType }
     *     
     */
    public void setAntragantwort(SpezAntragKfzType value) {
        this.antragantwort = value;
    }

}
