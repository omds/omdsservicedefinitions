
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import at.vvo.omds.types.omds3Types.r1_11_0.common.AWechselkennzeichenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Standardimplementierung von AWechselkennzeichen_Type
 * 
 * <p>Java-Klasse für Wechselkennzeichen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Wechselkennzeichen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}AWechselkennzeichen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WechselkennzeichenArt"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Neues Fahrzeug als WKZ in bestehenden Vertrag einbündeln"/&gt;
 *               &lt;enumeration value="Fahrzeugwechsel im bestehenden WKZ-Vertrag"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="BestehenderWechselkennzeichenvertrag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ErsetztWirdFahrgestellnummer" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}Fahrgestellnummer_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Wechselkennzeichen_Type", propOrder = {
    "wechselkennzeichenArt",
    "bestehenderWechselkennzeichenvertrag",
    "ersetztWirdFahrgestellnummer"
})
public class WechselkennzeichenType
    extends AWechselkennzeichenType
{

    @XmlElement(name = "WechselkennzeichenArt", required = true)
    protected String wechselkennzeichenArt;
    @XmlElement(name = "BestehenderWechselkennzeichenvertrag", required = true)
    protected String bestehenderWechselkennzeichenvertrag;
    @XmlElement(name = "ErsetztWirdFahrgestellnummer")
    protected String ersetztWirdFahrgestellnummer;

    /**
     * Ruft den Wert der wechselkennzeichenArt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWechselkennzeichenArt() {
        return wechselkennzeichenArt;
    }

    /**
     * Legt den Wert der wechselkennzeichenArt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWechselkennzeichenArt(String value) {
        this.wechselkennzeichenArt = value;
    }

    /**
     * Ruft den Wert der bestehenderWechselkennzeichenvertrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBestehenderWechselkennzeichenvertrag() {
        return bestehenderWechselkennzeichenvertrag;
    }

    /**
     * Legt den Wert der bestehenderWechselkennzeichenvertrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBestehenderWechselkennzeichenvertrag(String value) {
        this.bestehenderWechselkennzeichenvertrag = value;
    }

    /**
     * Ruft den Wert der ersetztWirdFahrgestellnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErsetztWirdFahrgestellnummer() {
        return ersetztWirdFahrgestellnummer;
    }

    /**
     * Legt den Wert der ersetztWirdFahrgestellnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErsetztWirdFahrgestellnummer(String value) {
        this.ersetztWirdFahrgestellnummer = value;
    }

}
