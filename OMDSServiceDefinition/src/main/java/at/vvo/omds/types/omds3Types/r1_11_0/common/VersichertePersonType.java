
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Versicherte Person basierend auf VersichertesInteresse_Type. Lfnr muss mit Lfnr der Person übereinstimmen.
 * 
 * <p>Java-Klasse für VersichertePerson_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersichertePerson_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresseMitAttributMetadaten_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="RisikoNatPerson" type="{urn:omds3CommonServiceTypes-1-1-0}RisikoNatPerson_Type"/&gt;
 *           &lt;element name="RisikoSonstPerson" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersichertePerson_Type", propOrder = {
    "risikoNatPerson",
    "risikoSonstPerson"
})
public class VersichertePersonType
    extends VersichertesInteresseMitAttributMetadatenType
{

    @XmlElement(name = "RisikoNatPerson")
    protected RisikoNatPersonType risikoNatPerson;
    @XmlElement(name = "RisikoSonstPerson")
    protected Object risikoSonstPerson;

    /**
     * Ruft den Wert der risikoNatPerson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RisikoNatPersonType }
     *     
     */
    public RisikoNatPersonType getRisikoNatPerson() {
        return risikoNatPerson;
    }

    /**
     * Legt den Wert der risikoNatPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RisikoNatPersonType }
     *     
     */
    public void setRisikoNatPerson(RisikoNatPersonType value) {
        this.risikoNatPerson = value;
    }

    /**
     * Ruft den Wert der risikoSonstPerson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getRisikoSonstPerson() {
        return risikoSonstPerson;
    }

    /**
     * Legt den Wert der risikoSonstPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setRisikoSonstPerson(Object value) {
        this.risikoSonstPerson = value;
    }

}
