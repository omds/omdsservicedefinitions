
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ mit Metadaten für xsd:string-Attribute
 * 
 * <p>Java-Klasse für AttributMetadatenString_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttributMetadatenString_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}AttributMetadaten_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Default" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Values" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="MinLaenge" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="MaxLaenge" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="Regex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributMetadatenString_Type", propOrder = {
    "_default",
    "values",
    "minLaenge",
    "maxLaenge",
    "regex"
})
public class AttributMetadatenStringType
    extends AttributMetadatenType
{

    @XmlElement(name = "Default")
    protected String _default;
    @XmlElement(name = "Values")
    protected List<String> values;
    @XmlElement(name = "MinLaenge")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer minLaenge;
    @XmlElement(name = "MaxLaenge")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer maxLaenge;
    @XmlElement(name = "Regex")
    protected String regex;

    /**
     * Ruft den Wert der default-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefault() {
        return _default;
    }

    /**
     * Legt den Wert der default-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefault(String value) {
        this._default = value;
    }

    /**
     * Gets the value of the values property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the values property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getValues() {
        if (values == null) {
            values = new ArrayList<String>();
        }
        return this.values;
    }

    /**
     * Ruft den Wert der minLaenge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinLaenge() {
        return minLaenge;
    }

    /**
     * Legt den Wert der minLaenge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinLaenge(Integer value) {
        this.minLaenge = value;
    }

    /**
     * Ruft den Wert der maxLaenge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxLaenge() {
        return maxLaenge;
    }

    /**
     * Legt den Wert der maxLaenge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxLaenge(Integer value) {
        this.maxLaenge = value;
    }

    /**
     * Ruft den Wert der regex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegex() {
        return regex;
    }

    /**
     * Legt den Wert der regex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegex(String value) {
        this.regex = value;
    }

}
