
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Fragen gemäß dem Gemeinsamen Meldestandard-Gesetz
 * 
 * <p>Java-Klasse für GMSG_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GMSG_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SteuerlichAnsaessig" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Land" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type"/&gt;
 *                   &lt;element name="Steuernummer" type="{urn:omds3CommonServiceTypes-1-1-0}AttributString_Type" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GMSG_Type", propOrder = {
    "steuerlichAnsaessig"
})
public class GMSGType {

    @XmlElement(name = "SteuerlichAnsaessig", required = true)
    protected List<GMSGType.SteuerlichAnsaessig> steuerlichAnsaessig;

    /**
     * Gets the value of the steuerlichAnsaessig property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the steuerlichAnsaessig property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSteuerlichAnsaessig().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GMSGType.SteuerlichAnsaessig }
     * 
     * 
     */
    public List<GMSGType.SteuerlichAnsaessig> getSteuerlichAnsaessig() {
        if (steuerlichAnsaessig == null) {
            steuerlichAnsaessig = new ArrayList<GMSGType.SteuerlichAnsaessig>();
        }
        return this.steuerlichAnsaessig;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Land" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type"/&gt;
     *         &lt;element name="Steuernummer" type="{urn:omds3CommonServiceTypes-1-1-0}AttributString_Type" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "land",
        "steuernummer"
    })
    public static class SteuerlichAnsaessig {

        @XmlElement(name = "Land", required = true)
        protected AttributEnumType land;
        @XmlElement(name = "Steuernummer")
        protected AttributStringType steuernummer;

        /**
         * Ruft den Wert der land-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AttributEnumType }
         *     
         */
        public AttributEnumType getLand() {
            return land;
        }

        /**
         * Legt den Wert der land-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AttributEnumType }
         *     
         */
        public void setLand(AttributEnumType value) {
            this.land = value;
        }

        /**
         * Ruft den Wert der steuernummer-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AttributStringType }
         *     
         */
        public AttributStringType getSteuernummer() {
            return steuernummer;
        }

        /**
         * Legt den Wert der steuernummer-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AttributStringType }
         *     
         */
        public void setSteuernummer(AttributStringType value) {
            this.steuernummer = value;
        }

    }

}
