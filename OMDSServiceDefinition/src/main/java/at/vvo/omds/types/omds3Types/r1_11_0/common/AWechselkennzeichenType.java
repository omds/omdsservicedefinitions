
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.WechselkennzeichenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Behandlung des Fahrzeugs im Wechselkennzeichen
 * 
 * <p>Java-Klasse für AWechselkennzeichen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AWechselkennzeichen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AWechselkennzeichen_Type")
@XmlSeeAlso({
    WechselkennzeichenType.class
})
public abstract class AWechselkennzeichenType {


}
