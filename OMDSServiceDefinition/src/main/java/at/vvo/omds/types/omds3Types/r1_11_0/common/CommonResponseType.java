
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.on1basis.AcknowledgeDocumentsResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionProposalResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionScopeResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.GetApplicationDocumentResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateVBResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.AddInformationToClaimResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.CheckClaimResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.CheckCoverageResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.GetClaimResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitClaimResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitReceiptResponse;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstraktes ResponseObjekt
 * 
 * <p>Java-Klasse für CommonResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Status" type="{urn:omds3CommonServiceTypes-1-1-0}ResponseStatus_Type"/&gt;
 *         &lt;element name="TechnischeObjekte" type="{urn:omds3CommonServiceTypes-1-1-0}TechnischesObjekt_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Gestartet" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="Beendet" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonResponse_Type", propOrder = {
    "status",
    "technischeObjekte",
    "gestartet",
    "beendet"
})
@XmlSeeAlso({
    CommonSearchResponseType.class,
    GetApplicationDocumentResponseType.class,
    CheckClaimResponseType.class,
    SubmitClaimResponseType.class,
    GetClaimResponseType.class,
    AddInformationToClaimResponse.class,
    SubmitReceiptResponse.class,
    CheckCoverageResponse.class,
    CommonProcessResponseType.class,
    CreateVBResponse.class,
    ConversionProposalResponse.class,
    ConversionScopeResponse.class,
    AcknowledgeDocumentsResponse.class
})
public abstract class CommonResponseType {

    @XmlElement(name = "Status", required = true)
    protected ResponseStatusType status;
    @XmlElement(name = "TechnischeObjekte")
    protected List<TechnischesObjektType> technischeObjekte;
    @XmlElement(name = "Gestartet")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gestartet;
    @XmlElement(name = "Beendet")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar beendet;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponseStatusType }
     *     
     */
    public ResponseStatusType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseStatusType }
     *     
     */
    public void setStatus(ResponseStatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the technischeObjekte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the technischeObjekte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTechnischeObjekte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TechnischesObjektType }
     * 
     * 
     */
    public List<TechnischesObjektType> getTechnischeObjekte() {
        if (technischeObjekte == null) {
            technischeObjekte = new ArrayList<TechnischesObjektType>();
        }
        return this.technischeObjekte;
    }

    /**
     * Ruft den Wert der gestartet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGestartet() {
        return gestartet;
    }

    /**
     * Legt den Wert der gestartet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGestartet(XMLGregorianCalendar value) {
        this.gestartet = value;
    }

    /**
     * Ruft den Wert der beendet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBeendet() {
        return beendet;
    }

    /**
     * Legt den Wert der beendet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBeendet(XMLGregorianCalendar value) {
        this.beendet = value;
    }

}
