
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementarproduktGenerischType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.KostenFixOderProzentType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.SelbstbehaltType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Wurzelelement für Sach-Privat-Elementarprodukte, auf welchem alle Sach-Privat Elementarprodukte aufbauen sollen
 * 
 * <p>Java-Klasse für ElementarproduktSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementarproduktSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ElementarproduktGenerisch_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersObjekteRefLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Pauschalbetrag" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *         &lt;element name="Unterversicherungsverzicht" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ProzentVersicherungssumme" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="Nebenkosten" type="{urn:omds3CommonServiceTypes-1-1-0}KostenFixOderProzent_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vorsorge" type="{urn:omds3CommonServiceTypes-1-1-0}KostenFixOderProzent_Type" minOccurs="0"/&gt;
 *         &lt;element name="Hoechsthaftungssumme" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementarproduktSachPrivat_Type", propOrder = {
    "versObjekteRefLfnr",
    "pauschalbetrag",
    "selbstbehalt",
    "unterversicherungsverzicht",
    "prozentVersicherungssumme",
    "nebenkosten",
    "vorsorge",
    "hoechsthaftungssumme"
})
@XmlSeeAlso({
    ElementarproduktGebaeudeType.class,
    ElementarproduktHaushaltType.class
})
public class ElementarproduktSachPrivatType
    extends ElementarproduktGenerischType
{

    @XmlElement(name = "VersObjekteRefLfnr", type = Integer.class)
    @XmlSchemaType(name = "unsignedShort")
    protected List<Integer> versObjekteRefLfnr;
    @XmlElement(name = "Pauschalbetrag")
    protected BigDecimal pauschalbetrag;
    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;
    @XmlElement(name = "Unterversicherungsverzicht")
    protected Boolean unterversicherungsverzicht;
    @XmlElement(name = "ProzentVersicherungssumme")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer prozentVersicherungssumme;
    @XmlElement(name = "Nebenkosten")
    protected KostenFixOderProzentType nebenkosten;
    @XmlElement(name = "Vorsorge")
    protected KostenFixOderProzentType vorsorge;
    @XmlElement(name = "Hoechsthaftungssumme")
    protected BigDecimal hoechsthaftungssumme;

    /**
     * Gets the value of the versObjekteRefLfnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the versObjekteRefLfnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersObjekteRefLfnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getVersObjekteRefLfnr() {
        if (versObjekteRefLfnr == null) {
            versObjekteRefLfnr = new ArrayList<Integer>();
        }
        return this.versObjekteRefLfnr;
    }

    /**
     * Ruft den Wert der pauschalbetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPauschalbetrag() {
        return pauschalbetrag;
    }

    /**
     * Legt den Wert der pauschalbetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPauschalbetrag(BigDecimal value) {
        this.pauschalbetrag = value;
    }

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

    /**
     * Ruft den Wert der unterversicherungsverzicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnterversicherungsverzicht() {
        return unterversicherungsverzicht;
    }

    /**
     * Legt den Wert der unterversicherungsverzicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnterversicherungsverzicht(Boolean value) {
        this.unterversicherungsverzicht = value;
    }

    /**
     * Ruft den Wert der prozentVersicherungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProzentVersicherungssumme() {
        return prozentVersicherungssumme;
    }

    /**
     * Legt den Wert der prozentVersicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProzentVersicherungssumme(Integer value) {
        this.prozentVersicherungssumme = value;
    }

    /**
     * Ruft den Wert der nebenkosten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public KostenFixOderProzentType getNebenkosten() {
        return nebenkosten;
    }

    /**
     * Legt den Wert der nebenkosten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public void setNebenkosten(KostenFixOderProzentType value) {
        this.nebenkosten = value;
    }

    /**
     * Ruft den Wert der vorsorge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public KostenFixOderProzentType getVorsorge() {
        return vorsorge;
    }

    /**
     * Legt den Wert der vorsorge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public void setVorsorge(KostenFixOderProzentType value) {
        this.vorsorge = value;
    }

    /**
     * Ruft den Wert der hoechsthaftungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHoechsthaftungssumme() {
        return hoechsthaftungssumme;
    }

    /**
     * Legt den Wert der hoechsthaftungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHoechsthaftungssumme(BigDecimal value) {
        this.hoechsthaftungssumme = value;
    }

}
