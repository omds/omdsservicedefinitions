
package at.vvo.omds.types.omds2Types.v2_16;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Rahmenvertrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Rahmenvertrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RahmenvertragsartCd" use="required" type="{urn:omds20}RahmenvertragsartCd_Type" /&gt;
 *       &lt;attribute name="Rahmenvertragsnummer" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Rahmenvertrag_Type")
public class ELRahmenvertragType {

    @XmlAttribute(name = "RahmenvertragsartCd", required = true)
    protected String rahmenvertragsartCd;
    @XmlAttribute(name = "Rahmenvertragsnummer", required = true)
    protected String rahmenvertragsnummer;

    /**
     * Ruft den Wert der rahmenvertragsartCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRahmenvertragsartCd() {
        return rahmenvertragsartCd;
    }

    /**
     * Legt den Wert der rahmenvertragsartCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRahmenvertragsartCd(String value) {
        this.rahmenvertragsartCd = value;
    }

    /**
     * Ruft den Wert der rahmenvertragsnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRahmenvertragsnummer() {
        return rahmenvertragsnummer;
    }

    /**
     * Legt den Wert der rahmenvertragsnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRahmenvertragsnummer(String value) {
        this.rahmenvertragsnummer = value;
    }

}
