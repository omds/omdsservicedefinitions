
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Risikoattribute natürlicher Personen
 * 
 * <p>Java-Klasse für RisikoNatPerson_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RisikoNatPerson_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Gewicht" type="{urn:omds3CommonServiceTypes-1-1-0}AttributInt_Type" minOccurs="0"/&gt;
 *         &lt;element name="Groesse" type="{urn:omds3CommonServiceTypes-1-1-0}AttributInt_Type" minOccurs="0"/&gt;
 *         &lt;element name="Raucher" type="{urn:omds3CommonServiceTypes-1-1-0}Raucher_Type" minOccurs="0"/&gt;
 *         &lt;element name="Sozialversicherungsanstalt" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" minOccurs="0"/&gt;
 *         &lt;element name="Gefahrenklasse" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" minOccurs="0"/&gt;
 *         &lt;element name="Beruf" type="{urn:omds3CommonServiceTypes-1-1-0}AttributString_Type" minOccurs="0"/&gt;
 *         &lt;element name="MedizinischerBeruf" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" minOccurs="0"/&gt;
 *         &lt;element name="FreizeitSportRisiken" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Risikozuschlaege" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GesetzlicheUV" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" minOccurs="0"/&gt;
 *         &lt;element name="PersonenartKranken" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" minOccurs="0"/&gt;
 *         &lt;element name="Berufsgruppe" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" minOccurs="0"/&gt;
 *         &lt;element name="ManuelleTaetigkeit" type="{urn:omds3CommonServiceTypes-1-1-0}AttributEnum_Type" minOccurs="0"/&gt;
 *         &lt;element name="BehandelnderArzt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="KontaktRueckfragen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheRisikodaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheRisikodaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RisikoNatPerson_Type", propOrder = {
    "gewicht",
    "groesse",
    "raucher",
    "sozialversicherungsanstalt",
    "gefahrenklasse",
    "beruf",
    "medizinischerBeruf",
    "freizeitSportRisiken",
    "risikozuschlaege",
    "gesetzlicheUV",
    "personenartKranken",
    "berufsgruppe",
    "manuelleTaetigkeit",
    "behandelnderArzt",
    "kontaktRueckfragen",
    "zusaetzlicheRisikodaten"
})
public class RisikoNatPersonType {

    @XmlElement(name = "Gewicht")
    protected AttributIntType gewicht;
    @XmlElement(name = "Groesse")
    protected AttributIntType groesse;
    @XmlElement(name = "Raucher")
    protected RaucherType raucher;
    @XmlElement(name = "Sozialversicherungsanstalt")
    protected AttributEnumType sozialversicherungsanstalt;
    @XmlElement(name = "Gefahrenklasse")
    protected AttributEnumType gefahrenklasse;
    @XmlElement(name = "Beruf")
    protected AttributStringType beruf;
    @XmlElement(name = "MedizinischerBeruf")
    protected AttributEnumType medizinischerBeruf;
    @XmlElement(name = "FreizeitSportRisiken")
    protected List<AttributEnumType> freizeitSportRisiken;
    @XmlElement(name = "Risikozuschlaege")
    protected List<AttributEnumType> risikozuschlaege;
    @XmlElement(name = "GesetzlicheUV")
    protected AttributEnumType gesetzlicheUV;
    @XmlElement(name = "PersonenartKranken")
    protected AttributEnumType personenartKranken;
    @XmlElement(name = "Berufsgruppe")
    protected AttributEnumType berufsgruppe;
    @XmlElement(name = "ManuelleTaetigkeit")
    protected AttributEnumType manuelleTaetigkeit;
    @XmlElement(name = "BehandelnderArzt")
    protected String behandelnderArzt;
    @XmlElement(name = "KontaktRueckfragen")
    protected String kontaktRueckfragen;
    @XmlElement(name = "ZusaetzlicheRisikodaten")
    protected List<ZusaetzlicheRisikodatenType> zusaetzlicheRisikodaten;

    /**
     * Ruft den Wert der gewicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributIntType }
     *     
     */
    public AttributIntType getGewicht() {
        return gewicht;
    }

    /**
     * Legt den Wert der gewicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributIntType }
     *     
     */
    public void setGewicht(AttributIntType value) {
        this.gewicht = value;
    }

    /**
     * Ruft den Wert der groesse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributIntType }
     *     
     */
    public AttributIntType getGroesse() {
        return groesse;
    }

    /**
     * Legt den Wert der groesse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributIntType }
     *     
     */
    public void setGroesse(AttributIntType value) {
        this.groesse = value;
    }

    /**
     * Ruft den Wert der raucher-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RaucherType }
     *     
     */
    public RaucherType getRaucher() {
        return raucher;
    }

    /**
     * Legt den Wert der raucher-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RaucherType }
     *     
     */
    public void setRaucher(RaucherType value) {
        this.raucher = value;
    }

    /**
     * Ruft den Wert der sozialversicherungsanstalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getSozialversicherungsanstalt() {
        return sozialversicherungsanstalt;
    }

    /**
     * Legt den Wert der sozialversicherungsanstalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setSozialversicherungsanstalt(AttributEnumType value) {
        this.sozialversicherungsanstalt = value;
    }

    /**
     * Ruft den Wert der gefahrenklasse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getGefahrenklasse() {
        return gefahrenklasse;
    }

    /**
     * Legt den Wert der gefahrenklasse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setGefahrenklasse(AttributEnumType value) {
        this.gefahrenklasse = value;
    }

    /**
     * Ruft den Wert der beruf-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributStringType }
     *     
     */
    public AttributStringType getBeruf() {
        return beruf;
    }

    /**
     * Legt den Wert der beruf-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributStringType }
     *     
     */
    public void setBeruf(AttributStringType value) {
        this.beruf = value;
    }

    /**
     * Ruft den Wert der medizinischerBeruf-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getMedizinischerBeruf() {
        return medizinischerBeruf;
    }

    /**
     * Legt den Wert der medizinischerBeruf-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setMedizinischerBeruf(AttributEnumType value) {
        this.medizinischerBeruf = value;
    }

    /**
     * Gets the value of the freizeitSportRisiken property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the freizeitSportRisiken property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreizeitSportRisiken().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributEnumType }
     * 
     * 
     */
    public List<AttributEnumType> getFreizeitSportRisiken() {
        if (freizeitSportRisiken == null) {
            freizeitSportRisiken = new ArrayList<AttributEnumType>();
        }
        return this.freizeitSportRisiken;
    }

    /**
     * Gets the value of the risikozuschlaege property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the risikozuschlaege property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRisikozuschlaege().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributEnumType }
     * 
     * 
     */
    public List<AttributEnumType> getRisikozuschlaege() {
        if (risikozuschlaege == null) {
            risikozuschlaege = new ArrayList<AttributEnumType>();
        }
        return this.risikozuschlaege;
    }

    /**
     * Ruft den Wert der gesetzlicheUV-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getGesetzlicheUV() {
        return gesetzlicheUV;
    }

    /**
     * Legt den Wert der gesetzlicheUV-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setGesetzlicheUV(AttributEnumType value) {
        this.gesetzlicheUV = value;
    }

    /**
     * Ruft den Wert der personenartKranken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getPersonenartKranken() {
        return personenartKranken;
    }

    /**
     * Legt den Wert der personenartKranken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setPersonenartKranken(AttributEnumType value) {
        this.personenartKranken = value;
    }

    /**
     * Ruft den Wert der berufsgruppe-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getBerufsgruppe() {
        return berufsgruppe;
    }

    /**
     * Legt den Wert der berufsgruppe-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setBerufsgruppe(AttributEnumType value) {
        this.berufsgruppe = value;
    }

    /**
     * Ruft den Wert der manuelleTaetigkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributEnumType }
     *     
     */
    public AttributEnumType getManuelleTaetigkeit() {
        return manuelleTaetigkeit;
    }

    /**
     * Legt den Wert der manuelleTaetigkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributEnumType }
     *     
     */
    public void setManuelleTaetigkeit(AttributEnumType value) {
        this.manuelleTaetigkeit = value;
    }

    /**
     * Ruft den Wert der behandelnderArzt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBehandelnderArzt() {
        return behandelnderArzt;
    }

    /**
     * Legt den Wert der behandelnderArzt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBehandelnderArzt(String value) {
        this.behandelnderArzt = value;
    }

    /**
     * Ruft den Wert der kontaktRueckfragen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKontaktRueckfragen() {
        return kontaktRueckfragen;
    }

    /**
     * Legt den Wert der kontaktRueckfragen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKontaktRueckfragen(String value) {
        this.kontaktRueckfragen = value;
    }

    /**
     * Gets the value of the zusaetzlicheRisikodaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheRisikodaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheRisikodaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheRisikodatenType }
     * 
     * 
     */
    public List<ZusaetzlicheRisikodatenType> getZusaetzlicheRisikodaten() {
        if (zusaetzlicheRisikodaten == null) {
            zusaetzlicheRisikodaten = new ArrayList<ZusaetzlicheRisikodatenType>();
        }
        return this.zusaetzlicheRisikodaten;
    }

}
