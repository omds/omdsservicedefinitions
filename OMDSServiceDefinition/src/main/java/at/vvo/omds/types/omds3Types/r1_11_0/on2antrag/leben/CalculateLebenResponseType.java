
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CalculateResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Responseobjekts für eine Leben-Berechnung
 * 
 * <p>Java-Klasse für CalculateLebenResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateLebenResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}SpezBerechnungLeben_Type"/&gt;
 *         &lt;element name="Upsellingvarianten" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}SpezBerechnungLeben_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateLebenResponse_Type", propOrder = {
    "berechnungsantwort",
    "upsellingvarianten"
})
public class CalculateLebenResponseType
    extends CalculateResponseGenType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected SpezBerechnungLebenType berechnungsantwort;
    @XmlElement(name = "Upsellingvarianten")
    protected List<SpezBerechnungLebenType> upsellingvarianten;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungLebenType }
     *     
     */
    public SpezBerechnungLebenType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungLebenType }
     *     
     */
    public void setBerechnungsantwort(SpezBerechnungLebenType value) {
        this.berechnungsantwort = value;
    }

    /**
     * Gets the value of the upsellingvarianten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the upsellingvarianten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpsellingvarianten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpezBerechnungLebenType }
     * 
     * 
     */
    public List<SpezBerechnungLebenType> getUpsellingvarianten() {
        if (upsellingvarianten == null) {
            upsellingvarianten = new ArrayList<SpezBerechnungLebenType>();
        }
        return this.upsellingvarianten;
    }

}
