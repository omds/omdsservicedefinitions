
package at.vvo.omds.types.omds3Types.r1_11_0.servicetypes;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Geschaeftsvorfall.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="Geschaeftsvorfall"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Neuantrag"/&gt;
 *     &lt;enumeration value="Aenderungsantrag"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Geschaeftsvorfall")
@XmlEnum
public enum Geschaeftsvorfall {

    @XmlEnumValue("Neuantrag")
    NEUANTRAG("Neuantrag"),
    @XmlEnumValue("Aenderungsantrag")
    AENDERUNGSANTRAG("Aenderungsantrag");
    private final String value;

    Geschaeftsvorfall(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Geschaeftsvorfall fromValue(String v) {
        for (Geschaeftsvorfall c: Geschaeftsvorfall.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
