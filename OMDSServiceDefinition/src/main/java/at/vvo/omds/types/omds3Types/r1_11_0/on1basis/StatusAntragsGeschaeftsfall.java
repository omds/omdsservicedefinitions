
package at.vvo.omds.types.omds3Types.r1_11_0.on1basis;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Deprecated: Status eines Antrags
 * 
 * <p>Java-Klasse für StatusAntragsGeschaeftsfall complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StatusAntragsGeschaeftsfall"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}AbstractStatusGeschaeftsfall_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragstatus" type="{urn:omds3CommonServiceTypes-1-1-0}SubmitApplicationStatus_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusAntragsGeschaeftsfall", propOrder = {
    "antragstatus"
})
public class StatusAntragsGeschaeftsfall
    extends AbstractStatusGeschaeftsfallType
{

    @XmlElement(name = "Antragstatus")
    protected int antragstatus;

    /**
     * Ruft den Wert der antragstatus-Eigenschaft ab.
     * 
     */
    public int getAntragstatus() {
        return antragstatus;
    }

    /**
     * Legt den Wert der antragstatus-Eigenschaft fest.
     * 
     */
    public void setAntragstatus(int value) {
        this.antragstatus = value;
    }

}
