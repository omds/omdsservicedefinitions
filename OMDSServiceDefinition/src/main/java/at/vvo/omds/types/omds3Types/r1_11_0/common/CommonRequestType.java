
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.on1basis.AcknowledgeDocumentsRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionProposalRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionScopeRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.GetApplicationDocumentRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateVBRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.AddInformationToClaimRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.CheckClaimRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.CheckCoverageRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.GetClaimRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitClaimRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitReceiptRequest;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstraktes RequestObjekt
 * 
 * <p>Java-Klasse für CommonRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TechnischeParameter" type="{urn:omds3CommonServiceTypes-1-1-0}TechnicalKeyValue_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TechnischeObjekte" type="{urn:omds3CommonServiceTypes-1-1-0}TechnischesObjekt_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="KorrelationsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonRequest_Type", propOrder = {
    "vuNr",
    "clientId",
    "technischeParameter",
    "technischeObjekte",
    "korrelationsId"
})
@XmlSeeAlso({
    CommonSearchRequestType.class,
    GetApplicationDocumentRequestType.class,
    CheckClaimRequestType.class,
    SubmitClaimRequestType.class,
    GetClaimRequestType.class,
    AddInformationToClaimRequest.class,
    SubmitReceiptRequest.class,
    CheckCoverageRequest.class,
    CommonProcessRequestType.class,
    CreateVBRequest.class,
    ConversionProposalRequest.class,
    ConversionScopeRequest.class,
    AcknowledgeDocumentsRequest.class
})
public abstract class CommonRequestType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "ClientId")
    protected String clientId;
    @XmlElement(name = "TechnischeParameter")
    protected List<TechnicalKeyValueType> technischeParameter;
    @XmlElement(name = "TechnischeObjekte")
    protected List<TechnischesObjektType> technischeObjekte;
    @XmlElement(name = "KorrelationsId", required = true)
    protected String korrelationsId;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der clientId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Legt den Wert der clientId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the technischeParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the technischeParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTechnischeParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TechnicalKeyValueType }
     * 
     * 
     */
    public List<TechnicalKeyValueType> getTechnischeParameter() {
        if (technischeParameter == null) {
            technischeParameter = new ArrayList<TechnicalKeyValueType>();
        }
        return this.technischeParameter;
    }

    /**
     * Gets the value of the technischeObjekte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the technischeObjekte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTechnischeObjekte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TechnischesObjektType }
     * 
     * 
     */
    public List<TechnischesObjektType> getTechnischeObjekte() {
        if (technischeObjekte == null) {
            technischeObjekte = new ArrayList<TechnischesObjektType>();
        }
        return this.technischeObjekte;
    }

    /**
     * Ruft den Wert der korrelationsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorrelationsId() {
        return korrelationsId;
    }

    /**
     * Legt den Wert der korrelationsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorrelationsId(String value) {
        this.korrelationsId = value;
    }

}
