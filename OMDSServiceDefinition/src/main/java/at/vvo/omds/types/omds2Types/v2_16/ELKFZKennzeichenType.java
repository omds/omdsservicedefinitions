
package at.vvo.omds.types.omds2Types.v2_16;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-KFZ-Kennzeichen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-KFZ-Kennzeichen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Pol_Kennz" use="required" type="{urn:omds20}Pol_Kennz_Type" /&gt;
 *       &lt;attribute name="Fahrgestnr" type="{urn:omds20}Fahrgestnr_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-KFZ-Kennzeichen_Type")
public class ELKFZKennzeichenType {

    @XmlAttribute(name = "Pol_Kennz", required = true)
    protected String polKennz;
    @XmlAttribute(name = "Fahrgestnr")
    protected String fahrgestnr;

    /**
     * Ruft den Wert der polKennz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolKennz() {
        return polKennz;
    }

    /**
     * Legt den Wert der polKennz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolKennz(String value) {
        this.polKennz = value;
    }

    /**
     * Ruft den Wert der fahrgestnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrgestnr() {
        return fahrgestnr;
    }

    /**
     * Legt den Wert der fahrgestnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrgestnr(String value) {
        this.fahrgestnr = value;
    }

}
