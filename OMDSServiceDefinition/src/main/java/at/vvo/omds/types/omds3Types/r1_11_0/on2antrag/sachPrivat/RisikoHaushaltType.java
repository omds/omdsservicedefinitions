
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VersichertesInteresseMitAttributMetadatenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type zur Risikobeschreibung Haushalt
 * 
 * <p>Java-Klasse für RisikoHaushalt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RisikoHaushalt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresseMitAttributMetadaten_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Wohnflaeche" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;element name="AusstattungCd" type="{urn:omds20}AusstattungCd_Type"/&gt;
 *         &lt;element name="NutzungCd" type="{urn:omds20}NutzungCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheHaushaltDaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}ZusaetzlicheHaushaltsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RisikoHaushalt_Type", propOrder = {
    "wohnflaeche",
    "ausstattungCd",
    "nutzungCd",
    "zusaetzlicheHaushaltDaten"
})
public class RisikoHaushaltType
    extends VersichertesInteresseMitAttributMetadatenType
{

    @XmlElement(name = "Wohnflaeche")
    @XmlSchemaType(name = "unsignedShort")
    protected int wohnflaeche;
    @XmlElement(name = "AusstattungCd", required = true)
    protected String ausstattungCd;
    @XmlElement(name = "NutzungCd")
    protected String nutzungCd;
    @XmlElement(name = "ZusaetzlicheHaushaltDaten")
    protected List<ZusaetzlicheHaushaltsdatenType> zusaetzlicheHaushaltDaten;

    /**
     * Ruft den Wert der wohnflaeche-Eigenschaft ab.
     * 
     */
    public int getWohnflaeche() {
        return wohnflaeche;
    }

    /**
     * Legt den Wert der wohnflaeche-Eigenschaft fest.
     * 
     */
    public void setWohnflaeche(int value) {
        this.wohnflaeche = value;
    }

    /**
     * Ruft den Wert der ausstattungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAusstattungCd() {
        return ausstattungCd;
    }

    /**
     * Legt den Wert der ausstattungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAusstattungCd(String value) {
        this.ausstattungCd = value;
    }

    /**
     * Ruft den Wert der nutzungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNutzungCd() {
        return nutzungCd;
    }

    /**
     * Legt den Wert der nutzungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNutzungCd(String value) {
        this.nutzungCd = value;
    }

    /**
     * Gets the value of the zusaetzlicheHaushaltDaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheHaushaltDaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheHaushaltDaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheHaushaltsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheHaushaltsdatenType> getZusaetzlicheHaushaltDaten() {
        if (zusaetzlicheHaushaltDaten == null) {
            zusaetzlicheHaushaltDaten = new ArrayList<ZusaetzlicheHaushaltsdatenType>();
        }
        return this.zusaetzlicheHaushaltDaten;
    }

}
