
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für Zusatzdaten zum Dokument
 * 
 * <p>Java-Klasse für DokumentenZuordnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DokumentenZuordnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DokumentenZuordnung_Type")
@XmlSeeAlso({
    EinfacheZuordnungType.class,
    PersonenZuordnungType.class,
    BetreuerZuordnungType.class,
    VertragsZuordnungType.class,
    SchadenZuordnungType.class,
    AntragsZuordnungType.class,
    GeschaeftsfallZuordnungType.class,
    ProvisionZuordnungType.class,
    MahnverfahrenZuordnungType.class
})
public abstract class DokumentenZuordnungType {


}
