
package at.vvo.omds.types.omds3Types.r1_11_0.on1basis;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DokumentenReferenzType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ZeitraumType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Informationen zu einem Dokument
 * 
 * <p>Java-Klasse für DocumentInfosResponseResult_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DocumentInfosResponseResult_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActualOffset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="ActualMaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="DokumentInfos" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentenReferenz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Zeitraum" type="{urn:omds3CommonServiceTypes-1-1-0}Zeitraum_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentInfosResponseResult_Type", propOrder = {
    "actualOffset",
    "actualMaxResults",
    "totalResults",
    "dokumentInfos",
    "zeitraum"
})
public class DocumentInfosResponseResultType {

    @XmlElement(name = "ActualOffset")
    @XmlSchemaType(name = "unsignedInt")
    protected long actualOffset;
    @XmlElement(name = "ActualMaxResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long actualMaxResults;
    @XmlElement(name = "TotalResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long totalResults;
    @XmlElement(name = "DokumentInfos")
    protected List<DokumentenReferenzType> dokumentInfos;
    @XmlElement(name = "Zeitraum", required = true)
    protected ZeitraumType zeitraum;

    /**
     * Ruft den Wert der actualOffset-Eigenschaft ab.
     * 
     */
    public long getActualOffset() {
        return actualOffset;
    }

    /**
     * Legt den Wert der actualOffset-Eigenschaft fest.
     * 
     */
    public void setActualOffset(long value) {
        this.actualOffset = value;
    }

    /**
     * Ruft den Wert der actualMaxResults-Eigenschaft ab.
     * 
     */
    public long getActualMaxResults() {
        return actualMaxResults;
    }

    /**
     * Legt den Wert der actualMaxResults-Eigenschaft fest.
     * 
     */
    public void setActualMaxResults(long value) {
        this.actualMaxResults = value;
    }

    /**
     * Ruft den Wert der totalResults-Eigenschaft ab.
     * 
     */
    public long getTotalResults() {
        return totalResults;
    }

    /**
     * Legt den Wert der totalResults-Eigenschaft fest.
     * 
     */
    public void setTotalResults(long value) {
        this.totalResults = value;
    }

    /**
     * Gets the value of the dokumentInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumentInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumentInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentenReferenzType }
     * 
     * 
     */
    public List<DokumentenReferenzType> getDokumentInfos() {
        if (dokumentInfos == null) {
            dokumentInfos = new ArrayList<DokumentenReferenzType>();
        }
        return this.dokumentInfos;
    }

    /**
     * Ruft den Wert der zeitraum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZeitraumType }
     *     
     */
    public ZeitraumType getZeitraum() {
        return zeitraum;
    }

    /**
     * Legt den Wert der zeitraum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZeitraumType }
     *     
     */
    public void setZeitraum(ZeitraumType value) {
        this.zeitraum = value;
    }

}
