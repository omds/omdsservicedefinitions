
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DokumentInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokumentBasisType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateApplicationKfzResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Response, der den Antrag enthält bzw. Fehlermeldungen
 * 
 * <p>Java-Klasse für CreateApplicationResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}BOAProcessResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragstatus" type="{urn:omds3CommonServiceTypes-1-1-0}SubmitApplicationStatus_Type" minOccurs="0"/&gt;
 *         &lt;element name="Antragsnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentInfo_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DokumenteAnforderungen" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokumentBasis_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationResponse_Type", propOrder = {
    "antragstatus",
    "antragsnummer",
    "dokumente",
    "dokumenteAnforderungen"
})
@XmlSeeAlso({
    CreateApplicationKfzResponseType.class,
    CreateApplicationResponseGenType.class
})
public abstract class CreateApplicationResponseType
    extends BOAProcessResponseType
{

    @XmlElement(name = "Antragstatus")
    protected Integer antragstatus;
    @XmlElement(name = "Antragsnummer")
    protected ObjektIdType antragsnummer;
    @XmlElement(name = "Dokumente")
    protected List<DokumentInfoType> dokumente;
    @XmlElement(name = "DokumenteAnforderungen")
    protected List<ProzessDokumentBasisType> dokumenteAnforderungen;

    /**
     * Ruft den Wert der antragstatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAntragstatus() {
        return antragstatus;
    }

    /**
     * Legt den Wert der antragstatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAntragstatus(Integer value) {
        this.antragstatus = value;
    }

    /**
     * Ruft den Wert der antragsnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getAntragsnummer() {
        return antragsnummer;
    }

    /**
     * Legt den Wert der antragsnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setAntragsnummer(ObjektIdType value) {
        this.antragsnummer = value;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentInfoType }
     * 
     * 
     */
    public List<DokumentInfoType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<DokumentInfoType>();
        }
        return this.dokumente;
    }

    /**
     * Gets the value of the dokumenteAnforderungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumenteAnforderungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumenteAnforderungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokumentBasisType }
     * 
     * 
     */
    public List<ProzessDokumentBasisType> getDokumenteAnforderungen() {
        if (dokumenteAnforderungen == null) {
            dokumenteAnforderungen = new ArrayList<ProzessDokumentBasisType>();
        }
        return this.dokumenteAnforderungen;
    }

}
