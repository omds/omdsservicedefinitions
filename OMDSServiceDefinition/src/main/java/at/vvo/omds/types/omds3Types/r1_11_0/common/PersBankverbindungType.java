
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * beschreibt eine Bankverbindug, welche einer Person ueber eine Referenz zugeordnet ist
 * 
 * <p>Java-Klasse für PersBankverbindung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PersBankverbindung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type"&gt;
 *       &lt;attribute name="PersonRefLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersBankverbindung_Type")
public class PersBankverbindungType
    extends BankverbindungType
{

    @XmlAttribute(name = "PersonRefLfnr", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer personRefLfnr;

    /**
     * Ruft den Wert der personRefLfnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPersonRefLfnr() {
        return personRefLfnr;
    }

    /**
     * Legt den Wert der personRefLfnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPersonRefLfnr(Integer value) {
        this.personRefLfnr = value;
    }

}
