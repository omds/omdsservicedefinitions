
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateOfferResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Responseobjekts für ein Offert Besitz
 * 
 * <p>Java-Klasse für CreateOfferSachPrivatResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferSachPrivatResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}OffertSachPrivat_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferSachPrivatResponse_Type", propOrder = {
    "offertantwort"
})
public class CreateOfferSachPrivatResponseType
    extends CreateOfferResponseGenType
{

    @XmlElement(name = "Offertantwort", required = true)
    protected OffertSachPrivatType offertantwort;

    /**
     * Ruft den Wert der offertantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OffertSachPrivatType }
     *     
     */
    public OffertSachPrivatType getOffertantwort() {
        return offertantwort;
    }

    /**
     * Legt den Wert der offertantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OffertSachPrivatType }
     *     
     */
    public void setOffertantwort(OffertSachPrivatType value) {
        this.offertantwort = value;
    }

}
