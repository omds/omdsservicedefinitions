
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Fragen FATCA bei natürlichen Personen
 * 
 * <p>Java-Klasse für FATCA_NatPersonType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FATCA_NatPersonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="US_Indizien" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="US_Steuerpflicht" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FATCA_NatPersonType", propOrder = {
    "usIndizien",
    "usSteuerpflicht"
})
public class FATCANatPersonType {

    @XmlElement(name = "US_Indizien")
    protected boolean usIndizien;
    @XmlElement(name = "US_Steuerpflicht")
    protected boolean usSteuerpflicht;

    /**
     * Ruft den Wert der usIndizien-Eigenschaft ab.
     * 
     */
    public boolean isUSIndizien() {
        return usIndizien;
    }

    /**
     * Legt den Wert der usIndizien-Eigenschaft fest.
     * 
     */
    public void setUSIndizien(boolean value) {
        this.usIndizien = value;
    }

    /**
     * Ruft den Wert der usSteuerpflicht-Eigenschaft ab.
     * 
     */
    public boolean isUSSteuerpflicht() {
        return usSteuerpflicht;
    }

    /**
     * Legt den Wert der usSteuerpflicht-Eigenschaft fest.
     * 
     */
    public void setUSSteuerpflicht(boolean value) {
        this.usSteuerpflicht = value;
    }

}
