
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ zur Übermittlung von Anforderungen an eine Autorisierung (z.B. einer elektronischen Unterschrift)
 * 
 * <p>Java-Klasse für AutorisierungsAnforderung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AutorisierungsAnforderung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AutorisierungsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LfnrPerson" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *         &lt;element name="Rolle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Autorisierungsart" type="{urn:omds3CommonServiceTypes-1-1-0}Autorisierungsart_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutorisierungsAnforderung_Type", propOrder = {
    "autorisierungsId",
    "lfnrPerson",
    "rolle",
    "autorisierungsart"
})
public class AutorisierungsAnforderungType {

    @XmlElement(name = "AutorisierungsId")
    protected String autorisierungsId;
    @XmlElement(name = "LfnrPerson")
    @XmlSchemaType(name = "unsignedByte")
    protected short lfnrPerson;
    @XmlElement(name = "Rolle")
    protected String rolle;
    @XmlElement(name = "Autorisierungsart", required = true)
    protected List<AutorisierungsartType> autorisierungsart;

    /**
     * Ruft den Wert der autorisierungsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutorisierungsId() {
        return autorisierungsId;
    }

    /**
     * Legt den Wert der autorisierungsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutorisierungsId(String value) {
        this.autorisierungsId = value;
    }

    /**
     * Ruft den Wert der lfnrPerson-Eigenschaft ab.
     * 
     */
    public short getLfnrPerson() {
        return lfnrPerson;
    }

    /**
     * Legt den Wert der lfnrPerson-Eigenschaft fest.
     * 
     */
    public void setLfnrPerson(short value) {
        this.lfnrPerson = value;
    }

    /**
     * Ruft den Wert der rolle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolle() {
        return rolle;
    }

    /**
     * Legt den Wert der rolle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolle(String value) {
        this.rolle = value;
    }

    /**
     * Gets the value of the autorisierungsart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the autorisierungsart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutorisierungsart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutorisierungsartType }
     * 
     * 
     */
    public List<AutorisierungsartType> getAutorisierungsart() {
        if (autorisierungsart == null) {
            autorisierungsart = new ArrayList<AutorisierungsartType>();
        }
        return this.autorisierungsart;
    }

}
