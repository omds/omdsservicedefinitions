
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Status_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="Status_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="OK"/&gt;
 *     &lt;enumeration value="OKNA"/&gt;
 *     &lt;enumeration value="NOK"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Status_Type")
@XmlEnum
public enum StatusType {


    /**
     * Request konnte verarbeitet werden
     * 
     */
    OK,

    /**
     * Request konnte nicht abschließend verarbeitet werden. Zum Beispiel wurde der Request in einen Workflow eingestellt aber noch nicht auf den Bestand angewendet.
     * 
     */
    OKNA,

    /**
     * Ein Fehler ist aufgetreten, Request konnte nicht verarbeitet werden
     * 
     */
    NOK;

    public String value() {
        return name();
    }

    public static StatusType fromValue(String v) {
        return valueOf(v);
    }

}
