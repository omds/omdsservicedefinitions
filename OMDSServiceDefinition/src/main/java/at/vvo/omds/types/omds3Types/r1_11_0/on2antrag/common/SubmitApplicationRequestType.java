
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DateianhangType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokumentType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.SubmitApplicationKfzRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Request für die Antragsüberleitung
 * 
 * <p>Java-Klasse für SubmitApplicationRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}BOAProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokument_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Antragsnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationRequest_Type", propOrder = {
    "dateianhaenge",
    "dokumente",
    "antragsnummer"
})
@XmlSeeAlso({
    SubmitApplicationKfzRequestType.class,
    SubmitApplicationRequestGenType.class
})
public abstract class SubmitApplicationRequestType
    extends BOAProcessRequestType
{

    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;
    @XmlElement(name = "Dokumente")
    protected List<ProzessDokumentType> dokumente;
    @XmlElement(name = "Antragsnummer")
    protected ObjektIdType antragsnummer;

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokumentType }
     * 
     * 
     */
    public List<ProzessDokumentType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<ProzessDokumentType>();
        }
        return this.dokumente;
    }

    /**
     * Ruft den Wert der antragsnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getAntragsnummer() {
        return antragsnummer;
    }

    /**
     * Legt den Wert der antragsnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setAntragsnummer(ObjektIdType value) {
        this.antragsnummer = value;
    }

}
