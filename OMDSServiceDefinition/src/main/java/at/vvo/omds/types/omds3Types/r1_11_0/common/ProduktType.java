
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.ProduktKfzType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.ZusatzproduktKfzType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Basistyp für ein Produkt
 * 
 * <p>Java-Klasse für Produkt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Produkt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VtgBeg" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="Produktgeneration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zahlweg" type="{urn:omds20}ZahlWegCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Zahlrhythmus" type="{urn:omds20}ZahlRhythmCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Hauptfaelligkeit" type="{urn:omds3CommonServiceTypes-1-1-0}Hauptfaelligkeit_Type" minOccurs="0"/&gt;
 *         &lt;element name="RefSicherstellungLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheProduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheProduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Produkt_Type", propOrder = {
    "vtgBeg",
    "produktgeneration",
    "zahlweg",
    "zahlrhythmus",
    "hauptfaelligkeit",
    "refSicherstellungLfnr",
    "zusaetzlicheProduktdaten"
})
@XmlSeeAlso({
    ProduktKfzType.class,
    ZusatzproduktKfzType.class,
    ProduktGenerischType.class
})
public abstract class ProduktType
    extends ProduktbausteinType
{

    @XmlElement(name = "VtgBeg")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar vtgBeg;
    @XmlElement(name = "Produktgeneration")
    protected String produktgeneration;
    @XmlElement(name = "Zahlweg")
    protected String zahlweg;
    @XmlElement(name = "Zahlrhythmus")
    protected String zahlrhythmus;
    @XmlElement(name = "Hauptfaelligkeit")
    @XmlSchemaType(name = "gMonthDay")
    protected XMLGregorianCalendar hauptfaelligkeit;
    @XmlElement(name = "RefSicherstellungLfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer refSicherstellungLfnr;
    @XmlElement(name = "ZusaetzlicheProduktdaten")
    protected List<ZusaetzlicheProduktdatenType> zusaetzlicheProduktdaten;

    /**
     * Ruft den Wert der vtgBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgBeg() {
        return vtgBeg;
    }

    /**
     * Legt den Wert der vtgBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgBeg(XMLGregorianCalendar value) {
        this.vtgBeg = value;
    }

    /**
     * Ruft den Wert der produktgeneration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduktgeneration() {
        return produktgeneration;
    }

    /**
     * Legt den Wert der produktgeneration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduktgeneration(String value) {
        this.produktgeneration = value;
    }

    /**
     * Ruft den Wert der zahlweg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlweg() {
        return zahlweg;
    }

    /**
     * Legt den Wert der zahlweg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlweg(String value) {
        this.zahlweg = value;
    }

    /**
     * Ruft den Wert der zahlrhythmus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlrhythmus() {
        return zahlrhythmus;
    }

    /**
     * Legt den Wert der zahlrhythmus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlrhythmus(String value) {
        this.zahlrhythmus = value;
    }

    /**
     * Ruft den Wert der hauptfaelligkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHauptfaelligkeit() {
        return hauptfaelligkeit;
    }

    /**
     * Legt den Wert der hauptfaelligkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHauptfaelligkeit(XMLGregorianCalendar value) {
        this.hauptfaelligkeit = value;
    }

    /**
     * Ruft den Wert der refSicherstellungLfnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefSicherstellungLfnr() {
        return refSicherstellungLfnr;
    }

    /**
     * Legt den Wert der refSicherstellungLfnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefSicherstellungLfnr(Integer value) {
        this.refSicherstellungLfnr = value;
    }

    /**
     * Gets the value of the zusaetzlicheProduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheProduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheProduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheProduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheProduktdatenType> getZusaetzlicheProduktdaten() {
        if (zusaetzlicheProduktdaten == null) {
            zusaetzlicheProduktdaten = new ArrayList<ZusaetzlicheProduktdatenType>();
        }
        return this.zusaetzlicheProduktdaten;
    }

}
