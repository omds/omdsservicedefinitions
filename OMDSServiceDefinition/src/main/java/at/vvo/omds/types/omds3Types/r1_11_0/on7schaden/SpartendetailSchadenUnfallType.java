
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Spartenerweiterung der Schadenmeldung für Unfall
 * 
 * <p>Java-Klasse für SpartendetailSchadenUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpartendetailSchadenUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SpartendetailSchaden_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ArbeitsunfaehigVon" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="ArbeitsunfaehigBis" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="Diagnose" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BehandlerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Koerperhaelfte" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte"&gt;
 *               &lt;enumeration value="0"/&gt;
 *               &lt;enumeration value="1"/&gt;
 *               &lt;enumeration value="2"/&gt;
 *               &lt;enumeration value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Koerperteil" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="BehoerdlicheAufnahme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpartendetailSchadenUnfall_Type", propOrder = {
    "arbeitsunfaehigVon",
    "arbeitsunfaehigBis",
    "diagnose",
    "behandlerName",
    "koerperhaelfte",
    "koerperteil",
    "behoerdlicheAufnahme"
})
public class SpartendetailSchadenUnfallType
    extends SpartendetailSchadenType
{

    @XmlElement(name = "ArbeitsunfaehigVon")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar arbeitsunfaehigVon;
    @XmlElement(name = "ArbeitsunfaehigBis")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar arbeitsunfaehigBis;
    @XmlElement(name = "Diagnose", required = true)
    protected String diagnose;
    @XmlElement(name = "BehandlerName")
    protected String behandlerName;
    @XmlElement(name = "Koerperhaelfte")
    protected Byte koerperhaelfte;
    @XmlElement(name = "Koerperteil")
    protected List<String> koerperteil;
    @XmlElement(name = "BehoerdlicheAufnahme")
    protected String behoerdlicheAufnahme;

    /**
     * Ruft den Wert der arbeitsunfaehigVon-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArbeitsunfaehigVon() {
        return arbeitsunfaehigVon;
    }

    /**
     * Legt den Wert der arbeitsunfaehigVon-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArbeitsunfaehigVon(XMLGregorianCalendar value) {
        this.arbeitsunfaehigVon = value;
    }

    /**
     * Ruft den Wert der arbeitsunfaehigBis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArbeitsunfaehigBis() {
        return arbeitsunfaehigBis;
    }

    /**
     * Legt den Wert der arbeitsunfaehigBis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArbeitsunfaehigBis(XMLGregorianCalendar value) {
        this.arbeitsunfaehigBis = value;
    }

    /**
     * Ruft den Wert der diagnose-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiagnose() {
        return diagnose;
    }

    /**
     * Legt den Wert der diagnose-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiagnose(String value) {
        this.diagnose = value;
    }

    /**
     * Ruft den Wert der behandlerName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBehandlerName() {
        return behandlerName;
    }

    /**
     * Legt den Wert der behandlerName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBehandlerName(String value) {
        this.behandlerName = value;
    }

    /**
     * Ruft den Wert der koerperhaelfte-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getKoerperhaelfte() {
        return koerperhaelfte;
    }

    /**
     * Legt den Wert der koerperhaelfte-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setKoerperhaelfte(Byte value) {
        this.koerperhaelfte = value;
    }

    /**
     * Gets the value of the koerperteil property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the koerperteil property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKoerperteil().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getKoerperteil() {
        if (koerperteil == null) {
            koerperteil = new ArrayList<String>();
        }
        return this.koerperteil;
    }

    /**
     * Ruft den Wert der behoerdlicheAufnahme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBehoerdlicheAufnahme() {
        return behoerdlicheAufnahme;
    }

    /**
     * Legt den Wert der behoerdlicheAufnahme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBehoerdlicheAufnahme(String value) {
        this.behoerdlicheAufnahme = value;
    }

}
