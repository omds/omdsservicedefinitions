
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VorversicherungenDetailType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VorversicherungenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Vorversicherungen, Implementierung speziell für Kfz. Alternativ siehe auch allgemeine spartenübergreifende Implementierung cst:VorversicherungenImpl_Type.
 * 
 * <p>Java-Klasse für VorversicherungenKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VorversicherungenKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Vorversicherungen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VorversicherungKfz" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VorversicherungenDetail_Type"&gt;
 *                 &lt;attribute name="VtgSparteCd" type="{urn:omds20}VtgSparteCd_Type" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="VorversicherungRechtsschutz" type="{urn:omds3CommonServiceTypes-1-1-0}VorversicherungenDetail_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VorversicherungenKfz_Type", propOrder = {
    "vorversicherungKfz",
    "vorversicherungRechtsschutz"
})
public class VorversicherungenKfzType
    extends VorversicherungenType
{

    @XmlElement(name = "VorversicherungKfz")
    protected List<VorversicherungenKfzType.VorversicherungKfz> vorversicherungKfz;
    @XmlElement(name = "VorversicherungRechtsschutz")
    protected VorversicherungenDetailType vorversicherungRechtsschutz;

    /**
     * Gets the value of the vorversicherungKfz property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vorversicherungKfz property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVorversicherungKfz().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VorversicherungenKfzType.VorversicherungKfz }
     * 
     * 
     */
    public List<VorversicherungenKfzType.VorversicherungKfz> getVorversicherungKfz() {
        if (vorversicherungKfz == null) {
            vorversicherungKfz = new ArrayList<VorversicherungenKfzType.VorversicherungKfz>();
        }
        return this.vorversicherungKfz;
    }

    /**
     * Ruft den Wert der vorversicherungRechtsschutz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VorversicherungenDetailType }
     *     
     */
    public VorversicherungenDetailType getVorversicherungRechtsschutz() {
        return vorversicherungRechtsschutz;
    }

    /**
     * Legt den Wert der vorversicherungRechtsschutz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VorversicherungenDetailType }
     *     
     */
    public void setVorversicherungRechtsschutz(VorversicherungenDetailType value) {
        this.vorversicherungRechtsschutz = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VorversicherungenDetail_Type"&gt;
     *       &lt;attribute name="VtgSparteCd" type="{urn:omds20}VtgSparteCd_Type" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class VorversicherungKfz
        extends VorversicherungenDetailType
    {

        @XmlAttribute(name = "VtgSparteCd", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
        protected String vtgSparteCd;

        /**
         * Ruft den Wert der vtgSparteCd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVtgSparteCd() {
            return vtgSparteCd;
        }

        /**
         * Legt den Wert der vtgSparteCd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVtgSparteCd(String value) {
            this.vtgSparteCd = value;
        }

    }

}
