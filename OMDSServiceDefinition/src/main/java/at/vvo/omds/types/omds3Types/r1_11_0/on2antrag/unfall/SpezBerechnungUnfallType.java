
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.SpezBerechnungType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für den Schritt Berechnung
 * 
 * <p>Java-Klasse für SpezBerechnungUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezBerechnungUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezBerechnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}VerkaufsproduktUnfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezBerechnungUnfall_Type", propOrder = {
    "verkaufsprodukt"
})
public class SpezBerechnungUnfallType
    extends SpezBerechnungType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktUnfallType verkaufsprodukt;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktUnfallType }
     *     
     */
    public VerkaufsproduktUnfallType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktUnfallType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktUnfallType value) {
        this.verkaufsprodukt = value;
    }

}
