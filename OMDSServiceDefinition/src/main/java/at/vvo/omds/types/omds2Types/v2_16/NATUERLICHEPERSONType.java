
package at.vvo.omds.types.omds2Types.v2_16;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für NATUERLICHE_PERSON_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NATUERLICHE_PERSON_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Familienname" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="80"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Vorname"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="GeschlechtCd" type="{urn:omds20}GeschlechtCd_Type" /&gt;
 *       &lt;attribute name="Gebdat" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="LandesCd" type="{urn:omds20}LandesCd_Type" /&gt;
 *       &lt;attribute name="FamilienstandCd" type="{urn:omds20}FamilienstandCd_Type" /&gt;
 *       &lt;attribute name="GebLandCd" type="{urn:omds20}LandesCd_Type" /&gt;
 *       &lt;attribute name="HauptWohnLandCd" type="{urn:omds20}LandesCd_Type" /&gt;
 *       &lt;attribute name="PersonID_VU" type="{urn:omds20}PersonID_Type" /&gt;
 *       &lt;attribute name="PersonID_Makler" type="{urn:omds20}PersonID_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NATUERLICHE_PERSON_Type")
public class NATUERLICHEPERSONType {

    @XmlAttribute(name = "Familienname", required = true)
    protected String familienname;
    @XmlAttribute(name = "Vorname")
    protected String vorname;
    @XmlAttribute(name = "GeschlechtCd")
    protected String geschlechtCd;
    @XmlAttribute(name = "Gebdat")
    protected XMLGregorianCalendar gebdat;
    @XmlAttribute(name = "LandesCd")
    protected String landesCd;
    @XmlAttribute(name = "FamilienstandCd")
    protected String familienstandCd;
    @XmlAttribute(name = "GebLandCd")
    protected String gebLandCd;
    @XmlAttribute(name = "HauptWohnLandCd")
    protected String hauptWohnLandCd;
    @XmlAttribute(name = "PersonID_VU")
    protected String personIDVU;
    @XmlAttribute(name = "PersonID_Makler")
    protected String personIDMakler;

    /**
     * Ruft den Wert der familienname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilienname() {
        return familienname;
    }

    /**
     * Legt den Wert der familienname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilienname(String value) {
        this.familienname = value;
    }

    /**
     * Ruft den Wert der vorname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Legt den Wert der vorname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorname(String value) {
        this.vorname = value;
    }

    /**
     * Ruft den Wert der geschlechtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeschlechtCd() {
        return geschlechtCd;
    }

    /**
     * Legt den Wert der geschlechtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeschlechtCd(String value) {
        this.geschlechtCd = value;
    }

    /**
     * Ruft den Wert der gebdat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGebdat() {
        return gebdat;
    }

    /**
     * Legt den Wert der gebdat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGebdat(XMLGregorianCalendar value) {
        this.gebdat = value;
    }

    /**
     * Ruft den Wert der landesCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCd() {
        return landesCd;
    }

    /**
     * Legt den Wert der landesCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCd(String value) {
        this.landesCd = value;
    }

    /**
     * Ruft den Wert der familienstandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilienstandCd() {
        return familienstandCd;
    }

    /**
     * Legt den Wert der familienstandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilienstandCd(String value) {
        this.familienstandCd = value;
    }

    /**
     * Ruft den Wert der gebLandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebLandCd() {
        return gebLandCd;
    }

    /**
     * Legt den Wert der gebLandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebLandCd(String value) {
        this.gebLandCd = value;
    }

    /**
     * Ruft den Wert der hauptWohnLandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHauptWohnLandCd() {
        return hauptWohnLandCd;
    }

    /**
     * Legt den Wert der hauptWohnLandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHauptWohnLandCd(String value) {
        this.hauptWohnLandCd = value;
    }

    /**
     * Ruft den Wert der personIDVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonIDVU() {
        return personIDVU;
    }

    /**
     * Legt den Wert der personIDVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonIDVU(String value) {
        this.personIDVU = value;
    }

    /**
     * Ruft den Wert der personIDMakler-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonIDMakler() {
        return personIDMakler;
    }

    /**
     * Legt den Wert der personIDMakler-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonIDMakler(String value) {
        this.personIDMakler = value;
    }

}
