
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Bereitstellung eines Dokuments durch die VU für den Kunden, nicht zu unterschreiben
 * 
 * <p>Java-Klasse für ProzessDokHandout_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProzessDokHandout_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokumentBasis_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DokData" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentData_Type"/&gt;
 *         &lt;element name="ArtAusfolgung"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *               &lt;enumeration value="0"/&gt;
 *               &lt;enumeration value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProzessDokHandout_Type", propOrder = {
    "dokData",
    "artAusfolgung"
})
public class ProzessDokHandoutType
    extends ProzessDokumentBasisType
{

    @XmlElement(name = "DokData", required = true)
    protected DokumentDataType dokData;
    @XmlElement(name = "ArtAusfolgung")
    protected short artAusfolgung;

    /**
     * Ruft den Wert der dokData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DokumentDataType }
     *     
     */
    public DokumentDataType getDokData() {
        return dokData;
    }

    /**
     * Legt den Wert der dokData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DokumentDataType }
     *     
     */
    public void setDokData(DokumentDataType value) {
        this.dokData = value;
    }

    /**
     * Ruft den Wert der artAusfolgung-Eigenschaft ab.
     * 
     */
    public short getArtAusfolgung() {
        return artAusfolgung;
    }

    /**
     * Legt den Wert der artAusfolgung-Eigenschaft fest.
     * 
     */
    public void setArtAusfolgung(short value) {
        this.artAusfolgung = value;
    }

}
