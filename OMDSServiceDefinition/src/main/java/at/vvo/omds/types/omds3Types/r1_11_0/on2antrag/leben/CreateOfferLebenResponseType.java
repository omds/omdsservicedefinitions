
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateOfferResponseGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type des Responseobjekts für eine Erstellung eines Leben-Offerts
 * 
 * <p>Java-Klasse für CreateOfferLebenResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferLebenResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferResponseGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertantwort" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}SpezOffertLeben_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferLebenResponse_Type", propOrder = {
    "offertantwort"
})
public class CreateOfferLebenResponseType
    extends CreateOfferResponseGenType
{

    @XmlElement(name = "Offertantwort", required = true)
    protected SpezOffertLebenType offertantwort;

    /**
     * Ruft den Wert der offertantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezOffertLebenType }
     *     
     */
    public SpezOffertLebenType getOffertantwort() {
        return offertantwort;
    }

    /**
     * Legt den Wert der offertantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezOffertLebenType }
     *     
     */
    public void setOffertantwort(SpezOffertLebenType value) {
        this.offertantwort = value;
    }

}
