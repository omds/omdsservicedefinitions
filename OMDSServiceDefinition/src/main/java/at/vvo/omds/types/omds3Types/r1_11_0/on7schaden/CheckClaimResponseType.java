
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Anworttyp beim Erzeugen einer Schadenmeldung
 * 
 * <p>Java-Klasse für CheckClaimResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CheckClaimResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Schadenereignis" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenereignis_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckClaimResponse_Type", propOrder = {
    "schadenereignis"
})
public class CheckClaimResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Schadenereignis")
    protected SchadenereignisType schadenereignis;

    /**
     * Ruft den Wert der schadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenereignisType }
     *     
     */
    public SchadenereignisType getSchadenereignis() {
        return schadenereignis;
    }

    /**
     * Legt den Wert der schadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenereignisType }
     *     
     */
    public void setSchadenereignis(SchadenereignisType value) {
        this.schadenereignis = value;
    }

}
