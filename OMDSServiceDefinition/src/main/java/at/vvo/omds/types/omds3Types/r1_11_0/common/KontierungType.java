
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.math.BigInteger;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Sollen nicht automatisch die Kontierungsdaten aus dem hinterlegten Benutzer genommen werden, können hier abweichende Kontierungen angegeben werden, insbesondere
 * 				wenn mehrere Vermittler an dem Vertrag beteiligt werden sollen. In der Regel wird die Aufteilung für Abschluss-, Folge- und Betreuungsprovision über alle Vermittler je Kategorie 100 Prozent ergeben.
 * 			  Es gibt aber auch Fälle, in denen die prozentuale Aufteilung nicht mit dem Antrag übermittelt wird, sondern an anderer Stelle festgelegt wird. Es sollen dann nur die beteiligten Vermittler übermittelt werden.
 * 				Daher können die prozentualen Angaben ab Version 1.9 auch entfallen.
 * 			
 * 
 * <p>Java-Klasse für Kontierung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Kontierung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vermittlernummer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Vermittlername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Abschluss" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="Folge" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="Betreuung" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Kontierung_Type", propOrder = {
    "vermittlernummer",
    "vermittlername",
    "abschluss",
    "folge",
    "betreuung"
})
public class KontierungType {

    @XmlElement(name = "Vermittlernummer", required = true)
    protected String vermittlernummer;
    @XmlElement(name = "Vermittlername")
    protected String vermittlername;
    @XmlElement(name = "Abschluss")
    protected BigInteger abschluss;
    @XmlElement(name = "Folge")
    protected BigInteger folge;
    @XmlElement(name = "Betreuung")
    protected BigInteger betreuung;

    /**
     * Ruft den Wert der vermittlernummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermittlernummer() {
        return vermittlernummer;
    }

    /**
     * Legt den Wert der vermittlernummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermittlernummer(String value) {
        this.vermittlernummer = value;
    }

    /**
     * Ruft den Wert der vermittlername-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermittlername() {
        return vermittlername;
    }

    /**
     * Legt den Wert der vermittlername-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermittlername(String value) {
        this.vermittlername = value;
    }

    /**
     * Ruft den Wert der abschluss-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAbschluss() {
        return abschluss;
    }

    /**
     * Legt den Wert der abschluss-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAbschluss(BigInteger value) {
        this.abschluss = value;
    }

    /**
     * Ruft den Wert der folge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFolge() {
        return folge;
    }

    /**
     * Legt den Wert der folge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFolge(BigInteger value) {
        this.folge = value;
    }

    /**
     * Ruft den Wert der betreuung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBetreuung() {
        return betreuung;
    }

    /**
     * Legt den Wert der betreuung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBetreuung(BigInteger value) {
        this.betreuung = value;
    }

}
