
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Basistyp für Metadaten von Attributen
 * 
 * <p>Java-Klasse für AttributMetadaten_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttributMetadaten_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Attribut" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="Aenderbar" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Bezeichnung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BeschreibungTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributMetadaten_Type", propOrder = {
    "attribut",
    "aenderbar",
    "bezeichnung",
    "beschreibungTxt"
})
@XmlSeeAlso({
    AttributMetadatenStringType.class,
    AttributMetadatenIntType.class,
    AttributMetadatenDezimalType.class,
    AttributMetadatenDatumType.class,
    AttributMetadatenEnumType.class
})
public abstract class AttributMetadatenType {

    @XmlElement(name = "Attribut", required = true)
    protected Object attribut;
    @XmlElement(name = "Aenderbar")
    protected boolean aenderbar;
    @XmlElement(name = "Bezeichnung")
    protected String bezeichnung;
    @XmlElement(name = "BeschreibungTxt")
    protected String beschreibungTxt;

    /**
     * Ruft den Wert der attribut-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAttribut() {
        return attribut;
    }

    /**
     * Legt den Wert der attribut-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAttribut(Object value) {
        this.attribut = value;
    }

    /**
     * Ruft den Wert der aenderbar-Eigenschaft ab.
     * 
     */
    public boolean isAenderbar() {
        return aenderbar;
    }

    /**
     * Legt den Wert der aenderbar-Eigenschaft fest.
     * 
     */
    public void setAenderbar(boolean value) {
        this.aenderbar = value;
    }

    /**
     * Ruft den Wert der bezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Legt den Wert der bezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

    /**
     * Ruft den Wert der beschreibungTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeschreibungTxt() {
        return beschreibungTxt;
    }

    /**
     * Legt den Wert der beschreibungTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeschreibungTxt(String value) {
        this.beschreibungTxt = value;
    }

}
