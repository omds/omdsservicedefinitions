
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Spezifischerer Typ für ein Gebäudeinhalts-Elementarprodukt für einen Ansatz, in dem mit Vererbung für die einzelnen Sparten gearbeitet wird
 * 
 * <p>Java-Klasse für ElementarproduktHaushalt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementarproduktHaushalt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}ElementarproduktSachPrivat_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementarproduktHaushalt_Type")
@XmlSeeAlso({
    GenElementarproduktHaushaltType.class
})
public class ElementarproduktHaushaltType
    extends ElementarproduktSachPrivatType
{


}
