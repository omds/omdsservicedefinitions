
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementarproduktType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakte Basisklasse für KFZ-Elementarprodukte
 * 
 * <p>Java-Klasse für ElementarproduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementarproduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Elementarprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}FahrzeugRefLfdNr" minOccurs="0"/&gt;
 *         &lt;element name="LeasingVerbundUnternehmen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementarproduktKfz_Type", propOrder = {
    "fahrzeugRefLfdNr",
    "leasingVerbundUnternehmen"
})
@XmlSeeAlso({
    HaftpflichtKfzType.class,
    KaskoKfzType.class,
    InsassenUnfallKfzType.class,
    LenkerUnfallKfzType.class,
    AssistanceKfzType.class
})
public abstract class ElementarproduktKfzType
    extends ElementarproduktType
{

    @XmlElement(name = "FahrzeugRefLfdNr")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer fahrzeugRefLfdNr;
    @XmlElement(name = "LeasingVerbundUnternehmen")
    protected Boolean leasingVerbundUnternehmen;

    /**
     * Ruft den Wert der fahrzeugRefLfdNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFahrzeugRefLfdNr() {
        return fahrzeugRefLfdNr;
    }

    /**
     * Legt den Wert der fahrzeugRefLfdNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFahrzeugRefLfdNr(Integer value) {
        this.fahrzeugRefLfdNr = value;
    }

    /**
     * Ruft den Wert der leasingVerbundUnternehmen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLeasingVerbundUnternehmen() {
        return leasingVerbundUnternehmen;
    }

    /**
     * Legt den Wert der leasingVerbundUnternehmen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLeasingVerbundUnternehmen(Boolean value) {
        this.leasingVerbundUnternehmen = value;
    }

}
