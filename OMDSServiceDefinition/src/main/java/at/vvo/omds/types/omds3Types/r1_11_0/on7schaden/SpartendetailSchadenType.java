
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Eine Erweiterung der Schadenmeldung fuer spezielle Sparten
 * 
 * <p>Java-Klasse für SpartendetailSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpartendetailSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpartendetailSchaden_Type")
@XmlSeeAlso({
    SpartendetailSchadenKfzType.class,
    SpartendetailSchadenUnfallType.class,
    SpartendetailSchadenKrankenType.class
})
public abstract class SpartendetailSchadenType {


}
