
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.PersonType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VersichertesInteresseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Versicherungsnehmer" type="{urn:omds3CommonServiceTypes-1-1-0}Person_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="VersInteresse" type="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type" minOccurs="0"/&gt;
 *         &lt;element name="EigenschaftCd" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheDeckungsauskunftsdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheDeckungsauskunftsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "versicherungsnehmer",
    "polizzennr",
    "vertragsID",
    "versInteresse",
    "eigenschaftCd",
    "zusaetzlicheDeckungsauskunftsdaten"
})
@XmlRootElement(name = "CheckCoverageRequest")
public class CheckCoverageRequest
    extends CommonRequestType
{

    @XmlElement(name = "Versicherungsnehmer")
    protected List<PersonType> versicherungsnehmer;
    @XmlElement(name = "Polizzennr")
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "VersInteresse")
    protected VersichertesInteresseType versInteresse;
    @XmlElement(name = "EigenschaftCd")
    protected List<String> eigenschaftCd;
    @XmlElement(name = "ZusaetzlicheDeckungsauskunftsdaten")
    protected List<ZusaetzlicheDeckungsauskunftsdatenType> zusaetzlicheDeckungsauskunftsdaten;

    /**
     * Gets the value of the versicherungsnehmer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the versicherungsnehmer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersicherungsnehmer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersonType }
     * 
     * 
     */
    public List<PersonType> getVersicherungsnehmer() {
        if (versicherungsnehmer == null) {
            versicherungsnehmer = new ArrayList<PersonType>();
        }
        return this.versicherungsnehmer;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der versInteresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VersichertesInteresseType }
     *     
     */
    public VersichertesInteresseType getVersInteresse() {
        return versInteresse;
    }

    /**
     * Legt den Wert der versInteresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VersichertesInteresseType }
     *     
     */
    public void setVersInteresse(VersichertesInteresseType value) {
        this.versInteresse = value;
    }

    /**
     * Gets the value of the eigenschaftCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the eigenschaftCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEigenschaftCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEigenschaftCd() {
        if (eigenschaftCd == null) {
            eigenschaftCd = new ArrayList<String>();
        }
        return this.eigenschaftCd;
    }

    /**
     * Gets the value of the zusaetzlicheDeckungsauskunftsdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheDeckungsauskunftsdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheDeckungsauskunftsdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheDeckungsauskunftsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheDeckungsauskunftsdatenType> getZusaetzlicheDeckungsauskunftsdaten() {
        if (zusaetzlicheDeckungsauskunftsdaten == null) {
            zusaetzlicheDeckungsauskunftsdaten = new ArrayList<ZusaetzlicheDeckungsauskunftsdatenType>();
        }
        return this.zusaetzlicheDeckungsauskunftsdaten;
    }

}
