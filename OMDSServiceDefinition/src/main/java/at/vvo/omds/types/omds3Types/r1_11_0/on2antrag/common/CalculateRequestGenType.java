
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.CalculateKrankenRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.CalculateLebenRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CalculateSachPrivatRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.CalculateUnfallRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Request für die Berechnung mit generischen Produktbausteinen
 * 
 * <p>Java-Klasse für CalculateRequestGen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateRequestGen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Produktmetadaten" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateRequestGen_Type", propOrder = {
    "produktmetadaten"
})
@XmlSeeAlso({
    CalculateSachPrivatRequestType.class,
    CalculateUnfallRequestType.class,
    CalculateLebenRequestType.class,
    CalculateKrankenRequest.class
})
public abstract class CalculateRequestGenType
    extends CalculateRequestType
{

    @XmlElement(name = "Produktmetadaten")
    protected boolean produktmetadaten;

    /**
     * Ruft den Wert der produktmetadaten-Eigenschaft ab.
     * 
     */
    public boolean isProduktmetadaten() {
        return produktmetadaten;
    }

    /**
     * Legt den Wert der produktmetadaten-Eigenschaft fest.
     * 
     */
    public void setProduktmetadaten(boolean value) {
        this.produktmetadaten = value;
    }

}
