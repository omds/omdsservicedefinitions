
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für den Upload / die Bereitstellung eines Dokuments als Teil eines Geschäftsfalls durch den Serviceaufrufer.
 * 
 * <p>Java-Klasse für ProzessDokument_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProzessDokument_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DokAnforderungsId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="Dokumenttyp" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Beschreibung" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="200"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Autorisierungen" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Autorisierung_Type"&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DokumentHinterlegt" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="DokData" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentData_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProzessDokument_Type", propOrder = {
    "dokAnforderungsId",
    "dokumenttyp",
    "beschreibung",
    "autorisierungen",
    "dokumentHinterlegt",
    "dokData"
})
public class ProzessDokumentType {

    @XmlElement(name = "DokAnforderungsId")
    protected ObjektIdType dokAnforderungsId;
    @XmlElement(name = "Dokumenttyp", required = true)
    protected String dokumenttyp;
    @XmlElement(name = "Beschreibung")
    protected String beschreibung;
    @XmlElement(name = "Autorisierungen")
    protected List<ProzessDokumentType.Autorisierungen> autorisierungen;
    @XmlElement(name = "DokumentHinterlegt")
    protected boolean dokumentHinterlegt;
    @XmlElement(name = "DokData")
    protected DokumentDataType dokData;

    /**
     * Ruft den Wert der dokAnforderungsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getDokAnforderungsId() {
        return dokAnforderungsId;
    }

    /**
     * Legt den Wert der dokAnforderungsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setDokAnforderungsId(ObjektIdType value) {
        this.dokAnforderungsId = value;
    }

    /**
     * Ruft den Wert der dokumenttyp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDokumenttyp() {
        return dokumenttyp;
    }

    /**
     * Legt den Wert der dokumenttyp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDokumenttyp(String value) {
        this.dokumenttyp = value;
    }

    /**
     * Ruft den Wert der beschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * Legt den Wert der beschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeschreibung(String value) {
        this.beschreibung = value;
    }

    /**
     * Gets the value of the autorisierungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the autorisierungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutorisierungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokumentType.Autorisierungen }
     * 
     * 
     */
    public List<ProzessDokumentType.Autorisierungen> getAutorisierungen() {
        if (autorisierungen == null) {
            autorisierungen = new ArrayList<ProzessDokumentType.Autorisierungen>();
        }
        return this.autorisierungen;
    }

    /**
     * Ruft den Wert der dokumentHinterlegt-Eigenschaft ab.
     * 
     */
    public boolean isDokumentHinterlegt() {
        return dokumentHinterlegt;
    }

    /**
     * Legt den Wert der dokumentHinterlegt-Eigenschaft fest.
     * 
     */
    public void setDokumentHinterlegt(boolean value) {
        this.dokumentHinterlegt = value;
    }

    /**
     * Ruft den Wert der dokData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DokumentDataType }
     *     
     */
    public DokumentDataType getDokData() {
        return dokData;
    }

    /**
     * Legt den Wert der dokData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DokumentDataType }
     *     
     */
    public void setDokData(DokumentDataType value) {
        this.dokData = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Autorisierung_Type"&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Autorisierungen
        extends AutorisierungType
    {


    }

}
