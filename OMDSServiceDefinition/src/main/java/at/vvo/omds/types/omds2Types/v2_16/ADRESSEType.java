
package at.vvo.omds.types.omds2Types.v2_16;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.RisikoAdresseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ADRESSE_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ADRESSE_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attGroup ref="{urn:omds20}Adresse_Attribute"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ADRESSE_Type")
@XmlSeeAlso({
    RisikoAdresseType.class
})
public class ADRESSEType {

    @XmlAttribute(name = "Pac")
    @XmlSchemaType(name = "unsignedInt")
    protected Long pac;
    @XmlAttribute(name = "LandesCd")
    protected String landesCd;
    @XmlAttribute(name = "PLZ")
    protected String plz;
    @XmlAttribute(name = "Ort")
    protected String ort;
    @XmlAttribute(name = "Strasse")
    protected String strasse;
    @XmlAttribute(name = "Hausnr")
    protected String hausnr;
    @XmlAttribute(name = "Zusatz")
    protected String zusatz;
    @XmlAttribute(name = "AdressID_VU")
    protected String adressIDVU;
    @XmlAttribute(name = "AdressID_Makler")
    protected String adressIDMakler;

    /**
     * Ruft den Wert der pac-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPac() {
        return pac;
    }

    /**
     * Legt den Wert der pac-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPac(Long value) {
        this.pac = value;
    }

    /**
     * Ruft den Wert der landesCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCd() {
        return landesCd;
    }

    /**
     * Legt den Wert der landesCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCd(String value) {
        this.landesCd = value;
    }

    /**
     * Ruft den Wert der plz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLZ() {
        return plz;
    }

    /**
     * Legt den Wert der plz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLZ(String value) {
        this.plz = value;
    }

    /**
     * Ruft den Wert der ort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Legt den Wert der ort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrt(String value) {
        this.ort = value;
    }

    /**
     * Ruft den Wert der strasse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Legt den Wert der strasse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrasse(String value) {
        this.strasse = value;
    }

    /**
     * Ruft den Wert der hausnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHausnr() {
        return hausnr;
    }

    /**
     * Legt den Wert der hausnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHausnr(String value) {
        this.hausnr = value;
    }

    /**
     * Ruft den Wert der zusatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz() {
        return zusatz;
    }

    /**
     * Legt den Wert der zusatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz(String value) {
        this.zusatz = value;
    }

    /**
     * Ruft den Wert der adressIDVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdressIDVU() {
        return adressIDVU;
    }

    /**
     * Legt den Wert der adressIDVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdressIDVU(String value) {
        this.adressIDVU = value;
    }

    /**
     * Ruft den Wert der adressIDMakler-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdressIDMakler() {
        return adressIDMakler;
    }

    /**
     * Legt den Wert der adressIDMakler-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdressIDMakler(String value) {
        this.adressIDMakler = value;
    }

}
