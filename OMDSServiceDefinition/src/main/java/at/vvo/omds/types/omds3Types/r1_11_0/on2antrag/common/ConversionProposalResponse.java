
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ErsatzpolizzeType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokHandoutType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vorschlag" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezAntrag_Type" minOccurs="0"/&gt;
 *         &lt;element name="Dokument" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokHandout_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Ersatzpolizze" type="{urn:omds3CommonServiceTypes-1-1-0}Ersatzpolizze_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vorschlag",
    "dokument",
    "ersatzpolizze"
})
@XmlRootElement(name = "ConversionProposalResponse")
public class ConversionProposalResponse
    extends CommonResponseType
{

    @XmlElement(name = "Vorschlag")
    protected SpezAntragType vorschlag;
    @XmlElement(name = "Dokument")
    protected List<ProzessDokHandoutType> dokument;
    @XmlElement(name = "Ersatzpolizze")
    protected List<ErsatzpolizzeType> ersatzpolizze;

    /**
     * Ruft den Wert der vorschlag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragType }
     *     
     */
    public SpezAntragType getVorschlag() {
        return vorschlag;
    }

    /**
     * Legt den Wert der vorschlag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragType }
     *     
     */
    public void setVorschlag(SpezAntragType value) {
        this.vorschlag = value;
    }

    /**
     * Gets the value of the dokument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokHandoutType }
     * 
     * 
     */
    public List<ProzessDokHandoutType> getDokument() {
        if (dokument == null) {
            dokument = new ArrayList<ProzessDokHandoutType>();
        }
        return this.dokument;
    }

    /**
     * Gets the value of the ersatzpolizze property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the ersatzpolizze property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErsatzpolizze().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErsatzpolizzeType }
     * 
     * 
     */
    public List<ErsatzpolizzeType> getErsatzpolizze() {
        if (ersatzpolizze == null) {
            ersatzpolizze = new ArrayList<ErsatzpolizzeType>();
        }
        return this.ersatzpolizze;
    }

}
