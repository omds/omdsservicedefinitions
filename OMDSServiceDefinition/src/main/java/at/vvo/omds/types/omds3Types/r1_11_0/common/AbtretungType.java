
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Sicherstellung vom Typ Abtretung / Zession. Abtretung ist die umfassenste Form der
 * 			Sicherstellung der Zessionär (Abtretungsempfänger) kann den Vertrag an Stelle des VN (Zedent) nach
 * 				seinen Wünschen umgestalten.
 * 
 * <p>Java-Klasse für Abtretung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Abtretung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Sicherstellung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Zessionar" type="{urn:omds3CommonServiceTypes-1-1-0}GlaeubigerSicherstellung_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Abtretung_Type", propOrder = {
    "zessionar"
})
public class AbtretungType
    extends SicherstellungType
{

    @XmlElement(name = "Zessionar", required = true)
    protected GlaeubigerSicherstellungType zessionar;

    /**
     * Ruft den Wert der zessionar-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GlaeubigerSicherstellungType }
     *     
     */
    public GlaeubigerSicherstellungType getZessionar() {
        return zessionar;
    }

    /**
     * Legt den Wert der zessionar-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GlaeubigerSicherstellungType }
     *     
     */
    public void setZessionar(GlaeubigerSicherstellungType value) {
        this.zessionar = value;
    }

}
