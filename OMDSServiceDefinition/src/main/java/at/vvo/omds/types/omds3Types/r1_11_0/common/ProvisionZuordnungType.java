
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Zuordnung zu Provisionsnote
 * 
 * <p>Java-Klasse für ProvisionZuordnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProvisionZuordnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}DokumentenZuordnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vermnr" type="{urn:omds20}Vermnr"/&gt;
 *         &lt;element name="BuchDat" type="{urn:omds20}Datum"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvisionZuordnung_Type", propOrder = {
    "vermnr",
    "buchDat"
})
public class ProvisionZuordnungType
    extends DokumentenZuordnungType
{

    @XmlElement(name = "Vermnr", required = true)
    protected String vermnr;
    @XmlElement(name = "BuchDat", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar buchDat;

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

    /**
     * Ruft den Wert der buchDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBuchDat() {
        return buchDat;
    }

    /**
     * Legt den Wert der buchDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBuchDat(XMLGregorianCalendar value) {
        this.buchDat = value;
    }

}
