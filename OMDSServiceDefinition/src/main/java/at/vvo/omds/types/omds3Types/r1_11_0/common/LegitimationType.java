
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import at.vvo.omds.types.omds2Types.v2_16.ELLegitimationType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Daten zur Legitimation des Antragstellers bzw. Kunden
 * 
 * <p>Java-Klasse für Legitimation_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Legitimation_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds20}EL-Legitimation_Type"&gt;
 *       &lt;attribute name="AusstellendesLand" type="{urn:omds20}LandesCd_Type" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Legitimation_Type")
public class LegitimationType
    extends ELLegitimationType
{

    @XmlAttribute(name = "AusstellendesLand", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected String ausstellendesLand;

    /**
     * Ruft den Wert der ausstellendesLand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAusstellendesLand() {
        return ausstellendesLand;
    }

    /**
     * Legt den Wert der ausstellendesLand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAusstellendesLand(String value) {
        this.ausstellendesLand = value;
    }

}
