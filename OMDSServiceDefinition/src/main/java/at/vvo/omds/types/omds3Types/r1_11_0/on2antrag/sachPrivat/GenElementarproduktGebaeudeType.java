
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Elementarprodukt Gebäudeversicherung für einen Ansatz, in dem nicht mit Vererbung gearbeitet wird.
 * 
 * <p>Java-Klasse für GenElementarproduktGebaeude_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GenElementarproduktGebaeude_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}ElementarproduktGebaeude_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Sparte" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}GebaeudeSpartenCd_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenElementarproduktGebaeude_Type", propOrder = {
    "sparte"
})
public class GenElementarproduktGebaeudeType
    extends ElementarproduktGebaeudeType
{

    @XmlElement(name = "Sparte", required = true)
    protected String sparte;

    /**
     * Ruft den Wert der sparte-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSparte() {
        return sparte;
    }

    /**
     * Legt den Wert der sparte-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSparte(String value) {
        this.sparte = value;
    }

}
