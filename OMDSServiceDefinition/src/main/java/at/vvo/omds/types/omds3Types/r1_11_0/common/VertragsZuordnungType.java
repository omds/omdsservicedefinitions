
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds2Types.v2_16.VtgRolleCdType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Zuordnung zu Vertrag
 * 
 * <p>Java-Klasse für VertragsZuordnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VertragsZuordnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}DokumentenZuordnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="AendGrundCd" type="{urn:omds20}AendGrundCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vermnr" type="{urn:omds20}Vermnr"/&gt;
 *         &lt;element name="Vertragsperson" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PersonId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *                   &lt;element name="Rolle" type="{urn:omds20}VtgRolleCd_Type"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VertragsZuordnung_Type", propOrder = {
    "polizzennr",
    "vertragsID",
    "aendGrundCd",
    "vermnr",
    "vertragsperson"
})
public class VertragsZuordnungType
    extends DokumentenZuordnungType
{

    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "AendGrundCd")
    protected String aendGrundCd;
    @XmlElement(name = "Vermnr", required = true)
    protected String vermnr;
    @XmlElement(name = "Vertragsperson")
    protected List<VertragsZuordnungType.Vertragsperson> vertragsperson;

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der aendGrundCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAendGrundCd() {
        return aendGrundCd;
    }

    /**
     * Legt den Wert der aendGrundCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAendGrundCd(String value) {
        this.aendGrundCd = value;
    }

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

    /**
     * Gets the value of the vertragsperson property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vertragsperson property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertragsperson().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VertragsZuordnungType.Vertragsperson }
     * 
     * 
     */
    public List<VertragsZuordnungType.Vertragsperson> getVertragsperson() {
        if (vertragsperson == null) {
            vertragsperson = new ArrayList<VertragsZuordnungType.Vertragsperson>();
        }
        return this.vertragsperson;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PersonId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
     *         &lt;element name="Rolle" type="{urn:omds20}VtgRolleCd_Type"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personId",
        "rolle"
    })
    public static class Vertragsperson {

        @XmlElement(name = "PersonId", required = true)
        protected ObjektIdType personId;
        @XmlElement(name = "Rolle", required = true)
        @XmlSchemaType(name = "string")
        protected VtgRolleCdType rolle;

        /**
         * Ruft den Wert der personId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ObjektIdType }
         *     
         */
        public ObjektIdType getPersonId() {
            return personId;
        }

        /**
         * Legt den Wert der personId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ObjektIdType }
         *     
         */
        public void setPersonId(ObjektIdType value) {
            this.personId = value;
        }

        /**
         * Ruft den Wert der rolle-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link VtgRolleCdType }
         *     
         */
        public VtgRolleCdType getRolle() {
            return rolle;
        }

        /**
         * Legt den Wert der rolle-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link VtgRolleCdType }
         *     
         */
        public void setRolle(VtgRolleCdType value) {
            this.rolle = value;
        }

    }

}
