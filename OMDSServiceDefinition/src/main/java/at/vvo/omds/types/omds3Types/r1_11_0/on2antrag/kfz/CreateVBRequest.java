
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.PersonType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VorlaeufigeDeckungType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vermittlernr" type="{urn:omds20}Vermnr"/&gt;
 *         &lt;element name="Art"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="NEU"/&gt;
 *               &lt;enumeration value="WIEDER"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Kennzeichen" type="{urn:omds20}Pol_Kennz_Type" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *         &lt;element name="Fahrzeug" maxOccurs="3" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="FzgArt" type="{urn:omds20}FzgArtCd_Type"/&gt;
 *                   &lt;element name="MarkeType"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;maxLength value="20"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Fahrgestnr" type="{urn:omds20}Fahrgestnr_Type" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Versicherungsnehmer" type="{urn:omds3CommonServiceTypes-1-1-0}Person_Type"/&gt;
 *         &lt;element name="GueltigAb" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="VorlaeufigeDeckung" type="{urn:omds3CommonServiceTypes-1-1-0}VorlaeufigeDeckung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheVBDaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ZusaetzlicheVBDaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vermittlernr",
    "art",
    "kennzeichen",
    "polizzennr",
    "fahrzeug",
    "versicherungsnehmer",
    "gueltigAb",
    "vorlaeufigeDeckung",
    "zusaetzlicheVBDaten"
})
@XmlRootElement(name = "CreateVBRequest")
public class CreateVBRequest
    extends CommonRequestType
{

    @XmlElement(name = "Vermittlernr", required = true)
    protected String vermittlernr;
    @XmlElement(name = "Art", required = true)
    protected String art;
    @XmlElement(name = "Kennzeichen")
    protected String kennzeichen;
    @XmlElement(name = "Polizzennr")
    protected String polizzennr;
    @XmlElement(name = "Fahrzeug")
    protected List<CreateVBRequest.Fahrzeug> fahrzeug;
    @XmlElement(name = "Versicherungsnehmer", required = true)
    protected PersonType versicherungsnehmer;
    @XmlElement(name = "GueltigAb")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltigAb;
    @XmlElement(name = "VorlaeufigeDeckung")
    protected List<VorlaeufigeDeckungType> vorlaeufigeDeckung;
    @XmlElement(name = "ZusaetzlicheVBDaten")
    protected List<ZusaetzlicheVBDatenType> zusaetzlicheVBDaten;

    /**
     * Ruft den Wert der vermittlernr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermittlernr() {
        return vermittlernr;
    }

    /**
     * Legt den Wert der vermittlernr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermittlernr(String value) {
        this.vermittlernr = value;
    }

    /**
     * Ruft den Wert der art-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArt() {
        return art;
    }

    /**
     * Legt den Wert der art-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArt(String value) {
        this.art = value;
    }

    /**
     * Ruft den Wert der kennzeichen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKennzeichen() {
        return kennzeichen;
    }

    /**
     * Legt den Wert der kennzeichen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKennzeichen(String value) {
        this.kennzeichen = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Gets the value of the fahrzeug property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the fahrzeug property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFahrzeug().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateVBRequest.Fahrzeug }
     * 
     * 
     */
    public List<CreateVBRequest.Fahrzeug> getFahrzeug() {
        if (fahrzeug == null) {
            fahrzeug = new ArrayList<CreateVBRequest.Fahrzeug>();
        }
        return this.fahrzeug;
    }

    /**
     * Ruft den Wert der versicherungsnehmer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getVersicherungsnehmer() {
        return versicherungsnehmer;
    }

    /**
     * Legt den Wert der versicherungsnehmer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setVersicherungsnehmer(PersonType value) {
        this.versicherungsnehmer = value;
    }

    /**
     * Ruft den Wert der gueltigAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigAb() {
        return gueltigAb;
    }

    /**
     * Legt den Wert der gueltigAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigAb(XMLGregorianCalendar value) {
        this.gueltigAb = value;
    }

    /**
     * Gets the value of the vorlaeufigeDeckung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vorlaeufigeDeckung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVorlaeufigeDeckung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VorlaeufigeDeckungType }
     * 
     * 
     */
    public List<VorlaeufigeDeckungType> getVorlaeufigeDeckung() {
        if (vorlaeufigeDeckung == null) {
            vorlaeufigeDeckung = new ArrayList<VorlaeufigeDeckungType>();
        }
        return this.vorlaeufigeDeckung;
    }

    /**
     * Gets the value of the zusaetzlicheVBDaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheVBDaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheVBDaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheVBDatenType }
     * 
     * 
     */
    public List<ZusaetzlicheVBDatenType> getZusaetzlicheVBDaten() {
        if (zusaetzlicheVBDaten == null) {
            zusaetzlicheVBDaten = new ArrayList<ZusaetzlicheVBDatenType>();
        }
        return this.zusaetzlicheVBDaten;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="FzgArt" type="{urn:omds20}FzgArtCd_Type"/&gt;
     *         &lt;element name="MarkeType"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;maxLength value="20"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Fahrgestnr" type="{urn:omds20}Fahrgestnr_Type" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fzgArt",
        "markeType",
        "fahrgestnr"
    })
    public static class Fahrzeug {

        @XmlElement(name = "FzgArt", required = true)
        protected String fzgArt;
        @XmlElement(name = "MarkeType", required = true)
        protected String markeType;
        @XmlElement(name = "Fahrgestnr")
        protected String fahrgestnr;

        /**
         * Ruft den Wert der fzgArt-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFzgArt() {
            return fzgArt;
        }

        /**
         * Legt den Wert der fzgArt-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFzgArt(String value) {
            this.fzgArt = value;
        }

        /**
         * Ruft den Wert der markeType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMarkeType() {
            return markeType;
        }

        /**
         * Legt den Wert der markeType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMarkeType(String value) {
            this.markeType = value;
        }

        /**
         * Ruft den Wert der fahrgestnr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFahrgestnr() {
            return fahrgestnr;
        }

        /**
         * Legt den Wert der fahrgestnr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFahrgestnr(String value) {
            this.fahrgestnr = value;
        }

    }

}
