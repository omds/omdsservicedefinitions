
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Vorgefertigter Typ für eine Praemienuebernahme im Ablebensfall
 * 
 * <p>Java-Klasse für ZusatzversicherungPraemienuebernahmeAbleben_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusatzversicherungPraemienuebernahmeAbleben_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}ZusatzversicherungLeben_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Wartefrist" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}Wartefrist_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusatzversicherungPraemienuebernahmeAbleben_Type", propOrder = {
    "wartefrist"
})
public class ZusatzversicherungPraemienuebernahmeAblebenType
    extends ZusatzversicherungLebenType
{

    @XmlElement(name = "Wartefrist")
    protected WartefristType wartefrist;

    /**
     * Ruft den Wert der wartefrist-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WartefristType }
     *     
     */
    public WartefristType getWartefrist() {
        return wartefrist;
    }

    /**
     * Legt den Wert der wartefrist-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WartefristType }
     *     
     */
    public void setWartefrist(WartefristType value) {
        this.wartefrist = value;
    }

}
