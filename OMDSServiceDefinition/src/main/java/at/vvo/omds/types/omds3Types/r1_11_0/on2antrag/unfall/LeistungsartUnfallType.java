
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall;

import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementarproduktGenerischType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.SelbstbehaltType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Elementarprodukt in der Sparte Unfall. Von diesem Typ werden etwaige unternehmesspezifische Deckungen oder potentielle Standard-Deckungen abgeleitet.
 * 
 * <p>Java-Klasse für LeistungsartUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LeistungsartUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ElementarproduktGenerisch_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LeistungsartUnfall_Type", propOrder = {
    "selbstbehalt"
})
public class LeistungsartUnfallType
    extends ElementarproduktGenerischType
{

    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

}
