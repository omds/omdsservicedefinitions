
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Das Ergebnisobjekt der Schadensuche
 * 
 * <p>Java-Klasse für SearchClaimResponseResult_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SearchClaimResponseResult_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActualOffset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="ActualMaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="Schadenereignisse" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SchadenereignisLight_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchClaimResponseResult_Type", propOrder = {
    "actualOffset",
    "actualMaxResults",
    "totalResults",
    "schadenereignisse"
})
public class SearchClaimResponseResultType {

    @XmlElement(name = "ActualOffset")
    @XmlSchemaType(name = "unsignedInt")
    protected long actualOffset;
    @XmlElement(name = "ActualMaxResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long actualMaxResults;
    @XmlElement(name = "TotalResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long totalResults;
    @XmlElement(name = "Schadenereignisse")
    protected List<SchadenereignisLightType> schadenereignisse;

    /**
     * Ruft den Wert der actualOffset-Eigenschaft ab.
     * 
     */
    public long getActualOffset() {
        return actualOffset;
    }

    /**
     * Legt den Wert der actualOffset-Eigenschaft fest.
     * 
     */
    public void setActualOffset(long value) {
        this.actualOffset = value;
    }

    /**
     * Ruft den Wert der actualMaxResults-Eigenschaft ab.
     * 
     */
    public long getActualMaxResults() {
        return actualMaxResults;
    }

    /**
     * Legt den Wert der actualMaxResults-Eigenschaft fest.
     * 
     */
    public void setActualMaxResults(long value) {
        this.actualMaxResults = value;
    }

    /**
     * Ruft den Wert der totalResults-Eigenschaft ab.
     * 
     */
    public long getTotalResults() {
        return totalResults;
    }

    /**
     * Legt den Wert der totalResults-Eigenschaft fest.
     * 
     */
    public void setTotalResults(long value) {
        this.totalResults = value;
    }

    /**
     * Gets the value of the schadenereignisse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the schadenereignisse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchadenereignisse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenereignisLightType }
     * 
     * 
     */
    public List<SchadenereignisLightType> getSchadenereignisse() {
        if (schadenereignisse == null) {
            schadenereignisse = new ArrayList<SchadenereignisLightType>();
        }
        return this.schadenereignisse;
    }

}
