
package at.vvo.omds.types.omds2Types.v2_16;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DachungsArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="DachungsArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BD"/&gt;
 *     &lt;enumeration value="TD"/&gt;
 *     &lt;enumeration value="EP"/&gt;
 *     &lt;enumeration value="BL"/&gt;
 *     &lt;enumeration value="HDA"/&gt;
 *     &lt;enumeration value="BMS"/&gt;
 *     &lt;enumeration value="BOS"/&gt;
 *     &lt;enumeration value="BS"/&gt;
 *     &lt;enumeration value="FMS"/&gt;
 *     &lt;enumeration value="FOS"/&gt;
 *     &lt;enumeration value="NHD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DachungsArtCd_Type")
@XmlEnum
public enum DachungsArtCdType {


    /**
     * Betondachstein
     * 
     */
    BD,

    /**
     * Tondachstein
     * 
     */
    TD,

    /**
     * Eternitplatten
     * 
     */
    EP,

    /**
     * Blech
     * 
     */
    BL,

    /**
     * harte Dachung - Andere
     * 
     */
    HDA,

    /**
     * Bitumenabdichtung mit Schüttung
     * 
     */
    BMS,

    /**
     * Bitumenabdichtung ohne Schüttung
     * 
     */
    BOS,

    /**
     * Bitumenschindel
     * 
     */
    BS,

    /**
     * Foliendach mit Schüttung
     * 
     */
    FMS,

    /**
     * Foliendach ohne Schüttung
     * 
     */
    FOS,

    /**
     * nicht harte Dachung - Andere
     * 
     */
    NHD;

    public String value() {
        return name();
    }

    public static DachungsArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
