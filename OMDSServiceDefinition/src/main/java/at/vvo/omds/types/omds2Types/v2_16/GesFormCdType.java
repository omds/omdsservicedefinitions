
package at.vvo.omds.types.omds2Types.v2_16;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GesFormCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="GesFormCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="GBH"/&gt;
 *     &lt;enumeration value="AG"/&gt;
 *     &lt;enumeration value="OG"/&gt;
 *     &lt;enumeration value="KG"/&gt;
 *     &lt;enumeration value="GBR"/&gt;
 *     &lt;enumeration value="SG"/&gt;
 *     &lt;enumeration value="GEN"/&gt;
 *     &lt;enumeration value="EUR"/&gt;
 *     &lt;enumeration value="EU"/&gt;
 *     &lt;enumeration value="GKG"/&gt;
 *     &lt;enumeration value="PS"/&gt;
 *     &lt;enumeration value="SP"/&gt;
 *     &lt;enumeration value="VE"/&gt;
 *     &lt;enumeration value="WEG"/&gt;
 *     &lt;enumeration value="EWI"/&gt;
 *     &lt;enumeration value="ARG"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GesFormCd_Type")
@XmlEnum
public enum GesFormCdType {


    /**
     * Gesellschaft mit beschränkter Haftung
     * 
     */
    GBH,

    /**
     * Aktiengesellschaft
     * 
     */
    AG,

    /**
     * Offene Gesellschaft
     * 
     */
    OG,

    /**
     * Kommanditgesellschaft
     * 
     */
    KG,

    /**
     * Gesellschaft bürgerlichen Rechts
     * 
     */
    GBR,

    /**
     * Stille Gesellschaft
     * 
     */
    SG,

    /**
     * Erwerbs- und Wirtschaftsgenossenschaft
     * 
     */
    GEN,

    /**
     * Ausländ./EU Rechtsform
     * 
     */
    EUR,

    /**
     * Einzelunternehmen
     * 
     */
    EU,

    /**
     * GmbH und Co KG
     * 
     */
    GKG,

    /**
     * Privatstiftung
     * 
     */
    PS,

    /**
     * Sparkasse
     * 
     */
    SP,

    /**
     * Verein
     * 
     */
    VE,

    /**
     * Wohnungseigentümergemeinschaft
     * 
     */
    WEG,

    /**
     * Europäische Wirtschaftliche Interessenvereinigung
     * 
     */
    EWI,

    /**
     * Arbeitsgemeinschaft
     * 
     */
    ARG;

    public String value() {
        return name();
    }

    public static GesFormCdType fromValue(String v) {
        return valueOf(v);
    }

}
