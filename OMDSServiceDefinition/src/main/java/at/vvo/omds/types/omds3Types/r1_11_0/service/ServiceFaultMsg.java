
package at.vvo.omds.types.omds3Types.r1_11_0.service;

import jakarta.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 4.0.3
 * 2024-05-09T17:02:10.755+02:00
 * Generated source version: 4.0.3
 */

@WebFault(name = "serviceFault", targetNamespace = "urn:omds3CommonServiceTypes-1-1-0")
public class ServiceFaultMsg extends Exception {

    private at.vvo.omds.types.omds3Types.r1_11_0.common.ServiceFault faultInfo;

    public ServiceFaultMsg() {
        super();
    }

    public ServiceFaultMsg(String message) {
        super(message);
    }

    public ServiceFaultMsg(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public ServiceFaultMsg(String message, at.vvo.omds.types.omds3Types.r1_11_0.common.ServiceFault serviceFault) {
        super(message);
        this.faultInfo = serviceFault;
    }

    public ServiceFaultMsg(String message, at.vvo.omds.types.omds3Types.r1_11_0.common.ServiceFault serviceFault, java.lang.Throwable cause) {
        super(message, cause);
        this.faultInfo = serviceFault;
    }

    public at.vvo.omds.types.omds3Types.r1_11_0.common.ServiceFault getFaultInfo() {
        return this.faultInfo;
    }
}
