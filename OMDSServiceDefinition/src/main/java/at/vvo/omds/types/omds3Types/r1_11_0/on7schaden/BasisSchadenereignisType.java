
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ServiceFault;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Typ mit gemeinsamen Rumpfdaten fuer Schadenereignis und Schadenereignis-Light
 * 
 * <p>Java-Klasse für BasisSchadenereignis_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BasisSchadenereignis_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaeftsfallSchadenereignis" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeSchadenmeldung" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="VorherigeSchadenmeldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Ereigniszpkt" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="EreignisbeschrTxt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="DeepLink" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheSchadensereignisdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheSchadensereignisdaten_Type" minOccurs="0"/&gt;
 *         &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasisSchadenereignis_Type", propOrder = {
    "vuNr",
    "geschaeftsfallSchadenereignis",
    "nachfolgendeSchadenmeldung",
    "vorherigeSchadenmeldungen",
    "ereigniszpkt",
    "ereignisbeschrTxt",
    "meldedat",
    "deepLink",
    "zusaetzlicheSchadensereignisdaten",
    "meldungen"
})
@XmlSeeAlso({
    SchadenereignisType.class,
    SchadenereignisLightType.class
})
public abstract class BasisSchadenereignisType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "GeschaeftsfallSchadenereignis")
    protected ObjektIdType geschaeftsfallSchadenereignis;
    @XmlElement(name = "NachfolgendeSchadenmeldung")
    protected ObjektIdType nachfolgendeSchadenmeldung;
    @XmlElement(name = "VorherigeSchadenmeldungen")
    protected List<ObjektIdType> vorherigeSchadenmeldungen;
    @XmlElement(name = "Ereigniszpkt", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ereigniszpkt;
    @XmlElement(name = "EreignisbeschrTxt", required = true)
    protected String ereignisbeschrTxt;
    @XmlElement(name = "Meldedat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;
    @XmlElement(name = "DeepLink")
    protected String deepLink;
    @XmlElement(name = "ZusaetzlicheSchadensereignisdaten")
    protected ZusaetzlicheSchadensereignisdatenType zusaetzlicheSchadensereignisdaten;
    @XmlElement(name = "Meldungen")
    protected List<ServiceFault> meldungen;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallSchadenereignis() {
        return geschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der geschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallSchadenereignis(ObjektIdType value) {
        this.geschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der nachfolgendeSchadenmeldung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getNachfolgendeSchadenmeldung() {
        return nachfolgendeSchadenmeldung;
    }

    /**
     * Legt den Wert der nachfolgendeSchadenmeldung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setNachfolgendeSchadenmeldung(ObjektIdType value) {
        this.nachfolgendeSchadenmeldung = value;
    }

    /**
     * Gets the value of the vorherigeSchadenmeldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vorherigeSchadenmeldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVorherigeSchadenmeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjektIdType }
     * 
     * 
     */
    public List<ObjektIdType> getVorherigeSchadenmeldungen() {
        if (vorherigeSchadenmeldungen == null) {
            vorherigeSchadenmeldungen = new ArrayList<ObjektIdType>();
        }
        return this.vorherigeSchadenmeldungen;
    }

    /**
     * Ruft den Wert der ereigniszpkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEreigniszpkt() {
        return ereigniszpkt;
    }

    /**
     * Legt den Wert der ereigniszpkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEreigniszpkt(XMLGregorianCalendar value) {
        this.ereigniszpkt = value;
    }

    /**
     * Ruft den Wert der ereignisbeschrTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEreignisbeschrTxt() {
        return ereignisbeschrTxt;
    }

    /**
     * Legt den Wert der ereignisbeschrTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEreignisbeschrTxt(String value) {
        this.ereignisbeschrTxt = value;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

    /**
     * Ruft den Wert der deepLink-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeepLink() {
        return deepLink;
    }

    /**
     * Legt den Wert der deepLink-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeepLink(String value) {
        this.deepLink = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheSchadensereignisdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheSchadensereignisdatenType }
     *     
     */
    public ZusaetzlicheSchadensereignisdatenType getZusaetzlicheSchadensereignisdaten() {
        return zusaetzlicheSchadensereignisdaten;
    }

    /**
     * Legt den Wert der zusaetzlicheSchadensereignisdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheSchadensereignisdatenType }
     *     
     */
    public void setZusaetzlicheSchadensereignisdaten(ZusaetzlicheSchadensereignisdatenType value) {
        this.zusaetzlicheSchadensereignisdaten = value;
    }

    /**
     * Gets the value of the meldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the meldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getMeldungen() {
        if (meldungen == null) {
            meldungen = new ArrayList<ServiceFault>();
        }
        return this.meldungen;
    }

}
