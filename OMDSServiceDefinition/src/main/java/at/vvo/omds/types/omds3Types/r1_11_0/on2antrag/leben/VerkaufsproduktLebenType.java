
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktGenerischType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VerkaufsproduktGenerischType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VersichertePersonType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Verkaufsprodukt in der Sparte Leben
 * 
 * <p>Java-Klasse für VerkaufsproduktLeben_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktLeben_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VerkaufsproduktGenerisch_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LebenProdukte" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}ProduktLeben_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Zusatzprodukte" type="{urn:omds3CommonServiceTypes-1-1-0}ProduktGenerisch_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VersichertePersonen" type="{urn:omds3CommonServiceTypes-1-1-0}VersichertePerson_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktLeben_Type", propOrder = {
    "lebenProdukte",
    "zusatzprodukte",
    "versichertePersonen"
})
public class VerkaufsproduktLebenType
    extends VerkaufsproduktGenerischType
{

    @XmlElement(name = "LebenProdukte", required = true)
    protected List<ProduktLebenType> lebenProdukte;
    @XmlElement(name = "Zusatzprodukte")
    protected List<ProduktGenerischType> zusatzprodukte;
    @XmlElement(name = "VersichertePersonen", required = true)
    protected List<VersichertePersonType> versichertePersonen;

    /**
     * Gets the value of the lebenProdukte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the lebenProdukte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLebenProdukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktLebenType }
     * 
     * 
     */
    public List<ProduktLebenType> getLebenProdukte() {
        if (lebenProdukte == null) {
            lebenProdukte = new ArrayList<ProduktLebenType>();
        }
        return this.lebenProdukte;
    }

    /**
     * Gets the value of the zusatzprodukte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusatzprodukte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusatzprodukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktGenerischType }
     * 
     * 
     */
    public List<ProduktGenerischType> getZusatzprodukte() {
        if (zusatzprodukte == null) {
            zusatzprodukte = new ArrayList<ProduktGenerischType>();
        }
        return this.zusatzprodukte;
    }

    /**
     * Gets the value of the versichertePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the versichertePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersichertePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VersichertePersonType }
     * 
     * 
     */
    public List<VersichertePersonType> getVersichertePersonen() {
        if (versichertePersonen == null) {
            versichertePersonen = new ArrayList<VersichertePersonType>();
        }
        return this.versichertePersonen;
    }

}
