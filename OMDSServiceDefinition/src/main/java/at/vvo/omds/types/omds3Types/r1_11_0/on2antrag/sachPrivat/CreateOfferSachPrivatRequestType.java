
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.CreateOfferRequestGenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ des Requestobjekts für ein Offert Sach-Privat
 * 
 * <p>Java-Klasse für CreateOfferSachPrivatRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferSachPrivatRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferRequestGen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}OffertSachPrivat_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferSachPrivatRequest_Type", propOrder = {
    "offertanfrage"
})
public class CreateOfferSachPrivatRequestType
    extends CreateOfferRequestGenType
{

    @XmlElement(name = "Offertanfrage", required = true)
    protected OffertSachPrivatType offertanfrage;

    /**
     * Ruft den Wert der offertanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OffertSachPrivatType }
     *     
     */
    public OffertSachPrivatType getOffertanfrage() {
        return offertanfrage;
    }

    /**
     * Legt den Wert der offertanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OffertSachPrivatType }
     *     
     */
    public void setOffertanfrage(OffertSachPrivatType value) {
        this.offertanfrage = value;
    }

}
