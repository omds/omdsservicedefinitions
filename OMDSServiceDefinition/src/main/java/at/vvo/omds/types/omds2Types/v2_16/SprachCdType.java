
package at.vvo.omds.types.omds2Types.v2_16;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SprachCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="SprachCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;minLength value="2"/&gt;
 *     &lt;maxLength value="2"/&gt;
 *     &lt;enumeration value="CS"/&gt;
 *     &lt;enumeration value="DE"/&gt;
 *     &lt;enumeration value="EN"/&gt;
 *     &lt;enumeration value="HU"/&gt;
 *     &lt;enumeration value="IT"/&gt;
 *     &lt;enumeration value="SK"/&gt;
 *     &lt;enumeration value="SL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SprachCd_Type")
@XmlEnum
public enum SprachCdType {


    /**
     * Tschechisch
     * 
     */
    CS,

    /**
     * Deutsch
     * 
     */
    DE,

    /**
     * Englisch
     * 
     */
    EN,

    /**
     * Ungarisch
     * 
     */
    HU,

    /**
     * Italienisch
     * 
     */
    IT,

    /**
     * Slowakisch
     * 
     */
    SK,

    /**
     * Slowenisch
     * 
     */
    SL;

    public String value() {
        return name();
    }

    public static SprachCdType fromValue(String v) {
        return valueOf(v);
    }

}
