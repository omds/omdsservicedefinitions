
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DokumentDataType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ZahlwegType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="Betrag" type="{urn:omds20}decimal"/&gt;
 *         &lt;element name="Grund" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zahlweg" type="{urn:omds3CommonServiceTypes-1-1-0}Zahlweg_Type"/&gt;
 *         &lt;element name="Belege" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentData_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="ZusaetzlicheBelegeinreichungsdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheBelegeinreichungsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "polizzennr",
    "vertragsID",
    "betrag",
    "grund",
    "zahlweg",
    "belege",
    "zusaetzlicheBelegeinreichungsdaten"
})
@XmlRootElement(name = "SubmitReceiptRequest")
public class SubmitReceiptRequest
    extends CommonRequestType
{

    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "Betrag", required = true)
    protected BigDecimal betrag;
    @XmlElement(name = "Grund")
    protected String grund;
    @XmlElement(name = "Zahlweg", required = true)
    protected ZahlwegType zahlweg;
    @XmlElement(name = "Belege", required = true)
    protected List<DokumentDataType> belege;
    @XmlElement(name = "ZusaetzlicheBelegeinreichungsdaten")
    protected List<ZusaetzlicheBelegeinreichungsdatenType> zusaetzlicheBelegeinreichungsdaten;

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der betrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBetrag() {
        return betrag;
    }

    /**
     * Legt den Wert der betrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBetrag(BigDecimal value) {
        this.betrag = value;
    }

    /**
     * Ruft den Wert der grund-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrund() {
        return grund;
    }

    /**
     * Legt den Wert der grund-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrund(String value) {
        this.grund = value;
    }

    /**
     * Ruft den Wert der zahlweg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZahlwegType }
     *     
     */
    public ZahlwegType getZahlweg() {
        return zahlweg;
    }

    /**
     * Legt den Wert der zahlweg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahlwegType }
     *     
     */
    public void setZahlweg(ZahlwegType value) {
        this.zahlweg = value;
    }

    /**
     * Gets the value of the belege property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the belege property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBelege().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentDataType }
     * 
     * 
     */
    public List<DokumentDataType> getBelege() {
        if (belege == null) {
            belege = new ArrayList<DokumentDataType>();
        }
        return this.belege;
    }

    /**
     * Gets the value of the zusaetzlicheBelegeinreichungsdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheBelegeinreichungsdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheBelegeinreichungsdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheBelegeinreichungsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheBelegeinreichungsdatenType> getZusaetzlicheBelegeinreichungsdaten() {
        if (zusaetzlicheBelegeinreichungsdaten == null) {
            zusaetzlicheBelegeinreichungsdaten = new ArrayList<ZusaetzlicheBelegeinreichungsdatenType>();
        }
        return this.zusaetzlicheBelegeinreichungsdaten;
    }

}
