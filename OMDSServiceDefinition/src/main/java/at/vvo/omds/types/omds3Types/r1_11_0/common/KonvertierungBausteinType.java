
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Allgemeiner Typ um die Behandlung der Unterbausteine in der Konvertierung zu spezifizieren
 * 
 * <p>Java-Klasse für KonvertierungBaustein_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KonvertierungBaustein_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ErsetztId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AntragsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Bezeichnung" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="255"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ZulaessigeAktion" type="{urn:omds3CommonServiceTypes-1-1-0}Konvertierungsaktion_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GewaehlteAktion" type="{urn:omds3CommonServiceTypes-1-1-0}Konvertierungsaktion_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KonvertierungBaustein_Type", propOrder = {
    "ersetztId",
    "antragsId",
    "bezeichnung",
    "zulaessigeAktionen",
    "gewaehlteAktion"
})
@XmlSeeAlso({
    KonvertierungProduktBausteinType.class
})
public class KonvertierungBausteinType {

    @XmlElement(name = "ErsetztId", required = true)
    protected String ersetztId;
    @XmlElement(name = "AntragsId")
    protected String antragsId;
    @XmlElement(name = "Bezeichnung")
    protected String bezeichnung;
    @XmlElement(name = "ZulaessigeAktion")
    protected List<KonvertierungsaktionType> zulaessigeAktionen;
    @XmlElement(name = "GewaehlteAktion")
    protected KonvertierungsaktionType gewaehlteAktion;

    /**
     * Ruft den Wert der ersetztId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErsetztId() {
        return ersetztId;
    }

    /**
     * Legt den Wert der ersetztId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErsetztId(String value) {
        this.ersetztId = value;
    }

    /**
     * Ruft den Wert der antragsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAntragsId() {
        return antragsId;
    }

    /**
     * Legt den Wert der antragsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAntragsId(String value) {
        this.antragsId = value;
    }

    /**
     * Ruft den Wert der bezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Legt den Wert der bezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

    /**
     * <p>Die Liste der Aktionen, die für einen Vertragsbaustein zulässig sind.
     *          Die zulässigen Aktionen müssen nur im Response vom ServiceProvider (VU) befüllt werden.
     *          Im Request eines Konvertierungsvorschlags durch den Consumer, können sie leer bleiben. </p>Gets the value of the zulaessigeAktionen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zulaessigeAktionen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZulaessigeAktionen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KonvertierungsaktionType }
     * 
     * 
     */
    public List<KonvertierungsaktionType> getZulaessigeAktionen() {
        if (zulaessigeAktionen == null) {
            zulaessigeAktionen = new ArrayList<KonvertierungsaktionType>();
        }
        return this.zulaessigeAktionen;
    }

    /**
     * Ruft den Wert der gewaehlteAktion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KonvertierungsaktionType }
     *     
     */
    public KonvertierungsaktionType getGewaehlteAktion() {
        return gewaehlteAktion;
    }

    /**
     * Legt den Wert der gewaehlteAktion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KonvertierungsaktionType }
     *     
     */
    public void setGewaehlteAktion(KonvertierungsaktionType value) {
        this.gewaehlteAktion = value;
    }

}
