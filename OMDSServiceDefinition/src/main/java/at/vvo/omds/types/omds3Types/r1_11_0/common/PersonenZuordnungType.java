
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Zuordnung zu einer Person
 * 
 * <p>Java-Klasse für PersonenZuordnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PersonenZuordnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}DokumentenZuordnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PersonId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *         &lt;element name="RolleText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonenZuordnung_Type", propOrder = {
    "personId",
    "rolleText"
})
public class PersonenZuordnungType
    extends DokumentenZuordnungType
{

    @XmlElement(name = "PersonId", required = true)
    protected ObjektIdType personId;
    @XmlElement(name = "RolleText")
    protected String rolleText;

    /**
     * Ruft den Wert der personId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getPersonId() {
        return personId;
    }

    /**
     * Legt den Wert der personId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setPersonId(ObjektIdType value) {
        this.personId = value;
    }

    /**
     * Ruft den Wert der rolleText-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolleText() {
        return rolleText;
    }

    /**
     * Legt den Wert der rolleText-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolleText(String value) {
        this.rolleText = value;
    }

}
