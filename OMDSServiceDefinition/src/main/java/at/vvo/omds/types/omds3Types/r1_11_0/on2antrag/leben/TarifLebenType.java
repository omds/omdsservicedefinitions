
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementFondsauswahlType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementarproduktGenerischType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Elementarprodukt in der Sparte Leben.
 * 
 * <p>Java-Klasse für TarifLeben_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TarifLeben_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ElementarproduktGenerisch_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GarantierteAblebenssumme" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}VersicherungssummeZusatzbaustein_Type" minOccurs="0"/&gt;
 *         &lt;element name="Rentenoption" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}Rentenoption_Type" minOccurs="0"/&gt;
 *         &lt;element name="Fondsauswahl" type="{urn:omds3CommonServiceTypes-1-1-0}ElementFondsauswahl_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Zusatzbausteine" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}ZusatzversicherungLeben_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TarifLeben_Type", propOrder = {
    "garantierteAblebenssumme",
    "rentenoption",
    "fondsauswahl",
    "zusatzbausteine"
})
public class TarifLebenType
    extends ElementarproduktGenerischType
{

    @XmlElement(name = "GarantierteAblebenssumme")
    protected VersicherungssummeZusatzbausteinType garantierteAblebenssumme;
    @XmlElement(name = "Rentenoption")
    protected RentenoptionType rentenoption;
    @XmlElement(name = "Fondsauswahl")
    protected List<ElementFondsauswahlType> fondsauswahl;
    @XmlElement(name = "Zusatzbausteine")
    protected List<ZusatzversicherungLebenType> zusatzbausteine;

    /**
     * Ruft den Wert der garantierteAblebenssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VersicherungssummeZusatzbausteinType }
     *     
     */
    public VersicherungssummeZusatzbausteinType getGarantierteAblebenssumme() {
        return garantierteAblebenssumme;
    }

    /**
     * Legt den Wert der garantierteAblebenssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VersicherungssummeZusatzbausteinType }
     *     
     */
    public void setGarantierteAblebenssumme(VersicherungssummeZusatzbausteinType value) {
        this.garantierteAblebenssumme = value;
    }

    /**
     * Ruft den Wert der rentenoption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RentenoptionType }
     *     
     */
    public RentenoptionType getRentenoption() {
        return rentenoption;
    }

    /**
     * Legt den Wert der rentenoption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RentenoptionType }
     *     
     */
    public void setRentenoption(RentenoptionType value) {
        this.rentenoption = value;
    }

    /**
     * Gets the value of the fondsauswahl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the fondsauswahl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFondsauswahl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElementFondsauswahlType }
     * 
     * 
     */
    public List<ElementFondsauswahlType> getFondsauswahl() {
        if (fondsauswahl == null) {
            fondsauswahl = new ArrayList<ElementFondsauswahlType>();
        }
        return this.fondsauswahl;
    }

    /**
     * Gets the value of the zusatzbausteine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusatzbausteine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusatzbausteine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusatzversicherungLebenType }
     * 
     * 
     */
    public List<ZusatzversicherungLebenType> getZusatzbausteine() {
        if (zusatzbausteine == null) {
            zusatzbausteine = new ArrayList<ZusatzversicherungLebenType>();
        }
        return this.zusatzbausteine;
    }

}
