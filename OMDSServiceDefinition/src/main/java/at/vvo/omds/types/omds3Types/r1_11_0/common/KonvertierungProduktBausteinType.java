
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Ableitung für die Produktebene (Ebene 2) mit der zusätzlichen Möglichkeit die Sparte zu signalisieren
 * 
 * <p>Java-Klasse für KonvertierungProduktBaustein_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KonvertierungProduktBaustein_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}KonvertierungBaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SpartenCd" type="{urn:omds20}SpartenCd_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KonvertierungProduktBaustein_Type", propOrder = {
    "spartenCd"
})
public class KonvertierungProduktBausteinType
    extends KonvertierungBausteinType
{

    @XmlElement(name = "SpartenCd")
    protected String spartenCd;

    /**
     * Ruft den Wert der spartenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenCd() {
        return spartenCd;
    }

    /**
     * Legt den Wert der spartenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenCd(String value) {
        this.spartenCd = value;
    }

}
