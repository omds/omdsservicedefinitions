
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DokumentenReferenzType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.PersBankverbindungType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Eine Beschreibung eines Schadenereignisses ohne Ids. Von diesem Type erben die Meldung und die Darstellung eines Schadenereignisses.
 * 
 * <p>Java-Klasse für Schadenereignis_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Schadenereignis_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BasisSchadenereignis_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SchadOrt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Ort_Type"/&gt;
 *         &lt;element name="BeteiligtePersonen" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BeteiligtePerson_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GeschaedigteInteressen" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaedigtesInteresse_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentenReferenz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schadenmelder" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenmelder_Type" minOccurs="0"/&gt;
 *         &lt;element name="Bankverbindung" type="{urn:omds3CommonServiceTypes-1-1-0}PersBankverbindung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schaden_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schadenereignis_Type", propOrder = {
    "schadOrt",
    "beteiligtePersonen",
    "geschaedigteInteressen",
    "dokumente",
    "schadenmelder",
    "bankverbindung",
    "schaeden"
})
public class SchadenereignisType
    extends BasisSchadenereignisType
{

    @XmlElement(name = "SchadOrt", required = true)
    protected OrtType schadOrt;
    @XmlElement(name = "BeteiligtePersonen")
    protected List<BeteiligtePersonType> beteiligtePersonen;
    @XmlElement(name = "GeschaedigteInteressen")
    protected List<GeschaedigtesInteresseType> geschaedigteInteressen;
    @XmlElement(name = "Dokumente")
    protected List<DokumentenReferenzType> dokumente;
    @XmlElement(name = "Schadenmelder")
    protected SchadenmelderType schadenmelder;
    @XmlElement(name = "Bankverbindung")
    protected List<PersBankverbindungType> bankverbindung;
    @XmlElement(name = "Schaeden", required = true)
    protected List<SchadenType> schaeden;

    /**
     * Ruft den Wert der schadOrt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrtType }
     *     
     */
    public OrtType getSchadOrt() {
        return schadOrt;
    }

    /**
     * Legt den Wert der schadOrt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrtType }
     *     
     */
    public void setSchadOrt(OrtType value) {
        this.schadOrt = value;
    }

    /**
     * Gets the value of the beteiligtePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the beteiligtePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeteiligtePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonType }
     * 
     * 
     */
    public List<BeteiligtePersonType> getBeteiligtePersonen() {
        if (beteiligtePersonen == null) {
            beteiligtePersonen = new ArrayList<BeteiligtePersonType>();
        }
        return this.beteiligtePersonen;
    }

    /**
     * Gets the value of the geschaedigteInteressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the geschaedigteInteressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeschaedigteInteressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeschaedigtesInteresseType }
     * 
     * 
     */
    public List<GeschaedigtesInteresseType> getGeschaedigteInteressen() {
        if (geschaedigteInteressen == null) {
            geschaedigteInteressen = new ArrayList<GeschaedigtesInteresseType>();
        }
        return this.geschaedigteInteressen;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentenReferenzType }
     * 
     * 
     */
    public List<DokumentenReferenzType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<DokumentenReferenzType>();
        }
        return this.dokumente;
    }

    /**
     * Ruft den Wert der schadenmelder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenmelderType }
     *     
     */
    public SchadenmelderType getSchadenmelder() {
        return schadenmelder;
    }

    /**
     * Legt den Wert der schadenmelder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenmelderType }
     *     
     */
    public void setSchadenmelder(SchadenmelderType value) {
        this.schadenmelder = value;
    }

    /**
     * Gets the value of the bankverbindung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bankverbindung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBankverbindung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersBankverbindungType }
     * 
     * 
     */
    public List<PersBankverbindungType> getBankverbindung() {
        if (bankverbindung == null) {
            bankverbindung = new ArrayList<PersBankverbindungType>();
        }
        return this.bankverbindung;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenType }
     * 
     * 
     */
    public List<SchadenType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenType>();
        }
        return this.schaeden;
    }

}
