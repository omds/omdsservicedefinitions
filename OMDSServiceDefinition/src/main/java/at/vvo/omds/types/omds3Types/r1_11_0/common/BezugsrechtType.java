
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type Bezugsrecht (nur Kontext Kfz), für Bezugsberechtigungen in anderen Sparten steht jetzt Bezugsberechtigung_Type zur Verfügung.
 * 
 * <p>Java-Klasse für Bezugsrecht_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Bezugsrecht_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds20}PERSON"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bezugsrecht_Type", propOrder = {
    "person"
})
public class BezugsrechtType {

    @XmlElement(name = "PERSON", namespace = "urn:omds20", required = true)
    protected PERSONType person;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PERSONType }
     *     
     */
    public PERSONType getPERSON() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PERSONType }
     *     
     */
    public void setPERSON(PERSONType value) {
        this.person = value;
    }

}
