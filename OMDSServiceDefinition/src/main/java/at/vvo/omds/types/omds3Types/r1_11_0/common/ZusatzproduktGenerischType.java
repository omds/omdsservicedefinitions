
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.ZusatzversicherungLebenType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Basistyp für ein Zusatzprodukt, 2. Generation
 * 
 * <p>Java-Klasse für ZusatzproduktGenerisch_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusatzproduktGenerisch_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Zusatzprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Eingeschlossen" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="EinschlussAenderbar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AttributMetadaten" type="{urn:omds3CommonServiceTypes-1-1-0}AttributMetadaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="BeschreibungTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Versicherungssumme" type="{urn:omds20}decimal14_2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusatzproduktGenerisch_Type", propOrder = {
    "eingeschlossen",
    "einschlussAenderbar",
    "attributMetadaten",
    "beschreibungTxt",
    "versicherungssumme"
})
@XmlSeeAlso({
    ZusatzversicherungLebenType.class
})
public class ZusatzproduktGenerischType
    extends ZusatzproduktType
{

    @XmlElement(name = "Eingeschlossen")
    protected boolean eingeschlossen;
    @XmlElement(name = "EinschlussAenderbar")
    protected Boolean einschlussAenderbar;
    @XmlElement(name = "AttributMetadaten")
    protected List<AttributMetadatenType> attributMetadaten;
    @XmlElement(name = "BeschreibungTxt")
    protected String beschreibungTxt;
    @XmlElement(name = "Versicherungssumme")
    protected BigDecimal versicherungssumme;

    /**
     * Ruft den Wert der eingeschlossen-Eigenschaft ab.
     * 
     */
    public boolean isEingeschlossen() {
        return eingeschlossen;
    }

    /**
     * Legt den Wert der eingeschlossen-Eigenschaft fest.
     * 
     */
    public void setEingeschlossen(boolean value) {
        this.eingeschlossen = value;
    }

    /**
     * Ruft den Wert der einschlussAenderbar-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEinschlussAenderbar() {
        return einschlussAenderbar;
    }

    /**
     * Legt den Wert der einschlussAenderbar-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEinschlussAenderbar(Boolean value) {
        this.einschlussAenderbar = value;
    }

    /**
     * Gets the value of the attributMetadaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the attributMetadaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributMetadaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributMetadatenType }
     * 
     * 
     */
    public List<AttributMetadatenType> getAttributMetadaten() {
        if (attributMetadaten == null) {
            attributMetadaten = new ArrayList<AttributMetadatenType>();
        }
        return this.attributMetadaten;
    }

    /**
     * Ruft den Wert der beschreibungTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeschreibungTxt() {
        return beschreibungTxt;
    }

    /**
     * Legt den Wert der beschreibungTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeschreibungTxt(String value) {
        this.beschreibungTxt = value;
    }

    /**
     * Ruft den Wert der versicherungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersicherungssumme() {
        return versicherungssumme;
    }

    /**
     * Legt den Wert der versicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersicherungssumme(BigDecimal value) {
        this.versicherungssumme = value;
    }

}
