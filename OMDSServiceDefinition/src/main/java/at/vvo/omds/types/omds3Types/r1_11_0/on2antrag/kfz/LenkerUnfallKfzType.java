
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für das Elementarprodukt KFZ-Lenkerunfall
 * 
 * <p>Java-Klasse für LenkerUnfallKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LenkerUnfallKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ElementarproduktKfz_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LenkerUnfallKfz_Type")
public class LenkerUnfallKfzType
    extends ElementarproduktKfzType
{


}
