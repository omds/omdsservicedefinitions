
package at.vvo.omds.types.omds2Types.v2_16;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VtgRolleCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="VtgRolleCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AP"/&gt;
 *     &lt;enumeration value="AZ"/&gt;
 *     &lt;enumeration value="BG"/&gt;
 *     &lt;enumeration value="BM"/&gt;
 *     &lt;enumeration value="BO"/&gt;
 *     &lt;enumeration value="IA"/&gt;
 *     &lt;enumeration value="FI"/&gt;
 *     &lt;enumeration value="GV"/&gt;
 *     &lt;enumeration value="LE"/&gt;
 *     &lt;enumeration value="KA"/&gt;
 *     &lt;enumeration value="VN"/&gt;
 *     &lt;enumeration value="ZB"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VtgRolleCd_Type")
@XmlEnum
public enum VtgRolleCdType {


    /**
     * Ansprechperson
     * 
     */
    AP,

    /**
     * abweichender Zulassungsbesitzer
     * 
     */
    AZ,

    /**
     * Bausparer gesetzlicher Vertreter
     * 
     */
    BG,

    /**
     * Bausparer mit Prämie
     * 
     */
    BM,

    /**
     * Bausparer ohne Prämie
     * 
     */
    BO,

    /**
     * Inkassoadresse
     * 
     */
    IA,

    /**
     * Firmeninhaber
     * 
     */
    FI,

    /**
     * Gesetzlicher Vertreter
     * 
     */
    GV,

    /**
     * Lenker
     * 
     */
    LE,

    /**
     * Korrespondenz/Zustelladresse
     * 
     */
    KA,

    /**
     * Versicherungsnehmer
     * 
     */
    VN,

    /**
     * Zustellbevollmächtigter
     * 
     */
    ZB;

    public String value() {
        return name();
    }

    public static VtgRolleCdType fromValue(String v) {
        return valueOf(v);
    }

}
