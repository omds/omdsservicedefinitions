
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.BOAProcessRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstakter Typ fuer Requests, die Teil eines Geschaeftsfalls mit eigener Id sind
 * 
 * <p>Java-Klasse für CommonProcessRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonProcessRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Geschaeftsfallnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonProcessRequest_Type", propOrder = {
    "geschaeftsfallnummer"
})
@XmlSeeAlso({
    BOAProcessRequestType.class
})
public abstract class CommonProcessRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Geschaeftsfallnummer")
    protected ObjektIdType geschaeftsfallnummer;

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

}
