
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Sicherstellung vom Typ Vinkulierung. Der Vinkulargläubiger hat das Recht auf Auszahlungen
 * 				aus dem Versicherungsverhältnis. Die Versicherung ist verpflichtet den Vinkulargläubiger über Vertragsänderungen
 * 				oder Prämienrückstände zu informieren. Die Benennung VinkulierungPersonen_Type ist schlecht gewählt,
 * 				dieses Objekt ist für alle Vinkulierungen innerhalb des Konzepts "Sicherstellungen" vorgesehen.
 * 			
 * 
 * <p>Java-Klasse für VinkulierungPersonen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VinkulierungPersonen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Sicherstellung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vinkularglaeubiger" type="{urn:omds3CommonServiceTypes-1-1-0}Vinkularglaeubiger_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VinkulierungPersonen_Type", propOrder = {
    "vinkularglaeubiger"
})
public class VinkulierungPersonenType
    extends SicherstellungType
{

    @XmlElement(name = "Vinkularglaeubiger", required = true)
    protected VinkularglaeubigerType vinkularglaeubiger;

    /**
     * Ruft den Wert der vinkularglaeubiger-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VinkularglaeubigerType }
     *     
     */
    public VinkularglaeubigerType getVinkularglaeubiger() {
        return vinkularglaeubiger;
    }

    /**
     * Legt den Wert der vinkularglaeubiger-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VinkularglaeubigerType }
     *     
     */
    public void setVinkularglaeubiger(VinkularglaeubigerType value) {
        this.vinkularglaeubiger = value;
    }

}
