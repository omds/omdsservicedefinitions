
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ServiceFault;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Typ mit gemeinsamen Rumpfdaten fuer Schaden und Schaden-Light 
 * 
 * <p>Java-Klasse für BasisSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BasisSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaeftsfallSchadenanlage" minOccurs="0"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element name="BearbStandSeit" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="VormaligeSchadennr" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeSchadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element name="SachbearbVU" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SachbearbVUType" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="ErledDat" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="DeepLink" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheSchadensdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheSchadensdaten_Type" minOccurs="0"/&gt;
 *         &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasisSchaden_Type", propOrder = {
    "geschaeftsfallSchadenanlage",
    "schadennr",
    "bearbStandSeit",
    "vormaligeSchadennr",
    "nachfolgendeSchadennr",
    "sachbearbVU",
    "polizzennr",
    "vertragsID",
    "erledDat",
    "deepLink",
    "zusaetzlicheSchadensdaten",
    "meldungen"
})
@XmlSeeAlso({
    SchadenType.class,
    SchadenLightType.class
})
public class BasisSchadenType {

    @XmlElement(name = "GeschaeftsfallSchadenanlage")
    protected ObjektIdType geschaeftsfallSchadenanlage;
    @XmlElement(name = "Schadennr")
    protected String schadennr;
    @XmlElement(name = "BearbStandSeit")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar bearbStandSeit;
    @XmlElement(name = "VormaligeSchadennr")
    protected List<String> vormaligeSchadennr;
    @XmlElement(name = "NachfolgendeSchadennr")
    protected String nachfolgendeSchadennr;
    @XmlElement(name = "SachbearbVU")
    protected SachbearbVUType sachbearbVU;
    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "ErledDat")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar erledDat;
    @XmlElement(name = "DeepLink")
    protected String deepLink;
    @XmlElement(name = "ZusaetzlicheSchadensdaten")
    protected ZusaetzlicheSchadensdatenType zusaetzlicheSchadensdaten;
    @XmlElement(name = "Meldungen")
    protected List<ServiceFault> meldungen;

    /**
     * Ruft den Wert der geschaeftsfallSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallSchadenanlage() {
        return geschaeftsfallSchadenanlage;
    }

    /**
     * Legt den Wert der geschaeftsfallSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallSchadenanlage(ObjektIdType value) {
        this.geschaeftsfallSchadenanlage = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der bearbStandSeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBearbStandSeit() {
        return bearbStandSeit;
    }

    /**
     * Legt den Wert der bearbStandSeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBearbStandSeit(XMLGregorianCalendar value) {
        this.bearbStandSeit = value;
    }

    /**
     * Gets the value of the vormaligeSchadennr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vormaligeSchadennr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVormaligeSchadennr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVormaligeSchadennr() {
        if (vormaligeSchadennr == null) {
            vormaligeSchadennr = new ArrayList<String>();
        }
        return this.vormaligeSchadennr;
    }

    /**
     * Ruft den Wert der nachfolgendeSchadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNachfolgendeSchadennr() {
        return nachfolgendeSchadennr;
    }

    /**
     * Legt den Wert der nachfolgendeSchadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNachfolgendeSchadennr(String value) {
        this.nachfolgendeSchadennr = value;
    }

    /**
     * Ruft den Wert der sachbearbVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SachbearbVUType }
     *     
     */
    public SachbearbVUType getSachbearbVU() {
        return sachbearbVU;
    }

    /**
     * Legt den Wert der sachbearbVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SachbearbVUType }
     *     
     */
    public void setSachbearbVU(SachbearbVUType value) {
        this.sachbearbVU = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der erledDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getErledDat() {
        return erledDat;
    }

    /**
     * Legt den Wert der erledDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setErledDat(XMLGregorianCalendar value) {
        this.erledDat = value;
    }

    /**
     * Ruft den Wert der deepLink-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeepLink() {
        return deepLink;
    }

    /**
     * Legt den Wert der deepLink-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeepLink(String value) {
        this.deepLink = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheSchadensdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheSchadensdatenType }
     *     
     */
    public ZusaetzlicheSchadensdatenType getZusaetzlicheSchadensdaten() {
        return zusaetzlicheSchadensdaten;
    }

    /**
     * Legt den Wert der zusaetzlicheSchadensdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheSchadensdatenType }
     *     
     */
    public void setZusaetzlicheSchadensdaten(ZusaetzlicheSchadensdatenType value) {
        this.zusaetzlicheSchadensdaten = value;
    }

    /**
     * Gets the value of the meldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the meldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getMeldungen() {
        if (meldungen == null) {
            meldungen = new ArrayList<ServiceFault>();
        }
        return this.meldungen;
    }

}
