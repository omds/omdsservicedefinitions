
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ArtBesichtigung_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>
 * &lt;simpleType name="ArtBesichtigung_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Besichtigung durch Betreuer"/&gt;
 *     &lt;enumeration value="Kaufvertrag Markenhaendler mit Beschreibung"/&gt;
 *     &lt;enumeration value="Besichtigung durch ARBOE, OEAMTC"/&gt;
 *     &lt;enumeration value="Nachbesichtigung"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ArtBesichtigung_Type")
@XmlEnum
public enum ArtBesichtigungType {

    @XmlEnumValue("Besichtigung durch Betreuer")
    BESICHTIGUNG_DURCH_BETREUER("Besichtigung durch Betreuer"),
    @XmlEnumValue("Kaufvertrag Markenhaendler mit Beschreibung")
    KAUFVERTRAG_MARKENHAENDLER_MIT_BESCHREIBUNG("Kaufvertrag Markenhaendler mit Beschreibung"),
    @XmlEnumValue("Besichtigung durch ARBOE, OEAMTC")
    BESICHTIGUNG_DURCH_ARBOE_OEAMTC("Besichtigung durch ARBOE, OEAMTC"),
    @XmlEnumValue("Nachbesichtigung")
    NACHBESICHTIGUNG("Nachbesichtigung");
    private final String value;

    ArtBesichtigungType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ArtBesichtigungType fromValue(String v) {
        for (ArtBesichtigungType c: ArtBesichtigungType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
