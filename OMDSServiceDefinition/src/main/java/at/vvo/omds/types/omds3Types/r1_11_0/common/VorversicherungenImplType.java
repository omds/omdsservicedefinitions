
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Standardimplementierung Vorversicherungen: Eine Liste von Vorversicherung_Type-Objekten
 * 
 * <p>Java-Klasse für VorversicherungenImpl_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VorversicherungenImpl_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Vorversicherungen_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vorversicherungen" type="{urn:omds3CommonServiceTypes-1-1-0}Vorversicherung_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VorversicherungenImpl_Type", propOrder = {
    "vorversicherungen"
})
public class VorversicherungenImplType
    extends VorversicherungenType
{

    @XmlElement(name = "Vorversicherungen", required = true)
    protected List<VorversicherungType> vorversicherungen;

    /**
     * Gets the value of the vorversicherungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vorversicherungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVorversicherungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VorversicherungType }
     * 
     * 
     */
    public List<VorversicherungType> getVorversicherungen() {
        if (vorversicherungen == null) {
            vorversicherungen = new ArrayList<VorversicherungType>();
        }
        return this.vorversicherungen;
    }

}
