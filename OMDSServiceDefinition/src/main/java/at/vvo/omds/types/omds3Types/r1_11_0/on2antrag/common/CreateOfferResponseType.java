
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DokumentInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokumentBasisType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateOfferKfzResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Response, der das Offert enthält bzw. Fehlermeldungen
 * 
 * <p>Java-Klasse für CreateOfferResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}BOAProcessResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentInfo_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DokumenteAnforderungen" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokumentBasis_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferResponse_Type", propOrder = {
    "dokumente",
    "dokumenteAnforderungen"
})
@XmlSeeAlso({
    CreateOfferKfzResponseType.class,
    CreateOfferResponseGenType.class
})
public abstract class CreateOfferResponseType
    extends BOAProcessResponseType
{

    @XmlElement(name = "Dokumente")
    protected List<DokumentInfoType> dokumente;
    @XmlElement(name = "DokumenteAnforderungen")
    protected List<ProzessDokumentBasisType> dokumenteAnforderungen;

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentInfoType }
     * 
     * 
     */
    public List<DokumentInfoType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<DokumentInfoType>();
        }
        return this.dokumente;
    }

    /**
     * Gets the value of the dokumenteAnforderungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumenteAnforderungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumenteAnforderungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokumentBasisType }
     * 
     * 
     */
    public List<ProzessDokumentBasisType> getDokumenteAnforderungen() {
        if (dokumenteAnforderungen == null) {
            dokumenteAnforderungen = new ArrayList<ProzessDokumentBasisType>();
        }
        return this.dokumenteAnforderungen;
    }

}
