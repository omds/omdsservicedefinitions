
package at.vvo.omds.types.omds2Types.v2_16;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MIGRATIONSINFORMATION_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MIGRATIONSINFORMATION_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="MigrationsCd" use="required" type="{urn:omds20}MigrationsCd_Type" /&gt;
 *       &lt;attribute name="Wert" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="255"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VorgaengerWert" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="255"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="GueltigAb" type="{urn:omds20}Datum" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MIGRATIONSINFORMATION_Type")
public class MIGRATIONSINFORMATIONType {

    @XmlAttribute(name = "MigrationsCd", required = true)
    protected String migrationsCd;
    @XmlAttribute(name = "Wert", required = true)
    protected String wert;
    @XmlAttribute(name = "VorgaengerWert", required = true)
    protected String vorgaengerWert;
    @XmlAttribute(name = "GueltigAb")
    protected XMLGregorianCalendar gueltigAb;

    /**
     * Ruft den Wert der migrationsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMigrationsCd() {
        return migrationsCd;
    }

    /**
     * Legt den Wert der migrationsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMigrationsCd(String value) {
        this.migrationsCd = value;
    }

    /**
     * Ruft den Wert der wert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWert() {
        return wert;
    }

    /**
     * Legt den Wert der wert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWert(String value) {
        this.wert = value;
    }

    /**
     * Ruft den Wert der vorgaengerWert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorgaengerWert() {
        return vorgaengerWert;
    }

    /**
     * Legt den Wert der vorgaengerWert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorgaengerWert(String value) {
        this.vorgaengerWert = value;
    }

    /**
     * Ruft den Wert der gueltigAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigAb() {
        return gueltigAb;
    }

    /**
     * Legt den Wert der gueltigAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigAb(XMLGregorianCalendar value) {
        this.gueltigAb = value;
    }

}
