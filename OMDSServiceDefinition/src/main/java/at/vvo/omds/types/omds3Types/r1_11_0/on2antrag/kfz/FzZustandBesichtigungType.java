
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import java.math.BigInteger;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.common.AFzZustandBesichtigungType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Standardimplementierung des AFzZustandBesichtigung_Type
 * 
 * <p>Java-Klasse für FzZustandBesichtigung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FzZustandBesichtigung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}AFzZustandBesichtigung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZustandsberichtLiegtAlsFormularBei" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ArtBesichtigung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ArtBesichtigung_Type"/&gt;
 *         &lt;element name="SchaedenAnScheibenKleinglas" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SchaedenAnScheibenKleinglas_Type" minOccurs="0"/&gt;
 *         &lt;element name="SchaedenAmFahrzeug" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SchaedenAmFahrzeug_Type" minOccurs="0"/&gt;
 *         &lt;element name="KilometerLtBesichtigung" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="Vorschaeden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Besichtigungsort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Besichtigungsdatum" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="GrundFuerNachbesichtigung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FzZustandBesichtigung_Type", propOrder = {
    "zustandsberichtLiegtAlsFormularBei",
    "artBesichtigung",
    "schaedenAnScheibenKleinglas",
    "schaedenAmFahrzeug",
    "kilometerLtBesichtigung",
    "vorschaeden",
    "besichtigungsort",
    "besichtigungsdatum",
    "grundFuerNachbesichtigung"
})
public class FzZustandBesichtigungType
    extends AFzZustandBesichtigungType
{

    @XmlElement(name = "ZustandsberichtLiegtAlsFormularBei")
    protected Boolean zustandsberichtLiegtAlsFormularBei;
    @XmlElement(name = "ArtBesichtigung", required = true)
    @XmlSchemaType(name = "string")
    protected ArtBesichtigungType artBesichtigung;
    @XmlElement(name = "SchaedenAnScheibenKleinglas")
    @XmlSchemaType(name = "string")
    protected SchaedenAnScheibenKleinglasType schaedenAnScheibenKleinglas;
    @XmlElement(name = "SchaedenAmFahrzeug")
    @XmlSchemaType(name = "string")
    protected SchaedenAmFahrzeugType schaedenAmFahrzeug;
    @XmlElement(name = "KilometerLtBesichtigung")
    protected BigInteger kilometerLtBesichtigung;
    @XmlElement(name = "Vorschaeden")
    protected String vorschaeden;
    @XmlElement(name = "Besichtigungsort")
    protected String besichtigungsort;
    @XmlElement(name = "Besichtigungsdatum")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar besichtigungsdatum;
    @XmlElement(name = "GrundFuerNachbesichtigung")
    protected String grundFuerNachbesichtigung;

    /**
     * Ruft den Wert der zustandsberichtLiegtAlsFormularBei-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isZustandsberichtLiegtAlsFormularBei() {
        return zustandsberichtLiegtAlsFormularBei;
    }

    /**
     * Legt den Wert der zustandsberichtLiegtAlsFormularBei-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setZustandsberichtLiegtAlsFormularBei(Boolean value) {
        this.zustandsberichtLiegtAlsFormularBei = value;
    }

    /**
     * Ruft den Wert der artBesichtigung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ArtBesichtigungType }
     *     
     */
    public ArtBesichtigungType getArtBesichtigung() {
        return artBesichtigung;
    }

    /**
     * Legt den Wert der artBesichtigung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ArtBesichtigungType }
     *     
     */
    public void setArtBesichtigung(ArtBesichtigungType value) {
        this.artBesichtigung = value;
    }

    /**
     * Ruft den Wert der schaedenAnScheibenKleinglas-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchaedenAnScheibenKleinglasType }
     *     
     */
    public SchaedenAnScheibenKleinglasType getSchaedenAnScheibenKleinglas() {
        return schaedenAnScheibenKleinglas;
    }

    /**
     * Legt den Wert der schaedenAnScheibenKleinglas-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchaedenAnScheibenKleinglasType }
     *     
     */
    public void setSchaedenAnScheibenKleinglas(SchaedenAnScheibenKleinglasType value) {
        this.schaedenAnScheibenKleinglas = value;
    }

    /**
     * Ruft den Wert der schaedenAmFahrzeug-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchaedenAmFahrzeugType }
     *     
     */
    public SchaedenAmFahrzeugType getSchaedenAmFahrzeug() {
        return schaedenAmFahrzeug;
    }

    /**
     * Legt den Wert der schaedenAmFahrzeug-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchaedenAmFahrzeugType }
     *     
     */
    public void setSchaedenAmFahrzeug(SchaedenAmFahrzeugType value) {
        this.schaedenAmFahrzeug = value;
    }

    /**
     * Ruft den Wert der kilometerLtBesichtigung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getKilometerLtBesichtigung() {
        return kilometerLtBesichtigung;
    }

    /**
     * Legt den Wert der kilometerLtBesichtigung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setKilometerLtBesichtigung(BigInteger value) {
        this.kilometerLtBesichtigung = value;
    }

    /**
     * Ruft den Wert der vorschaeden-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorschaeden() {
        return vorschaeden;
    }

    /**
     * Legt den Wert der vorschaeden-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorschaeden(String value) {
        this.vorschaeden = value;
    }

    /**
     * Ruft den Wert der besichtigungsort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBesichtigungsort() {
        return besichtigungsort;
    }

    /**
     * Legt den Wert der besichtigungsort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBesichtigungsort(String value) {
        this.besichtigungsort = value;
    }

    /**
     * Ruft den Wert der besichtigungsdatum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBesichtigungsdatum() {
        return besichtigungsdatum;
    }

    /**
     * Legt den Wert der besichtigungsdatum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBesichtigungsdatum(XMLGregorianCalendar value) {
        this.besichtigungsdatum = value;
    }

    /**
     * Ruft den Wert der grundFuerNachbesichtigung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrundFuerNachbesichtigung() {
        return grundFuerNachbesichtigung;
    }

    /**
     * Legt den Wert der grundFuerNachbesichtigung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrundFuerNachbesichtigung(String value) {
        this.grundFuerNachbesichtigung = value;
    }

}
