
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktMitVpType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.SelbstbehaltType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für ein Produkt in der Sparte Unfall. Von diesem Typ können einzelne VUs ihre eigenen Produkte ableiten, wenn sie möchten.
 * 
 * <p>Java-Klasse für ProduktUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProduktMitVp_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Leistungsarten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}LeistungsartUnfall_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktUnfall_Type", propOrder = {
    "leistungsarten",
    "selbstbehalt"
})
public class ProduktUnfallType
    extends ProduktMitVpType
{

    @XmlElement(name = "Leistungsarten")
    protected List<LeistungsartUnfallType> leistungsarten;
    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;

    /**
     * Gets the value of the leistungsarten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the leistungsarten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLeistungsarten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LeistungsartUnfallType }
     * 
     * 
     */
    public List<LeistungsartUnfallType> getLeistungsarten() {
        if (leistungsarten == null) {
            leistungsarten = new ArrayList<LeistungsartUnfallType>();
        }
        return this.leistungsarten;
    }

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

}
