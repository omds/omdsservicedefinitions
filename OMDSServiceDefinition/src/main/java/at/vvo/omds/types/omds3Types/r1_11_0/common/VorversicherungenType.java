
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.VorversicherungenKfzType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Typ zur Beschreibung der Vorversicherungen, wird verwendet in SpezAntrag_Type.
 * 				Es gibt zwei Ableitungen im Standard: VorversicherungenKfz_Type und VorversicherungenImpl_Type (s.u.), jede VU kann aber auch eigene Implementierungen vornehmen.
 * 
 * <p>Java-Klasse für Vorversicherungen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Vorversicherungen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vorversicherungen_Type")
@XmlSeeAlso({
    VorversicherungenImplType.class,
    VorversicherungenKfzType.class
})
public abstract class VorversicherungenType {


}
