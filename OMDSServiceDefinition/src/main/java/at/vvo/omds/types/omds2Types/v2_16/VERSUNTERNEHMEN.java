
package at.vvo.omds.types.omds2Types.v2_16;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element ref="{urn:omds20}EL-Kommunikation"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="VUNr" use="required" type="{urn:omds20}VUNr" /&gt;
 *       &lt;attribute name="VUKurzBez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="8"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VUBezeichnung"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="100"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VUNrFusion" type="{urn:omds20}VUNr" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "elKommunikation"
})
@XmlRootElement(name = "VERS_UNTERNEHMEN")
public class VERSUNTERNEHMEN {

    @XmlElement(name = "EL-Kommunikation")
    protected List<ELKommunikationType> elKommunikation;
    @XmlAttribute(name = "VUNr", required = true)
    protected String vuNr;
    @XmlAttribute(name = "VUKurzBez")
    protected String vuKurzBez;
    @XmlAttribute(name = "VUBezeichnung")
    protected String vuBezeichnung;
    @XmlAttribute(name = "VUNrFusion")
    protected String vuNrFusion;

    /**
     * Gets the value of the elKommunikation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the elKommunikation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELKommunikation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELKommunikationType }
     * 
     * 
     */
    public List<ELKommunikationType> getELKommunikation() {
        if (elKommunikation == null) {
            elKommunikation = new ArrayList<ELKommunikationType>();
        }
        return this.elKommunikation;
    }

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der vuKurzBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUKurzBez() {
        return vuKurzBez;
    }

    /**
     * Legt den Wert der vuKurzBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUKurzBez(String value) {
        this.vuKurzBez = value;
    }

    /**
     * Ruft den Wert der vuBezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUBezeichnung() {
        return vuBezeichnung;
    }

    /**
     * Legt den Wert der vuBezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUBezeichnung(String value) {
        this.vuBezeichnung = value;
    }

    /**
     * Ruft den Wert der vuNrFusion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNrFusion() {
        return vuNrFusion;
    }

    /**
     * Legt den Wert der vuNrFusion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNrFusion(String value) {
        this.vuNrFusion = value;
    }

}
