
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds2Types.v2_16.VtgRolleCdType;
import at.vvo.omds.types.omds2Types.v2_16.WaehrungsCdType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Die Darstellung eines Schadens (spartenbezogenes Unterobjekt eines Schadenereignisses)
 * 
 * <p>Java-Klasse für Schaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Schaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BasisSchaden_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BearbStandCd" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BearbStandCd_Type" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenzuordnung"/&gt;
 *         &lt;element name="SchadenTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BeteiligtePersonen" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ReferenzAufBeteiligtePersonSchaden_Type"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="Vertragsrolle" type="{urn:omds20}VtgRolleCd_Type"/&gt;
 *                     &lt;sequence&gt;
 *                       &lt;element name="Schadensrolle" type="{urn:omds20}BetRolleCd_Type"/&gt;
 *                       &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *                     &lt;/sequence&gt;
 *                   &lt;/choice&gt;
 *                   &lt;element name="ZusaetzlicheRollendaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheRollendaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LeistungGeschaetzt" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="WaehrungsCd" type="{urn:omds20}WaehrungsCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Spartendetails" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SpartendetailSchaden_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schaden_Type", propOrder = {
    "bearbStandCd",
    "schadenzuordnung",
    "schadenTxt",
    "beteiligtePersonen",
    "leistungGeschaetzt",
    "waehrungsCd",
    "spartendetails"
})
public class SchadenType
    extends BasisSchadenType
{

    @XmlElement(name = "BearbStandCd")
    protected String bearbStandCd;
    @XmlElement(name = "Schadenzuordnung", required = true)
    protected SchadenzuordnungType schadenzuordnung;
    @XmlElement(name = "SchadenTxt")
    protected String schadenTxt;
    @XmlElement(name = "BeteiligtePersonen")
    protected List<SchadenType.BeteiligtePersonen> beteiligtePersonen;
    @XmlElement(name = "LeistungGeschaetzt")
    protected BigDecimal leistungGeschaetzt;
    @XmlElement(name = "WaehrungsCd")
    @XmlSchemaType(name = "string")
    protected WaehrungsCdType waehrungsCd;
    @XmlElement(name = "Spartendetails")
    protected SpartendetailSchadenType spartendetails;

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

    /**
     * Ruft den Wert der schadenzuordnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public SchadenzuordnungType getSchadenzuordnung() {
        return schadenzuordnung;
    }

    /**
     * Legt den Wert der schadenzuordnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public void setSchadenzuordnung(SchadenzuordnungType value) {
        this.schadenzuordnung = value;
    }

    /**
     * Ruft den Wert der schadenTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenTxt() {
        return schadenTxt;
    }

    /**
     * Legt den Wert der schadenTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenTxt(String value) {
        this.schadenTxt = value;
    }

    /**
     * Gets the value of the beteiligtePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the beteiligtePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeteiligtePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenType.BeteiligtePersonen }
     * 
     * 
     */
    public List<SchadenType.BeteiligtePersonen> getBeteiligtePersonen() {
        if (beteiligtePersonen == null) {
            beteiligtePersonen = new ArrayList<SchadenType.BeteiligtePersonen>();
        }
        return this.beteiligtePersonen;
    }

    /**
     * Ruft den Wert der leistungGeschaetzt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLeistungGeschaetzt() {
        return leistungGeschaetzt;
    }

    /**
     * Legt den Wert der leistungGeschaetzt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLeistungGeschaetzt(BigDecimal value) {
        this.leistungGeschaetzt = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der spartendetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public SpartendetailSchadenType getSpartendetails() {
        return spartendetails;
    }

    /**
     * Legt den Wert der spartendetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public void setSpartendetails(SpartendetailSchadenType value) {
        this.spartendetails = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ReferenzAufBeteiligtePersonSchaden_Type"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice&gt;
     *           &lt;element name="Vertragsrolle" type="{urn:omds20}VtgRolleCd_Type"/&gt;
     *           &lt;sequence&gt;
     *             &lt;element name="Schadensrolle" type="{urn:omds20}BetRolleCd_Type"/&gt;
     *             &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
     *           &lt;/sequence&gt;
     *         &lt;/choice&gt;
     *         &lt;element name="ZusaetzlicheRollendaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheRollendaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vertragsrolle",
        "schadensrolle",
        "geschInteresseLfnr",
        "zusaetzlicheRollendaten"
    })
    public static class BeteiligtePersonen
        extends ReferenzAufBeteiligtePersonSchadenType
    {

        @XmlElement(name = "Vertragsrolle")
        @XmlSchemaType(name = "string")
        protected VtgRolleCdType vertragsrolle;
        @XmlElement(name = "Schadensrolle")
        protected String schadensrolle;
        @XmlElement(name = "GeschInteresseLfnr")
        @XmlSchemaType(name = "unsignedShort")
        protected Integer geschInteresseLfnr;
        @XmlElement(name = "ZusaetzlicheRollendaten")
        protected List<ZusaetzlicheRollendatenType> zusaetzlicheRollendaten;

        /**
         * Ruft den Wert der vertragsrolle-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link VtgRolleCdType }
         *     
         */
        public VtgRolleCdType getVertragsrolle() {
            return vertragsrolle;
        }

        /**
         * Legt den Wert der vertragsrolle-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link VtgRolleCdType }
         *     
         */
        public void setVertragsrolle(VtgRolleCdType value) {
            this.vertragsrolle = value;
        }

        /**
         * Ruft den Wert der schadensrolle-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSchadensrolle() {
            return schadensrolle;
        }

        /**
         * Legt den Wert der schadensrolle-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSchadensrolle(String value) {
            this.schadensrolle = value;
        }

        /**
         * Ruft den Wert der geschInteresseLfnr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getGeschInteresseLfnr() {
            return geschInteresseLfnr;
        }

        /**
         * Legt den Wert der geschInteresseLfnr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setGeschInteresseLfnr(Integer value) {
            this.geschInteresseLfnr = value;
        }

        /**
         * Gets the value of the zusaetzlicheRollendaten property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheRollendaten property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getZusaetzlicheRollendaten().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ZusaetzlicheRollendatenType }
         * 
         * 
         */
        public List<ZusaetzlicheRollendatenType> getZusaetzlicheRollendaten() {
            if (zusaetzlicheRollendaten == null) {
                zusaetzlicheRollendaten = new ArrayList<ZusaetzlicheRollendatenType>();
            }
            return this.zusaetzlicheRollendaten;
        }

    }

}
