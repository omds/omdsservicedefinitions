
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktGenerischType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.SelbstbehaltType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Wurzelelement für Sach-Privat-Produkte, auf welchem alle Sach-Privat Produkte aufbauen sollen
 * 
 * <p>Java-Klasse für ProduktSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProduktGenerisch_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersObjekteRefLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktSachPrivat_Type", propOrder = {
    "versObjekteRefLfnr",
    "selbstbehalt"
})
@XmlSeeAlso({
    ProduktGebaeudeversicherungType.class,
    ProduktHaushaltsversicherungType.class
})
public class ProduktSachPrivatType
    extends ProduktGenerischType
{

    @XmlElement(name = "VersObjekteRefLfnr", type = Integer.class)
    @XmlSchemaType(name = "unsignedShort")
    protected List<Integer> versObjekteRefLfnr;
    @XmlElement(name = "Selbstbehalt")
    protected List<SelbstbehaltType> selbstbehalt;

    /**
     * Gets the value of the versObjekteRefLfnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the versObjekteRefLfnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersObjekteRefLfnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getVersObjekteRefLfnr() {
        if (versObjekteRefLfnr == null) {
            versObjekteRefLfnr = new ArrayList<Integer>();
        }
        return this.versObjekteRefLfnr;
    }

    /**
     * Gets the value of the selbstbehalt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the selbstbehalt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelbstbehalt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelbstbehaltType }
     * 
     * 
     */
    public List<SelbstbehaltType> getSelbstbehalt() {
        if (selbstbehalt == null) {
            selbstbehalt = new ArrayList<SelbstbehaltType>();
        }
        return this.selbstbehalt;
    }

}
