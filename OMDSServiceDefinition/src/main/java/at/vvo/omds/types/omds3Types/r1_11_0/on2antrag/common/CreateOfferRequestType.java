
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateOfferKfzRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Request für das Offert
 * 
 * <p>Java-Klasse für CreateOfferRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}BOAProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DokAnfordVermittler" type="{urn:omds3CommonServiceTypes-1-1-0}ProzessDokRequest_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferRequest_Type", propOrder = {
    "dokumentenAnforderungenVermittler"
})
@XmlSeeAlso({
    CreateOfferKfzRequestType.class,
    CreateOfferRequestGenType.class
})
public abstract class CreateOfferRequestType
    extends BOAProcessRequestType
{

    @XmlElement(name = "DokAnfordVermittler")
    protected List<ProzessDokRequestType> dokumentenAnforderungenVermittler;

    /**
     * <p>Die Dokumente, welche der Vermittler für den Response anfordert.</p>Gets the value of the dokumentenAnforderungenVermittler property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokumentenAnforderungenVermittler property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumentenAnforderungenVermittler().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProzessDokRequestType }
     * 
     * 
     */
    public List<ProzessDokRequestType> getDokumentenAnforderungenVermittler() {
        if (dokumentenAnforderungenVermittler == null) {
            dokumentenAnforderungenVermittler = new ArrayList<ProzessDokRequestType>();
        }
        return this.dokumentenAnforderungenVermittler;
    }

}
