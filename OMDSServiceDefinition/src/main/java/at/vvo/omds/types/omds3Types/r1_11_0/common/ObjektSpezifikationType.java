
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SchadenmelderVermittlerType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Eine abstrakte Spezifikation eines Objekts
 * 
 * <p>Java-Klasse für ObjektSpezifikation_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObjektSpezifikation_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjektSpezifikation_Type")
@XmlSeeAlso({
    PolizzenObjektSpezifikationType.class,
    SchadenObjektSpezifikationType.class,
    SchadenmelderVermittlerType.class
})
public abstract class ObjektSpezifikationType {


}
