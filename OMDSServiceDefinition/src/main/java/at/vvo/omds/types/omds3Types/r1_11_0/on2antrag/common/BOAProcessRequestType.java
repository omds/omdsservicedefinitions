
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonProcessRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ErsatzpolizzeType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstraktes Zwischenelement für alle BOA-Request-Types
 * 
 * <p>Java-Klasse für BOAProcessRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BOAProcessRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Aenderungsgrund" type="{urn:omds20}AendGrundCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Ersatzpolizzen" type="{urn:omds3CommonServiceTypes-1-1-0}Ersatzpolizze_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BOAProcessRequest_Type", propOrder = {
    "aenderungsgrund",
    "ersatzpolizzen"
})
@XmlSeeAlso({
    SubmitApplicationRequestType.class,
    CreateApplicationRequestType.class,
    CreateOfferRequestType.class,
    CalculateRequestType.class
})
public class BOAProcessRequestType
    extends CommonProcessRequestType
{

    @XmlElement(name = "Aenderungsgrund")
    protected String aenderungsgrund;
    @XmlElement(name = "Ersatzpolizzen")
    protected List<ErsatzpolizzeType> ersatzpolizzen;

    /**
     * Ruft den Wert der aenderungsgrund-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungsgrund() {
        return aenderungsgrund;
    }

    /**
     * Legt den Wert der aenderungsgrund-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungsgrund(String value) {
        this.aenderungsgrund = value;
    }

    /**
     * Gets the value of the ersatzpolizzen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the ersatzpolizzen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErsatzpolizzen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErsatzpolizzeType }
     * 
     * 
     */
    public List<ErsatzpolizzeType> getErsatzpolizzen() {
        if (ersatzpolizzen == null) {
            ersatzpolizzen = new ArrayList<ErsatzpolizzeType>();
        }
        return this.ersatzpolizzen;
    }

}
