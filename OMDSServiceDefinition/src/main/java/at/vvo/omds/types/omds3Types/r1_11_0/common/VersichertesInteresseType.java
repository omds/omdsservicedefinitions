
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds2Types.v2_16.ELBetragType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Obertyp für versicherte Interessen
 * 
 * <p>Java-Klasse für VersichertesInteresse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersichertesInteresse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ErsetztId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheRisikodaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheRisikodaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Bewertung" type="{urn:omds20}EL-Betrag_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Lfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersichertesInteresse_Type", propOrder = {
    "ersetztId",
    "zusaetzlicheRisikodaten",
    "bewertung"
})
@XmlSeeAlso({
    FahrzeugType.class,
    VersichertesInteresseMitAttributMetadatenType.class
})
public abstract class VersichertesInteresseType {

    @XmlElement(name = "ErsetztId")
    protected String ersetztId;
    @XmlElement(name = "ZusaetzlicheRisikodaten")
    protected List<ZusaetzlicheRisikodatenType> zusaetzlicheRisikodaten;
    @XmlElement(name = "Bewertung")
    protected List<ELBetragType> bewertung;
    @XmlAttribute(name = "Lfnr", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;

    /**
     * Ruft den Wert der ersetztId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErsetztId() {
        return ersetztId;
    }

    /**
     * Legt den Wert der ersetztId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErsetztId(String value) {
        this.ersetztId = value;
    }

    /**
     * Gets the value of the zusaetzlicheRisikodaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheRisikodaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheRisikodaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheRisikodatenType }
     * 
     * 
     */
    public List<ZusaetzlicheRisikodatenType> getZusaetzlicheRisikodaten() {
        if (zusaetzlicheRisikodaten == null) {
            zusaetzlicheRisikodaten = new ArrayList<ZusaetzlicheRisikodatenType>();
        }
        return this.zusaetzlicheRisikodaten;
    }

    /**
     * Gets the value of the bewertung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bewertung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBewertung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELBetragType }
     * 
     * 
     */
    public List<ELBetragType> getBewertung() {
        if (bewertung == null) {
            bewertung = new ArrayList<ELBetragType>();
        }
        return this.bewertung;
    }

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

}
