
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CalculateLebenRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "CalculateLebenRequest");
    private final static QName _CalculateLebenResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "CalculateLebenResponse");
    private final static QName _CreateOfferLebenRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "CreateOfferLebenRequest");
    private final static QName _CreateOfferLebenResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "CreateOfferLebenResponse");
    private final static QName _CreateApplicationLebenRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "CreateApplicationLebenRequest");
    private final static QName _CreateApplicationLebenResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "CreateApplicationLebenResponse");
    private final static QName _SubmitApplicationLebenRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "SubmitApplicationLebenRequest");
    private final static QName _SubmitApplicationLebenResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "SubmitApplicationLebenResponse");
    private final static QName _ZusatzversicherungUnfallinvaliditaetTypeVersicherungssumme_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", "Versicherungssumme");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CalculateLebenRequestType }
     * 
     */
    public CalculateLebenRequestType createCalculateLebenRequestType() {
        return new CalculateLebenRequestType();
    }

    /**
     * Create an instance of {@link CalculateLebenResponseType }
     * 
     */
    public CalculateLebenResponseType createCalculateLebenResponseType() {
        return new CalculateLebenResponseType();
    }

    /**
     * Create an instance of {@link CreateOfferLebenRequestType }
     * 
     */
    public CreateOfferLebenRequestType createCreateOfferLebenRequestType() {
        return new CreateOfferLebenRequestType();
    }

    /**
     * Create an instance of {@link CreateOfferLebenResponseType }
     * 
     */
    public CreateOfferLebenResponseType createCreateOfferLebenResponseType() {
        return new CreateOfferLebenResponseType();
    }

    /**
     * Create an instance of {@link CreateApplicationLebenRequestType }
     * 
     */
    public CreateApplicationLebenRequestType createCreateApplicationLebenRequestType() {
        return new CreateApplicationLebenRequestType();
    }

    /**
     * Create an instance of {@link CreateApplicationLebenResponseType }
     * 
     */
    public CreateApplicationLebenResponseType createCreateApplicationLebenResponseType() {
        return new CreateApplicationLebenResponseType();
    }

    /**
     * Create an instance of {@link SubmitApplicationLebenRequestType }
     * 
     */
    public SubmitApplicationLebenRequestType createSubmitApplicationLebenRequestType() {
        return new SubmitApplicationLebenRequestType();
    }

    /**
     * Create an instance of {@link SubmitApplicationLebenResponseType }
     * 
     */
    public SubmitApplicationLebenResponseType createSubmitApplicationLebenResponseType() {
        return new SubmitApplicationLebenResponseType();
    }

    /**
     * Create an instance of {@link VerkaufsproduktLebenType }
     * 
     */
    public VerkaufsproduktLebenType createVerkaufsproduktLebenType() {
        return new VerkaufsproduktLebenType();
    }

    /**
     * Create an instance of {@link ProduktLebenType }
     * 
     */
    public ProduktLebenType createProduktLebenType() {
        return new ProduktLebenType();
    }

    /**
     * Create an instance of {@link TarifLebenType }
     * 
     */
    public TarifLebenType createTarifLebenType() {
        return new TarifLebenType();
    }

    /**
     * Create an instance of {@link ZusatzversicherungLebenType }
     * 
     */
    public ZusatzversicherungLebenType createZusatzversicherungLebenType() {
        return new ZusatzversicherungLebenType();
    }

    /**
     * Create an instance of {@link ZusatzversicherungBerufsunfaehigkeitType }
     * 
     */
    public ZusatzversicherungBerufsunfaehigkeitType createZusatzversicherungBerufsunfaehigkeitType() {
        return new ZusatzversicherungBerufsunfaehigkeitType();
    }

    /**
     * Create an instance of {@link ZusatzversicherungErwerbsunfaehigkeitType }
     * 
     */
    public ZusatzversicherungErwerbsunfaehigkeitType createZusatzversicherungErwerbsunfaehigkeitType() {
        return new ZusatzversicherungErwerbsunfaehigkeitType();
    }

    /**
     * Create an instance of {@link ZusatzversicherungPraemienuebernahmeAblebenType }
     * 
     */
    public ZusatzversicherungPraemienuebernahmeAblebenType createZusatzversicherungPraemienuebernahmeAblebenType() {
        return new ZusatzversicherungPraemienuebernahmeAblebenType();
    }

    /**
     * Create an instance of {@link ZusatzversicherungUnfalltodType }
     * 
     */
    public ZusatzversicherungUnfalltodType createZusatzversicherungUnfalltodType() {
        return new ZusatzversicherungUnfalltodType();
    }

    /**
     * Create an instance of {@link ZusatzversicherungUnfallinvaliditaetType }
     * 
     */
    public ZusatzversicherungUnfallinvaliditaetType createZusatzversicherungUnfallinvaliditaetType() {
        return new ZusatzversicherungUnfallinvaliditaetType();
    }

    /**
     * Create an instance of {@link ZusatzproduktLebenType }
     * 
     */
    public ZusatzproduktLebenType createZusatzproduktLebenType() {
        return new ZusatzproduktLebenType();
    }

    /**
     * Create an instance of {@link RentenoptionType }
     * 
     */
    public RentenoptionType createRentenoptionType() {
        return new RentenoptionType();
    }

    /**
     * Create an instance of {@link VersicherungssummeZusatzbausteinType }
     * 
     */
    public VersicherungssummeZusatzbausteinType createVersicherungssummeZusatzbausteinType() {
        return new VersicherungssummeZusatzbausteinType();
    }

    /**
     * Create an instance of {@link SpezBerechnungLebenType }
     * 
     */
    public SpezBerechnungLebenType createSpezBerechnungLebenType() {
        return new SpezBerechnungLebenType();
    }

    /**
     * Create an instance of {@link SpezOffertLebenType }
     * 
     */
    public SpezOffertLebenType createSpezOffertLebenType() {
        return new SpezOffertLebenType();
    }

    /**
     * Create an instance of {@link SpezAntragLebenType }
     * 
     */
    public SpezAntragLebenType createSpezAntragLebenType() {
        return new SpezAntragLebenType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateLebenRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CalculateLebenRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "CalculateLebenRequest")
    public JAXBElement<CalculateLebenRequestType> createCalculateLebenRequest(CalculateLebenRequestType value) {
        return new JAXBElement<CalculateLebenRequestType>(_CalculateLebenRequest_QNAME, CalculateLebenRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateLebenResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CalculateLebenResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "CalculateLebenResponse")
    public JAXBElement<CalculateLebenResponseType> createCalculateLebenResponse(CalculateLebenResponseType value) {
        return new JAXBElement<CalculateLebenResponseType>(_CalculateLebenResponse_QNAME, CalculateLebenResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferLebenRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateOfferLebenRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "CreateOfferLebenRequest")
    public JAXBElement<CreateOfferLebenRequestType> createCreateOfferLebenRequest(CreateOfferLebenRequestType value) {
        return new JAXBElement<CreateOfferLebenRequestType>(_CreateOfferLebenRequest_QNAME, CreateOfferLebenRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferLebenResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateOfferLebenResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "CreateOfferLebenResponse")
    public JAXBElement<CreateOfferLebenResponseType> createCreateOfferLebenResponse(CreateOfferLebenResponseType value) {
        return new JAXBElement<CreateOfferLebenResponseType>(_CreateOfferLebenResponse_QNAME, CreateOfferLebenResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationLebenRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateApplicationLebenRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "CreateApplicationLebenRequest")
    public JAXBElement<CreateApplicationLebenRequestType> createCreateApplicationLebenRequest(CreateApplicationLebenRequestType value) {
        return new JAXBElement<CreateApplicationLebenRequestType>(_CreateApplicationLebenRequest_QNAME, CreateApplicationLebenRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationLebenResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateApplicationLebenResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "CreateApplicationLebenResponse")
    public JAXBElement<CreateApplicationLebenResponseType> createCreateApplicationLebenResponse(CreateApplicationLebenResponseType value) {
        return new JAXBElement<CreateApplicationLebenResponseType>(_CreateApplicationLebenResponse_QNAME, CreateApplicationLebenResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationLebenRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitApplicationLebenRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "SubmitApplicationLebenRequest")
    public JAXBElement<SubmitApplicationLebenRequestType> createSubmitApplicationLebenRequest(SubmitApplicationLebenRequestType value) {
        return new JAXBElement<SubmitApplicationLebenRequestType>(_SubmitApplicationLebenRequest_QNAME, SubmitApplicationLebenRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationLebenResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitApplicationLebenResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "SubmitApplicationLebenResponse")
    public JAXBElement<SubmitApplicationLebenResponseType> createSubmitApplicationLebenResponse(SubmitApplicationLebenResponseType value) {
        return new JAXBElement<SubmitApplicationLebenResponseType>(_SubmitApplicationLebenResponse_QNAME, SubmitApplicationLebenResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersicherungssummeZusatzbausteinType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link VersicherungssummeZusatzbausteinType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "Versicherungssumme", scope = ZusatzversicherungUnfallinvaliditaetType.class)
    public JAXBElement<VersicherungssummeZusatzbausteinType> createZusatzversicherungUnfallinvaliditaetTypeVersicherungssumme(VersicherungssummeZusatzbausteinType value) {
        return new JAXBElement<VersicherungssummeZusatzbausteinType>(_ZusatzversicherungUnfallinvaliditaetTypeVersicherungssumme_QNAME, VersicherungssummeZusatzbausteinType.class, ZusatzversicherungUnfallinvaliditaetType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersicherungssummeZusatzbausteinType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link VersicherungssummeZusatzbausteinType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", name = "Versicherungssumme", scope = ZusatzversicherungUnfalltodType.class)
    public JAXBElement<VersicherungssummeZusatzbausteinType> createZusatzversicherungUnfalltodTypeVersicherungssumme(VersicherungssummeZusatzbausteinType value) {
        return new JAXBElement<VersicherungssummeZusatzbausteinType>(_ZusatzversicherungUnfallinvaliditaetTypeVersicherungssumme_QNAME, VersicherungssummeZusatzbausteinType.class, ZusatzversicherungUnfalltodType.class, value);
    }

}
