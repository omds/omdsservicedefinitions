
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Spezifiziert die Konvertierung eines Vertrags in einen neuen Vertrag
 * 
 * <p>Java-Klasse für KonvertierungsumfangVertrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KonvertierungsumfangVertrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Ersatzpolizze_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Bezeichnung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Baustein" type="{urn:omds3CommonServiceTypes-1-1-0}KonvertierungProduktBaustein_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Kombinationen" type="{urn:omds3CommonServiceTypes-1-1-0}Kombinationen_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KonvertierungsumfangVertrag_Type", propOrder = {
    "bezeichnung",
    "bausteine",
    "kombinationen"
})
public class KonvertierungsumfangVertragType
    extends ErsatzpolizzeType
{

    @XmlElement(name = "Bezeichnung")
    protected String bezeichnung;
    @XmlElement(name = "Baustein", required = true)
    protected List<KonvertierungProduktBausteinType> bausteine;
    @XmlElement(name = "Kombinationen")
    protected KombinationenType kombinationen;

    /**
     * Ruft den Wert der bezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Legt den Wert der bezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

    /**
     * <p>Die Liste der Bausteine, die in der Konvertierungsanforderung behandelt werden.</p>Gets the value of the bausteine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bausteine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBausteine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KonvertierungProduktBausteinType }
     * 
     * 
     */
    public List<KonvertierungProduktBausteinType> getBausteine() {
        if (bausteine == null) {
            bausteine = new ArrayList<KonvertierungProduktBausteinType>();
        }
        return this.bausteine;
    }

    /**
     * Ruft den Wert der kombinationen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KombinationenType }
     *     
     */
    public KombinationenType getKombinationen() {
        return kombinationen;
    }

    /**
     * Legt den Wert der kombinationen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KombinationenType }
     *     
     */
    public void setKombinationen(KombinationenType value) {
        this.kombinationen = value;
    }

}
