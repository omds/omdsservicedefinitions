
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Zuordnung Schaden
 * 
 * <p>Java-Klasse für SchadenZuordnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenZuordnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}DokumentenZuordnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *         &lt;element name="BearbStand" type="{urn:omds20}BearbStandCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Person" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PersonId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *                   &lt;element name="Rolle" type="{urn:omds20}BetRolleCd_Type"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenZuordnung_Type", propOrder = {
    "schadennr",
    "bearbStand",
    "person"
})
public class SchadenZuordnungType
    extends DokumentenZuordnungType
{

    @XmlElement(name = "Schadennr", required = true)
    protected String schadennr;
    @XmlElement(name = "BearbStand")
    protected String bearbStand;
    @XmlElement(name = "Person")
    protected List<SchadenZuordnungType.Person> person;

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der bearbStand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStand() {
        return bearbStand;
    }

    /**
     * Legt den Wert der bearbStand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStand(String value) {
        this.bearbStand = value;
    }

    /**
     * Gets the value of the person property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the person property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPerson().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenZuordnungType.Person }
     * 
     * 
     */
    public List<SchadenZuordnungType.Person> getPerson() {
        if (person == null) {
            person = new ArrayList<SchadenZuordnungType.Person>();
        }
        return this.person;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PersonId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
     *         &lt;element name="Rolle" type="{urn:omds20}BetRolleCd_Type"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personId",
        "rolle"
    })
    public static class Person {

        @XmlElement(name = "PersonId", required = true)
        protected ObjektIdType personId;
        @XmlElement(name = "Rolle", required = true)
        protected String rolle;

        /**
         * Ruft den Wert der personId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ObjektIdType }
         *     
         */
        public ObjektIdType getPersonId() {
            return personId;
        }

        /**
         * Legt den Wert der personId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ObjektIdType }
         *     
         */
        public void setPersonId(ObjektIdType value) {
            this.personId = value;
        }

        /**
         * Ruft den Wert der rolle-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRolle() {
            return rolle;
        }

        /**
         * Legt den Wert der rolle-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRolle(String value) {
            this.rolle = value;
        }

    }

}
