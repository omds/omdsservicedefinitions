
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Details zur Treuhaenderfrage
 * 
 * <p>Java-Klasse für Treuhaenderfrage_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Treuhaenderfrage_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Treuhaender" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Beschreibung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Treuhaenderfrage_Type", propOrder = {
    "treuhaender",
    "beschreibung"
})
public class TreuhaenderfrageType {

    @XmlElement(name = "Treuhaender")
    protected boolean treuhaender;
    @XmlElement(name = "Beschreibung")
    protected String beschreibung;

    /**
     * Ruft den Wert der treuhaender-Eigenschaft ab.
     * 
     */
    public boolean isTreuhaender() {
        return treuhaender;
    }

    /**
     * Legt den Wert der treuhaender-Eigenschaft fest.
     * 
     */
    public void setTreuhaender(boolean value) {
        this.treuhaender = value;
    }

    /**
     * Ruft den Wert der beschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * Legt den Wert der beschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeschreibung(String value) {
        this.beschreibung = value;
    }

}
