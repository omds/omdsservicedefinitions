
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_11_0.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceFault_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "serviceFault");
    private final static QName _OrdnungsbegriffZuordFremd_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "OrdnungsbegriffZuordFremd");
    private final static QName _ObjektId_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "ObjektId");
    private final static QName _Person_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "Person");
    private final static QName _Adresse_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "Adresse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_11_0.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GMSGType }
     * 
     */
    public GMSGType createGMSGType() {
        return new GMSGType();
    }

    /**
     * Create an instance of {@link SchadenZuordnungType }
     * 
     */
    public SchadenZuordnungType createSchadenZuordnungType() {
        return new SchadenZuordnungType();
    }

    /**
     * Create an instance of {@link VertragsZuordnungType }
     * 
     */
    public VertragsZuordnungType createVertragsZuordnungType() {
        return new VertragsZuordnungType();
    }

    /**
     * Create an instance of {@link DokumentenReferenzPostserviceType }
     * 
     */
    public DokumentenReferenzPostserviceType createDokumentenReferenzPostserviceType() {
        return new DokumentenReferenzPostserviceType();
    }

    /**
     * Create an instance of {@link FahrzeugType }
     * 
     */
    public FahrzeugType createFahrzeugType() {
        return new FahrzeugType();
    }

    /**
     * Create an instance of {@link ZahlwegType }
     * 
     */
    public ZahlwegType createZahlwegType() {
        return new ZahlwegType();
    }

    /**
     * Create an instance of {@link ProzessDokumentType }
     * 
     */
    public ProzessDokumentType createProzessDokumentType() {
        return new ProzessDokumentType();
    }

    /**
     * Create an instance of {@link ServiceFault }
     * 
     */
    public ServiceFault createServiceFault() {
        return new ServiceFault();
    }

    /**
     * Create an instance of {@link ObjektIdType }
     * 
     */
    public ObjektIdType createObjektIdType() {
        return new ObjektIdType();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link AdresseType }
     * 
     */
    public AdresseType createAdresseType() {
        return new AdresseType();
    }

    /**
     * Create an instance of {@link AgentFilterType }
     * 
     */
    public AgentFilterType createAgentFilterType() {
        return new AgentFilterType();
    }

    /**
     * Create an instance of {@link ElementIdType }
     * 
     */
    public ElementIdType createElementIdType() {
        return new ElementIdType();
    }

    /**
     * Create an instance of {@link ResponseStatusType }
     * 
     */
    public ResponseStatusType createResponseStatusType() {
        return new ResponseStatusType();
    }

    /**
     * Create an instance of {@link Referenz }
     * 
     */
    public Referenz createReferenz() {
        return new Referenz();
    }

    /**
     * Create an instance of {@link LegitimationType }
     * 
     */
    public LegitimationType createLegitimationType() {
        return new LegitimationType();
    }

    /**
     * Create an instance of {@link DateianhangType }
     * 
     */
    public DateianhangType createDateianhangType() {
        return new DateianhangType();
    }

    /**
     * Create an instance of {@link DokumentBinaryDataType }
     * 
     */
    public DokumentBinaryDataType createDokumentBinaryDataType() {
        return new DokumentBinaryDataType();
    }

    /**
     * Create an instance of {@link AutorisierungType }
     * 
     */
    public AutorisierungType createAutorisierungType() {
        return new AutorisierungType();
    }

    /**
     * Create an instance of {@link AutorisierungsAnforderungType }
     * 
     */
    public AutorisierungsAnforderungType createAutorisierungsAnforderungType() {
        return new AutorisierungsAnforderungType();
    }

    /**
     * Create an instance of {@link AutorisierungsartType }
     * 
     */
    public AutorisierungsartType createAutorisierungsartType() {
        return new AutorisierungsartType();
    }

    /**
     * Create an instance of {@link ProzessDokRequestType }
     * 
     */
    public ProzessDokRequestType createProzessDokRequestType() {
        return new ProzessDokRequestType();
    }

    /**
     * Create an instance of {@link ProzessDokRequirementType }
     * 
     */
    public ProzessDokRequirementType createProzessDokRequirementType() {
        return new ProzessDokRequirementType();
    }

    /**
     * Create an instance of {@link ProzessDokToReturnType }
     * 
     */
    public ProzessDokToReturnType createProzessDokToReturnType() {
        return new ProzessDokToReturnType();
    }

    /**
     * Create an instance of {@link ProzessDokHandoutType }
     * 
     */
    public ProzessDokHandoutType createProzessDokHandoutType() {
        return new ProzessDokHandoutType();
    }

    /**
     * Create an instance of {@link KontierungType }
     * 
     */
    public KontierungType createKontierungType() {
        return new KontierungType();
    }

    /**
     * Create an instance of {@link KombinationenType }
     * 
     */
    public KombinationenType createKombinationenType() {
        return new KombinationenType();
    }

    /**
     * Create an instance of {@link KombinationType }
     * 
     */
    public KombinationType createKombinationType() {
        return new KombinationType();
    }

    /**
     * Create an instance of {@link ZusatzproduktGenerischType }
     * 
     */
    public ZusatzproduktGenerischType createZusatzproduktGenerischType() {
        return new ZusatzproduktGenerischType();
    }

    /**
     * Create an instance of {@link BeteiligtePersonVertragType }
     * 
     */
    public BeteiligtePersonVertragType createBeteiligtePersonVertragType() {
        return new BeteiligtePersonVertragType();
    }

    /**
     * Create an instance of {@link PraemieType }
     * 
     */
    public PraemieType createPraemieType() {
        return new PraemieType();
    }

    /**
     * Create an instance of {@link VersicherungssteuerType }
     * 
     */
    public VersicherungssteuerType createVersicherungssteuerType() {
        return new VersicherungssteuerType();
    }

    /**
     * Create an instance of {@link TechnicalKeyValueType }
     * 
     */
    public TechnicalKeyValueType createTechnicalKeyValueType() {
        return new TechnicalKeyValueType();
    }

    /**
     * Create an instance of {@link VertragspersonType }
     * 
     */
    public VertragspersonType createVertragspersonType() {
        return new VertragspersonType();
    }

    /**
     * Create an instance of {@link VinkulierungType }
     * 
     */
    public VinkulierungType createVinkulierungType() {
        return new VinkulierungType();
    }

    /**
     * Create an instance of {@link VinkularglaeubigerType }
     * 
     */
    public VinkularglaeubigerType createVinkularglaeubigerType() {
        return new VinkularglaeubigerType();
    }

    /**
     * Create an instance of {@link BezugsrechtType }
     * 
     */
    public BezugsrechtType createBezugsrechtType() {
        return new BezugsrechtType();
    }

    /**
     * Create an instance of {@link BezugsberechtigungGesetzlicheErbenType }
     * 
     */
    public BezugsberechtigungGesetzlicheErbenType createBezugsberechtigungGesetzlicheErbenType() {
        return new BezugsberechtigungGesetzlicheErbenType();
    }

    /**
     * Create an instance of {@link BezugsberechtigungTestamentarischeErbenType }
     * 
     */
    public BezugsberechtigungTestamentarischeErbenType createBezugsberechtigungTestamentarischeErbenType() {
        return new BezugsberechtigungTestamentarischeErbenType();
    }

    /**
     * Create an instance of {@link BezugsberechtigungUeberbringerType }
     * 
     */
    public BezugsberechtigungUeberbringerType createBezugsberechtigungUeberbringerType() {
        return new BezugsberechtigungUeberbringerType();
    }

    /**
     * Create an instance of {@link BezugsberechtigungNamentlich }
     * 
     */
    public BezugsberechtigungNamentlich createBezugsberechtigungNamentlich() {
        return new BezugsberechtigungNamentlich();
    }

    /**
     * Create an instance of {@link PersonNamentlichesBezugsrechtType }
     * 
     */
    public PersonNamentlichesBezugsrechtType createPersonNamentlichesBezugsrechtType() {
        return new PersonNamentlichesBezugsrechtType();
    }

    /**
     * Create an instance of {@link BezugsberechtigungVersicherungsnehmerType }
     * 
     */
    public BezugsberechtigungVersicherungsnehmerType createBezugsberechtigungVersicherungsnehmerType() {
        return new BezugsberechtigungVersicherungsnehmerType();
    }

    /**
     * Create an instance of {@link BezugsberechtigungVersichertePersonType }
     * 
     */
    public BezugsberechtigungVersichertePersonType createBezugsberechtigungVersichertePersonType() {
        return new BezugsberechtigungVersichertePersonType();
    }

    /**
     * Create an instance of {@link BezugsberechtigungIndividuell }
     * 
     */
    public BezugsberechtigungIndividuell createBezugsberechtigungIndividuell() {
        return new BezugsberechtigungIndividuell();
    }

    /**
     * Create an instance of {@link BonusMalusSystemType }
     * 
     */
    public BonusMalusSystemType createBonusMalusSystemType() {
        return new BonusMalusSystemType();
    }

    /**
     * Create an instance of {@link OffeneSchaedenType }
     * 
     */
    public OffeneSchaedenType createOffeneSchaedenType() {
        return new OffeneSchaedenType();
    }

    /**
     * Create an instance of {@link OffenerSchadenType }
     * 
     */
    public OffenerSchadenType createOffenerSchadenType() {
        return new OffenerSchadenType();
    }

    /**
     * Create an instance of {@link VorversicherungenImplType }
     * 
     */
    public VorversicherungenImplType createVorversicherungenImplType() {
        return new VorversicherungenImplType();
    }

    /**
     * Create an instance of {@link VorversicherungType }
     * 
     */
    public VorversicherungType createVorversicherungType() {
        return new VorversicherungType();
    }

    /**
     * Create an instance of {@link VorversicherungenDetailType }
     * 
     */
    public VorversicherungenDetailType createVorversicherungenDetailType() {
        return new VorversicherungenDetailType();
    }

    /**
     * Create an instance of {@link DatenverwendungType }
     * 
     */
    public DatenverwendungType createDatenverwendungType() {
        return new DatenverwendungType();
    }

    /**
     * Create an instance of {@link ErsatzpolizzenType }
     * 
     */
    public ErsatzpolizzenType createErsatzpolizzenType() {
        return new ErsatzpolizzenType();
    }

    /**
     * Create an instance of {@link ErsatzpolizzeType }
     * 
     */
    public ErsatzpolizzeType createErsatzpolizzeType() {
        return new ErsatzpolizzeType();
    }

    /**
     * Create an instance of {@link ErsatzpolizzeMitAendGrundType }
     * 
     */
    public ErsatzpolizzeMitAendGrundType createErsatzpolizzeMitAendGrundType() {
        return new ErsatzpolizzeMitAendGrundType();
    }

    /**
     * Create an instance of {@link KonvertierungsumfangVertragType }
     * 
     */
    public KonvertierungsumfangVertragType createKonvertierungsumfangVertragType() {
        return new KonvertierungsumfangVertragType();
    }

    /**
     * Create an instance of {@link KonvertierungBausteinType }
     * 
     */
    public KonvertierungBausteinType createKonvertierungBausteinType() {
        return new KonvertierungBausteinType();
    }

    /**
     * Create an instance of {@link KonvertierungProduktBausteinType }
     * 
     */
    public KonvertierungProduktBausteinType createKonvertierungProduktBausteinType() {
        return new KonvertierungProduktBausteinType();
    }

    /**
     * Create an instance of {@link KonvertierungsaktionBelassenType }
     * 
     */
    public KonvertierungsaktionBelassenType createKonvertierungsaktionBelassenType() {
        return new KonvertierungsaktionBelassenType();
    }

    /**
     * Create an instance of {@link KonvertierungsaktionUebernehmenType }
     * 
     */
    public KonvertierungsaktionUebernehmenType createKonvertierungsaktionUebernehmenType() {
        return new KonvertierungsaktionUebernehmenType();
    }

    /**
     * Create an instance of {@link KonvertierungsaktionStornoType }
     * 
     */
    public KonvertierungsaktionStornoType createKonvertierungsaktionStornoType() {
        return new KonvertierungsaktionStornoType();
    }

    /**
     * Create an instance of {@link DokumentInfoType }
     * 
     */
    public DokumentInfoType createDokumentInfoType() {
        return new DokumentInfoType();
    }

    /**
     * Create an instance of {@link ZahlungsdatenType }
     * 
     */
    public ZahlungsdatenType createZahlungsdatenType() {
        return new ZahlungsdatenType();
    }

    /**
     * Create an instance of {@link KreditkarteType }
     * 
     */
    public KreditkarteType createKreditkarteType() {
        return new KreditkarteType();
    }

    /**
     * Create an instance of {@link BankverbindungType }
     * 
     */
    public BankverbindungType createBankverbindungType() {
        return new BankverbindungType();
    }

    /**
     * Create an instance of {@link PersBankverbindungType }
     * 
     */
    public PersBankverbindungType createPersBankverbindungType() {
        return new PersBankverbindungType();
    }

    /**
     * Create an instance of {@link VersichertePersonType }
     * 
     */
    public VersichertePersonType createVersichertePersonType() {
        return new VersichertePersonType();
    }

    /**
     * Create an instance of {@link RisikoNatPersonType }
     * 
     */
    public RisikoNatPersonType createRisikoNatPersonType() {
        return new RisikoNatPersonType();
    }

    /**
     * Create an instance of {@link ZulassungsdatenType }
     * 
     */
    public ZulassungsdatenType createZulassungsdatenType() {
        return new ZulassungsdatenType();
    }

    /**
     * Create an instance of {@link VersicherteLiegenschaftType }
     * 
     */
    public VersicherteLiegenschaftType createVersicherteLiegenschaftType() {
        return new VersicherteLiegenschaftType();
    }

    /**
     * Create an instance of {@link KostenFixOderProzentType }
     * 
     */
    public KostenFixOderProzentType createKostenFixOderProzentType() {
        return new KostenFixOderProzentType();
    }

    /**
     * Create an instance of {@link SelbstbehaltType }
     * 
     */
    public SelbstbehaltType createSelbstbehaltType() {
        return new SelbstbehaltType();
    }

    /**
     * Create an instance of {@link UploadDokumentType }
     * 
     */
    public UploadDokumentType createUploadDokumentType() {
        return new UploadDokumentType();
    }

    /**
     * Create an instance of {@link DokumentenReferenzType }
     * 
     */
    public DokumentenReferenzType createDokumentenReferenzType() {
        return new DokumentenReferenzType();
    }

    /**
     * Create an instance of {@link EinfacheZuordnungType }
     * 
     */
    public EinfacheZuordnungType createEinfacheZuordnungType() {
        return new EinfacheZuordnungType();
    }

    /**
     * Create an instance of {@link PersonenZuordnungType }
     * 
     */
    public PersonenZuordnungType createPersonenZuordnungType() {
        return new PersonenZuordnungType();
    }

    /**
     * Create an instance of {@link BetreuerZuordnungType }
     * 
     */
    public BetreuerZuordnungType createBetreuerZuordnungType() {
        return new BetreuerZuordnungType();
    }

    /**
     * Create an instance of {@link AntragsZuordnungType }
     * 
     */
    public AntragsZuordnungType createAntragsZuordnungType() {
        return new AntragsZuordnungType();
    }

    /**
     * Create an instance of {@link GeschaeftsfallZuordnungType }
     * 
     */
    public GeschaeftsfallZuordnungType createGeschaeftsfallZuordnungType() {
        return new GeschaeftsfallZuordnungType();
    }

    /**
     * Create an instance of {@link ProvisionZuordnungType }
     * 
     */
    public ProvisionZuordnungType createProvisionZuordnungType() {
        return new ProvisionZuordnungType();
    }

    /**
     * Create an instance of {@link MahnverfahrenZuordnungType }
     * 
     */
    public MahnverfahrenZuordnungType createMahnverfahrenZuordnungType() {
        return new MahnverfahrenZuordnungType();
    }

    /**
     * Create an instance of {@link ZeitraumType }
     * 
     */
    public ZeitraumType createZeitraumType() {
        return new ZeitraumType();
    }

    /**
     * Create an instance of {@link PolizzenObjektSpezifikationType }
     * 
     */
    public PolizzenObjektSpezifikationType createPolizzenObjektSpezifikationType() {
        return new PolizzenObjektSpezifikationType();
    }

    /**
     * Create an instance of {@link SchadenObjektSpezifikationType }
     * 
     */
    public SchadenObjektSpezifikationType createSchadenObjektSpezifikationType() {
        return new SchadenObjektSpezifikationType();
    }

    /**
     * Create an instance of {@link AttributMsgType }
     * 
     */
    public AttributMsgType createAttributMsgType() {
        return new AttributMsgType();
    }

    /**
     * Create an instance of {@link AttributMetadatenStringType }
     * 
     */
    public AttributMetadatenStringType createAttributMetadatenStringType() {
        return new AttributMetadatenStringType();
    }

    /**
     * Create an instance of {@link AttributMetadatenIntType }
     * 
     */
    public AttributMetadatenIntType createAttributMetadatenIntType() {
        return new AttributMetadatenIntType();
    }

    /**
     * Create an instance of {@link AttributMetadatenDezimalType }
     * 
     */
    public AttributMetadatenDezimalType createAttributMetadatenDezimalType() {
        return new AttributMetadatenDezimalType();
    }

    /**
     * Create an instance of {@link AttributMetadatenDatumType }
     * 
     */
    public AttributMetadatenDatumType createAttributMetadatenDatumType() {
        return new AttributMetadatenDatumType();
    }

    /**
     * Create an instance of {@link AttributMetadatenEnumType }
     * 
     */
    public AttributMetadatenEnumType createAttributMetadatenEnumType() {
        return new AttributMetadatenEnumType();
    }

    /**
     * Create an instance of {@link EintragSchluessellisteType }
     * 
     */
    public EintragSchluessellisteType createEintragSchluessellisteType() {
        return new EintragSchluessellisteType();
    }

    /**
     * Create an instance of {@link AttributStringType }
     * 
     */
    public AttributStringType createAttributStringType() {
        return new AttributStringType();
    }

    /**
     * Create an instance of {@link AttributIntType }
     * 
     */
    public AttributIntType createAttributIntType() {
        return new AttributIntType();
    }

    /**
     * Create an instance of {@link AttributDezimalType }
     * 
     */
    public AttributDezimalType createAttributDezimalType() {
        return new AttributDezimalType();
    }

    /**
     * Create an instance of {@link AttributDoubleType }
     * 
     */
    public AttributDoubleType createAttributDoubleType() {
        return new AttributDoubleType();
    }

    /**
     * Create an instance of {@link AttributDatumType }
     * 
     */
    public AttributDatumType createAttributDatumType() {
        return new AttributDatumType();
    }

    /**
     * Create an instance of {@link AttributEnumType }
     * 
     */
    public AttributEnumType createAttributEnumType() {
        return new AttributEnumType();
    }

    /**
     * Create an instance of {@link AttributMultiEnumType }
     * 
     */
    public AttributMultiEnumType createAttributMultiEnumType() {
        return new AttributMultiEnumType();
    }

    /**
     * Create an instance of {@link AbtretungType }
     * 
     */
    public AbtretungType createAbtretungType() {
        return new AbtretungType();
    }

    /**
     * Create an instance of {@link VerpfaendungType }
     * 
     */
    public VerpfaendungType createVerpfaendungType() {
        return new VerpfaendungType();
    }

    /**
     * Create an instance of {@link PfandglaeubigerType }
     * 
     */
    public PfandglaeubigerType createPfandglaeubigerType() {
        return new PfandglaeubigerType();
    }

    /**
     * Create an instance of {@link VinkulierungPersonenType }
     * 
     */
    public VinkulierungPersonenType createVinkulierungPersonenType() {
        return new VinkulierungPersonenType();
    }

    /**
     * Create an instance of {@link GlaeubigerSicherstellungType }
     * 
     */
    public GlaeubigerSicherstellungType createGlaeubigerSicherstellungType() {
        return new GlaeubigerSicherstellungType();
    }

    /**
     * Create an instance of {@link EinwVerarbGesDatenType }
     * 
     */
    public EinwVerarbGesDatenType createEinwVerarbGesDatenType() {
        return new EinwVerarbGesDatenType();
    }

    /**
     * Create an instance of {@link EinwGesDatenVNType }
     * 
     */
    public EinwGesDatenVNType createEinwGesDatenVNType() {
        return new EinwGesDatenVNType();
    }

    /**
     * Create an instance of {@link EinwSprachaufzType }
     * 
     */
    public EinwSprachaufzType createEinwSprachaufzType() {
        return new EinwSprachaufzType();
    }

    /**
     * Create an instance of {@link FATCAType }
     * 
     */
    public FATCAType createFATCAType() {
        return new FATCAType();
    }

    /**
     * Create an instance of {@link FATCANatPersonType }
     * 
     */
    public FATCANatPersonType createFATCANatPersonType() {
        return new FATCANatPersonType();
    }

    /**
     * Create an instance of {@link FATCASonstPersonType }
     * 
     */
    public FATCASonstPersonType createFATCASonstPersonType() {
        return new FATCASonstPersonType();
    }

    /**
     * Create an instance of {@link PEPType }
     * 
     */
    public PEPType createPEPType() {
        return new PEPType();
    }

    /**
     * Create an instance of {@link TreuhaenderfrageType }
     * 
     */
    public TreuhaenderfrageType createTreuhaenderfrageType() {
        return new TreuhaenderfrageType();
    }

    /**
     * Create an instance of {@link ElementFondsauswahlType }
     * 
     */
    public ElementFondsauswahlType createElementFondsauswahlType() {
        return new ElementFondsauswahlType();
    }

    /**
     * Create an instance of {@link at.vvo.omds.types.omds3Types.r1_11_0.common.BasisProduktbausteinType.Konvertierung }
     * 
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.common.BasisProduktbausteinType.Konvertierung createBasisProduktbausteinTypeKonvertierung() {
        return new at.vvo.omds.types.omds3Types.r1_11_0.common.BasisProduktbausteinType.Konvertierung();
    }

    /**
     * Create an instance of {@link GMSGType.SteuerlichAnsaessig }
     * 
     */
    public GMSGType.SteuerlichAnsaessig createGMSGTypeSteuerlichAnsaessig() {
        return new GMSGType.SteuerlichAnsaessig();
    }

    /**
     * Create an instance of {@link SchadenZuordnungType.Person }
     * 
     */
    public SchadenZuordnungType.Person createSchadenZuordnungTypePerson() {
        return new SchadenZuordnungType.Person();
    }

    /**
     * Create an instance of {@link VertragsZuordnungType.Vertragsperson }
     * 
     */
    public VertragsZuordnungType.Vertragsperson createVertragsZuordnungTypeVertragsperson() {
        return new VertragsZuordnungType.Vertragsperson();
    }

    /**
     * Create an instance of {@link DokumentenReferenzPostserviceType.Kontrollwert }
     * 
     */
    public DokumentenReferenzPostserviceType.Kontrollwert createDokumentenReferenzPostserviceTypeKontrollwert() {
        return new DokumentenReferenzPostserviceType.Kontrollwert();
    }

    /**
     * Create an instance of {@link FahrzeugType.KmStand }
     * 
     */
    public FahrzeugType.KmStand createFahrzeugTypeKmStand() {
        return new FahrzeugType.KmStand();
    }

    /**
     * Create an instance of {@link ZahlwegType.Kundenkonto }
     * 
     */
    public ZahlwegType.Kundenkonto createZahlwegTypeKundenkonto() {
        return new ZahlwegType.Kundenkonto();
    }

    /**
     * Create an instance of {@link ProzessDokumentType.Autorisierungen }
     * 
     */
    public ProzessDokumentType.Autorisierungen createProzessDokumentTypeAutorisierungen() {
        return new ProzessDokumentType.Autorisierungen();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFault }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ServiceFault }{@code >}
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "serviceFault")
    public JAXBElement<ServiceFault> createServiceFault(ServiceFault value) {
        return new JAXBElement<ServiceFault>(_ServiceFault_QNAME, ServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "OrdnungsbegriffZuordFremd")
    public JAXBElement<String> createOrdnungsbegriffZuordFremd(String value) {
        return new JAXBElement<String>(_OrdnungsbegriffZuordFremd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "ObjektId")
    public JAXBElement<ObjektIdType> createObjektId(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_ObjektId_QNAME, ObjektIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PersonType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "Person")
    public JAXBElement<PersonType> createPerson(PersonType value) {
        return new JAXBElement<PersonType>(_Person_QNAME, PersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdresseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AdresseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "Adresse")
    public JAXBElement<AdresseType> createAdresse(AdresseType value) {
        return new JAXBElement<AdresseType>(_Adresse_QNAME, AdresseType.class, null, value);
    }

}
