
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Angaben zu einer Vorversicherung in der Standardimplementierung, erweitert VorversicherungenDetail_Type
 * 
 * <p>Java-Klasse für Vorversicherung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Vorversicherung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VorversicherungenDetail_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VtgSparteCd" type="{urn:omds20}VtgSparteCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="SpartenCd" type="{urn:omds20}SpartenCd_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vorversicherung_Type", propOrder = {
    "vtgSparteCd",
    "spartenCd",
    "polizzennr"
})
public class VorversicherungType
    extends VorversicherungenDetailType
{

    @XmlElement(name = "VtgSparteCd")
    protected String vtgSparteCd;
    @XmlElement(name = "SpartenCd")
    protected List<String> spartenCd;
    @XmlElement(name = "Polizzennr")
    protected String polizzennr;

    /**
     * Ruft den Wert der vtgSparteCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVtgSparteCd() {
        return vtgSparteCd;
    }

    /**
     * Legt den Wert der vtgSparteCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVtgSparteCd(String value) {
        this.vtgSparteCd = value;
    }

    /**
     * Gets the value of the spartenCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the spartenCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpartenCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSpartenCd() {
        if (spartenCd == null) {
            spartenCd = new ArrayList<String>();
        }
        return this.spartenCd;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

}
