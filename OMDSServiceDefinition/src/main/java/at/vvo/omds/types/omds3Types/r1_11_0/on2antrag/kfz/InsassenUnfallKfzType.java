
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für das Elementarprodukt KFZ-Insassenunfall
 * 
 * <p>Java-Klasse für InsassenUnfallKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InsassenUnfallKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ElementarproduktKfz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InsassenUnfallSystem" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}InsassenUnfallSystem_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsassenUnfallKfz_Type", propOrder = {
    "insassenUnfallSystem"
})
public class InsassenUnfallKfzType
    extends ElementarproduktKfzType
{

    @XmlElement(name = "InsassenUnfallSystem", required = true)
    @XmlSchemaType(name = "string")
    protected InsassenUnfallSystemType insassenUnfallSystem;

    /**
     * Ruft den Wert der insassenUnfallSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InsassenUnfallSystemType }
     *     
     */
    public InsassenUnfallSystemType getInsassenUnfallSystem() {
        return insassenUnfallSystem;
    }

    /**
     * Legt den Wert der insassenUnfallSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InsassenUnfallSystemType }
     *     
     */
    public void setInsassenUnfallSystem(InsassenUnfallSystemType value) {
        this.insassenUnfallSystem = value;
    }

}
