
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import at.vvo.omds.types.omds3Types.r1_11_0.common.AuthorizationFilter;
import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Dieser Typ enthält eine Schadennr oder eine GeschaeftsfallId
 * 
 * <p>Java-Klasse für GetClaimRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetClaimRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaeftsfallSchadenereignis"/&gt;
 *           &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaeftsfallSchadenanlage"/&gt;
 *           &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetClaimRequest_Type", propOrder = {
    "authFilter",
    "geschaeftsfallSchadenereignis",
    "geschaeftsfallSchadenanlage",
    "schadennr"
})
public class GetClaimRequestType
    extends CommonRequestType
{

    @XmlElement(name = "AuthFilter")
    protected AuthorizationFilter authFilter;
    @XmlElement(name = "GeschaeftsfallSchadenereignis")
    protected ObjektIdType geschaeftsfallSchadenereignis;
    @XmlElement(name = "GeschaeftsfallSchadenanlage")
    protected ObjektIdType geschaeftsfallSchadenanlage;
    @XmlElement(name = "Schadennr")
    protected String schadennr;

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallSchadenereignis() {
        return geschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der geschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallSchadenereignis(ObjektIdType value) {
        this.geschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallSchadenanlage() {
        return geschaeftsfallSchadenanlage;
    }

    /**
     * Legt den Wert der geschaeftsfallSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallSchadenanlage(ObjektIdType value) {
        this.geschaeftsfallSchadenanlage = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

}
