
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Vorgefertigter Typ für eine Zusatzversicherung bei Unfallinvaliditaet
 * 
 * <p>Java-Klasse für ZusatzversicherungUnfallinvaliditaet_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusatzversicherungUnfallinvaliditaet_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}ZusatzversicherungLeben_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Versicherungssumme" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}VersicherungssummeZusatzbaustein_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusatzversicherungUnfallinvaliditaet_Type", propOrder = {
    "rest"
})
public class ZusatzversicherungUnfallinvaliditaetType
    extends ZusatzversicherungLebenType
{

    @XmlElementRef(name = "Versicherungssumme", namespace = "urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben", type = JAXBElement.class, required = false)
    protected List<JAXBElement<VersicherungssummeZusatzbausteinType>> rest;

    /**
     * Ruft das restliche Contentmodell ab. 
     * 
     * <p>
     * Sie rufen diese "catch-all"-Eigenschaft aus folgendem Grund ab: 
     * Der Feldname "Versicherungssumme" wird von zwei verschiedenen Teilen eines Schemas verwendet. Siehe: 
     * Zeile 131 von file:/C:/Users/jensb/git/omdsservicedefinitions_develop/OMDSServiceDefinition/src/main/resources/def/r1_11_0/omds3_ON2_Antrag_Leben.xsd
     * Zeile 1623 von file:/C:/Users/jensb/git/omdsservicedefinitions_develop/OMDSServiceDefinition/src/main/resources/def/r1_11_0/omds3CommonServiceTypes.xsd
     * <p>
     * Um diese Eigenschaft zu entfernen, wenden Sie eine Eigenschaftenanpassung für eine
     * der beiden folgenden Deklarationen an, um deren Namen zu ändern: 
     * Gets the value of the rest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link VersicherungssummeZusatzbausteinType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<VersicherungssummeZusatzbausteinType>> getRest() {
        if (rest == null) {
            rest = new ArrayList<JAXBElement<VersicherungssummeZusatzbausteinType>>();
        }
        return this.rest;
    }

}
