
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import at.vvo.omds.types.omds3Types.r1_11_0.common.BonusMalusSystemType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VinkularglaeubigerType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.SpezAntragType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ der das Produkt beschreibt und in Antragsanfrage und Antragsantwort verwendet wird
 * 
 * <p>Java-Klasse für SpezAntragKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezAntragKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezAntrag_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}VerkaufsproduktKfz_Type"/&gt;
 *         &lt;element name="BonusMalus" type="{urn:omds3CommonServiceTypes-1-1-0}BonusMalusSystem_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vinkulierung" type="{urn:omds3CommonServiceTypes-1-1-0}Vinkularglaeubiger_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheKfzDaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ZusaetzlicheKfzdaten_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezAntragKfz_Type", propOrder = {
    "verkaufsprodukt",
    "bonusMalus",
    "vinkulierung",
    "zusaetzlicheKfzDaten"
})
public class SpezAntragKfzType
    extends SpezAntragType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktKfzType verkaufsprodukt;
    @XmlElement(name = "BonusMalus")
    protected BonusMalusSystemType bonusMalus;
    @XmlElement(name = "Vinkulierung")
    protected VinkularglaeubigerType vinkulierung;
    @XmlElement(name = "ZusaetzlicheKfzDaten")
    protected ZusaetzlicheKfzdatenType zusaetzlicheKfzDaten;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktKfzType }
     *     
     */
    public VerkaufsproduktKfzType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktKfzType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktKfzType value) {
        this.verkaufsprodukt = value;
    }

    /**
     * Ruft den Wert der bonusMalus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BonusMalusSystemType }
     *     
     */
    public BonusMalusSystemType getBonusMalus() {
        return bonusMalus;
    }

    /**
     * Legt den Wert der bonusMalus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BonusMalusSystemType }
     *     
     */
    public void setBonusMalus(BonusMalusSystemType value) {
        this.bonusMalus = value;
    }

    /**
     * Ruft den Wert der vinkulierung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VinkularglaeubigerType }
     *     
     */
    public VinkularglaeubigerType getVinkulierung() {
        return vinkulierung;
    }

    /**
     * Legt den Wert der vinkulierung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VinkularglaeubigerType }
     *     
     */
    public void setVinkulierung(VinkularglaeubigerType value) {
        this.vinkulierung = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheKfzDaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheKfzdatenType }
     *     
     */
    public ZusaetzlicheKfzdatenType getZusaetzlicheKfzDaten() {
        return zusaetzlicheKfzDaten;
    }

    /**
     * Legt den Wert der zusaetzlicheKfzDaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheKfzdatenType }
     *     
     */
    public void setZusaetzlicheKfzDaten(ZusaetzlicheKfzdatenType value) {
        this.zusaetzlicheKfzDaten = value;
    }

}
