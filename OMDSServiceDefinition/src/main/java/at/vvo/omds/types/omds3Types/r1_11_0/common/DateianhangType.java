
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.activation.DataHandler;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlMimeType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Deprecated, verwende ProzessDokument_Type.
 * 
 * <p>Java-Klasse für Dateianhang_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Dateianhang_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Mimetype" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DateiType" type="{urn:omds3CommonServiceTypes-1-1-0}TypeDateianhang_Type" minOccurs="0"/&gt;
 *         &lt;element name="DateiName"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DateiData" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *         &lt;element name="DateiBeschreibung" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="200"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dateianhang_Type", propOrder = {
    "mimetype",
    "dateiType",
    "dateiName",
    "dateiData",
    "dateiBeschreibung"
})
public class DateianhangType {

    @XmlElement(name = "Mimetype", required = true)
    protected String mimetype;
    @XmlElement(name = "DateiType")
    protected Integer dateiType;
    @XmlElement(name = "DateiName", required = true)
    protected String dateiName;
    @XmlElement(name = "DateiData", required = true)
    @XmlMimeType("application/octet-stream")
    protected DataHandler dateiData;
    @XmlElement(name = "DateiBeschreibung")
    protected String dateiBeschreibung;

    /**
     * Ruft den Wert der mimetype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimetype() {
        return mimetype;
    }

    /**
     * Legt den Wert der mimetype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimetype(String value) {
        this.mimetype = value;
    }

    /**
     * Ruft den Wert der dateiType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDateiType() {
        return dateiType;
    }

    /**
     * Legt den Wert der dateiType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDateiType(Integer value) {
        this.dateiType = value;
    }

    /**
     * Ruft den Wert der dateiName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateiName() {
        return dateiName;
    }

    /**
     * Legt den Wert der dateiName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateiName(String value) {
        this.dateiName = value;
    }

    /**
     * Ruft den Wert der dateiData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getDateiData() {
        return dateiData;
    }

    /**
     * Legt den Wert der dateiData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setDateiData(DataHandler value) {
        this.dateiData = value;
    }

    /**
     * Ruft den Wert der dateiBeschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateiBeschreibung() {
        return dateiBeschreibung;
    }

    /**
     * Legt den Wert der dateiBeschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateiBeschreibung(String value) {
        this.dateiBeschreibung = value;
    }

}
