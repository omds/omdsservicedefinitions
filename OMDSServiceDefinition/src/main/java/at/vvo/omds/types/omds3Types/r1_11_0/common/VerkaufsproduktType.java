
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_16.ELRahmenvereinbarungType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.VerkaufsproduktKfzType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Basistyp für ein Produktbündel
 * 
 * <p>Java-Klasse für Verkaufsprodukt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Verkaufsprodukt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VtgBeg" type="{urn:omds20}Datum"/&gt;
 *         &lt;element name="Verkaufsproduktgeneration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zahlweg" type="{urn:omds20}ZahlWegCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Zahlrhythmus" type="{urn:omds20}ZahlRhythmCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Hauptfaelligkeit" type="{urn:omds3CommonServiceTypes-1-1-0}Hauptfaelligkeit_Type" minOccurs="0"/&gt;
 *         &lt;element name="Berechnungsvariante" type="{urn:omds3CommonServiceTypes-1-1-0}Berechnungsvariante_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vermittlernr" type="{urn:omds20}Vermnr" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Rahmenvereinbarung" minOccurs="0"/&gt;
 *         &lt;element name="RefSicherstellungLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheVerkaufproduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheVerkaufproduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Verkaufsprodukt_Type", propOrder = {
    "vtgBeg",
    "verkaufsproduktgeneration",
    "zahlweg",
    "zahlrhythmus",
    "hauptfaelligkeit",
    "berechnungsvariante",
    "vermittlernr",
    "elRahmenvereinbarung",
    "refSicherstellungLfnr",
    "zusaetzlicheVerkaufproduktdaten"
})
@XmlSeeAlso({
    VerkaufsproduktKfzType.class,
    VerkaufsproduktGenerischType.class
})
public abstract class VerkaufsproduktType
    extends ProduktbausteinType
{

    @XmlElement(name = "VtgBeg", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar vtgBeg;
    @XmlElement(name = "Verkaufsproduktgeneration")
    protected String verkaufsproduktgeneration;
    @XmlElement(name = "Zahlweg")
    protected String zahlweg;
    @XmlElement(name = "Zahlrhythmus")
    protected String zahlrhythmus;
    @XmlElement(name = "Hauptfaelligkeit")
    @XmlSchemaType(name = "gMonthDay")
    protected XMLGregorianCalendar hauptfaelligkeit;
    @XmlElement(name = "Berechnungsvariante")
    @XmlSchemaType(name = "string")
    protected BerechnungsvarianteType berechnungsvariante;
    @XmlElement(name = "Vermittlernr")
    protected String vermittlernr;
    @XmlElement(name = "EL-Rahmenvereinbarung", namespace = "urn:omds20")
    protected ELRahmenvereinbarungType elRahmenvereinbarung;
    @XmlElement(name = "RefSicherstellungLfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer refSicherstellungLfnr;
    @XmlElement(name = "ZusaetzlicheVerkaufproduktdaten")
    protected List<ZusaetzlicheVerkaufproduktdatenType> zusaetzlicheVerkaufproduktdaten;

    /**
     * Ruft den Wert der vtgBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgBeg() {
        return vtgBeg;
    }

    /**
     * Legt den Wert der vtgBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgBeg(XMLGregorianCalendar value) {
        this.vtgBeg = value;
    }

    /**
     * Ruft den Wert der verkaufsproduktgeneration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerkaufsproduktgeneration() {
        return verkaufsproduktgeneration;
    }

    /**
     * Legt den Wert der verkaufsproduktgeneration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerkaufsproduktgeneration(String value) {
        this.verkaufsproduktgeneration = value;
    }

    /**
     * Ruft den Wert der zahlweg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlweg() {
        return zahlweg;
    }

    /**
     * Legt den Wert der zahlweg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlweg(String value) {
        this.zahlweg = value;
    }

    /**
     * Ruft den Wert der zahlrhythmus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlrhythmus() {
        return zahlrhythmus;
    }

    /**
     * Legt den Wert der zahlrhythmus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlrhythmus(String value) {
        this.zahlrhythmus = value;
    }

    /**
     * Ruft den Wert der hauptfaelligkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHauptfaelligkeit() {
        return hauptfaelligkeit;
    }

    /**
     * Legt den Wert der hauptfaelligkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHauptfaelligkeit(XMLGregorianCalendar value) {
        this.hauptfaelligkeit = value;
    }

    /**
     * Ruft den Wert der berechnungsvariante-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BerechnungsvarianteType }
     *     
     */
    public BerechnungsvarianteType getBerechnungsvariante() {
        return berechnungsvariante;
    }

    /**
     * Legt den Wert der berechnungsvariante-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BerechnungsvarianteType }
     *     
     */
    public void setBerechnungsvariante(BerechnungsvarianteType value) {
        this.berechnungsvariante = value;
    }

    /**
     * Ruft den Wert der vermittlernr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermittlernr() {
        return vermittlernr;
    }

    /**
     * Legt den Wert der vermittlernr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermittlernr(String value) {
        this.vermittlernr = value;
    }

    /**
     * Ruft den Wert der elRahmenvereinbarung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ELRahmenvereinbarungType }
     *     
     */
    public ELRahmenvereinbarungType getELRahmenvereinbarung() {
        return elRahmenvereinbarung;
    }

    /**
     * Legt den Wert der elRahmenvereinbarung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELRahmenvereinbarungType }
     *     
     */
    public void setELRahmenvereinbarung(ELRahmenvereinbarungType value) {
        this.elRahmenvereinbarung = value;
    }

    /**
     * Ruft den Wert der refSicherstellungLfnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefSicherstellungLfnr() {
        return refSicherstellungLfnr;
    }

    /**
     * Legt den Wert der refSicherstellungLfnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefSicherstellungLfnr(Integer value) {
        this.refSicherstellungLfnr = value;
    }

    /**
     * Gets the value of the zusaetzlicheVerkaufproduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheVerkaufproduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheVerkaufproduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheVerkaufproduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheVerkaufproduktdatenType> getZusaetzlicheVerkaufproduktdaten() {
        if (zusaetzlicheVerkaufproduktdaten == null) {
            zusaetzlicheVerkaufproduktdaten = new ArrayList<ZusaetzlicheVerkaufproduktdatenType>();
        }
        return this.zusaetzlicheVerkaufproduktdaten;
    }

}
