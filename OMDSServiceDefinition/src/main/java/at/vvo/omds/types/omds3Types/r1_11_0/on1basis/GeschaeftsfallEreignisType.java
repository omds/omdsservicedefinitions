
package at.vvo.omds.types.omds3Types.r1_11_0.on1basis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ArtAenderungType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.GeschaeftsobjektArtType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Ereignis in einem Geschäftsprozess, z.B. der Abschluss eines Arbeitsschritts im Prozess
 * 
 * <p>Java-Klasse für GeschaeftsfallEreignis_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GeschaeftsfallEreignis_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}AbstractStateChangeEvent_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Vermnr" type="{urn:omds20}Vermnr"/&gt;
 *         &lt;element name="Geschaeftsfallnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *         &lt;element name="GeschaeftsfallArt" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallArt_Type"/&gt;
 *         &lt;element name="Zeitpunkt" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="Txt" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="255"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="BisherigerStatus" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}GeschaeftsfallStatus_Type" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}GeschaeftsfallStatus_Type"/&gt;
 *         &lt;element name="Dokument" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="DokumentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Typ" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Objekt" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Objektart" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsobjektArt_Type"/&gt;
 *                   &lt;element name="ObjektId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *                   &lt;element name="ArtAenderung" type="{urn:omds3CommonServiceTypes-1-1-0}ArtAenderung_Type"/&gt;
 *                   &lt;element name="GueltigAb" type="{urn:omds20}Datum"/&gt;
 *                   &lt;element name="BisherigerStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Dokument" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="DokumentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Typ" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeschaeftsfallEreignis_Type", propOrder = {
    "id",
    "vermnr",
    "geschaeftsfallnummer",
    "geschaeftsfallArt",
    "zeitpunkt",
    "txt",
    "bisherigerStatus",
    "status",
    "dokument",
    "objekt"
})
@XmlSeeAlso({
    GeschaeftsfallEreignisAntragType.class,
    GeschaeftsfallEreignisSchadenType.class
})
public class GeschaeftsfallEreignisType
    extends AbstractStateChangeEventType
{

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "Vermnr", required = true)
    protected String vermnr;
    @XmlElement(name = "Geschaeftsfallnummer", required = true)
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "GeschaeftsfallArt", required = true)
    protected String geschaeftsfallArt;
    @XmlElement(name = "Zeitpunkt", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar zeitpunkt;
    @XmlElement(name = "Txt")
    protected String txt;
    @XmlElement(name = "BisherigerStatus")
    @XmlSchemaType(name = "string")
    protected GeschaeftsfallStatusType bisherigerStatus;
    @XmlElement(name = "Status", required = true)
    @XmlSchemaType(name = "string")
    protected GeschaeftsfallStatusType status;
    @XmlElement(name = "Dokument")
    protected List<GeschaeftsfallEreignisType.Dokument> dokument;
    @XmlElement(name = "Objekt")
    protected List<GeschaeftsfallEreignisType.Objekt> objekt;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallArt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeschaeftsfallArt() {
        return geschaeftsfallArt;
    }

    /**
     * Legt den Wert der geschaeftsfallArt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeschaeftsfallArt(String value) {
        this.geschaeftsfallArt = value;
    }

    /**
     * Ruft den Wert der zeitpunkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getZeitpunkt() {
        return zeitpunkt;
    }

    /**
     * Legt den Wert der zeitpunkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setZeitpunkt(XMLGregorianCalendar value) {
        this.zeitpunkt = value;
    }

    /**
     * Ruft den Wert der txt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxt() {
        return txt;
    }

    /**
     * Legt den Wert der txt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxt(String value) {
        this.txt = value;
    }

    /**
     * Ruft den Wert der bisherigerStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GeschaeftsfallStatusType }
     *     
     */
    public GeschaeftsfallStatusType getBisherigerStatus() {
        return bisherigerStatus;
    }

    /**
     * Legt den Wert der bisherigerStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GeschaeftsfallStatusType }
     *     
     */
    public void setBisherigerStatus(GeschaeftsfallStatusType value) {
        this.bisherigerStatus = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GeschaeftsfallStatusType }
     *     
     */
    public GeschaeftsfallStatusType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GeschaeftsfallStatusType }
     *     
     */
    public void setStatus(GeschaeftsfallStatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the dokument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the dokument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeschaeftsfallEreignisType.Dokument }
     * 
     * 
     */
    public List<GeschaeftsfallEreignisType.Dokument> getDokument() {
        if (dokument == null) {
            dokument = new ArrayList<GeschaeftsfallEreignisType.Dokument>();
        }
        return this.dokument;
    }

    /**
     * Gets the value of the objekt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the objekt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjekt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeschaeftsfallEreignisType.Objekt }
     * 
     * 
     */
    public List<GeschaeftsfallEreignisType.Objekt> getObjekt() {
        if (objekt == null) {
            objekt = new ArrayList<GeschaeftsfallEreignisType.Objekt>();
        }
        return this.objekt;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="DokumentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Typ" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dokumentId",
        "typ"
    })
    public static class Dokument {

        @XmlElement(name = "DokumentId", required = true)
        protected String dokumentId;
        @XmlElement(name = "Typ")
        protected int typ;

        /**
         * Ruft den Wert der dokumentId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDokumentId() {
            return dokumentId;
        }

        /**
         * Legt den Wert der dokumentId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDokumentId(String value) {
            this.dokumentId = value;
        }

        /**
         * Ruft den Wert der typ-Eigenschaft ab.
         * 
         */
        public int getTyp() {
            return typ;
        }

        /**
         * Legt den Wert der typ-Eigenschaft fest.
         * 
         */
        public void setTyp(int value) {
            this.typ = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Objektart" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsobjektArt_Type"/&gt;
     *         &lt;element name="ObjektId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
     *         &lt;element name="ArtAenderung" type="{urn:omds3CommonServiceTypes-1-1-0}ArtAenderung_Type"/&gt;
     *         &lt;element name="GueltigAb" type="{urn:omds20}Datum"/&gt;
     *         &lt;element name="BisherigerStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Dokument" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="DokumentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Typ" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "objektart",
        "objektId",
        "artAenderung",
        "gueltigAb",
        "bisherigerStatus",
        "status",
        "dokument"
    })
    public static class Objekt {

        @XmlElement(name = "Objektart", required = true)
        @XmlSchemaType(name = "string")
        protected GeschaeftsobjektArtType objektart;
        @XmlElement(name = "ObjektId", required = true)
        protected ObjektIdType objektId;
        @XmlElement(name = "ArtAenderung", required = true)
        @XmlSchemaType(name = "string")
        protected ArtAenderungType artAenderung;
        @XmlElement(name = "GueltigAb", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar gueltigAb;
        @XmlElement(name = "BisherigerStatus")
        protected String bisherigerStatus;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "Dokument")
        protected List<GeschaeftsfallEreignisType.Objekt.Dokument> dokument;

        /**
         * Ruft den Wert der objektart-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link GeschaeftsobjektArtType }
         *     
         */
        public GeschaeftsobjektArtType getObjektart() {
            return objektart;
        }

        /**
         * Legt den Wert der objektart-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link GeschaeftsobjektArtType }
         *     
         */
        public void setObjektart(GeschaeftsobjektArtType value) {
            this.objektart = value;
        }

        /**
         * Ruft den Wert der objektId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ObjektIdType }
         *     
         */
        public ObjektIdType getObjektId() {
            return objektId;
        }

        /**
         * Legt den Wert der objektId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ObjektIdType }
         *     
         */
        public void setObjektId(ObjektIdType value) {
            this.objektId = value;
        }

        /**
         * Ruft den Wert der artAenderung-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ArtAenderungType }
         *     
         */
        public ArtAenderungType getArtAenderung() {
            return artAenderung;
        }

        /**
         * Legt den Wert der artAenderung-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ArtAenderungType }
         *     
         */
        public void setArtAenderung(ArtAenderungType value) {
            this.artAenderung = value;
        }

        /**
         * Ruft den Wert der gueltigAb-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getGueltigAb() {
            return gueltigAb;
        }

        /**
         * Legt den Wert der gueltigAb-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setGueltigAb(XMLGregorianCalendar value) {
            this.gueltigAb = value;
        }

        /**
         * Ruft den Wert der bisherigerStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBisherigerStatus() {
            return bisherigerStatus;
        }

        /**
         * Legt den Wert der bisherigerStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBisherigerStatus(String value) {
            this.bisherigerStatus = value;
        }

        /**
         * Ruft den Wert der status-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Legt den Wert der status-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the dokument property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the dokument property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDokument().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GeschaeftsfallEreignisType.Objekt.Dokument }
         * 
         * 
         */
        public List<GeschaeftsfallEreignisType.Objekt.Dokument> getDokument() {
            if (dokument == null) {
                dokument = new ArrayList<GeschaeftsfallEreignisType.Objekt.Dokument>();
            }
            return this.dokument;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="DokumentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Typ" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "dokumentId",
            "typ"
        })
        public static class Dokument {

            @XmlElement(name = "DokumentId", required = true)
            protected String dokumentId;
            @XmlElement(name = "Typ")
            protected int typ;

            /**
             * Ruft den Wert der dokumentId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDokumentId() {
                return dokumentId;
            }

            /**
             * Legt den Wert der dokumentId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDokumentId(String value) {
                this.dokumentId = value;
            }

            /**
             * Ruft den Wert der typ-Eigenschaft ab.
             * 
             */
            public int getTyp() {
                return typ;
            }

            /**
             * Legt den Wert der typ-Eigenschaft fest.
             * 
             */
            public void setTyp(int value) {
                this.typ = value;
            }

        }

    }

}
