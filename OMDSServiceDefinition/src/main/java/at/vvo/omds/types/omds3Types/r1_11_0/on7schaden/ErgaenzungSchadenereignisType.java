
package at.vvo.omds.types.omds3Types.r1_11_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ObjektIdType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Objekt Nachmeldung zu Schadenereignis
 * 
 * <p>Java-Klasse für ErgaenzungSchadenereignis_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErgaenzungSchadenereignis_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AnforderungsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaeftsfallSchadenereignis" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheSchadensereignisdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheSchadensereignisdaten_Type" minOccurs="0"/&gt;
 *         &lt;element name="ErgaenzungSchaden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ErgaenzungSchaden_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErgaenzungSchadenereignis_Type", propOrder = {
    "anforderungsId",
    "id",
    "geschaeftsfallSchadenereignis",
    "zusaetzlicheSchadensereignisdaten",
    "ergaenzungSchaden"
})
public class ErgaenzungSchadenereignisType {

    @XmlElement(name = "AnforderungsId")
    protected String anforderungsId;
    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "GeschaeftsfallSchadenereignis")
    protected ObjektIdType geschaeftsfallSchadenereignis;
    @XmlElement(name = "ZusaetzlicheSchadensereignisdaten")
    protected ZusaetzlicheSchadensereignisdatenType zusaetzlicheSchadensereignisdaten;
    @XmlElement(name = "ErgaenzungSchaden", required = true)
    protected List<ErgaenzungSchadenType> ergaenzungSchaden;

    /**
     * Ruft den Wert der anforderungsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnforderungsId() {
        return anforderungsId;
    }

    /**
     * Legt den Wert der anforderungsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnforderungsId(String value) {
        this.anforderungsId = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * GeschäftsfallId der Anlage des ursprünglichen Schadenereignis-Objektes
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallSchadenereignis() {
        return geschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der geschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallSchadenereignis(ObjektIdType value) {
        this.geschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheSchadensereignisdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheSchadensereignisdatenType }
     *     
     */
    public ZusaetzlicheSchadensereignisdatenType getZusaetzlicheSchadensereignisdaten() {
        return zusaetzlicheSchadensereignisdaten;
    }

    /**
     * Legt den Wert der zusaetzlicheSchadensereignisdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheSchadensereignisdatenType }
     *     
     */
    public void setZusaetzlicheSchadensereignisdaten(ZusaetzlicheSchadensereignisdatenType value) {
        this.zusaetzlicheSchadensereignisdaten = value;
    }

    /**
     * Gets the value of the ergaenzungSchaden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the ergaenzungSchaden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgaenzungSchaden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErgaenzungSchadenType }
     * 
     * 
     */
    public List<ErgaenzungSchadenType> getErgaenzungSchaden() {
        if (ergaenzungSchaden == null) {
            ergaenzungSchaden = new ArrayList<ErgaenzungSchadenType>();
        }
        return this.ergaenzungSchaden;
    }

}
