
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ zur Übermittlung von Metadaten einer Autorisierung (z.B. einer elektronischen Unterschrift)
 * 
 * <p>Java-Klasse für Autorisierung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Autorisierung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AutorisierungsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LfnrPerson" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *         &lt;element name="Rolle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Autorisierungsart" type="{urn:omds3CommonServiceTypes-1-1-0}Autorisierungsart_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Autorisierung_Type", propOrder = {
    "autorisierungsId",
    "lfnrPerson",
    "rolle",
    "autorisierungsart"
})
@XmlSeeAlso({
    at.vvo.omds.types.omds3Types.r1_11_0.common.ProzessDokumentType.Autorisierungen.class
})
public class AutorisierungType {

    @XmlElement(name = "AutorisierungsId")
    protected String autorisierungsId;
    @XmlElement(name = "LfnrPerson")
    @XmlSchemaType(name = "unsignedByte")
    protected short lfnrPerson;
    @XmlElement(name = "Rolle")
    protected String rolle;
    @XmlElement(name = "Autorisierungsart", required = true)
    protected AutorisierungsartType autorisierungsart;

    /**
     * Ruft den Wert der autorisierungsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutorisierungsId() {
        return autorisierungsId;
    }

    /**
     * Legt den Wert der autorisierungsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutorisierungsId(String value) {
        this.autorisierungsId = value;
    }

    /**
     * Ruft den Wert der lfnrPerson-Eigenschaft ab.
     * 
     */
    public short getLfnrPerson() {
        return lfnrPerson;
    }

    /**
     * Legt den Wert der lfnrPerson-Eigenschaft fest.
     * 
     */
    public void setLfnrPerson(short value) {
        this.lfnrPerson = value;
    }

    /**
     * Ruft den Wert der rolle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolle() {
        return rolle;
    }

    /**
     * Legt den Wert der rolle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolle(String value) {
        this.rolle = value;
    }

    /**
     * Ruft den Wert der autorisierungsart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutorisierungsartType }
     *     
     */
    public AutorisierungsartType getAutorisierungsart() {
        return autorisierungsart;
    }

    /**
     * Legt den Wert der autorisierungsart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutorisierungsartType }
     *     
     */
    public void setAutorisierungsart(AutorisierungsartType value) {
        this.autorisierungsart = value;
    }

}
