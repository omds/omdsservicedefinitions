
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.SpezBerechnungType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Typ für den Schritt Berechnung
 * 
 * <p>Java-Klasse für SpezBerechnungLeben_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezBerechnungLeben_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezBerechnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-5-0.on2antrag.leben}VerkaufsproduktLeben_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezBerechnungLeben_Type", propOrder = {
    "verkaufsprodukt"
})
public class SpezBerechnungLebenType
    extends SpezBerechnungType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktLebenType verkaufsprodukt;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktLebenType }
     *     
     */
    public VerkaufsproduktLebenType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktLebenType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktLebenType value) {
        this.verkaufsprodukt = value;
    }

}
