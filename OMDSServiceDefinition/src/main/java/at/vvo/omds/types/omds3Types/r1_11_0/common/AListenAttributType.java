
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakte Ebene fuer Listen mit einem oder mehreren wählbaren Werten
 * 
 * <p>Java-Klasse für AListenAttribut_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AListenAttribut_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Attribut_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Values" type="{urn:omds3CommonServiceTypes-1-1-0}EintragSchluesselliste_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="MinAnz" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AListenAttribut_Type", propOrder = {
    "values",
    "minAnz"
})
@XmlSeeAlso({
    AttributEnumType.class,
    AttributMultiEnumType.class
})
public abstract class AListenAttributType
    extends AttributType
{

    @XmlElement(name = "Values")
    protected List<EintragSchluessellisteType> values;
    @XmlElement(name = "MinAnz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer minAnz;

    /**
     * Gets the value of the values property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the values property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EintragSchluessellisteType }
     * 
     * 
     */
    public List<EintragSchluessellisteType> getValues() {
        if (values == null) {
            values = new ArrayList<EintragSchluessellisteType>();
        }
        return this.values;
    }

    /**
     * Ruft den Wert der minAnz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinAnz() {
        return minAnz;
    }

    /**
     * Legt den Wert der minAnz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinAnz(Integer value) {
        this.minAnz = value;
    }

}
