
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import at.vvo.omds.types.omds3Types.r1_11_0.common.ProduktType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakte Basisklasse für KFZ-Zusatzprodukte, die mit einer KFZ-Versicherung gebündelt werden können.
 * 
 * <p>Java-Klasse für ZusatzproduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusatzproduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}FahrzeugRefLfdNr" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusatzproduktKfz_Type", propOrder = {
    "fahrzeugRefLfdNr"
})
@XmlSeeAlso({
    ProduktKfzRechtsschutzType.class
})
public abstract class ZusatzproduktKfzType
    extends ProduktType
{

    @XmlElement(name = "FahrzeugRefLfdNr")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer fahrzeugRefLfdNr;

    /**
     * Ruft den Wert der fahrzeugRefLfdNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFahrzeugRefLfdNr() {
        return fahrzeugRefLfdNr;
    }

    /**
     * Legt den Wert der fahrzeugRefLfdNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFahrzeugRefLfdNr(Integer value) {
        this.fahrzeugRefLfdNr = value;
    }

}
