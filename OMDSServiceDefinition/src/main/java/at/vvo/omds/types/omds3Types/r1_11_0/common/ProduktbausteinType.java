
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Typ für alle Produktbausteine im Antragsprozess
 * 
 * <p>Java-Klasse für Produktbaustein_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Produktbaustein_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}BasisProduktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VtgEnde" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="Praemie" type="{urn:omds3CommonServiceTypes-1-1-0}Praemie_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="JahrespraemieNto" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Produktbaustein_Type", propOrder = {
    "vtgEnde",
    "praemie",
    "jahrespraemieNto"
})
@XmlSeeAlso({
    ProduktbausteinGenerischType.class,
    VerkaufsproduktType.class,
    ProduktType.class,
    ElementarproduktType.class,
    ZusatzproduktType.class
})
public abstract class ProduktbausteinType
    extends BasisProduktbausteinType
{

    @XmlElement(name = "VtgEnde")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar vtgEnde;
    @XmlElement(name = "Praemie")
    protected List<PraemieType> praemie;
    @XmlElement(name = "JahrespraemieNto")
    protected BigDecimal jahrespraemieNto;

    /**
     * Ruft den Wert der vtgEnde-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgEnde() {
        return vtgEnde;
    }

    /**
     * Legt den Wert der vtgEnde-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgEnde(XMLGregorianCalendar value) {
        this.vtgEnde = value;
    }

    /**
     * Gets the value of the praemie property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the praemie property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPraemie().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PraemieType }
     * 
     * 
     */
    public List<PraemieType> getPraemie() {
        if (praemie == null) {
            praemie = new ArrayList<PraemieType>();
        }
        return this.praemie;
    }

    /**
     * Ruft den Wert der jahrespraemieNto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getJahrespraemieNto() {
        return jahrespraemieNto;
    }

    /**
     * Legt den Wert der jahrespraemieNto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setJahrespraemieNto(BigDecimal value) {
        this.jahrespraemieNto = value;
    }

}
