
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ErsatzpolizzeType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vertrag" type="{urn:omds3CommonServiceTypes-1-1-0}Ersatzpolizze_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vertraege"
})
@XmlRootElement(name = "ConversionScopeRequest")
public class ConversionScopeRequest
    extends CommonRequestType
{

    @XmlElement(name = "Vertrag", required = true)
    protected List<ErsatzpolizzeType> vertraege;

    /**
     * <p>Die Liste der Verträge, die in der Konvertierung behandelt werden sollen.</p>Gets the value of the vertraege property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vertraege property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertraege().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErsatzpolizzeType }
     * 
     * 
     */
    public List<ErsatzpolizzeType> getVertraege() {
        if (vertraege == null) {
            vertraege = new ArrayList<ErsatzpolizzeType>();
        }
        return this.vertraege;
    }

}
