
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Type zusätzliche Kfz-Daten; Deprecated, Elemente sind ab
 * 				Version 1.11 in Fahrzeug_Type enthalten.
 * 
 * <p>Java-Klasse für ZusaetzlicheKfzdaten_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusaetzlicheKfzdaten_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="KfzKennzeichen"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="3"/&gt;
 *               &lt;maxLength value="9"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Fahrgestellnummer" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}Fahrgestellnummer_Type"/&gt;
 *         &lt;element name="ErfolgtAnmeldungZeitgleichMitAbmeldung" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AbmeldedatumWechselkennzeichenFahrzeug" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="Wechselkennzeichen" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}Wechselkennzeichen_Type" minOccurs="0"/&gt;
 *         &lt;element name="Fahrzeugzustand" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FzZustandBesichtigung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}FzZustandBesichtigung_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheAntragsdatenKfz" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ZusaetzlicheAntragsdatenKfz_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusaetzlicheKfzdaten_Type", propOrder = {
    "kfzKennzeichen",
    "fahrgestellnummer",
    "erfolgtAnmeldungZeitgleichMitAbmeldung",
    "abmeldedatumWechselkennzeichenFahrzeug",
    "wechselkennzeichen",
    "fahrzeugzustand",
    "fzZustandBesichtigung",
    "zusaetzlicheAntragsdatenKfz"
})
public class ZusaetzlicheKfzdatenType {

    @XmlElement(name = "KfzKennzeichen", required = true)
    protected String kfzKennzeichen;
    @XmlElement(name = "Fahrgestellnummer", required = true)
    protected String fahrgestellnummer;
    @XmlElement(name = "ErfolgtAnmeldungZeitgleichMitAbmeldung")
    protected Boolean erfolgtAnmeldungZeitgleichMitAbmeldung;
    @XmlElement(name = "AbmeldedatumWechselkennzeichenFahrzeug")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar abmeldedatumWechselkennzeichenFahrzeug;
    @XmlElement(name = "Wechselkennzeichen")
    protected WechselkennzeichenType wechselkennzeichen;
    @XmlElement(name = "Fahrzeugzustand")
    protected String fahrzeugzustand;
    @XmlElement(name = "FzZustandBesichtigung")
    protected FzZustandBesichtigungType fzZustandBesichtigung;
    @XmlElement(name = "ZusaetzlicheAntragsdatenKfz")
    protected ZusaetzlicheAntragsdatenKfzType zusaetzlicheAntragsdatenKfz;

    /**
     * Ruft den Wert der kfzKennzeichen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKfzKennzeichen() {
        return kfzKennzeichen;
    }

    /**
     * Legt den Wert der kfzKennzeichen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKfzKennzeichen(String value) {
        this.kfzKennzeichen = value;
    }

    /**
     * Ruft den Wert der fahrgestellnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrgestellnummer() {
        return fahrgestellnummer;
    }

    /**
     * Legt den Wert der fahrgestellnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrgestellnummer(String value) {
        this.fahrgestellnummer = value;
    }

    /**
     * Ruft den Wert der erfolgtAnmeldungZeitgleichMitAbmeldung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isErfolgtAnmeldungZeitgleichMitAbmeldung() {
        return erfolgtAnmeldungZeitgleichMitAbmeldung;
    }

    /**
     * Legt den Wert der erfolgtAnmeldungZeitgleichMitAbmeldung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setErfolgtAnmeldungZeitgleichMitAbmeldung(Boolean value) {
        this.erfolgtAnmeldungZeitgleichMitAbmeldung = value;
    }

    /**
     * Ruft den Wert der abmeldedatumWechselkennzeichenFahrzeug-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAbmeldedatumWechselkennzeichenFahrzeug() {
        return abmeldedatumWechselkennzeichenFahrzeug;
    }

    /**
     * Legt den Wert der abmeldedatumWechselkennzeichenFahrzeug-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAbmeldedatumWechselkennzeichenFahrzeug(XMLGregorianCalendar value) {
        this.abmeldedatumWechselkennzeichenFahrzeug = value;
    }

    /**
     * Ruft den Wert der wechselkennzeichen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WechselkennzeichenType }
     *     
     */
    public WechselkennzeichenType getWechselkennzeichen() {
        return wechselkennzeichen;
    }

    /**
     * Legt den Wert der wechselkennzeichen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WechselkennzeichenType }
     *     
     */
    public void setWechselkennzeichen(WechselkennzeichenType value) {
        this.wechselkennzeichen = value;
    }

    /**
     * Ruft den Wert der fahrzeugzustand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrzeugzustand() {
        return fahrzeugzustand;
    }

    /**
     * Legt den Wert der fahrzeugzustand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrzeugzustand(String value) {
        this.fahrzeugzustand = value;
    }

    /**
     * Ruft den Wert der fzZustandBesichtigung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FzZustandBesichtigungType }
     *     
     */
    public FzZustandBesichtigungType getFzZustandBesichtigung() {
        return fzZustandBesichtigung;
    }

    /**
     * Legt den Wert der fzZustandBesichtigung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FzZustandBesichtigungType }
     *     
     */
    public void setFzZustandBesichtigung(FzZustandBesichtigungType value) {
        this.fzZustandBesichtigung = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheAntragsdatenKfz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheAntragsdatenKfzType }
     *     
     */
    public ZusaetzlicheAntragsdatenKfzType getZusaetzlicheAntragsdatenKfz() {
        return zusaetzlicheAntragsdatenKfz;
    }

    /**
     * Legt den Wert der zusaetzlicheAntragsdatenKfz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheAntragsdatenKfzType }
     *     
     */
    public void setZusaetzlicheAntragsdatenKfz(ZusaetzlicheAntragsdatenKfzType value) {
        this.zusaetzlicheAntragsdatenKfz = value;
    }

}
