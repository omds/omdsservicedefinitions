
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.math.BigDecimal;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Typ fuer Sicherstellungen
 * 
 * <p>Java-Klasse für Sicherstellung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Sicherstellung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Lfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;element name="Betrag" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Unanfechtbarkeitssumme" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sicherstellung_Type", propOrder = {
    "lfnr",
    "betrag",
    "unanfechtbarkeitssumme"
})
@XmlSeeAlso({
    AbtretungType.class,
    VerpfaendungType.class,
    VinkulierungPersonenType.class
})
public abstract class SicherstellungType {

    @XmlElement(name = "Lfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;
    @XmlElement(name = "Betrag")
    protected BigDecimal betrag;
    @XmlElement(name = "Unanfechtbarkeitssumme")
    protected BigDecimal unanfechtbarkeitssumme;

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

    /**
     * Ruft den Wert der betrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBetrag() {
        return betrag;
    }

    /**
     * Legt den Wert der betrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBetrag(BigDecimal value) {
        this.betrag = value;
    }

    /**
     * Ruft den Wert der unanfechtbarkeitssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnanfechtbarkeitssumme() {
        return unanfechtbarkeitssumme;
    }

    /**
     * Legt den Wert der unanfechtbarkeitssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnanfechtbarkeitssumme(BigDecimal value) {
        this.unanfechtbarkeitssumme = value;
    }

}
