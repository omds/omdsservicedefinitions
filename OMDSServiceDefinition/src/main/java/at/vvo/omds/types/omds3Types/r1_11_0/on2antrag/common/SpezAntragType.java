
package at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import at.vvo.omds.types.omds3Types.r1_11_0.common.AbgelehnteRisikenType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.AntragsartType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.BeteiligtePersonVertragType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.DatenverwendungType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.EinwilligungType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ErsatzpolizzenType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.KontierungType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.PolizzenversandType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.SicherstellungType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VertragspersonType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.VorversicherungenType;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ZahlungsdatenType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.SpezAntragKfzType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.SpezAntragKrankenType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.AntragSachPrivatType;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.SpezAntragUnfallType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Basistyp für Antrag, der bei Request und bei Response gleich ist
 * 
 * <p>Java-Klasse für SpezAntrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezAntrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezBOASchritt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsart" type="{urn:omds3CommonServiceTypes-1-1-0}Antragsart_Type" minOccurs="0"/&gt;
 *         &lt;element name="Personen" type="{urn:omds3CommonServiceTypes-1-1-0}BeteiligtePersonVertrag_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Versicherungsnehmer" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;element name="AbweichenderPraemienzahler" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="WeitereVersicherungsnehmer" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="WeitereVertragspersonen" type="{urn:omds3CommonServiceTypes-1-1-0}Vertragsperson_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AbgelehnteRisiken" type="{urn:omds3CommonServiceTypes-1-1-0}AbgelehnteRisiken_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vorversicherungen" type="{urn:omds3CommonServiceTypes-1-1-0}Vorversicherungen_Type" minOccurs="0"/&gt;
 *         &lt;element name="Zahlungsdaten" type="{urn:omds3CommonServiceTypes-1-1-0}Zahlungsdaten_Type"/&gt;
 *         &lt;element name="Sepa" type="{urn:omds3CommonServiceTypes-1-1-0}SepaCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Polizzenversand" type="{urn:omds3CommonServiceTypes-1-1-0}PolizzenversandType" minOccurs="0"/&gt;
 *         &lt;element name="Datenschutzbestimmungen" type="{urn:omds3CommonServiceTypes-1-1-0}Datenverwendung_Type"/&gt;
 *         &lt;element name="Kontierung" type="{urn:omds3CommonServiceTypes-1-1-0}Kontierung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Ersatzpolizzennummer" type="{urn:omds3CommonServiceTypes-1-1-0}Ersatzpolizzen_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusendungWeitereDokumente" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheAntragsdaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}ZusaetzlicheAntragsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Sicherstellungen" type="{urn:omds3CommonServiceTypes-1-1-0}Sicherstellung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Einwilligungen" type="{urn:omds3CommonServiceTypes-1-1-0}Einwilligung_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezAntrag_Type", propOrder = {
    "antragsart",
    "personen",
    "versicherungsnehmer",
    "abweichenderPraemienzahler",
    "weitereVersicherungsnehmer",
    "weitereVertragspersonen",
    "abgelehnteRisiken",
    "vorversicherungen",
    "zahlungsdaten",
    "sepa",
    "polizzenversand",
    "datenschutzbestimmungen",
    "kontierung",
    "ersatzpolizzennummer",
    "zusendungWeitereDokumente",
    "zusaetzlicheAntragsdaten",
    "sicherstellungen",
    "einwilligungen",
    "polizzennr",
    "vertragsID"
})
@XmlSeeAlso({
    SpezAntragKfzType.class,
    AntragSachPrivatType.class,
    SpezAntragUnfallType.class,
    SpezAntragKrankenType.class,
    SpezAntragPersonenType.class
})
public abstract class SpezAntragType
    extends SpezBOASchrittType
{

    @XmlElement(name = "Antragsart")
    @XmlSchemaType(name = "string")
    protected AntragsartType antragsart;
    @XmlElement(name = "Personen", required = true)
    protected List<BeteiligtePersonVertragType> personen;
    @XmlElement(name = "Versicherungsnehmer")
    @XmlSchemaType(name = "unsignedShort")
    protected int versicherungsnehmer;
    @XmlElement(name = "AbweichenderPraemienzahler")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer abweichenderPraemienzahler;
    @XmlElement(name = "WeitereVersicherungsnehmer", type = Integer.class)
    @XmlSchemaType(name = "unsignedShort")
    protected List<Integer> weitereVersicherungsnehmer;
    @XmlElement(name = "WeitereVertragspersonen")
    protected List<VertragspersonType> weitereVertragspersonen;
    @XmlElement(name = "AbgelehnteRisiken")
    protected AbgelehnteRisikenType abgelehnteRisiken;
    @XmlElement(name = "Vorversicherungen")
    protected VorversicherungenType vorversicherungen;
    @XmlElement(name = "Zahlungsdaten", required = true)
    protected ZahlungsdatenType zahlungsdaten;
    @XmlElement(name = "Sepa")
    @XmlSchemaType(name = "unsignedByte")
    protected Short sepa;
    @XmlElement(name = "Polizzenversand")
    @XmlSchemaType(name = "string")
    protected PolizzenversandType polizzenversand;
    @XmlElement(name = "Datenschutzbestimmungen", required = true)
    protected DatenverwendungType datenschutzbestimmungen;
    @XmlElement(name = "Kontierung")
    protected List<KontierungType> kontierung;
    @XmlElement(name = "Ersatzpolizzennummer")
    protected ErsatzpolizzenType ersatzpolizzennummer;
    @XmlElement(name = "ZusendungWeitereDokumente")
    protected List<String> zusendungWeitereDokumente;
    @XmlElement(name = "ZusaetzlicheAntragsdaten")
    protected List<ZusaetzlicheAntragsdatenType> zusaetzlicheAntragsdaten;
    @XmlElement(name = "Sicherstellungen")
    protected List<SicherstellungType> sicherstellungen;
    @XmlElement(name = "Einwilligungen")
    protected List<EinwilligungType> einwilligungen;
    @XmlElement(name = "Polizzennr")
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;

    /**
     * Ruft den Wert der antragsart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AntragsartType }
     *     
     */
    public AntragsartType getAntragsart() {
        return antragsart;
    }

    /**
     * Legt den Wert der antragsart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AntragsartType }
     *     
     */
    public void setAntragsart(AntragsartType value) {
        this.antragsart = value;
    }

    /**
     * Gets the value of the personen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the personen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonVertragType }
     * 
     * 
     */
    public List<BeteiligtePersonVertragType> getPersonen() {
        if (personen == null) {
            personen = new ArrayList<BeteiligtePersonVertragType>();
        }
        return this.personen;
    }

    /**
     * Ruft den Wert der versicherungsnehmer-Eigenschaft ab.
     * 
     */
    public int getVersicherungsnehmer() {
        return versicherungsnehmer;
    }

    /**
     * Legt den Wert der versicherungsnehmer-Eigenschaft fest.
     * 
     */
    public void setVersicherungsnehmer(int value) {
        this.versicherungsnehmer = value;
    }

    /**
     * Ruft den Wert der abweichenderPraemienzahler-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbweichenderPraemienzahler() {
        return abweichenderPraemienzahler;
    }

    /**
     * Legt den Wert der abweichenderPraemienzahler-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbweichenderPraemienzahler(Integer value) {
        this.abweichenderPraemienzahler = value;
    }

    /**
     * Gets the value of the weitereVersicherungsnehmer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the weitereVersicherungsnehmer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWeitereVersicherungsnehmer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getWeitereVersicherungsnehmer() {
        if (weitereVersicherungsnehmer == null) {
            weitereVersicherungsnehmer = new ArrayList<Integer>();
        }
        return this.weitereVersicherungsnehmer;
    }

    /**
     * Gets the value of the weitereVertragspersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the weitereVertragspersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWeitereVertragspersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VertragspersonType }
     * 
     * 
     */
    public List<VertragspersonType> getWeitereVertragspersonen() {
        if (weitereVertragspersonen == null) {
            weitereVertragspersonen = new ArrayList<VertragspersonType>();
        }
        return this.weitereVertragspersonen;
    }

    /**
     * Ruft den Wert der abgelehnteRisiken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AbgelehnteRisikenType }
     *     
     */
    public AbgelehnteRisikenType getAbgelehnteRisiken() {
        return abgelehnteRisiken;
    }

    /**
     * Legt den Wert der abgelehnteRisiken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AbgelehnteRisikenType }
     *     
     */
    public void setAbgelehnteRisiken(AbgelehnteRisikenType value) {
        this.abgelehnteRisiken = value;
    }

    /**
     * Ruft den Wert der vorversicherungen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VorversicherungenType }
     *     
     */
    public VorversicherungenType getVorversicherungen() {
        return vorversicherungen;
    }

    /**
     * Legt den Wert der vorversicherungen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VorversicherungenType }
     *     
     */
    public void setVorversicherungen(VorversicherungenType value) {
        this.vorversicherungen = value;
    }

    /**
     * Ruft den Wert der zahlungsdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZahlungsdatenType }
     *     
     */
    public ZahlungsdatenType getZahlungsdaten() {
        return zahlungsdaten;
    }

    /**
     * Legt den Wert der zahlungsdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahlungsdatenType }
     *     
     */
    public void setZahlungsdaten(ZahlungsdatenType value) {
        this.zahlungsdaten = value;
    }

    /**
     * Ruft den Wert der sepa-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSepa() {
        return sepa;
    }

    /**
     * Legt den Wert der sepa-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSepa(Short value) {
        this.sepa = value;
    }

    /**
     * Ruft den Wert der polizzenversand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PolizzenversandType }
     *     
     */
    public PolizzenversandType getPolizzenversand() {
        return polizzenversand;
    }

    /**
     * Legt den Wert der polizzenversand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PolizzenversandType }
     *     
     */
    public void setPolizzenversand(PolizzenversandType value) {
        this.polizzenversand = value;
    }

    /**
     * Ruft den Wert der datenschutzbestimmungen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DatenverwendungType }
     *     
     */
    public DatenverwendungType getDatenschutzbestimmungen() {
        return datenschutzbestimmungen;
    }

    /**
     * Legt den Wert der datenschutzbestimmungen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DatenverwendungType }
     *     
     */
    public void setDatenschutzbestimmungen(DatenverwendungType value) {
        this.datenschutzbestimmungen = value;
    }

    /**
     * Gets the value of the kontierung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the kontierung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKontierung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KontierungType }
     * 
     * 
     */
    public List<KontierungType> getKontierung() {
        if (kontierung == null) {
            kontierung = new ArrayList<KontierungType>();
        }
        return this.kontierung;
    }

    /**
     * Ruft den Wert der ersatzpolizzennummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ErsatzpolizzenType }
     *     
     */
    public ErsatzpolizzenType getErsatzpolizzennummer() {
        return ersatzpolizzennummer;
    }

    /**
     * Legt den Wert der ersatzpolizzennummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ErsatzpolizzenType }
     *     
     */
    public void setErsatzpolizzennummer(ErsatzpolizzenType value) {
        this.ersatzpolizzennummer = value;
    }

    /**
     * Gets the value of the zusendungWeitereDokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusendungWeitereDokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusendungWeitereDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getZusendungWeitereDokumente() {
        if (zusendungWeitereDokumente == null) {
            zusendungWeitereDokumente = new ArrayList<String>();
        }
        return this.zusendungWeitereDokumente;
    }

    /**
     * Gets the value of the zusaetzlicheAntragsdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheAntragsdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheAntragsdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheAntragsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheAntragsdatenType> getZusaetzlicheAntragsdaten() {
        if (zusaetzlicheAntragsdaten == null) {
            zusaetzlicheAntragsdaten = new ArrayList<ZusaetzlicheAntragsdatenType>();
        }
        return this.zusaetzlicheAntragsdaten;
    }

    /**
     * Gets the value of the sicherstellungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the sicherstellungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSicherstellungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SicherstellungType }
     * 
     * 
     */
    public List<SicherstellungType> getSicherstellungen() {
        if (sicherstellungen == null) {
            sicherstellungen = new ArrayList<SicherstellungType>();
        }
        return this.sicherstellungen;
    }

    /**
     * Gets the value of the einwilligungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the einwilligungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEinwilligungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EinwilligungType }
     * 
     * 
     */
    public List<EinwilligungType> getEinwilligungen() {
        if (einwilligungen == null) {
            einwilligungen = new ArrayList<EinwilligungType>();
        }
        return this.einwilligungen;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

}
