
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Abstrakter Typ für alle Gemeinsamkeiten von Produktbausteinen
 * 
 * <p>Java-Klasse für BasisProduktbaustein_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BasisProduktbaustein_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TarifId" type="{urn:omds3CommonServiceTypes-1-1-0}TarifId_Type" minOccurs="0"/&gt;
 *         &lt;element name="Bezeichnung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Bedingungen" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Konvertierung" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ErsetztId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Konvertierungsaktion" type="{urn:omds3CommonServiceTypes-1-1-0}Konvertierungsaktion_Type" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Kombinationen" type="{urn:omds3CommonServiceTypes-1-1-0}Kombinationen_Type" minOccurs="0"/&gt;
 *         &lt;element name="Attribut" type="{urn:omds3CommonServiceTypes-1-1-0}Attribut_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasisProduktbaustein_Type", propOrder = {
    "id",
    "tarifId",
    "bezeichnung",
    "bedingungen",
    "meldungen",
    "konvertierung",
    "kombinationen",
    "attribut"
})
@XmlSeeAlso({
    ProduktbausteinType.class
})
public abstract class BasisProduktbausteinType {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "TarifId")
    protected TarifIdType tarifId;
    @XmlElement(name = "Bezeichnung")
    protected String bezeichnung;
    @XmlElement(name = "Bedingungen")
    protected List<String> bedingungen;
    @XmlElement(name = "Meldungen")
    protected List<ServiceFault> meldungen;
    @XmlElement(name = "Konvertierung")
    protected BasisProduktbausteinType.Konvertierung konvertierung;
    @XmlElement(name = "Kombinationen")
    protected KombinationenType kombinationen;
    @XmlElement(name = "Attribut")
    protected List<AttributType> attribut;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der tarifId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TarifIdType }
     *     
     */
    public TarifIdType getTarifId() {
        return tarifId;
    }

    /**
     * Legt den Wert der tarifId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TarifIdType }
     *     
     */
    public void setTarifId(TarifIdType value) {
        this.tarifId = value;
    }

    /**
     * Ruft den Wert der bezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Legt den Wert der bezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

    /**
     * Gets the value of the bedingungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bedingungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBedingungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getBedingungen() {
        if (bedingungen == null) {
            bedingungen = new ArrayList<String>();
        }
        return this.bedingungen;
    }

    /**
     * Gets the value of the meldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the meldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getMeldungen() {
        if (meldungen == null) {
            meldungen = new ArrayList<ServiceFault>();
        }
        return this.meldungen;
    }

    /**
     * Ruft den Wert der konvertierung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BasisProduktbausteinType.Konvertierung }
     *     
     */
    public BasisProduktbausteinType.Konvertierung getKonvertierung() {
        return konvertierung;
    }

    /**
     * Legt den Wert der konvertierung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BasisProduktbausteinType.Konvertierung }
     *     
     */
    public void setKonvertierung(BasisProduktbausteinType.Konvertierung value) {
        this.konvertierung = value;
    }

    /**
     * Ruft den Wert der kombinationen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KombinationenType }
     *     
     */
    public KombinationenType getKombinationen() {
        return kombinationen;
    }

    /**
     * Legt den Wert der kombinationen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KombinationenType }
     *     
     */
    public void setKombinationen(KombinationenType value) {
        this.kombinationen = value;
    }

    /**
     * Gets the value of the attribut property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the attribut property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribut().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributType }
     * 
     * 
     */
    public List<AttributType> getAttribut() {
        if (attribut == null) {
            attribut = new ArrayList<AttributType>();
        }
        return this.attribut;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ErsetztId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Konvertierungsaktion" type="{urn:omds3CommonServiceTypes-1-1-0}Konvertierungsaktion_Type" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ersetztId",
        "konvertierungsaktion"
    })
    public static class Konvertierung {

        @XmlElement(name = "ErsetztId", required = true)
        protected String ersetztId;
        @XmlElement(name = "Konvertierungsaktion")
        protected KonvertierungsaktionType konvertierungsaktion;

        /**
         * Ruft den Wert der ersetztId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErsetztId() {
            return ersetztId;
        }

        /**
         * Legt den Wert der ersetztId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErsetztId(String value) {
            this.ersetztId = value;
        }

        /**
         * Ruft den Wert der konvertierungsaktion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link KonvertierungsaktionType }
         *     
         */
        public KonvertierungsaktionType getKonvertierungsaktion() {
            return konvertierungsaktion;
        }

        /**
         * Legt den Wert der konvertierungsaktion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link KonvertierungsaktionType }
         *     
         */
        public void setKonvertierungsaktion(KonvertierungsaktionType value) {
            this.konvertierungsaktion = value;
        }

    }

}
