
package at.vvo.omds.types.omds3Types.r1_11_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Basistyp für ein Zusatzprodukt
 * 
 * <p>Java-Klasse für Zusatzprodukt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Zusatzprodukt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VtgBeg" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheZusatzproduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheZusatzproduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Zusatzprodukt_Type", propOrder = {
    "vtgBeg",
    "zusaetzlicheZusatzproduktdaten"
})
@XmlSeeAlso({
    ZusatzproduktGenerischType.class
})
public abstract class ZusatzproduktType
    extends ProduktbausteinType
{

    @XmlElement(name = "VtgBeg")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar vtgBeg;
    @XmlElement(name = "ZusaetzlicheZusatzproduktdaten")
    protected List<ZusaetzlicheZusatzproduktdatenType> zusaetzlicheZusatzproduktdaten;

    /**
     * Ruft den Wert der vtgBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgBeg() {
        return vtgBeg;
    }

    /**
     * Legt den Wert der vtgBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgBeg(XMLGregorianCalendar value) {
        this.vtgBeg = value;
    }

    /**
     * Gets the value of the zusaetzlicheZusatzproduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheZusatzproduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheZusatzproduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheZusatzproduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheZusatzproduktdatenType> getZusaetzlicheZusatzproduktdaten() {
        if (zusaetzlicheZusatzproduktdaten == null) {
            zusaetzlicheZusatzproduktdaten = new ArrayList<ZusaetzlicheZusatzproduktdatenType>();
        }
        return this.zusaetzlicheZusatzproduktdaten;
    }

}
