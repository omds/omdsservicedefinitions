﻿==============
Version 1.11.0
==============

Was ist neu oder anders in Version 1.11.0 im Vergleich zur Version 1.10.0?
==========================================================================

1.  Vinkulierung in Kfz als deprecated markiert, es sollte das neuere Konzept Sicherstellung (inkl. Vinkulierung)
    genutzt werden (seit Version 1.7.0).
2.  In Vinkularglaeubiger_Type wurde die Vertragsnummer optional.
3.  ZusatzproduktKfz_Type bekommt eine optionale FahrzeugRefLfdNr vom Typ xsd:unsignedShort.
4.  Optionaler Bezug auf ein versichertes Interesse (oder mehrere versicherte Interessen) als VersInteresseRefLfnr
    in VorversicherungenDetail_Type eingefügt.
5.  HaftpflichtKfz_Type die Versicherungssumme wurde optional, damit sie im Request nicht
    zwangsläufig mit übermittelt werden muss.
6.  ZusaetzlicheKfzDaten als deprecated markiert, der Fahrzeug_Type wurde um diese Felder ergänzt.
7.  Die FahrzeugRefLfdNr wurde von xsd:string auf xsd:unsignedShort geändert und passt damit vom Typ zur
    Lfnr in VersichertesInteresse_Type, von welchem auch Fahrzeug_Type abgeleitet ist. Das ist streng genommen
    nicht abwärtskompatibel. Da aber im Fahrzeug schon bisher die Lfnr vom Typ xsd:unsignedShort war,
    müssen die Werte auch schon bisher eine Zahl gewesen sein und daher auch bisherige XMLs valide sein.
    Bei der Implementierung kommt es allerdings zu änderungen, da die Referenz in den Bausteinen jetzt z.B. in
    Java mit "int" und nicht mehr mit "String" abgebildet wird.
8.  Korrektur der Geschäftsfallnummer in GeschaeftsfallEreignis_Type von xsd:string auf cst:ObjektId_Type.
9.  Die verwendete OMDS 2 Version 2.16 schreibt jetzt ein Pattern im Format omds:Datum-Zeit vor.
    Dieses Pattern ist neu und sieht 1-3 Millisekunden vor.
    Betroffen sind:
      * Service zur OMDS 2 Abholung (OMDSPackageInfoType)
      * Schaden: Ereigniszeitpunkt, Meldedatum
      * Geschäftsfall-Log / GetStateChanges: Ereigniszeitpunkt
      * BOA-Services u. weitere: GueltigAb in ObjektId_Type.
        ObjektId wird an vielen Stellen in BOA verwendet, oftmals wahrscheinlich ohne GueltigAb.
      * Dokumententypen das Filedatum (DokumentData_Type, DokumentInfo_Type, DokumentenReferenz_Type)
10. Erweiterungen Metadaten Postservice.
11. DocumentType Wert 52 für BM Verzichtserklärung wurde gestrichen, da er doppelt definiert wurde.
12. Antragstrecke signalisiert Genehmigungsvorbehalt mit Fehlercode 40550.
13. RequestUpselling in Calculate ist optional und wurde als deprecated markiert.
14. Umstellung der Generierung der Java-Klassen auf Java 17 (einige Klassenpfade ändern sich von
    "javax" auf "jakarta"), entfernen Javaklassen älterer Versionen, da aufgrund der unveränderten
    Namespaces unterschiedliche Versionen derzeit ohnehin nicht in einer Applikation betrieben werden können.
15. ZustimmungZurVerwendungDerDatenZuWerbezwecken ist jetzt optional. Dies ermöglicht die Frage unbeantwortet zu lassen.
16. RequestUpselling ist künftig optional und deprecated
17. Neue Möglichkeit optionale Dokumente im Request von CreateOffer und CreateApplication anzufordern mittels
    cst:ProzessDokRequest_Type. Dokumente werden dann im Response zurückgegeben. TODO Doku dazu
18. Personen in Calculate als Deprecated markiert, dort sollten keine Personendaten mehr mitgegeben werden.
19. SubmitApplicationStatus_Type neuer Zustand 15: Antrag ausgesteuert und wartet auf Rückmeldung vom Vermittler.


==============
Version 1.10.0
==============

Was ist neu oder anders in Version 1.10.0 im Vergleich zur Version 1.9.0?
====================================================?====================

1.  Post-Service, bestehend aus den Services GetDocumentsOfPeriod, AcknowledgeDocuments und GetArcImage.
2.  Geschäftsfall-Log: Service GetStateChanges
3.  Elektronische Versicherungsbestätigung: Service CreateVB
4.  Referenz auf versicherte Interessen (Risikoobjekte) kann in Produkten Sach-Privat entfallen,
    z.B. wenn es nur ein Risikoobjekt gibt oder wenn die Deckung keinen Bezug auf ein spezifisches Risikobjekt benötigt.
2.  Ergänzt fehlenden Typ für Jahrespraemien in Praemie_Type als omds:decimal
3.  Dokumentation Statusübergänge am Antrag korrigiert, Dokumentation BOA-Prozess verbessert.
4.  RisikoGebaeude_Type PreisProQm kein Pflichtfeld mehr.
5.  Ersatzpolizze_Type nicht mehr abstrakt, kann direkt verwendet werden
6.  Dokumentation Konvertierungshilfe verschoben in eigenes Verzeichnis ON_2.03 "BOA Vorbereitung"
7   Dokumentenreferenz_Type ist final
8   Im Response von GetArcImage (Typ ArcContent) dürfen die Metadaten des Dokuments (ArcImageInfo) entfallen.

=============
Version 1.9.0
=============

Was ist neu oder anders in Version 1.9.0 im Vergleich zur Version 1.8.0?
========================================================================
1.  Version 1.9.0 ist abwärtskompatibel zur Version 1.8.0 in dem Sinne, dass ein XML welches unter 1.8.0 valide ist
    auch unter 1.9.0 valide ist. Generierte Objekte können abweichen und neue Elemente können im XML enthalten sein.
2.  Neue OMDS Version 2.15-00
3.  Neues Feature: Konvertierungshilfe, für Konvertierungsvorschläge der VU zu einem bestehenden Vertrag
    (conversionScope, conversionProposal)
4.  Dokumententypen erweitert: Rahmenvereinbarung, Infoblatt Berater
5.  Nicht verwendeter CommonSearchRequest_Type und CommonSearchResponse_Type entfernt.
6.  Fehler in Autorisierung_Type korrigiert AutorisierungsId hatte keinen Typ, Rolle nur optional
7.  ProzessDokumentBasis_Type Beschreibung Dokument nur optional
8.  JahrespraemieNto als deprecated markiert, dafür neue Felder JahrespraemieNto und JahrespraemieBto bei Prämie
9.  Ein weiterer Zustand Antrag: "Antrag ausgesteuert" für Anträge die in manuelle Bearbeitung gehen
10. ArtAusfolgung ergänzt um "Dokument ist nur für den Vermittler bestimmt"
11. Messverfahren CO2-Ausstoß im Kfz-Objekt ergänzt
12. Aufnahme von Polizzennr und VertragsId im Antragsobjekt, um diese bei sofortiger Polizzierung retournieren zu können.
13. Fondsdaten um eine optionale Bezeichnung ergänzt.
14. Angaben zur Verteilung in Kontierungen optional, Mengenbeschränkung auf max. 3 Kontierungen entfernt.


=============
Version 1.8.0
=============

Was ist neu oder anders in Version 1.8.0 (Hotfix 1) im Vergleich zur Version 1.7.0?
===================================================================================
1.  Version 1.8.0 ist abwärtskompatibel zur Version 1.7.0 in dem Sinne, dass ein XML welches unter 1.7.0 valide ist
    auch unter 1.8.0 valide ist. Generierte Objekte können abweichen und neue Elemente können im XML enthalten sein.
2.  Neue Services Deckungsauskunft und Belegeinreichung aufgenommen:
    Neue Methoden sind CheckCoverage und SubmitReceipt.
3.  Haftpflicht in Kfz wird optionaler Vertragsbestandteil (bisher verpflichtend)
4.  SpartendetailSchaden_Type bekommt folgende neue Attribute Koerperhaelfte, Koerperteil und BehoerdlicheAufnahme
5.  Neu aufgenommen Legitimation_Type
6.  Mit dem Wert "parameters" gab es ein Problem bei der Generierung von Clients unter Visual Studio. Daher wurde
    <message name="..."><part name="parameters" element="ost:.."/></message>
    geändert auf
    <message name="..."><part name="param" element="ost:.."/></message>
7.  Anpassung des Veröffentlichungsprocederes im Dokument ON_1.01.3 Versionierung: Der Steuerkreis
    entscheidet über die Veröffentlichung unabhängig vom formalen Status der jeweiligen Norm.
8.  Kfz-Kasko: Merkmal Neuwertklausel ja/nein auch bei Teilkasko verfügbar.
9.  Selbstbehalt in Sach-Privat auf Produktebene verfügbar. Selbstbehalte bekommen eine Art
    und können künftig von VU nochmals abgeleitet werden.
10. Kfz-Risikoobjekt erhält ein optionales Element "Historisch" vom Typ boolean.
11. Möglichkeit den Dateinamen beim Upload / Download von Binaries mit anzugeben.
12. Neues Feld TarifId um einen Antrags- bzw. Vertragsbaustein auf eine konkrete Produktkomponente beziehen zu können.
    Der Typ von TarifId ist abstrakt und muss von der VU überschrieben werden, z.B. mit einem Enum der zulässigen
    Werte.
13. Hauptfälligkeit, Zahlrhythmus und Zahlweg stehen jetzt optional auch auf Verkaufsproduktebene zur Verfügung.
    Auf der Produktebene ist die Hauptfälligkeit jetzt optional und Hauptfälligkeit, Zahlrhythmus und Zahlweg sind im
    Kommentar als deprecated markiert.
14. Neues Element SpezBOASchritt_Type: Die Schritte in BOA erben von diesem gemeinsamen Element und
    bauen aufeinander auf.
15. Baujahr Gebäude ist optional
16. Neuer DocumentType 42 = Deckungsvergleich


=============
Version 1.7.0
=============

Was ist neu oder anders in Version 1.7.0 im Vergleich zur Version 1.6.0?
=====================================================================================
1.  Version 1.7.0 ist abwärtskompatibel zur Version 1.6.0 in dem Sinne, dass ein XML welches unter 1.6.0 valide ist
    auch unter 1.7.0 valide ist. Generierte Objekte können abweichen und neue Elemente können im XML enthalten sein.
2.  Berechnung-Offert-Antrag wurde für die Sparten Unfall und Leben hinzugefügt.
    Für Leben wurde die Möglichkeit geschaffen bis zu 4 Produktebenen abzubilden (bisher max. 3 Ebenen).
3.  Struktur für Konvertierung in BOA wurde nochmals überarbeitet und verwendet jetzt
    Vererbung, damit sie künftig erweitert werden kann. Dazu wurde eine neue Zwischenschicht
    in der Vererbungshierarchie von B/O/A eingefügt.
4.  Dokumentenhandling in BOA wurde verbessert.
5.  Neue OMDS Version 2.14-00
    - Es sind neue Personenmerkmale hinzugekommen, einige Merkmale sind nicht mehr verpflichtend
    - Neue Änderungsarten: Fahrzeugwechsel, Wechselkennzeichen Aus- und Einschluss.
    - Neue Legitimierungsart: Firmenbuchauszug
6.  Aufnahme Vermittlername in Kontierung_Type
7.  Alle BOA-Sparten: Sicherstellungen und Einwilligungen sind im Antragsobjekt neu hinzu gekommen.
8.  Erweiterungsmöglichkeit für die Risikomerkmale von versicherten Interessen.


=============
Version 1.6.0
=============

Was ist neu oder anders in Version 1.6.0 im Vergleich zur Version 1.5.0?
=====================================================================================
1.  Version 1.6.0 ist abwärtskompatibel zur Version 1.5.0 in dem Sinne, dass ein XML welches unter 1.5.0 valide ist
    auch unter 1.6.0 valide ist. Generierte Objekte können abweichen und neue Elemente können im XML enthalten sein.
2.  Aufnahme der Aufbauart in Kfz
3.  Aufnahme Felder für Konvertierung in Berechnung-Offert-Antrag
4.  Erweiterung der Liste der Dokumentenarten
5.  Aufnahmen eines Änderungsgrundes in BOA
6.  Verwendet OMDS 2.14-00 (SNAPSHOT) statt 2.11-00:
    - neue Vertragsrollen: AZ - Abweichender Zulassungsbesitzer, FI - Firmeninhaber, ZB - Zustellbevollmächtigter
    - neues Feld GesFormCdType in Sonst-Person
    - TIN für "Titel nachgestellt"
    - neuer LegArtCd_Type: FA - Firmenbuchauszug
    - Geburtsland an der Person
7.  Entfernen der Restriktion auf genau 4 Zeichen im Status
8.  Verwendung technische Objekte in CommonRequest_Type


=============
Version 1.5.0
=============

Was ist neu oder anders in Version 1.5.0 im Vergleich zur Version 1.4.0?
=====================================================================================
1.  Version 1.5.0 ist abwärtskompatibel zur Version 1.4.0 in dem Sinne, dass ein XML welches unter 1.4.0 valide ist
    auch unter 1.5.0 valide ist. Generierte Objekte können abweichen und neue Elemente können im XML enthalten sein.
2.  Berechnung-Offert-Antrag wurde für die Sparte Sach-Privat hinzugefügt.
3.  Es wurden Methoden definiert, um Schäden zu melden und Schadenmeldungen zu suchen und aufzurufen.
4.  Vinkulierung ist nicht mehr im allgemeinen Antrags-Spezifikation zu finden, sondern
    nur bei KFZ. Hintergrund ist, dass für Sicherstellungen (inkl. Vinkulierung) mit 1.7.0 eine
    allgemeinere Lösung kommt.
5.  In der Berechnung können optional schon Daten zu Personen angegeben werden. Dies erfolgt im
    Vorgriff auf Personenversicherungen, wo dies benötigt wird. In der Praxis hat sich überdies
    erwiesen, dass auch bei Sachversicherungen bei manchen Versicherern bereits in der Berechnung
    Personendaten erforderlich sind.
6.  Es wurden neue "generische" Produktbausteine geschaffen, welche optional Produkt-Metadaten
    für den Client zu den Produktbausteinen enthalten können.
7.  Es wurden neue Ableitungen für das Versicherte Interesse geschaffen.
8.  Für die Übermittlung von Metadaten zu Attributen wurden spezielle Typen angelegt
9.  Das allgemeine Response-Objekt kann "Referenzen" aufnehmen. Dies sind Deep-Links oder andere Querverweise.


=============
Version 1.4.0
=============

Was ist neu oder anders in Version 1.4.0 im Vergleich zur Version 1.3.0?
=====================================================================================
1.  Version 1.4.0 ist abwärtskompatibel zur Version 1.3.0 in dem Sinne, dass ein XML welches unter 1.3.0 valide ist
    auch unter 1.4.0 valide ist. Generierte Objekte können abweichen und neue Elemente können im XML enthalten sein.
2.  Folgend dem Beschluss des Steuerkreises zur Versionierung vom 28.5.2019 sind alle
    Definitionen, die noch nicht durch zwei Implementierungen validiert wurden,
    aus dem offiziellen Release entfernt worden.
    Dies betrifft insb. Schaden, Sparten außer Kfz, maklerseitige Services, Services
    zu Partner und Vertragsänderungen.
    Diese Teile des Standards sind im Branch "Develop" des Git-Repository aber weiterhin verfügbar:
    https://bitbucket.org/omds/omdsservicedefinitions/src/develop/
3.  Antragsdaten Kfz können auch direkt in SubmitApplicationKfz übergeben werden, ein
    vorheriger Aufruf von CreateApplicateionKfz ist optional.
4.  Für BerechnungsdatenKfz, OffertdatenKfz und AntragsdatenKfz sind eigene Elemente
    definiert.
5.  Die Zulassungsdaten sind in die omds3CommonServiceTypes gewandert und nun
    Teil des Typs "Fahrzeug_Type". In 1.3 waren sie Teil von BOA-Kfz.
6.  Vertragsbeginn ist in Ebene 2 und 3 optional. Vertragsbeginn und Vertragsende sind
    nur noch vom Typ omds:Datum und nicht omds_Datum-Zeit.
7.  Das Element Vorversicherungen wurde verallgemeinert: Es steht jetzt ein abstakter Typ
    zur Beschreibung der Vorversicherungen zur Verfügung und eine Implementierung für Kfz.
    Bei der Implementierung für Kfz wurde zusätzlich die Möglichkeit der Angabe einer Sparte (VtgSparteCd)
    aufgenommen. Die Vorversicherungen haben das neue optionale Merkmal "Ausländische Vorversicherung".
    ZusaetzlicheVorversicherungsdaten_Type wurde als zusätzliche VU-spezifische Erweiterungsmöglichkeit eingeführt.
8.  Es wurde ein neues abstraktes Element "AbgelehnteRisiken" bei den Antragsdaten eingeführt.
9.  Es wurden einige optionale Tarifmerkmale im Kfz-Verkaufsprodukt aufgenommen: Geburtsdatum, Postleitzahl,
    Vermittlernummer und Nat. Person / Sonstige Person.
10. Für den Fahrzeugzustand wurde ein zusätzliches Element vom Typ xsd:string eingeführt. Das bisherige
    Element "Fahrzeugzustand" wurde zu FzZustandBesichtigung. Der Zustandsbeschreibung_Type wurde zu
    ArtBesichtigung_Type und die Schlüsselwerte wurden etwas allgemeiner formuliert.
11. Drei Elemente die bislang in den Antragsdaten Kfz enthalten waren, sind in die allgemeinen Antragsdaten
    aufgenommen worden: Ersatzpolizzennummer, ZusendungWeitereDokumente, Vorversicherungen
12. Der abstrakte Produktbaustein_Type wurde zu BasisProduktbaustein_Type; Die Unterscheidung in
    ProduktbausteinAntragsprozess_Type und Produktbaustein_Auskunft_Type wurde aufgegeben.
    Der ProduktbausteinAntragsprozess_Type heisst jetzt einfach Produktbaustein_Type. Da alle involvierten Typen
    abstrakte Typen sind, hat diese Vereinfachung keine Auswirkung auf die tatsächlichen Produktbausteine.
13. Das optionale Element VvdVertrag in ZusaetzlicheKfzdaten_Type wurde umbenannt zu, da es für
    den Standard zu spezifisch ist. Es wurde das Element ZusaetzlicheAntragsdatenKfz als zusätzliche VU-spezifische
    Erweiterungsmöglichkeit eingeführt, um dieses Element aufnehmen zu können.
14. MTOM Unterstützung wurde aufgenommen für:
    - Download OMDSDatensätze getOMDSPackage
    - Download Dokumente getArcImage
    - Upload und Download Dokumenente bei BOA createApplication, submitApplication


=============
Version 1.3.0
=============

Was ist neu oder anders in Version 1.3.0 im Vergleich zur Version 1.2.0?
=====================================================================================

1.  Version 1.3.0 ist abwärtskompatibel zur Version 1.2.0 in dem Sinne, dass ein XML welches unter 1.2.0 valide ist
    auch unter 1.3.0 valide ist. Generierte Objekte können abweichen und neue Elemente können im XML enthalten sein.
2.  Neu enthalten ist: KFZ Berechnung - Offert - Antrag
3.  Schaden hat bislang Status "Empfehlung" und wurde bisher noch in keiner Task-force erprobt.
    Schaden wurde nicht-abwärtskompatibel verändert:
    * VUNr in Requests ist jetzt obligatorisch
    * Bereinigung von Inner-Classes
    * SchadenStatus_Type erbt jetzt von AbstraktesEreignisStatusAenderung_Type und kann damit auch in
      Service "GetStatusChanges" verwendet werden.
    * InformationenPerson_Type - Fehlerkorrektur: Personennr war nicht als optional vermerkt
    * GetNumberOfDocumentsRequest_Type und GetDocumentInfosRequest_Type:
       a) Objektspezifikation, also der Bezug auf ein Geschäftsobjekt, ist optional
       b) Zeitraumangabe: Änderung von omds:EL-Zeitraum_Type auf neues Element Zeitraum_Type,
          welches keine Art des Zeitraums enthält
    * GetDocumentInfosRequest_Type: Vorgaben zur Offset und MaxResults
    * GetDocumentInfosResponse_Type: verwendet statt Typ ArcImageInfo den neuen Typ DokumentenReferenz_Type,
      welcher eine Referenz auf ein Geschäftsobjekt enthalten kann
    * SearchClaimRequest_Type, ChangedClaimsListRequest_Type und LossEventListRequest_Type verwenden statt
      omds:EL-Zeitraum_Type den neuen Typ Zeitraum_Type

4.  ServiceFault kann einen zusätzlichen Rückgabewert enthalten: Den String "elementReference".
5.  Fehlerkorrektur in ElementIdType: Das Attribut idValidUntil darf null sein.


=============
Version 1.2.0
=============

Was ist neu oder anders in Version 1.2.0 im Vergleich zur Version 1.1.1?
=====================================================================================

1.  Version 1.2.0 ist abwärtskompatibel zur Version 1.1.1, WSDLs und XSDs sind unverändert.

2.  Dokument "ON_1.02.1_AuthentifizierungAllgemein 1.2.0.docx" wurde um OAuth ergänzt und
    es wurde ein erläuterndes Dokument zu OAuth hinzugefügt: "Erläuterungen_OAuth_zu_ON_1.02.1.docx".
    Die Einführung von OAuth hat aber keine Auswirkungen auf die SOAP-Definitions-Files.

=============
Version 1.1.1
=============

Was ist neu oder anders in Version 1.1.1 im Vergleich zur Version 1.1.0?
========================================================================

1.  Es werden einige Fehler korrigiert, es gibt keine grundsaetzlichen Aenderungen.
    Daher bleiben die Namespaces unveraendert gegenueber Version 1.1.0.

2.  Die Filenames der WSDL und XSD-Files haben die Versionsnummer nicht mehr angehängt,
    da dies als unhandlich in der Generierung beeinsprucht wurde. Die Versionsnummer 1.1.1 ist
    aber als Attribut im Kopf der XML-Dateien enthalten.

3.  Das Element serviceFault im File omds3CommonServiceTypes.xsd hatte keinen Type zugewiesen.
    Dies wurde korrigiert auf Type ServiceFault.
 
4.  Das Element GeschInteresseLfnr hatte keinen Typ und hat jetzt den Typ xsd:unsignedIint bekommen.

5.  In GetDocumentInfosResponse_Type hatten ActualOffset, ActualMaxResults und TotalResults keinen Typ.
    Der Typ wurde für die drei Elemente auf xsd:unsignedInt festgelegt.

6.  In MeldungsZusammenfassung_Type hatte das Element LfdNr keinen Typ. Der Typ wurde festgelegt mit xsd:unsignedInt.

7.  In Schadenereignis_Type hatten die Elemente vormaligeIdGeschaeftsfall und nachfolgendeIdGeschäftsfall keinen Typ.
    Der Typ wurde festgelegt mit 'xsd:string'. Der Umlaut im Element 'nachfolgendeIdGeschäftsfall' wurde aufgelöst
    zu 'nachfolgendeIdGeschaeftsfall'.

8.  In SchadenLight_Type hatte das Element 'bearbStandCd' keinen Typ. Dieser wurde festgelegt mit 'BearbStandCd_Type'.
    Ferner hatten die Elemente 'vormaligeSchadennr' und 'nachfolgendeSchadennr' keinen Typ. Dieser wurde festgelegt mit 'xsd:string'.

9.  In SchadenType hatte das Element 'bearbStandCd' keinen Typ. Dieser wurde festgelegt mit 'BearbStandCd_Type'.
    Ferner hatten die Elemente 'vormaligeSchadennr' und 'nachfolgendeSchadennr' keinen Typ. Dieser wurde festgelegt mit 'xsd:string'.

10. In Meldungszusammenfassung_Type im ErgebnisSchaeden das Element 'LfdNr' war kein Typ festelegt. Dieser wurde auf
    'xsd:unsignedInt' festgelegt.

11. In omds3Services.wsdl und in omds3ServicesBroker.wsdl waren die Faultelemente bei einigen Services für wsdl und soap 
    unterschiedlich benannt. Die Benennung wurde angegelichen.


=============
Version 1.1.0
=============

Was ist neu oder anders in Version 1.1.0 im Vergleich zur Version 1.0.0?
========================================================================

1.  Filenames wurden mit Versionsnummer '1-1-0' ergaenzt, Namespaces haben ebenfalls die Versionsnummer erhalten.

2.  Die Services für Schadenmeldung und Schadenstatus wurden ergänzt.

3.  Neues WSDL für Services auf der Maklerseite: 'omds3ServicesBroker-1-1-0.wsdl'.
	 
4.  Ein neues XSD fuer Typen, die allen OMDS 3 Services gemeinsam sind: 'omds3CommonServiceTypes-1-1-0.xsd'.
    Übergeordnete Datentypen und Elemente sind in diese Datei verschoben worden.

5.  Im Element UserDataResponse ist das Unterlement 'address' enfallen, da die Adressdaten auch im Unterelement 'person' übermittelt werden können.

6.  Die Message 'serviceFault' im WSDL ist umbenannt worden nach 'ServiceFaultMsg', um Verwechslung mit dem Typ 'ServiceFault' im XSD zu vermeiden.
    Die Referenzen auf das Element 'serviceFault' in den XSD-Files sind ersetzt worden durch lokale Elemente 'ServiceFault' mit dem Typ 'ServiceFault'

7.  Im Typ 'HttpActionLinkType' fehlten die verschiedenen in der Dokumentation vorgesehenen Types im Enum. Diese wurden ergänzt.

8.  Im Typ 'ElementIdType' fehlte der Typ des Elements 'idIsSingleUse'. Dieser wurde als boolean deklariert.

9.  Exemplarische Demonstration einer Versicherungs-spezifischen Erweiterung des Services wurde aufgenommen als 'omds3exampleVuServiceTypes-1-1-0.xsd'.

10. Die Deklaration einer UsernameToken-Policy wurde ins WSDL 'omds3Services-1-1-0.wsdl' aufgenommen. 

11. Typ PolicyDocumentType zu DocumentType umbenannt, da die Dokumententypen nicht nur auf Polizzen bezogen sind.
    Die Dokumententypen für Schaden wurden in 'DocumentType' als Enum ergänzt.

13. Ein Schreibfehler wurde in MaklerID_Type korrigiert: 'MaklertID_Type' wurde zu 'MaklerID_Type'.

14. AgentFilter umbenannt zu AgentFilter_Type
    Subelemente in AgentFilter_Type wurden umbenannt: 'agentID' zu 'MaklerID' und 'agentNumber' zu 'Vermnr'.

15. In 'ArcImageInfosRequest' und in 'PolizzenObjektSpezifikation_Type' wurde das Element 'policyPartyRole' umbenannt zu 'policyPartnerRole', 
    da der Typ 'PolicyPartnerRole' heißt.

16. Für das Feld 'errorType' im Typ 'ServiceFault' wurde ein Enum für 1 = Fehler, 2 = Warnung und 3 = Hinweis hinterlegt.



