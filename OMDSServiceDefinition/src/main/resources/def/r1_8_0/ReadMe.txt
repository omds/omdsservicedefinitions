Releases
================================
OMDS 3 Version 1.0.0 - Okt. 2017 - erste Veröffentlichung der 'Quick-Wins'
OMDS 3 Version 1.1.0 - Okt. 2017 - Veröffentlichung Schaden, Services die von Maklern betrieben werden, Bugfixes zu 1.0.0
OMDS 3 Version 1.1.1 - Dez. 2017 - Bugfixes zu 1.1.0: Bei einigen Elementen war der z.B. Typ nicht definiert
OMDS 3 Version 1.2.0 - Mai  2018 - Autorisierung neu beschrieben, keine Änderungen in den Services selbst
OMDS 3 Version 1.3.0 - Jun. 2019 - Berechnung-Offert-Antrag Kfz neu
OMDS 3 Version 1.4.0 - Okt. 2019 - Berechnung-Offert-Antrag Kfz überarbeitet, Verwendung OMDS 2.11
OMDS 3 Version 1.5.0 - Jul. 2020 - Berechnung-Offert-Antrag Sach-privat, Schaden überarbeitet, Verwendung OMDS 2.11
OMDS 3 Version 1.6.0 - Jul. 2021 - Service-Release: Verwendung OMDS 2.14 (SNAPSHOT)
OMDS 3 Version 1.7.0 - Jän. 2022 - Neue Sparten: Berechnung-Offert-Antrag Unfall, Leben, Verwendung OMDS 2.14.0,
                                   Handling von Dokumenten im Prozess, Konvertierung-Fahrzeugwechsel-Wechselkennzeichen.
OMDS 3 Version 1.8.0 - Aug. 2022 - Service-Release: Deckungsprüfung, Belegeinreichung, Veröffentlichungsprozess,
                                   Legitimation, Haftpflicht optional in Kfz
