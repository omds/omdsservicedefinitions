Releases
================================
OMDS 3 Version 1.0.0 - erste Veroeffentlichung der 'Quick-Wins'
OMDS 3 Version 1.1.0 - Veroeffentlichung Schaden, Services die von Maklern betrieben werden, Bugfixes zu 1.0.0
OMDS 3 Version 1.1.1 - Bugfixes zu 1.1.0: Bei einigen Elementen war der z.B. Typ nicht definiert
OMDS 3 Version 1.2.0 - Autorisierung neu Beschrieben, keine Änderungen in den Services selbst
OMDS 3 Version 1.3.0 - Berechnung-Offert-Antrag Kfz neu
OMDS 3 Version 1.4.0 - Berechnung-Offert-Antrag Kfz überarbeitet, Verwendung OMDS 2.11
OMDS 3 Version 1.5.0 - Berechnung-Offert-Antrag Sach-privat, Schaden überarbeitet, Verwendung OMDS 2.11