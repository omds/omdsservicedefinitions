﻿=============
Version 1.2.0
=============

Was ist neu oder anders in Version 1.2.0 im Vergleich zur Version 1.1.1?
=====================================================================================

1. WSDLs und XSDs sind unverändert

2. Dokument "ON_1.02.1_AuthentifizierungAllgemein 1.2.0.docx" wurde um OAuth ergänzt und
es wurde ein erläuterndes Dokument zu OAuth hinzugefügt: "Erläuterungen_OAuth_zu_ON_1.02.1.docx".
Die Einführung von OAuth hat aber keine Auswirkungen auf die SOAP-Definitions-Files.

=============
Version 1.1.1
=============

Was ist neu oder anders in Version 1.1.1 im Vergleich zur Version 1.1.0?
========================================================================

1. Es werden einige Fehler korrigiert, es gibt keine grundsaetzlichen Aenderungen. Daher 
ist bleiben die Namespaces unveraendert gegenueber Version 1.1.0.

2. - Die Filenames der WSDL und XSD-Files haben die Versionsnummer nicht mehr angehängt,
da dies als unhandlich in der Generierung beeinsprucht wurde. Die Versionsnummer 1.1.1 ist 
aber als Attribut im Kopf der XML-Dateien enthalten.

3. Das Element serviceFault im File omds3CommonServiceTypes.xsd hatte keinen Type zugewiesen. 
Dies wurde korrigiert auf Type ServiceFault.
 
4. Das Element GeschInteresseLfnr hatte keinen Typ und hat jetzt den Typ xsd:unsignedIint bekommen.

5. In GetDocumentInfosResponse_Type hatten ActualOffset, ActualMaxResults und TotalResults keinen Typ.
Der Typ wurde für die drei Elemente auf xsd:unsignedInt festgelegt.

6. In MeldungsZusammenfassung_Type hatte das Element LfdNr keinen Typ. Der Typ wurde festgelegt mit xsd:unsignedInt.

7. In Schadenereignis_Type hatten die Elemente vormaligeIdGeschaeftsfall und nachfolgendeIdGeschäftsfall keinen Typ.
Der Typ wurde festgelegt mit 'xsd:string'. Der Umlaut im Element 'nachfolgendeIdGeschäftsfall' wurde aufgelöst 
zu 'nachfolgendeIdGeschaeftsfall'.

8. In SchadenLight_Type hatte das Element 'bearbStandCd' keinen Typ. Dieser wurde festgelegt mit 'BearbStandCd_Type'.
Ferner hatten die Elemente 'vormaligeSchadennr' und 'nachfolgendeSchadennr' keinen Typ. Dieser wurde festgelegt mit 'xsd:string'.

9. In SchadenType hatte das Element 'bearbStandCd' keinen Typ. Dieser wurde festgelegt mit 'BearbStandCd_Type'.
Ferner hatten die Elemente 'vormaligeSchadennr' und 'nachfolgendeSchadennr' keinen Typ. Dieser wurde festgelegt mit 'xsd:string'.

10. In Meldungszusammenfassung_Type im ErgebnisSchaeden das Element 'LfdNr' war kein Typ festelegt. Dieser wurde auf
'xsd:unsignedInt' festgelegt.

11. In omds3Services.wsdl und in omds3ServicesBroker.wsdl waren die Faultelemente bei einigen Services für wsdl und soap 
unterschiedlich benannt. Die Benennung wurde angegelichen.


=============
Version 1.1.0
=============

Was ist neu oder anders in Version 1.1.0 im Vergleich zur Version 1.0.0?
========================================================================

1. Filenames wurden mit Versionsnummer '1-1-0' ergaenzt, Namespaces haben ebenfalls die Versionsnummer erhalten.

2. Die Services für Schadenmeldung und Schadenstatus wurden ergänzt.

3. Neues WSDL für Services auf der Maklerseite: 'omds3ServicesBroker-1-1-0.wsdl'.
	 
4. Ein neues XSD fuer Typen, die allen OMDS 3 Services gemeinsam sind: 'omds3CommonServiceTypes-1-1-0.xsd'.
   Übergeordnete Datentypen und Elemente sind in diese Datei verschoben worden.

5. Im Element UserDataResponse ist das Unterlement 'address' enfallen, da die Adressdaten auch im Unterelement 'person' übermittelt werden können.

6. Die Message 'serviceFault' im WSDL ist umbenannt worden nach 'ServiceFaultMsg', um Verwechslung mit dem Typ 'ServiceFault' im XSD zu vermeiden.
   Die Referenzen auf das Element 'serviceFault' in den XSD-Files sind ersetzt worden durch lokale Elemente 'ServiceFault' mit dem Typ 'ServiceFault' 

7. Im Typ 'HttpActionLinkType' fehlten die verschiedenen in der Dokumentation vorgesehenen Types im Enum. Diese wurden ergänzt.

8. Im Typ 'ElementIdType' fehlte der Typ des Elements 'idIsSingleUse'. Dieser wurde als boolean deklariert.

9. Exemplarische Demonstration einer Versicherungs-spezifischen Erweiterung des Services wurde aufgenommen als 'omds3exampleVuServiceTypes-1-1-0.xsd'.

10. Die Deklaration einer UsernameToken-Policy wurde ins WSDL 'omds3Services-1-1-0.wsdl' aufgenommen. 

11. Typ PolicyDocumentType zu DocumentType umbenannt, da die Dokumententypen nicht nur auf Polizzen bezogen sind.
    Die Dokumententypen für Schaden wurden in 'DocumentType' als Enum ergänzt.

13. Ein Schreibfehler wurde in MaklerID_Type korrigiert: 'MaklertID_Type' wurde zu 'MaklerID_Type'.

14. AgentFilter umbenannt zu AgentFilter_Type
    Subelemente in AgentFilter_Type wurden umbenannt: 'agentID' zu 'MaklerID' und 'agentNumber' zu 'Vermnr'.

15. In 'ArcImageInfosRequest' und in 'PolizzenObjektSpezifikation_Type' wurde das Element 'policyPartyRole' umbenannt zu 'policyPartnerRole', 
    da der Typ 'PolicyPartnerRole' heißt.

16. Für das Feld 'errorType' im Typ 'ServiceFault' wurde ein Enum für 1 = Fehler, 2 = Warnung und 3 = Hinweis hinterlegt.



