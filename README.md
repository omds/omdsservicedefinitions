# README #

Dieses Projekt enthält die Servicedefinitionen von OMDS 3 als WSDLs und XSDs.
Ferner sind die Dokumentationen der Services als Word-Dateien enthalten.

Git-Workflow
------------
Das Projekt folgt dem GitFlow-Workflow, welcher [hier](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) beschrieben ist.

Es gibt folgende Haupt-Branches:

* **master** - der Master repräsentiert seit dem 1.9.2019 die offizielle Hauptentwicklungslinie und enthält die Releases als Tags
* **develop** - der Branch für die laufende Entwicklung, der Development-Branch kann der Veröffentlichung um mehrere Versionen vorauseilen.
Es können auch Services oder Objekte wieder verworfen werden.

* **release/...** - der aktuelle Release-Candidate für die kommende Version. Dieser enthält nur solche Services und Objekte, die mit hoher Wahrscheinlichkeit im neuen Release enthalten sind.
* **feature/...** - Feature-Branches, in denen eigene Features modelliert werden, die noch so früh in der Diskussion sind, dass sie nicht einmal in den Development-Branch aufgenommen wurden.

Daneben gibt es Feature und Release-Branches.

Projektaufbau
-------------
Die Inhalte des Projekts sind in den folgenden Verzeichnissen organisiert:

* doc - Die Dokumentation der Services als Word Files
* docEntw - Die Dokumentation von Services, die noch nicht für den Release vorgesehen sind.
* docGen - Die mittels XMLSPY generierte Dokumentation (nur in den Veröffentlichungen enthalten)
* src/main/java - beispielhaft mit CXF generierte Java-Klassen
* src/main/resources/bind - Binding Definitionen für Apache CXF Framework
* src/main/resources/def - XSD und WSDL-Files

Im Verzeichnis *src/main/resources/def* sind in Unterverzeichnisssen auch ältere Versionen
von OMDS 3 Definitionen enthahlten, damit ein einfacher Vergleich zwischen Versionen möglich ist. 

In der jeweiligen Version finden sich folgende Files:

* omds3Services.wsdl - ist das WSDL mit den OMDS 3 Webservices
* omds3_ON ... .xsd  - ist ein XSD mit OMDS 3 Datentypen. Es Referenziert die OMDS 2.x Datentypen
* omds...-00.xsd     - ist ein XSD mit OMDS 2.x Datentypen
